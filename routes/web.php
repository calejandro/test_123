<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO: Remove when over with debugging routes
use App\Company;
use App\User;
use App\Event;
use App\Minisite;

Auth::routes();

Route::get('/filemanager/dialog.php', function(){
  ob_start();
  require(public_path(). '/filemanager/dialog.php');
  return ob_get_clean();
});

// Locales
//Route::post('lang', 'HomeController@changeLocal')->name('locale');
Route::get('language/{lang}', function($lang){
  Session::put('locale', $lang);
  return back();
})->name('langroute');

// Minisite V2 URL
Route::get('/s/{slug}-{minisiteId}/{page_slug}-{page}/{hash}/{lang?}', 'PageController@redirectNewUrl')
  ->where('page_slug', '[a-z0-9-]+')
  ->where('slug', '[a-z0-9-]+')
  ->name('minisite_page_show');
Route::get('/s/{slug}-{minisiteId}/{hash}/{lang?}', 'MinisiteController@redirectNewUrl')
  ->where('slug', '[a-z0-9-]+')
  ->name('minisite_show');

// subdomain routing
Route::domain('{domain}.'.config('app.url'))->group(function(){
  Route::get('/{page_slug}-{page}/{hash?}/{lang?}', 'PageController@show')
    ->where('page_slug', '[a-z0-9-]+')
    ->where('slug', '[a-z0-9-]+')
    ->where('lang', 'en|fr|de')
    ->name('minisite_page_subdomain_show');

  Route::get('/{hash?}/{lang?}', 'MinisiteController@show')
    ->where('slug', '[a-z0-9-]+')
    ->where('lang', 'en|fr|de')
    ->name('minisite_subdomain_show');

});

Route::get('/', 'HomeController@index')->name('home'); // TODO: Dashboard

Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'auth.admin']], function () {

  Route::get('changelog', 'AdminDashboardController@changelog')->name('admin.dashboard.changelog');

  Route::get('companies/{id}/_lock', 'CompaniesController@lock')->name('companies._lock');
  Route::get('companies/{id}/_unlock', 'CompaniesController@unlock')->name('companies._unlock');
  Route::resource('companies', 'CompaniesController');
  Route::resource('categories', 'CategoriesController');
  Route::resource('admins', 'UserController', ['except' => ['show']]);

  Route::get('admins/{user_id}/edit_permissions', 'UserController@editPermissions')->name('admin_edit_permissions');
  Route::post('admins/{user_id}/edit_permissions', 'UserController@updatePermissions')->name('admin_update_permissions');
});

// Auth based on company in URL
Route::group(['middleware' => ['auth', 'auth.owner.company:redirect']], function () {

  // Users
  Route::resource('companies.users', 'CompanyUserController', ['only' => [
      'index',
      'create',
      'store'
  ]]);

  Route::group(['middleware' => ['auth.owner.user:redirect']], function () {
    Route::resource('companies.users', 'CompanyUserController', ['except' => [
      'show',
      'index',
      'create',
      'store'
    ]]);

    Route::get('companies/{company}/users/{user_id}/edit_permissions', 'CompanyUserController@editPermissions')->name('edit_permissions');
    Route::post('companies/{company}/users/{user_id}/edit_permissions', 'CompanyUserController@updatePermissions')->name('update_permissions');
  });

  // Categories
  Route::resource('companies.categories', 'CompanyCategoryController', ['only' => [
    'index',
    'create',
    'store'
  ]]);

  Route::resource('companies.categories', 'CompanyCategoryController', ['except' => [
    'show',
    'index',
    'create',
    'store'
  ]])->middleware('auth.owner.category:redirect');

  // Events
  Route::get('/companies/{company}/events/statistics', 'EventStatisticsController@index')->name('companies.events.statistics.index');
  Route::get('/companies/{company}/events/statistics/export', 'EventStatisticsController@export')->name('companies.events.statistics.export');
  Route::get('/companies/{company}/events/past', 'CompanyEventController@past')->name('past_events');

  Route::resource('companies.events', 'CompanyEventController')->only(['index', 'create', 'store']);
  Route::group(['middleware' => ['event.active', 'auth.owner.event:redirect']], function () {
    Route::get('/companies/{company}/events/{event}/duplicate', 'CompanyEventController@duplicate')->name('companies.events.duplicate');
    Route::resource('companies.events', 'CompanyEventController')->except(['index', 'create', 'store']);
  });

  // Company SMTP configuration
  Route::get('/companies/{company}/config/edit', 'CompanySmtpController@edit')->name('companies.config');
  Route::patch('/companies/{company}/config/edit', 'CompanySmtpController@update')->name('companies.config.update');
});


// Auth based on event in URL
Route::group(['middleware' => ['auth', 'auth.owner.event:redirect', 'event.active']], function () {
  
  Route::post(
    'events/{event}/update_sessions_controls',
    'CompanyEventController@updateSessionsControls'
  )->name('events.update_sessions_controls');

  Route::post(
    'events/{event}/update_mandatory_category_session_control',
    'CompanyEventController@updateMandatoryCategorySessionControl'
  )->name('events.update_mandatory_category_session_control');
  
  // Emails
  Route::get('events/{event}/emails/createInvit', 'EmailsController@createInvit')->name('events.emails.createInvit');
  Route::get('events/{event}/emails/createOther', 'EmailsController@createOther')->name('events.emails.createOther');

  Route::resource('events.emails', 'EmailsController', ['only' => ['index']]);

  Route::group(['middleware' => ['auth.owner.email:redirect']], function () {
    Route::post('events/{event}/emails/{email}/duplicate', 'EmailsController@duplicate')
      ->name('events.email.duplicate');
    Route::get('events/{event}/emails/{email}/render/{display?}', 'EmailsController@render')
      ->where('display', 'mobile|desktop')
      ->name('events.emails.render');
    Route::get('events/{event}/emails/{email}/{lang?}/render/{display?}', 'EmailsController@render')
      ->where('display', 'mobile|desktop')
      ->name('events.emails.render.lang');
    Route::get('events/{event}/emails/{email}/render_template', 'EmailsController@renderTemplate')
      ->name('events.emails.render_template');
    Route::get('events/{event}/emails/{email}/{lang?}/render_template', 'EmailsController@renderTemplate')
      ->name('events.emails.render_template.lang');
  
    Route::get('events/{event}/emails/{email}/send', 'EmailsController@send')
      ->name('events.emails.send');

    Route::resource('events.emails', 'EmailsController', ['except' => ['show', 'create', 'edit', 'index', 'store']]);

    Route::get('events/{event}/emails/{email}/edit', 'EmailsController@edit')
      ->name('events.emails.edit');
    Route::get('events/{event}/emails/{email}/{lang?}/edit', 'EmailsController@edit')
      ->name('events.emails.edit.lang');
  });

  // Guests
  Route::post('events/{event}/guests/import', 'GuestsController@import')->name('events.guests.import');
  Route::get('events/{event}/guests/export', 'GuestsController@export')->name('events.guests.export');
  Route::get('events/{event}/guests/duplicates', 'GuestsController@duplicates')->name('events.guests.duplicates');
  Route::post('events/{event}/guests/validate_emails', 'GuestsController@validateEmails')->name('events.guests.validate_emails');

  Route::resource('events.guests', 'GuestsController', ['only' => ['create', 'store', 'index']]);
  Route::resource('events.guests', 'GuestsController', ['except' => [
    'show',
    'create',
    'store',
    'index'
  ]])->middleware('auth.owner.guest:redirect');

  // Checkin confirm
  // Compatibility redirection routes, will be removed
  // Can be safely removed after 10.08.2019. Except for event id 466 (Sponsorize tests).
  Route::get('events/{event}/guests/checkin/{hash_control}', function ($event, $hash_control) {
    return redirect()->route('guests.checkin', ['hash' => $hash_control]);
  });

  Route::get('events/{event}/checkin', 'CheckinController@index')->name('events.checkin.index');
  
  // Companions
  Route::get('events/{event}/guests/companions/export', 'GuestsController@exportCompanions')->name('events.guests.companions.export');
  Route::resource('events.companions', 'CompanionController', ['only' => [
    'edit',
    'update'
  ]])->middleware('auth.owner.companion:redirect');

  // Minisite
  Route::resource('events.minisites', 'MinisiteController', ['only' => ['index', 'create', 'store']]);
  Route::resource('events.minisites', 'MinisiteController', ['except' => [
    'destroy',
    'index',
    'create',
    'store'
  ]])->middleware('auth.owner.minisite:redirect');

  // Pages
  Route::get('events/{event}/minisites/{minisite}/pages/{lang?}/create', 'PageController@create')->name('page_create');
  Route::resource('events.minisites.pages', 'PageController', [ 'only' => ['store'] ]);
  Route::group(['middleware' => ['auth.owner.page:redirect']], function () {
    Route::get('events/{event}/minisites/{minisite}/pages/{page}/{lang?}/edit/{getlang?}', 'PageController@edit')->name('page_edit');
    Route::patch('events/{event}/minisites/{minisite}/pages/{page}/{lang?}/update', 'PageController@update')->name('page_update');
    Route::resource('events.minisites.pages', 'PageController', [ 'except' => ['update', 'store'] ]);
  });

  // SubscribeForms
  Route::get('events/{event}/subscribeForm/edit', 'SubscribeFormController@edit')->name('events.subscribeForm.edit');

  // SurveyForms
  Route::get('events/{event}/surveyForm/results', 'SurveyFormController@results')->name('events.surveyForm.results');
  Route::get('events/{event}/surveyForm/edit', 'SurveyFormController@edit')->name('events.surveyForm.edit');

  // Sessions
  Route::resource('events.sessions', 'SessionController', [ 'only' => ['create', 'store', 'index'] ]);

  Route::resource('events.sessions', 'SessionController', ['except' => [
    'show',
    'create',
    'store',
    'index'
  ]])->middleware('auth.owner.session:redirect');

  // SessionGroups
  Route::resource('events.session_groups', 'SessionGroupController', ['only' => [ 'create', 'store' ]]);

  Route::resource('events.session_groups', 'SessionGroupController', ['only' => [
      'edit',
      'update',
      'destroy'
  ]])->middleware('auth.owner.session_group:redirect');

  // Tickets
  Route::get('events/{event}/ticket', 'TicketController@edit')->name('events.ticket.edit');
  Route::patch('events/{event}/ticket/{ticket}', 'TicketController@update')
    ->middleware('auth.owner.ticket:redirect')
    ->name('events.ticket.update');
  Route::get('events/{event}/ticket/{lang}/download/demo', 'TicketController@generateDemoTicket')
    ->name('events.ticket.download_demo');
  Route::get('events/{event}/ticket/download/guest/{guest}', 'TicketController@generateTicket')
    ->middleware('auth.owner.guest:forbidden')
    ->name('events.ticket.download');
  Route::get('events/{event}/ticket/download/companion/{companion}', 'TicketController@generateCompanionTicket')
    ->middleware('auth.owner.companion:forbidden')
    ->name('events.ticket.companion.download');
});

Route::group(['middleware' => ['auth']], function(){

  // Guest checkin
  Route::group(['middleware' => ['auth.owner.guest:forbidden']], function(){
    Route::get('guests/checkin/{hash}', 'GuestsController@checkin')->name('guests.checkin');
    Route::post('guests/checkin/{hash}/confirm', 'GuestsController@confirmCheckin')->name('guests.confirm_checkin');

    Route::get('sessions/{session}/guests/checkin/{hash}', 'GuestsController@checkinSession')->name('sessions.guests.checkin');
    Route::post('sessions/{session}/guests/checkin/confirm', 'GuestsController@confirmCheckinSession')->name('sessions.guests.confirm_checkin');
  });

  Route::get('checkin', 'CheckinController@index')->name('checkin.index');

  // Minisite
  Route::group(['middleware' => ['auth.owner.page:forbidden']], function () {
    Route::post('minisites/{minisite}/pages/{page}/_page_up', 'PageController@pageUp')
      ->name('minisite.pages.page_up');
    Route::post('minisites/{minisite}/pages/{page}/_page_down', 'PageController@pageDown')
      ->name('minisite.pages.page_down');
  });
});


// Public routes
Route::get('survey/{hash}', 'SurveyFormController@show')->name('events.surveyForm.show');
Route::get('subscribe/{hash}', 'SubscribeFormController@show')->name('events.subscribeForm.show');
Route::get('subscribe/{hash}/companion', 'SubscribeFormController@showCompanion')->name('events.subscribeForm.showCompanion');
Route::get('decline/{hash}', 'GuestsController@decline')->name('events.guests.decline');


// Override unused registration routes
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'HomeController@index')->name('home');
Route::post('/register', 'HomeController@index')->name('home');
