<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| TEST Routes
|--------------------------------------------------------------------------
| For selenium & co.
|
*/

// Auth based on event in URL
Route::group(['middleware' => ['auth', 'auth.owner:event', 'event.active']], function () {

    // Event
    Route::get('events/{id}/guests/_import_smoke', 'Test\GuestsController@importSmoke');
    Route::get('events/{eventId}/ticket/_overview', 'Test\TicketController@overview');
});