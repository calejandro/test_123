<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'auth', 'auth.owner.event:forbidden', 'event.active']], function () {
    // Guests
    Route::get('events/{event_id}/guests/_attributes', 'Api\GuestsController@attributes');
    Route::get('events/{event_id}/guests/_minattributes', 'Api\GuestsController@minAttributes');
    Route::post('events/{event_id}/guests/_import', 'Api\GuestsController@import');
    Route::delete('events/{event_id}/guests/_destroy_all', 'Api\GuestsController@destroyAll');
    Route::post('events/{event_id}/guests/anonymise', 'Api\GuestsController@anonymise');

    Route::group(['middleware' => ['auth.owner.guest:forbidden']], function () {
        Route::post('events/{event_id}/guests/solve-duplicate/{guest_id}', 'Api\GuestsController@solveDuplicate');
        Route::post('events/{event_id}/guests/{guest_id}/_uncheckin', 'Api\GuestsController@unCheckin');
        Route::post('events/{event_id}/sessions/{session_id}/guests/{guest_id}/_uncheckin', 'Api\GuestsController@unCheckinSession');
        Route::resource('events.guests', 'Api\GuestsController', ['only' => ['index', 'destroy', 'update']]);
    });

    // Guests filtering API route
    Route::post('events/{event_id}/guests/filter', 'Api\GuestsController@filter');
    Route::post('events/{event_id}/guests/_filter_all', 'Api\GuestsController@filterAll');

    // Companions
    Route::group(['middleware' => ['auth.owner.companion:forbidden']], function () {
        Route::post('events/{event_id}/companions/{companion_id}/_uncheckin', 'Api\CompanionsController@unCheckin');
        Route::post('events/{event_id}/sessions/{session_id}/companions/{companion_id}/_uncheckin', 'Api\CompanionsController@unCheckinSession');
        Route::resource('events.companions', 'Api\CompanionsController', ['only' => ['destroy', 'update']]);
    });

    // SubscribeForms
    Route::get('events/{event_id}/subscribeform/show', 'Api\SubscribeFormController@show');
    Route::patch('events/{event_id}/subscribeform', 'Api\SubscribeFormController@update');

    // SurveyForms
    Route::get('events/{event_id}/surveyform/show', 'Api\SurveyFormController@show');
    Route::patch('events/{event_id}/surveyform', 'Api\SurveyFormController@update');
    Route::get('events/{event_id}/surveyform/_results', 'Api\SurveyFormController@results');

    // Event
    Route::post('events/{event_id}/_remove_extended_field', 'Api\EventsController@removeExtendedField');
    Route::get('events/{event_id}/_stats', 'Api\EventsController@stats');
    Route::post('events/{event_id}/_checkin/{hash}', 'Api\EventsController@checkin');
    Route::get('events/{event_id}/_extendedFields', 'Api\EventsController@extendedFields');

    // Session
    Route::post('events/{event_id}/sessions/{session_id}/_checkin/{hash}', 'Api\SessionsController@checkin')
        ->middleware('auth.owner.guest:forbidden');

    // Verification jobs
    Route::get('events/{event_id}/_check_email_verification_jobs', 'Api\GuestsController@checkEmailVerificationJobs');

    // Emails
    Route::post('events/{event_id}/emails/{mail_id}/send', 'Api\EmailsController@send');
    Route::post('events/{event_id}/emails/{mail_id}/resend', 'Api\EmailsController@resendInvalid');
    Route::post('events/{event_id}/emails/{lang}', 'Api\EmailsController@store');
    Route::patch('events/{event_id}/emails/{mail_id}/{lang}', 'Api\EmailsController@update');
    Route::resource('events.emails', 'Api\EmailsController', ['only' => ['index']]);

    // JobFeedbacks
    Route::get('events/{event_id}/emails/{email_id}/jobfeedbacks', 'Api\JobFeedbacksController@index')
        ->middleware('auth.owner.email:forbidden');
    Route::get('emails/{email_id}/jobfeedbacks/{job_id}/export', 'Api\JobFeedbacksController@exportShipment')
        ->middleware('auth.owner.email:forbidden');
});

Route::group(['middleware' => ['api', 'auth', 'auth.owner.company:forbidden']], function () {
    // Templates
    Route::resource('companies.templates', 'Api\TemplatesController', ['only' => [
        'index',
        'create',
        'store'
    ]]);
    Route::resource('companies.templates', 'Api\TemplatesController', ['except' => [
        'index',
        'create',
        'store'
    ]])->middleware('auth.owner.template:forbidden');
});

Route::group(['middleware' => ['api', 'event.active']], function () {
    
    // Guests
    Route::group(['middleware' => ['token.owner.guest']], function () {
        Route::patch('events/{event_id}/guests/{guest_id}/_subscribe', 'Api\GuestsController@subscribe');
        Route::patch('events/{event_id}/guests/{guest_id}/_subscribe_nominative', 'Api\GuestsController@subscribeNominative');
    });

    Route::group(['middleware' => ['event.minisite.public']], function () {
        Route::post('events/{event_id}/guests/_register', 'Api\GuestsController@register');
        Route::post('events/{event_id}/guests/_register_nominative', 'Api\GuestsController@registerNominative');
    });

    // Companions
    Route::patch('events/{event_id}/companions/{companion_id}/_update_participation', 'Api\CompanionsController@updateParticipation')
        ->middleware('token.owner.companion');

    Route::resource('events.guests.companions', 'Api\CompanionsController', [
        'only' => ['index']
    ])->middleware('token.owner.guest');

    // SurveyForms
    Route::patch('events/{event_id}/guests/{guest_id}/surveyform/_reply', 'Api\SurveyFormController@reply')
        ->middleware('token.owner.guest');

    Route::group(['middleware' => ['event.minisite.public']], function () {

        // SubscribeForms
        Route::get('events/{event_id}/subscribeform', 'Api\SubscribeFormController@get');
        Route::get('events/{event_id}/subscribeform/_companion', 'Api\SubscribeFormController@showCompanion');
        
        // SurveyForms
        Route::get('events/{event_id}/surveyform', 'Api\SurveyFormController@get');

        // Sessions
        Route::get('events/{event_id}/sessions', 'Api\SessionsController@index');

        // Event
        Route::get('events/{event_id}', 'Api\EventsController@show');
    });

    // Sessions
    Route::get('guests/{guest_id}/sessions', 'Api\SessionsController@indexOfGuest')
        ->middleware('token.owner.guest');

    Route::resource('events', 'Api\EventsController', [
        'only' => ['index'] // Auth based
    ]);
});
