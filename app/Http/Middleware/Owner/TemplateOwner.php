<?php

namespace App\Http\Middleware\Owner;

use App\Template;
use App\Company;

class TemplateOwner extends Owner
{
    protected $ressource = 'template';
    protected $ressourceClass = Template::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $template = Template::with('company')->findOrFail($id);
        return $template->company;
    }
}
