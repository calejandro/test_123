<?php

namespace App\Http\Middleware\Owner;

use App\Event;
use App\Company;

class EventOwner extends Owner
{
    protected $ressource = 'event';
    protected $ressourceClass = Event::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $event = Event::with('company')->findOrFail($id);
        return $event->company;
    }
}
