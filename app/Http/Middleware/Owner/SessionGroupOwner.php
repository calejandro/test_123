<?php

namespace App\Http\Middleware\Owner;

use App\SessionGroup;

class SessionGroupOwner extends Owner
{
    protected $ressource = 'session_group';
    protected $ressourceClass = SessionGroup::class;
}
