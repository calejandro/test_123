<?php

namespace App\Http\Middleware\Owner;

use App\Ticket;

class TicketOwner extends Owner
{
    protected $ressource = 'ticket';
    protected $ressourceClass = Ticket::class;
}
