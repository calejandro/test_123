<?php

namespace App\Http\Middleware\Owner;

use App\Session;

class SessionOwner extends Owner
{
    protected $ressource = 'session';
    protected $ressourceClass = Session::class;
}
