<?php

namespace App\Http\Middleware\Owner;

use App\Companion;
use App\Company;

class CompanionOwner extends Owner
{
    protected $ressource = 'companion';
    protected $ressourceClass = Companion::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $companion = Companion::with('guest', 'guest.event', 'guest.event.company')->findOrFail($id);
        return $companion->guest->event->company;
    }
}