<?php

namespace App\Http\Middleware\Owner;

use App\User;
use App\Company;

class UserOwner extends Owner
{
    protected $ressource = 'user';
    protected $ressourceClass = User::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $user = User::with('company')->findOrFail($id);
        return $user->company;
    }
}
