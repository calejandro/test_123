<?php

namespace App\Http\Middleware\Owner;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Auth;
use App\Event;
use App\Company;

abstract class Owner
{
    protected $auth;

    protected $ressource = '';
    protected $ressourceClass = null;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
    * Default incoming request handle for generic cases.
    * If used, $this->ressource and $this->ressourceClass have to be overrided
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string  $mode : 'redirect' or 'forbidden'
    * @return mixed
    */
    public function handle($request, Closure $next, $mode)
    {
        if (Auth::user()->isAdmin()) {
            return $next($request);
        }

        $id = $this->getRessourceId($request, $this->ressource);
        if ($id === null) {
            return $this->forbidden();
        }

        $company = $this->retrieveModelCompany($id);

        if ($company === null || $company->id !== Auth::user()->company_id) {
            return $this->accessRefused($mode);
        }

        return $next($request);
    }

    /**
     * Generic case, can be overrided to use handle without overriding it
     */
    protected function retrieveModelCompany($id) : ?Company
    {
        $model = $this->ressourceClass::with('event', 'event.company')->findOrFail($id);
        return $model->event->company;
    }

    /**
     * @param mode: 'redirect' or 'forbidden'
     */
    protected function accessRefused($mode = 'redirect')
    {
        if ($mode === 'redirect') {
            return $this->redirect();
        }
        else if ($mode === 'forbidden') {
            return $this->forbidden();
        }
        else {
            return $this->forbidden();
        }
    }

    /**
     * Redirect to events index or guests list if controller
     */
    protected function redirect()
    {
        if (Auth::user()->isController()) {
            return redirect()->route('events.guests.index', [Auth::user()->event]);
        }
        return redirect()->route('companies.events.index', [Auth::user()->company]);
    }

    protected function forbidden()
    {
        return response('Access denied', 403);
    }

    protected function getRessourceId($request, string $ressource)
    {
        $params = $request->route()->parameters();

        if (array_key_exists($ressource, $params)) {
            // Laravel can cast ressource in integer or Object depending of controller functions parameters
            if(is_numeric($params[$ressource])) {
                return (int) $params[$ressource];
            }
            else {
                return $params[$ressource]->id;
            }
        }
        else if (array_key_exists($ressource . '_id', $params)) {
            return (int) $params[$ressource . '_id'];
        }
        else {
            return null;
        }
    }
}
