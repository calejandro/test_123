<?php

namespace App\Http\Middleware\Owner;

use App\Page;
use App\Company;

class PageOwner extends Owner
{
    protected $ressource = 'page';
    protected $ressourceClass = Page::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $page = Page::with('minisite', 'minisite.event', 'minisite.event.company')->findOrFail($id);
        return $page->minisite->event->company;
    }
}
