<?php

namespace App\Http\Middleware\Owner;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class CompanyOwner extends Owner
{
    public function __construct(Guard $auth)
    {
        parent::__construct($auth);
    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string $mode : 'redirect' or 'forbidden'
    * @return mixed
    */
    public function handle($request, Closure $next, $mode)
    {
        $params = $request->route()->parameters();
        if (Auth::user()->isAdmin()) {
            return $next($request);
        }

        $company = Auth::user()->company()->first();

        $companyId = $this->getRessourceId($request, 'company');

        if ((int) $companyId !== $company->id) {
            return $this->accessRefused($mode);
        }
    
        return $next($request);
    }
}
