<?php

namespace App\Http\Middleware\Owner;

use App\Email;

class EmailOwner extends Owner
{
    protected $ressource = 'email';
    protected $ressourceClass = Email::class;
}
