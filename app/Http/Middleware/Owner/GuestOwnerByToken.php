<?php

namespace App\Http\Middleware\Owner;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Guest;

use Log;

class GuestOwnerByToken extends Owner
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $mode : 'redirect' or 'forbidden', 'forbidden' by default
     * @return mixed
     */
    public function handle($request, Closure $next, $mode='forbidden')
    {
        $token = $request->header('Authorization');
        $guestId = $this->getRessourceId($request, 'guest');
        $eventId = $this->getRessourceId($request, 'event');

        $guest = Guest::findOrFail($guestId);

        if ($eventId !== null && $guest->event_id !== $eventId) {
            return $this->accessRefused($mode);
        }

        if ($guest->accessHash !== $token) {
            return $this->accessRefused($mode);
        }

        return $next($request);
    }
}
