<?php

namespace App\Http\Middleware\Owner;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Companion;

class CompanionOwnerByToken extends Owner
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $mode : 'redirect' or 'forbidden', 'forbidden' by default
     * @return mixed
     */
    public function handle($request, Closure $next, $mode='forbidden')
    {
        $token = $request->header('Authorization');
        $companionId = (int) $this->getRessourceId($request, 'companion');
        $eventId = (int) $this->getRessourceId($request, 'event');

        $companion = Companion::with('guest')->findOrFail($companionId);

        if ($companion->guest->event_id !== $eventId) {
            return $this->accessRefused($mode);
        }

        if ($companion->access_hash !== $token) {
            return $this->accessRefused($mode);
        }

        return $next($request);
    }
}
