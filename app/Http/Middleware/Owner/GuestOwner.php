<?php

namespace App\Http\Middleware\Owner;

use App\Guest;
use App\Companion;

class GuestOwner extends Owner
{
    protected $ressource = 'guest';
    protected $ressourceClass = Guest::class;

    protected function getRessourceId($request, string $ressource)
    {
        $id = parent::getRessourceId($request, $ressource);

        $params = $request->route()->parameters();

        if ($id === null && array_key_exists('hash', $params)) {
            $guest = Guest::where('accessHash', $params['hash'])->first();

            if ($guest === null) {
                $companion = Companion::where('access_hash', $params['hash'])->firstOrFail();
                $guest = $companion->guest;
            }

            return $guest->id;
        }

        return $id;
    }
}
