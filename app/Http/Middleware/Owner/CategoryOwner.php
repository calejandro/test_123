<?php

namespace App\Http\Middleware\Owner;

use App\Category;
use App\Company;

class CategoryOwner extends Owner
{
    protected $ressource = 'category';
    protected $ressourceClass = Category::class;

    protected function retrieveModelCompany($id) : ?Company
    {
        $category = Category::with('company')->findOrFail($id);
        return $category->company;
    }
}
