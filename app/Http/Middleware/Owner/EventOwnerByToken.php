<?php

namespace App\Http\Middleware\Owner;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Guest;

class EventOwnerByToken extends Owner
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $mode : 'redirect' or 'forbidden', 'forbidden' by default
     * @return mixed
     */
    public function handle($request, Closure $next, $mode='forbidden')
    {
        $token = $request->header('Authorization');
        $eventId = $this->getRessourceId($request, 'event');

        $guest = Guest::where('accessHash', $token)->firstOrFail();

        if ($guest->event_id !== $eventId) {
            return $this->accessRefused($mode);
        }

        return $next($request);
    }
}
