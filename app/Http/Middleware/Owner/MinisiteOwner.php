<?php

namespace App\Http\Middleware\Owner;

use App\Minisite;

class MinisiteOwner extends Owner
{
    protected $ressource = 'minisite';
    protected $ressourceClass = Minisite::class;
}
