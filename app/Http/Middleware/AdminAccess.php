<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use Auth;

class AdminAccess
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = $this->auth->user()->company()->first();
        if ($company != null) {
            if (Auth::user()->isController()) {
                return redirect()->route('events.guests.index', [Auth::user()->event]);
            }
            return redirect()->route('companies.events.index', [$company]);
        }

        return $next($request);
    }
}
