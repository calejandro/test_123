<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Auth\Middleware\Authenticate;

use Auth;

class AuthLogin extends Authenticate
{
    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(AuthFactory $auth)
    {
        parent::__construct($auth);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);

        $company = Auth::user()->company()->first();
        if ($company != null && $company->lock) {
            Session::flash('flash_message', 'Account locked');
            unset(Auth::user()->key);
            $request->session()->flush();
            return redirect()->route('login');
        }

        return $next($request);
    }
}
