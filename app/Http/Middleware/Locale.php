<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

use App\Utils\LocaleUtil;

use App\User;
use App\Event;

class Locale
{
  public function handle($request, Closure $next)
  {
    if (Session::has('locale') AND in_array(Session::get('locale'), Config::get('app.locales')))
    {
      LocaleUtil::changeLocal(Session::get('locale'));
    }
    else
    {
      $local = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : Config::get('app.fallback_locale');
      App::setLocale($local);
      LocaleUtil::changeLocal($local);
    }

    return $next($request);
  }
}

?>
