<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Log;
use Auth;
use App\Event;

/**
 * Check that the event is active
 */
class EventActive
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        $params = $request->route()->parameters();

        $id = null;
        if(array_key_exists('event', $params)){
            if (is_array($params['event'])) {
                $id = $params['event']['id'];
            }
            else if (is_object($params['event'])) {
                $id = $params['event']->id;
            }
            else {
                $id = $params['event'];
            }
        }
        else if (array_key_exists('event_id', $params)){
            $id = $params['event_id'];
        }
        else if(array_key_exists('id', $params)){
            $id = $params['id'];
        }
        else{
            return $next($request);
        }

        $event = Event::where([
            'active' => true,
            'id' => $id
        ])->firstOrFail();

        return $next($request);
    }
}
