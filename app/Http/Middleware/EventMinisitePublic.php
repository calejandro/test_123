<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Event;
use App\Companion;
use App\Guest;

use Auth;

/**
 * If no Guest/Companion Authorization Header is present And no user is auth then only events with public minisite can pass through
 */
class EventMinisitePublic
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $eventId = $this->getEventId($request);
        $event = Event::findOrFail($eventId);

        if ($event->public_minisite) {
            return $next($request);
        }

        if (Auth::user() !== null) {
            // User must have access right to the event
            if (Auth::user()->isAdmin()) {
                return $next($request);
            }

            if (Auth::user()->company_id !== $event->company_id) {
                return response('Access denied', 403);
            }
        }
        else if ($token === null || $token === 'null') {
            // Event must be public
            return response('Not found', 404);
        }
        else {
            // Token must be valid
            $guest = Guest::where('accessHash', $token)->first();

            if ($guest !== null) {
                if ($guest->event_id !== $eventId) {
                    return response('Access denied', 403);
                }
            }
            else {
                $companion = Companion::with('guest')->where('access_hash', $token)->first();
                if ($companion !== null) {
                    if ($companion->guest->event_id !== $eventId) {
                        return response('Access denied', 403);
                    }
                }
                else if(!$event->isValidDemoHash($token)) { 
                    // last chance is with a demo hash
                    return response('Access denied', 403);
                }
            }
        }

        return $next($request);
    }

    private function getEventId($request) : int
    {
        $params = $request->route()->parameters();
        if (array_key_exists('event', $params)) {
            return $params['event']->id;
        }
        else if (array_key_exists('event_id', $params)){
            return (int) $params['event_id'];
        }
    }
}
