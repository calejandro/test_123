<?php

namespace App\Http\Requests\Guest;
use App\Event;
use Illuminate\Foundation\Http\FormRequest;

class GuestRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $eventId=$this->input('eventId');
         $events_visible_captcha = Event::findOrFail($eventId);
         if($events_visible_captcha->visible_captcha)
            $validate='required|captcha';           
         else
            $validate='';

        return [            
            'firstname' => 'string|max:255',
			'lastname' => 'string|max:255',
            'email' => 'string|email|max:255',
            'extendedFields' => 'array',
            'companions' => 'nullable|integer',
            'sessions' => 'nullable|array', // format [ sessionId => nbPlaces, sessionId => nbPlaces,  ]
            'recaptchaResponse' =>  $validate, // captcha validation
            'companionsData' => 'nullable|array',
            'companionsData.*.firstname'=> 'nullable|string|max:255',
            'companionsData.*.lastname'=> 'nullable|string|max:255',
            'companionsData.*.email'=> 'required|email|max:255',
            'companionsData.*.company'=> 'nullable|string|max:255',
            'companionsData.*.id'=> 'nullable'
        ];
    }
}
