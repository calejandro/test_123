<?php

namespace App\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class GuestRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'firstname' => 'nullable|string|max:255',
      'lastname' => 'nullable|string|max:255',
      'email' => 'required|string|email|max:255',
      'company' => 'nullable|string|max:255',
      'extended_fields' => 'nullable|array',
      'invited' => 'nullable|boolean',
      'declined' => 'nullable|boolean',
      'subscribed' => 'nullable|boolean',
      'checkin' => 'nullable|boolean'
    ];
  }
}
