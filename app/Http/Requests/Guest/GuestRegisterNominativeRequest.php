<?php

namespace App\Http\Requests\Guest;
use App\Event;

use Illuminate\Foundation\Http\FormRequest;

class GuestRegisterNominativeRequest extends GuestSubscribeNominativeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        try {
            $eventId=$this->input('eventId');

            if(Event::findOrFail($eventId)->visible_captcha)
               $rules = ['recaptchaResponse' => 'required|captcha'];

        } catch (\Throwable $th) {
            return parent::rules();
        }

        return parent::rules() + $rules;
    }
}
