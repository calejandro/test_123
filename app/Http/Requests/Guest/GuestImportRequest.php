<?php

namespace App\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class GuestImportRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    $minimumMapping = ['email'];
    return [
      'mapping' => [
            'required', 
            'array',
            function($attribute, $value, $fail) use ($minimumMapping) {
                foreach($value as $key => $value) {
                    $index = array_search($value, $minimumMapping);
                    if($index !== FALSE){
                        unset($minimumMapping[$index]);
                    }
                }
                if(count($minimumMapping) > 0) {
                    return $fail(Lang::choice('validation.custom.assign_fields', count($minimumMapping)) . implode(", ", $minimumMapping));
                }
            }
        ],
      'ignored' => 'nullable|array',
      'guests' => 'required|array'
    ];
  }
}
