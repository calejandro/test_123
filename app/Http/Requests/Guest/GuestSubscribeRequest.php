<?php

namespace App\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class GuestSubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'string|max:255',
			'lastname' => 'string|max:255',
            'email' => 'string|email|max:255',
            'extendedFields' => 'array',
            'companions' => 'nullable|integer',
            'sessions' => 'nullable|array', // format [ sessionId => nbPlaces, sessionId => nbPlaces,  ]
            'companionsData' => 'nullable|array',
            'companionsData.*.firstname'=> 'required|string|max:255',
            'companionsData.*.lastname'=> 'required|string|max:255',
            'companionsData.*.email'=> 'required|email|max:255',
            'companionsData.*.company'=> 'nullable|string|max:255',
            'companionsData.*.id'=> 'nullable'
        ];
    }
}
