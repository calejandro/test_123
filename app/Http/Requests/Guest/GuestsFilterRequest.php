<?php

namespace App\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class GuestsFilterRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'type' => [
        'nullable', // null(=all)
        Rule::in(['guest', 'companion'])
      ],
      'firstname' => 'nullable|string|max:255',
      'lastname' => 'nullable|string|max:255',
      'email' => 'nullable|string|max:255',
      'company' => 'nullable|string|max:255',
      'invited' => 'nullable|boolean', // true/false/null(=all)
      'subscribed' => 'nullable|boolean', // true/false/null(=all)
      'declined' => 'nullable|boolean', // true/false/null(=all)
      'checkin' => 'nullable|boolean', // true/false/null(=all)
      'companions' => 'nullable|boolean', // true/false/null(=all)
      'valid_email' => [ 
        'nullable', // null(=all)
        Rule::in([0, 1, 2, 3])
      ],
      'language' => 'nullable|string|max:2',
      
      'extendedFields' => 'nullable|array',  // ['FIELD' => 'VALUE']
      'surveyFields' => 'nullable|array',  // ['FIELD_NAME' => 'VALUE']
      
      'mainGuest' => 'nullable',
      'mainGuest.firstname' => 'nullable|string|max:255',
      'mainGuest.lastname' => 'nullable|string|max:255',
      'mainGuest.email' => 'nullable|string|max:255',
      'mainGuest.company' => 'nullable|string|max:255',

      'sessions' => 'nullable',
      'sessions.subscribed' => 'nullable|array', // [ SESSION_ID => true/false/null ]
      'sessions.checkin' => 'nullable|array' // [ SESSION_ID => true/false/null ]
    ];
  }
}
