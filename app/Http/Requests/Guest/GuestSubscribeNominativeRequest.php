<?php

namespace App\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class GuestSubscribeNominativeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'string|max:255',
            'lastname' => 'string|max:255',
            'email' => 'string|email|max:255',
            'extendedFields' => 'array',
            'sessions' => 'array', // format [ sessionId, sessionId, sessionId ]
            'companionsData' => 'nullable|array',
            'companionsData.*.firstname'=> 'nullable|string|max:255',
            'companionsData.*.lastname'=> 'nullable|string|max:255',
            'companionsData.*.email'=> 'required|email|max:255',
            'companionsData.*.company'=> 'nullable|string|max:255',
            'companionsData.*.id'=> 'nullable', // null if not exists in DB
            'companionsData.*.sessions'=> 'nullable|array' // format [ sessionId, sessionId, sessionId ]
        ];
    }
}
