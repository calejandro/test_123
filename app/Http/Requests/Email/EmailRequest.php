<?php

namespace App\Http\Requests\Email;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

use App\EmailType;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'joinICS' => 'boolean',
            'type' => [ 
                'nullable', 
                Rule::in(EmailType::all())
            ],
            'sendWithDefaultMail' => 'boolean',
            'template' => 'string',
            'html' => 'string',
            'from_column' => 'nullable|string|max:255'
        ];
    }
}
