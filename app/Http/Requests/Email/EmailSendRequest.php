<?php

namespace App\Http\Requests\Email;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class EmailSendRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'guests' => 'nullable|array',
      'companions' => 'nullable|array',
      'fromColumn' => 'nullable|string',
      'password' => 'required|string',
      'mailMode' => 'boolean',
      'joinICS' => 'boolean'
    ];
  }
}
