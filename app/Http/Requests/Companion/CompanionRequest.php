<?php

namespace App\Http\Requests\Companion;

use Illuminate\Foundation\Http\FormRequest;

class CompanionRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'firstname' => 'nullable|string|max:255',
      'lastname' => 'nullable|string|max:255',
      'company' => 'nullable|string|max:255',
      'email' => 'nullable|string|email|max:255',
      'extended_fields' => 'nullable|array',
      'checkin' => 'nullable|boolean'
    ];
  }
}
