<?php

namespace App\Http\Requests\Companion;

use Illuminate\Foundation\Http\FormRequest;

class CompanionUpdateParticipationRequest extends CompanionRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    $parentRules = parent::rules();

    // remove not editable fields
    unset($parentRules['checkin']);

    return $parentRules + [
      'access_hash' => 'required|string'
    ];
  }
}
