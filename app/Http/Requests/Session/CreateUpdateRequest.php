<?php

namespace App\Http\Requests\Session;

use Illuminate\Foundation\Http\FormRequest;

use App\SessionGroup;

use Lang;

class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_fr' => 'string|required_without_all:name_en,name_de|max:255',
            'name_en' => 'string|required_without_all:name_fr,name_de|max:255',
            'name_de' => 'string|required_without_all:name_en,name_fr|max:255',
            'places' => 'int|required',
            'session_group_id' => [
                'int',
                'nullable',
                function($attribute, $value, $fail) {
                    if ($value) {
                        if (SessionGroup::find($value) === NULL) {
                            return $fail(Lang::get('validation.custom.provide_valid_session_group'));
                        }
                    }
                }
            ]
        ];
    }
}
