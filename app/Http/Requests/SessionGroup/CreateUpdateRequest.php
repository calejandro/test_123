<?php

namespace App\Http\Requests\SessionGroup;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     public function attributes()
    {
        return [
            'allowed_session_subscription' => ucfirst(trans_choice('models.sessions.allowed_sessions', 2))
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_fr' => 'string|required_without_all:name_en,name_de|max:255',
            'name_en' => 'string|required_without_all:name_fr,name_de|max:255',
            'name_de' => 'string|required_without_all:name_en,name_fr|max:255',
            'allowed_session_subscriptions' => 'numeric|min:1|nullable'
        ];
    }
}
