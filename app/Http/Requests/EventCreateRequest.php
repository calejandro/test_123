<?php

namespace App\Http\Requests;

use Lang;

use Illuminate\Foundation\Http\FormRequest;
class EventCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_fr' => 'required_if:language_fr,1|max:255',
            'name_en' => 'required_if:language_en,1|max:255',
            'name_de' => 'required_if:language_de,1|max:255',
            'nb_places' => 'integer|min:0',
            'companions_limit' => 'required|integer|min:0',
            'start_date' => 'date_format:"d/m/Y H:i"',
            'end_date' => 'date_format:"d/m/Y H:i"|after_or_equal:start_date',
            'address' => 'required',
            'advanced_minisite_mode' => 'required|boolean',
            'allow_subscription_update' => 'nullable|boolean',
            'nominative_companions' => 'nullable|boolean',
            'type_id' => 'required|integer',
            'description' => 'nullable',
            'organisator_firstname' => 'nullable|string',
            'organisator_lastname' => 'nullable|string',
            'organisator_email' => 'nullable|email',
            'language_fr' => 'string|required_without_all:language_en,language_de|max:255',
            'language_en' => 'string|required_without_all:language_fr,language_de|max:255',
            'language_de' => 'string|required_without_all:language_en,language_fr|max:255',
        ];
    }
}
