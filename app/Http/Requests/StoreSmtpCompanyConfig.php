<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Log;
use Swift_SmtpTransport;
use Lang;
use Config;

class StoreSmtpCompanyConfig extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  public function isValidEmailConfig()
  {
    if($this->input('smtp_host') == "" || $this->route('company')->id===92){ // TODO: remove Bypass company auth config for specific test
      return true;
    }else{
      try{
        $transport = new \Swift_SmtpTransport($this->input('smtp_host'), $this->input('smtp_port'));
        $transport->setUsername($this->input('smtp_username'));
        $transport->setPassword($this->input('smtp_password'));
        $mailer = new \Swift_Mailer($transport);
        $mailer->getTransport()->start();
        return true;
      } catch (\Swift_TransportException $e) {
        Log::info($e);
        return false;
      } catch (\Exception $e) {
        Log::info($e);
        return false;
      }
    }
  }

  public function isSameSmtpConfigAsDefault()
  {
    return $this->input('smtp_host') === Config::get('mail.host') &&
      $this->input('smtp_username') === Config::get('mail.username');
  }

  protected function getValidatorInstance()
  {
    return parent::getValidatorInstance()->after(function ($validator) {
      // Call the after method of the FormRequest (see below)
      $this->after($validator);
    });
  }

  public function after($validator)
  {
    if (!$this->isValidEmailConfig()) {
      $validator->errors()->add('config', Lang::get('interface.config_smtp_validation_error'));
    }
    if ($this->isSameSmtpConfigAsDefault()) {
      $validator->errors()->add('config', Lang::get('interface.config_smtp_validation_same_as_default', [
        'email' => Config::get('mail.username')
      ]));
    }
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'smtp_from_mail_address' => 'nullable|email|required_with:smtp_from_name,smtp_username',
      'smtp_from_name' => 'nullable|string|required_with:smtp_from_mail_address,smtp_username',
      'smtp_host' => 'nullable|string|required_with:smtp_port,smtp_username,smtp_password',
      'smtp_port' => 'nullable|numeric|required_with:smtp_port,smtp_username,smtp_password',
      'smtp_username' => 'nullable|string|required_with:smtp_port,smtp_username,smtp_password',
      'smtp_password' => 'nullable|required_with:smtp_port,smtp_username,smtp_password'
    ];
  }
}
