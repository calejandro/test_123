<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MinisiteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = ($this->minisite) ? $this->minisite->id : null; // not null in case of update
        return [
            // 'title' => 'required',
            'slug' => 'regex:/^[a-z0-9\-]*$/|max:63|unique:minisites,slug,'.$id,
            'menu_font_size' => 'required',
            'title_font_size' => 'required|min:0',
            'site_background_image' => 'image|max:5000',
            'site_top_image' => 'image|max:5000',
            'bottom_image_path' => 'image|max:5000',
        ];
    }
}
