<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currently_editing_lang' => [
                'required',
                Rule::in(config('app.locales'))
            ],
            'name_fr' => 'required_if:currently_editing_lang,==,fr',
            'name_en' => 'required_if:currently_editing_lang,==,en',
            'name_de' => 'required_if:currently_editing_lang,==,de',
            'slug_fr' => 'required_if:currently_editing_lang,==,fr',
            'slug_en' => 'required_if:currently_editing_lang,==,en',
            'slug_de' => 'required_if:currently_editing_lang,==,de',
            'page_type_id' => 'required'
        ];
    }

    // I18n of the error messages, see validation.php
    // custom > name_* and slug_*
}
