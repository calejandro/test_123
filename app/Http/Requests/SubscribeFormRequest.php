<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visible' => 'required|boolean',
            'template' => 'required_without:advancedFormTemplate',
            'html' => 'required_without:advancedFormTemplate',
            'advanced_form_companion_template' => 'nullable',
            'advanced_form_companion_html' => 'nullable',
            'formTemplate' => 'required_without:advancedFormTemplate',
            'formHtml' => 'required_without:advancedFormTemplate',
            'advancedFormTemplate' => 'required_without:formTemplate',
            'advancedFormHtml' => 'nullable',
            'label_color' => 'required|int',
            'allow_basefields_modification' => 'required|boolean',
            'companion_lastname_required' => 'required|boolean',
            'companion_firstname_required' => 'required|boolean',
            'companion_company_required' => 'required|boolean',
            'submit_text_fr' => 'nullable|string|max:255',
            'submit_text_en' => 'nullable|string|max:255',
            'submit_text_de' => 'nullable|string|max:255',
            'submit_text_es' => 'nullable|string|max:255',
            'submit_color_bg' => 'required|string|max:255',
            'submit_color_text' => 'required|string|max:255',
        ];
    }
}
