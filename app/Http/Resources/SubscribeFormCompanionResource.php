<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscribeFormCompanionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'visible' => $this->visible,
            'advanced_form_companion_template' => $this->advanced_form_companion_template,
            'advanced_form_companion_html' => $this->advanced_form_companion_html,
            'label_color' => $this->label_color,
            'allow_basefields_modification' => $this->allow_basefields_modification,
            'companion_lastname_required' => $this->companion_lastname_required,
            'companion_firstname_required' => $this->companion_firstname_required,
            'companion_company_required' => $this->companion_company_required,
            'submit_text' => $this->submit_text, // i18n
            'submit_color_bg' => $this->submit_color_bg,
            'submit_color_text' => $this->submit_color_text
        ];
    }
}
