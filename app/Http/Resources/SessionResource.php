<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subscribed' => $this->countSessionSubscribers(),
            'checkin' => $this->countSessionCheckin()
        ];
    }

    private function countSessionSubscribers()
    {
        return ($this->pivot->where('session_id', $this->id)->exists() ? 1 : 0) +
            $this->companions->where('guest_id', $this->pivot->guest_id)->count();
    }

    private function countSessionCheckin()
    {
        return ($this->pivot->checkin > 0 ? 1 : 0) + $this->companions()
            ->wherePivot('checkin', true)
            ->where('guest_id', $this->pivot->guest_id)
            ->count();
    }
}
