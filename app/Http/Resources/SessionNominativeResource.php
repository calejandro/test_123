<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SessionNominativeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subscribed' => $this->countSessionSubscribers(),
            'checkin' => $this->pivot->checkin
        ];
    }

    private function countSessionSubscribers(){
        return ($this->pivot->where('session_id', $this->id)->exists() ? 1 : 0);
    }
}
