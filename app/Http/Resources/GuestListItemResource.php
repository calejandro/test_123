<?php

namespace App\Http\Resources;

use Log;

use App\Event;
use App\Minisite;

use Illuminate\Http\Resources\Json\JsonResource;


use App\Http\Resources\GuestListCollectionResource as GuestListCollectionResource;
use App\Http\Resources\SessionResource as SessionResource;
use App\Http\Resources\SurveyResponseResource as SurveyResponseResource;

/**
 * Guest and Companion shared representation for Guest List view
 */
class GuestListItemResource extends JsonResource
{
    protected $listEvent;
    protected $listMinisite;

    public function listEvent(Event $listEvent)
    {
        $this->listEvent = $listEvent;
        return $this;
    }
    public function listMinisite(?Minisite $listMinisite)
    {
        $this->listMinisite = $listMinisite;
        return $this;
    }

    public function isGuest(): bool
    {
        return $this->guest_id === null;
    }

    public function subscribeFormUrl()
    {
        if ($this->isGuest()) {
            return $this->listEvent->subscribeFormLinkWithHash($this->accessHash, $this->listMinisite);
        } else {
            return $this->listEvent->subscribeFormLinkWithHash($this->access_hash, $this->listMinisite, true);
        }
    }

    public static function collection($resource)
    {
        return new GuestListCollectionResource($resource, get_called_class());
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event_id' => $this->isGuest() ? $this->event_id : $this->guest->event_id,
            'guest_id' => $this->guest_id, // can be null
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'company' => $this->company,
            'email' => $this->email,
            'access_hash' => $this->accessHash,
            'checkin' => $this->countCheckin(),
            'invited' => $this->isGuest() ? $this->invited : null,
            'subscribed' => $this->isGuest() ? $this->subscribed : true,
            'subscribed_at' => $this->isGuest() ? $this->subscribed_at : null,
            'declined' => $this->declined,
            'language' => $this->isGuest() ? $this->language : $this->guest->language,
            'valid_email' => $this->valid_email,
            'extended_fields' => $this->isGuest() ? (object) $this->extendedFields : (object) $this->extended_fields,
            'sessions' => $this->guestSessions(),
            'companions' => $this->nbCompanions,
            'survey' => $this->guest_id === null ? SurveyResponseResource::collection($this->surveyResponses) : [],
            'is_companion' => !$this->isGuest(),
            'guest' => $this->isGuest() ? null : GuestListItemResource::make($this->guest)
                ->listEvent($this->listEvent)
                ->listMinisite($this->listMinisite),
            'subscribeFormUrl' => $this->subscribeFormUrl()
        ];
    }

    private function guestSessions()
    {
        if ($this->listEvent->nominative_companions) {
            return SessionNominativeResource::collection($this->sessions);
        }
        else {
            $allSessions = $this->sessions->concat($this->companions->pluck('sessions')->flatten());
            $allSessions = $allSessions->unique('id');

            return SessionResource::collection($allSessions);
        }
    }

    private function countCheckin()
    {
        if ($this->listEvent->nominative_companions) {
            return $this->checkin;
        }
        else {
            return ($this->checkin > 0 ? 1 : 0) + $this->companions->where('checkin', true)->count();
        }
    }
}
