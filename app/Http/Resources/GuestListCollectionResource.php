<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

use App\Http\Resources\GuestListItemResource as GuestListItemResource;

class GuestListCollectionResource extends AnonymousResourceCollection
{
    protected $listEvent;
    protected $listMinisite;

    public function listEvent($value)
    {
        $this->listEvent = $value;
        return $this;
    }
    public function listMinisite($value)
    {
        $this->listMinisite = $value;
        return $this;
    }

    public function toArray($request)
    {
        return $this->collection->map(function(GuestListItemResource $resource) use ($request) {
            return $resource
                ->listEvent($this->listEvent)
                ->listMinisite($this->listMinisite)
                ->toArray($request);
        })->all();
    }
}