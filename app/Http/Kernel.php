<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Spatie\Cors\Cors::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\Locale::class,
        ],

        'api' => [
            // 'throttle:60,1',
            // 'bindings', // remove to have API session access
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\Locale::class
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' =>  \App\Http\Middleware\AuthLogin::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'auth.admin' => \App\Http\Middleware\AdminAccess::class,
        'auth.locked' => \App\Http\Middleware\AuthLocked::class,
        'auth.owner.event' => \App\Http\Middleware\Owner\EventOwner::class,
        'auth.owner.company' => \App\Http\Middleware\Owner\CompanyOwner::class,
        'auth.owner.minisite' => \App\Http\Middleware\Owner\MinisiteOwner::class,
        'auth.owner.page' => \App\Http\Middleware\Owner\PageOwner::class,
        'auth.owner.session' => \App\Http\Middleware\Owner\SessionOwner::class,
        'auth.owner.session_group' => \App\Http\Middleware\Owner\SessionGroupOwner::class,
        'auth.owner.ticket' => \App\Http\Middleware\Owner\TicketOwner::class,
        'auth.owner.guest' => \App\Http\Middleware\Owner\GuestOwner::class,
        'auth.owner.companion' => \App\Http\Middleware\Owner\CompanionOwner::class,
        'auth.owner.email' => \App\Http\Middleware\Owner\EmailOwner::class,
        'auth.owner.user' => \App\Http\Middleware\Owner\UserOwner::class,
        'auth.owner.category' => \App\Http\Middleware\Owner\CategoryOwner::class,
        'auth.owner.template' => \App\Http\Middleware\Owner\TemplateOwner::class,
        'event.active' => \App\Http\Middleware\EventActive::class,
        'event.minisite.public' => \App\Http\Middleware\EventMinisitePublic::class,
        'token.owner.guest' => \App\Http\Middleware\Owner\GuestOwnerByToken::class,
        'token.owner.event' => \App\Http\Middleware\Owner\EventOwnerByToken::class,
        'token.owner.companion' => \App\Http\Middleware\Owner\CompanionOwnerByToken::class
    ];
}
