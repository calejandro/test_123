<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

use App\Page;
use App\Minisite;
use App\Event;
use App\PageType;
use App\Guest;
use App\Companion;

use App\Http\Requests\Page\CreateUpdateRequest as PageCreateUpdateRequest;

use App\Facades\TemplateService;
use App\Facades\PageService;
use App\Facades\MinisiteService;

use App\Utils\LocaleUtil;

class PageController extends Controller
{
    public function index(Minisite $minisite)
    {
      return view('pages.index', compact(['minisite']));
    }

    public function create(Event $event, Minisite $minisite, $lang = 'fr')
    {
      // Page types depending on event nominative companion
      if($event->nominative_companions){
        $pageTypes = PageType::orderBy('name', 'asc')->get()->pluck('name', 'id');
      }else{
        $pageTypes = PageType::accessibleByGuest()->orderBy('name', 'asc')->get()->pluck('name', 'id');
      }

      setcookie("subfolder", "subfolder-" . $event->company()->get()->first()->id, 0, "/");
      return view('pages.create', compact(['event', 'minisite', 'pageTypes', 'lang']));
    }

    public function store(PageCreateUpdateRequest $request, Event $event, Minisite $minisite)
    {
        $data = $request->except(['currently_editing_lang']);
        $page = new Page($data);
        $page->lang = 'fr'; // TODO: remove field in db ?
        $page->order_id = $minisite->pages()->count() + 1;
        $minisite->pages()->save($page);

        return redirect()->route('events.minisites.index', [$event])
          ->withSuccess(trans('interface.minisites_create_page.success_alert'));

    }

    public function redirectNewUrl($slug, $minisiteId, $page_slug, Page $page, $hash, $lang='fr')
    {
      $minisite = Minisite::findOrFail($minisiteId);
      return redirect()->route('minisite_page_subdomain_show', [
        'slug' => $minisite->slug,
        'page_slug' => $page_slug,
        'page' => $page->id,
        'hash' => $hash,
        'lang' => $lang
      ]);
    }  

    /**
     * Show minisite page
     * If minisite is public $hash may be null
     */
    public function show($slug, $page_slug, Page $page, $hash=null, $lang=null)
    {
      $minisite = Minisite::where('slug', $slug)->with('event', 'pages')->firstOrFail();
      $pages = $minisite->pages()->get();
      $event = $minisite->event()->with('surveyForm')->first();

      $setLang = $event->getLangsAttribute()[0];

      if(in_array($hash, ['fr', 'de', 'en'])) { // avoid to have double $lang in URL
        $setLang = $hash;
        $hash = null;
      }

      $guest = Guest::with('event', 'event.surveyForm')->where('accessHash', $hash)->active()->first();
      $companion = Companion::with('guest')->where('access_hash', $hash)->first();
  
      if (!MinisiteService::verifyPageAccess($event, $page, $guest, $companion, $hash)) {
        return view('subscribeForms.notallowed');
      }

      $demo = $event->isValidDemoHash($hash);

      // Defining minisite lang
      if(is_null($lang) && isset($guest) && !is_null($guest->language) && in_array($guest->language, $event->getLangsAttribute())){
        $lang = $guest->language;
      }else if(is_null($lang)){
        $lang = $setLang;
      }

      LocaleUtil::changeLocal($lang);

      // Survey form
      $surveyForm = $event->surveyForm()->first();

      // Subscribe form
      $subscribeForm = $event->subscribeForm()->first();

      $minAttributes = Guest::getMinAttributes();

      return view('minisite.show', compact([
        'minisite',
        'pages',
        'page',
        'event',
        'surveyForm',
        'subscribeForm',
        'guest', // can be null !
        'companion', // can be null !
        'hash',
        'lang',
        'minAttributes',
        'demo'
      ]));
    }

    public function edit(Event $event, Minisite $minisite, Page $page, $lang = 'fr', $get_lang = null)
    {
      $company = $event->company()->get();
      
      // Page types depending on event nominative companion
      if($event->nominative_companions){
        $pageTypes = PageType::orderBy('name', 'asc')->get()->pluck('name', 'id');
      }else{
        $pageTypes = PageType::accessibleByGuest()->orderBy('name', 'asc')->get()->pluck('name', 'id');
      }

      setcookie("subfolder", "subfolder-" . $event->company()->get()->first()->id, 0, "/");
      return view('pages.edit', compact(['company', 'event', 'minisite', 'page', 'pageTypes', 'lang', 'get_lang']));
    }

    public function update(PageCreateUpdateRequest $request, Event $event, Minisite $minisite, Page $page, $lang = 'fr')
    {
      $data = $request->except(['currently_editing_lang']);
      $page->update($data);

      return redirect()
        ->route('page_edit', [$event, $minisite, $page, $lang])
        ->withSuccess(trans('interface.minisites_update_page.success_alert'));
    }

    public function destroy(Event $event, Minisite $minisite, Page $page)
    {
      PageService::decreaseOrderAfter($minisite, $page);

      $page->delete();

      return redirect()->route('events.minisites.index', $event);
    }

    public function pageUp(Minisite $minisite, Page $page)
    {
      // Here update page number
      $upPage = Page::where(['minisite_id' => $minisite->id, 'order_id' => ($page->order_id - 1)])->first();

      if (isset($upPage)) {
        PageService::invertOrder($page, $upPage);
      }

      return redirect()->route('events.minisites.index', [$minisite->event]);
    }

    public function pageDown(Minisite $minisite, Page $page)
    {
      $upPage = Page::where(['minisite_id' => $minisite->id, 'order_id' => $page->order_id + 1])->first();

      if (isset($upPage)) {
        PageService::invertOrder($page, $upPage);
      }

      return redirect()->route('events.minisites.index', [$minisite->event]);
    }

}
