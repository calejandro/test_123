<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Facades\TemplateService;
use App\Facades\GuestService;

use App\Guest;
use App\Event;
use App\SurveyForm;
use Illuminate\Http\Request;
use Session;

use DbView;

class SurveyFormController extends Controller
{
    public function edit(Event $event)
    {
        $this->authorize('edit', SurveyForm::class);

        $randGuest = $event->guests()->active()->inRandomOrder()->first();
        $accessHash = $event->getDemoHash();
        return view('surveyForms.edit', compact(['event', 'accessHash']));
    }

    public function results(Event $event)
    {
        $surveyForm = $event->surveyForm()->first();
        return view('surveyForms.results', compact(['event', 'surveyForm']));
    }

    /**
    *   @param $hash: guest hash
    */
    public function show($hash)
    {
        $event = Event::findWithHash($hash);
        $demo = false;
        if($event == null) {
            $guest = Guest::with('event', 'event.surveyForm')->where('accessHash', $hash)->active()->firstOrFail();
            $event = $guest->event()->first();
        }
        else {
            // Demo
            $demo = true;
            $guest = GuestService::generateDemoGuest($event);
        }

        if (!$event->active) {
            abort(404, 'Page not found');
        }

        if($event->hasMinisite() && $event->advanced_minisite_mode) {
            $minisite = $event->minisite()->first();
            $url = $minisite->surveyFormLink($hash);
            if($url == null) {
                $url = $minisite->indexLink($hash);
            }
            return redirect($url);
        }

        $surveyForm = $event->surveyForm()->first();

        $surveyFormRender = $surveyForm->formHtml;
        if($event->advanced_subscription_form_mode){
            $surveyFormRender = '<div id="app-survey-form-view"><advanced-survey-form></advanced-survey-form></div>';
        }

        $viewBody = TemplateService::generate($surveyForm->html, [
            'eventSurveyForm' => $surveyFormRender,
        ]);

        return view('surveyForms.show', compact('event', 'surveyForm', 'guest', 'viewBody', 'demo'));
    }
}
