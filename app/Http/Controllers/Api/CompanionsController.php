<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Guest;
use App\Companion;
use App\Session;

use App\Facades\SessionService;
use App\Facades\CompanionService;

use App\Http\Controllers\Controller;

use App\Http\Requests\Companion\CompanionRequest;
use App\Http\Requests\Companion\CompanionUpdateParticipationRequest;

class CompanionsController extends Controller
{
  /**
   * This method returns companions of a given guest.
   * It is used for nominative companions page in order to
   * pre-fill companion(s) form(s).
   */
  public function index(int $eventId, int $guestId)
  {
    $guest = Guest::where('id', $guestId)->first();
    $companions = $guest->companions()->get();
    return response()->json($companions);
  }

  public function destroy(int $eventId, $id)
  {
    $companion = Companion::findOrFail($id);
    $companion->delete();

    return response()->json($companion);
  }

  public function update($event_id, $id, CompanionRequest $request)
  {
    $companion = Companion::with('guest')->findOrFail($id);
    $companion->guest->checkActive();

    $event = Event::findOrFail($event_id);

    $updatedCompanion = CompanionService::update($companion, $event, $request);

    return response()->json($updatedCompanion);
  }

  public function unCheckin(int $event_id, int $id)
  {
    $companion = Companion::findOrFail($id);
    $companion->checkin = false;
    $companion->save();
    return response()->json($companion);
  }

  public function unCheckinSession(int $event_id, int $session_id, int $id)
  {
    $companion = Companion::findOrFail($id);
    $session = Session::findOrFail($session_id);
    SessionService::unCheckinCompanion($companion, $session);
    return response()->json($companion);
  }

  public function updateParticipation(int $event_id, int $id, CompanionUpdateParticipationRequest $request)
  {
    $event = Event::with('companions')->findOrFail($event_id);
    $companion = $event->companions()->findOrFail($id);
    if ($request->access_hash !== $companion->access_hash)
    {
      return response()->json("Unauthorized", 401);
    }

    return response()->json(CompanionService::update($companion, $event, $request));
  }
}
