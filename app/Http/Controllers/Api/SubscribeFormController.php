<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubscribeForm;
use App\Event;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\SubscribeFormRequest;

use App\Http\Resources\SubscribeFormGuestResource;
use App\Http\Resources\SubscribeFormCompanionResource;

class SubscribeFormController extends Controller
{
  // This is exactly the same content as the show method but not the same goal and could change
  public function get(int $eventId)
  {
    $event = Event::with('subscribeForm')->findOrFail($eventId);
    $res = new SubscribeFormGuestResource($event->subscribeForm()->first());
    return response()->json($res);
  }

  public function show(int $eventId)
  {
    $event = Event::with('subscribeForm')->findOrFail($eventId);
    return response()->json($event->subscribeForm()->first());
  }

  public function showCompanion(int $eventId)
  {
    $event = Event::with('subscribeForm')->findOrFail($eventId);
    $res = new SubscribeFormCompanionResource($event->subscribeForm()->first());
    return response()->json($res);
  }

  public function update(int $eventId, SubscribeFormRequest $request)
  {
    $event = Event::with('subscribeForm')->findOrFail($eventId);
    $subscribeForm = $event->subscribeForm()->first();

    $data = $request->all();
    $subscribeForm->label_color = $data["label_color"];
    if (isset($data['allow_basefields_modification'])) {
      $subscribeForm->allow_basefields_modification = $data['allow_basefields_modification'];
    }
    if (isset($data['companion_lastname_required'])) {
      $subscribeForm->companion_lastname_required = $data['companion_lastname_required'];
    }
    if (isset($data['companion_firstname_required'])) {
      $subscribeForm->companion_firstname_required = $data['companion_firstname_required'];
    }
    if (isset($data['companion_company_required'])) {
      $subscribeForm->companion_company_required = $data['companion_company_required'];
    }

    $subscribeForm->update($data);
    return response()->json($subscribeForm);
  }
}
