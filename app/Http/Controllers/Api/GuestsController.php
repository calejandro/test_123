<?php

namespace App\Http\Controllers\Api;

use Log;
use App\Companion;

use Mail;
use Lang;
use \Config;
use Validator;
use Illuminate\Support\Collection;

use App\Session;
use App\Guest;
use App\Company;
use App\Event;
use App\Email;
use App\JobFeedback;
use App\Minisite;
use App\JobMailVerificationFeedback;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Facades\GuestService;
use App\Facades\CompanionService;
use App\Facades\EmailService;
use App\Facades\SessionService;
use App\Facades\EmailValidationService;
use App\Facades\SessionGroupService;

use App\Jobs\SendEmail;
use App\Jobs\VerifySingleMailValidity;
use App\Jobs\VerifyBatchMailsValidity;
use App\Jobs\UpdateBatchMailsVerificationStatus;
use App\Jobs\VerifyExtendedFieldsConsistency;

use Illuminate\Http\Request;
use App\Http\Requests\Guest\GuestRequest;
use App\Http\Requests\Guest\GuestImportRequest;

use App\Http\Requests\Guest\GuestSubscribeRequest;
use App\Http\Requests\Guest\GuestRegisterRequest;
use App\Http\Requests\Guest\GuestSubscribeNominativeRequest;
use App\Http\Requests\Guest\GuestRegisterNominativeRequest;

use App\Http\Requests\Guest\GuestsFilterRequest;
use App\Http\Requests\Guest\GuestsFilterPaginateRequest;

use App\Exceptions\GuestPartialUpdateException;

use App\Http\Controllers\Controller;

use App\Http\Resources\GuestListItemResource as GuestListItemResource;

class GuestsController extends Controller
{
  public function index($id)
  {
    SessionService::logSessionDebug("Api\GuestsController::index"); // TODO: remove, for debug purpose

    $event = Event::with('guests')->findOrFail($id);
    $guests = $event->guests()->with('sessions', 'companions', 'companions.sessions')->active()->orderBy('lastname', 'asc')->get()->toArray();
    $minisite = $event->minisite()->first();

    // Add totalGuestPlaces & totalGuestCheckin info (count places with companions)
    foreach($guests as &$guest) {
      $sessions = [];

      foreach($guest['sessions'] as $session) {
        $session['totalGuestPlaces'] = 1;
        if($session['pivot']['checkin']) {
          $session['totalGuestCheckin'] = 1;
        }
        else {
          $session['totalGuestCheckin'] = 0;
        }
        $sessions[$session['id']] = $session;
      }

      foreach($guest['companions'] as $companion) {
            
        foreach($companion['sessions'] as $compSession) {
          if(isset($sessions[$compSession['id']])) {
            $sessions[$compSession['id']]['totalGuestPlaces'] = $sessions[$compSession['id']]['totalGuestPlaces'] + 1;
            if($compSession['pivot']['checkin']) {
              $sessions[$compSession['id']]['totalGuestCheckin'] = $sessions[$compSession['id']]['totalGuestCheckin'] + 1;
            }
          }
          else {
            $compSession['totalGuestPlaces'] = 1;
            if($compSession['pivot']['checkin']) {
              $compSession['totalGuestCheckin'] = 1;
            }
            else {
              $compSession['totalGuestCheckin'] = 0;
            }
            $sessions[$compSession['id']] = $compSession;
          }
        }
      }

      $guest['sessions'] = array_values($sessions);

      // Add total checkin to event
      $guest['totalCheckin'] = $guest['checkin'] ? 1 : 0;
      foreach ($guest['companions'] as $companion) {
        $guest['totalCheckin'] = $companion['checkin'] ? $guest['totalCheckin'] + 1 : $guest['totalCheckin'];
      }

      // Adding the subscribe url to the guest content properties
      $guest['subscribeFormUrl'] = '#'; // The link will be # if no minisite and event advanced mode
      if($event->advanced_minisite_mode && !is_null($minisite) && !is_null($minisite->subscribeFormLink($guest['accessHash']))){
        $guest['subscribeFormUrl'] = $minisite->subscribeFormLink($guest['accessHash']);
      } else if(!$event->advanced_minisite_mode){
        $guest['subscribeFormUrl'] = route('events.subscribeForm.show', [$guest['accessHash']]);
      }

    }

    return response()->json($guests);
  }

  /*
  *
  * The filter guests method used to filter list on the guest list page.
  * Receives a post request containing filters to apply and then return the results.
  *
  * Filters are contained in $request->filters. 
  * Please see GuestIndex.vue component to review the origin of requests.
  * 
  */
  public function filter(int $eventId, GuestsFilterPaginateRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::filter"); // TODO: remove, for debug purpose

    $event = Event::where('id', $eventId)->with('sessions', 'minisite', 'surveyForm')->firstOrFail();

    $pagination = $request->has('pagination') ? $request->pagination : 25;
    $page = intval($request->page);

    // Retrieve guests that correspond to filters
    $guestResults = collect([]);

    if ($request->type === null || $request->type === 'guest') {
      $guestResults = GuestService::search($event, $request);
    }

    // Retrieve companions that correspond to filters
    $companionResults = collect([]);

    if ($request->type === null || $request->type === 'companion') {
      $companionResults = CompanionService::search($event, $request, null);
    }

    // Merge and sort companions and guests
    $mixed = $guestResults->concat($companionResults)->sort(function($a, $b) {

      $aIsGuest = $a->guest_id === null;
      $bIsGuest = $b->guest_id === null;

      $aLastname = $aIsGuest ? $a->lastname : $a->guest->lastname;
      $bLastname = $bIsGuest ? $b->lastname : $b->guest->lastname;

      $aGuestId = $aIsGuest ? $a->id : $a->guest_id;
      $bGuestId = $bIsGuest ? $b->id : $b->guest_id;

      // Sort first by guestLastname then by guestId then by type then by companionLastname
      if ($aLastname === $bLastname) {

        if ($aGuestId === $bGuestId) { 

          if ($aIsGuest === $bIsGuest) {
            return strnatcmp($a->lastname, $b->lastname); // then by companionLastname
          }
          else {
            return $aIsGuest ? -1 : 1;
          }
          
        }
        else {
          return $aGuestId - $bGuestId; // then by guestId
        }
      }
      else {
        return strnatcmp($aLastname, $bLastname);  // Sort first by guestLastname
      }

    });

    // Cannot use laravel auto pagination with two different models
    $from = ($page - 1) * $pagination;
    $to = ($page - 1) * $pagination + $pagination;
    $total = $mixed->count();
    $lastPage = ceil($total / $pagination);
    return response()->json([
      'data' => GuestListItemResource::collection($mixed->splice($from, $to))
                  ->listEvent($event)
                  ->listMinisite($event->minisite),
      'meta' => [
        'total' => $total,
        'per_page' => $pagination,
        'last_page' => $lastPage,
        'from' => $from + 1,
        'to' => $to + 1 <= $total ? $to + 1 : $total,
        'current_page' => $page <= $lastPage ? $page : $lastPage
      ]
    ]);
  }

  /**
   * Apply filters on guests and companions and return filtered ids in separate lists
   */
  public function filterAll(int $eventId, GuestsFilterRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::filterAll"); // TODO: remove, for debug purpose

    $event = Event::with('guests', 'guests.companions')->findOrFail($eventId);

    $filteredGuests = [];
    if ($request->type !== 'companion') {
      $filteredGuests = GuestService::search($event, $request)->pluck('id');
    }
   
    $filteredCompanions = [];
    if ($request->type !== 'guest') {
      $filteredCompanions = CompanionService::search($event, $request, null)->pluck('id');
    }

    return response()->json([
      'guests' => $filteredGuests,
      'companions' => $filteredCompanions
    ]);
  }

  /**
   * Destroy all guests and companions
   */
  public function destroyAll($id)
  {
    $event = Event::with('guests', 'guests.companions')->findOrFail($id);
    $this->authorize('destroy', Guest::class);

    $guests = $event->guests()->update(['active' => false]);
    $event->companions()->delete();

    SessionService::logSessionDebug("Api\GuestsController::destroyAll"); // TODO: remove, for debug purpose

    return response()->json(true);
  }

  public function destroy($event_id, $id)
  {
    $guest = Guest::findOrFail($id);
    $guest->active = false;
    $guest->save();

    SessionService::logSessionDebug("Api\GuestsController::destroy"); // TODO: remove, for debug purpose

    return response()->json($guest);
  }

  public function update(int $eventId, int $id, GuestRequest $request)
  {
    $requestData = $request->all();

    $guest = Guest::findOrFail($id);
    $guest->checkActive();

    $guest->firstname = $requestData["firstname"];
    $guest->lastname = $requestData["lastname"];
    $guest->email = $requestData["email"];
    $guest->extendedFields = $requestData["extended_fields"];

    // To avoid update subsribed_at when updating other fields
    if ($requestData["declined"] !== null) {
      $guest->declined = $requestData["declined"]; // if true, will remove companions and sessions
    }
    if ($requestData["subscribed"] !== null) {
      $guest->subscribed = $requestData["subscribed"];
    }
    if ($requestData["invited"] !== null) {
      $guest->invited = $requestData["invited"];
    }
    if ($requestData["checkin"] !== null) {
      $guest->checkin = $requestData["checkin"];
    }

    $guest->save();

    return response()->json($guest);
  }

  public function anonymise($event_id, Request $request)
  {
    $user = Auth::user();
    $password = $request->password;

    if (Hash::check($password, $user->password)) {
      $event = Event::with('guests')->findOrFail($event_id);
      $guests = $event->guests()->get();

      GuestService::anonymise($guests);

      return response()->json("Guests anonymised.", 200);
    }
    else {
      return response()->json("Incorrect password !", 401);
    }
  }

  public function attributes($event_id)
  {
    $event = Event::findOrFail($event_id);
    $attributes = Guest::getMinAttributes();

    if($event->extendedFileds){
      foreach($event->extendedFileds as $field){
        $attributes[] = $field;
      }
    }

    return response()->json($attributes);
  }

  public function minAttributes($event_id)
  {
    $event = Event::findOrFail($event_id);
    return response()->json(Guest::getMinAttributes());
  }

  public function import($event_id, GuestImportRequest $request)
  {
    ini_set('max_execution_time', 60); // we need more time !
    $this->authorize('create', Guest::class);

    $event = Event::findOrFail($event_id);

    if (EmailValidationService::isOneRunning($event)) {
      return response()->json("Importation locked", 422);
    }

    // Create guests mapped array
    $guests = GuestService::mapRawGuests($event, $request['guests'], $request['mapping'], $request['ignored']);
    $guests = GuestService::trimEmails($guests);

    // Validate raw guests
    $validator = Validator::make($guests, [
      '*.email' => function($attribute, $value, $fail) {
          // Customize error message for: 'required|string|email|max:255'
          if (empty($value)) {
            return $fail($attribute.'_empty');
          }
          if (strlen($value) > 255) {
            return $fail($attribute.'_length');
          }
          if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return $fail($attribute.'_email');
          }
      },
      '*.language' => function($attribute, $value, $fail) {
          if(!empty($value) && strlen($value) == 2 && !preg_match('/^[a-z]{2}$/', $value)){
            return $fail($attribute.'_badFormat');
          }
      }
    ])->validate();

    $guests = GuestService::cleanLanguages($guests);

    // Insert
    Guest::insert($guests);

    // Building base jobVerificationFeedback object
    // Create even if no guest needs to be validated, to ensure front-end will ask guestlist updated
    $verificationFeedback = new JobMailVerificationFeedback();
    $verificationFeedback->event_id = $event_id;
    $verificationFeedback->save();

    // Trigger job to validate list of emails, job is created inside it
    VerifyBatchMailsValidity::dispatch($guests, $event_id, $verificationFeedback);

    // Update Event
    if($guests[0]['extendedFields'] != null) {
      $extFields = json_decode($guests[0]['extendedFields'], true);
      $event->addExtendedFileds(array_keys($extFields));
      $event->save();
    }

    // Test if list has now dupliates
    // TODO: This code is not DRY! It is present in the GuestsController. Should do a new API point to test duplicates. And this could take time!
    foreach ($event->guests()->get() as $row) {
      $dup = Guest::where(['email' => $row->email, 'active' => 1, 'event_id' => $event->id])->get();
      if(count($dup) > 1){
        return response()->json("Duplicates", 201); // Duplicate row and original guest object
      }
    }

    // Temporary: Check data consistency after import
    VerifyExtendedFieldsConsistency::dispatch(Auth::user(), $event, "Check after guest import (Api\GuestsController->import)");

    return response()->json("Import successfully done without duplicates.", 200);
  }

  /**
   * Register new guest with nominative companions
   * only for public minisite
   */
  public function registerNominative($eventId, GuestRegisterNominativeRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::registerNominative"); // TODO: remove, for debug purpose

    $companions = $request->companionsData == null ? 0 : count($request->companionsData);
    $event = Event::with('company', 'sessionGroups', 'sessionGroups.sessions', 'sessions')->findOrFail($eventId);
    $company = $event->company()->firstOrFail();

    $guest = $event->guests()->with('sessions')->active()->where('email', $request->email)->first();

    // Check session groups consitency
    $guestSessionsIds = collect($request->sessions);
    $sessionIdsByCompanion = collect($request->companionsData)->pluck('sessions');

    $sessions = $event->sessions; // optimization

    // isn't need because they can subscribe in more than one sessions for group
    // if (!SessionGroupService::checkSessionGroupRegisteringUniq($guestSessionsIds, $sessionIdsByCompanion, $event)) {
    //   return $this->guestSessionGroupNotUniqResponse($guest); // 403
    // }

    // Check if place remain in global event
    $fullEventResponse = $this->guestSubscribeEventFullResponse($event, $company, $guest, $companions, $request->email);
    if ($fullEventResponse != null) {
      return $fullEventResponse; // 403
    }

    // Check that he can register to all wanted sessions
    $fullSessions = SessionService::checkGuestAndNominativeCompanionsCanRegister($request, $guest);
    if ($fullSessions->count() > 0) {
      return $this->guestSessionsFullResponse($guest, $fullSessions); // 403
    }

    if ($guest != null) { // Guest exists, update if not subscribed

      // Check that guest isn't allready subscribed
      if ($guest->subscribed) {
        return $this->guestAllreadySubscribedResponse($guest); // 403
      }

      // From here all validation passed

      // Update guest in DB
      $guest->subscribed = true;
      $res = GuestService::updateGuestAndCompanions($request->all(), $event, $guest); // create companions here
      $guestCompanionsOrdered = $res['companions'];
      
      // Send session full notification message
      $this->sendFullSessionEmailForNominative(collect($request->companionsData), $sessions,
        $guestSessionsIds, $guest, $event, $company);

      // Register / Unregister to session
      SessionService::attachGuest($guest, $sessions->whereIn('id', $guestSessionsIds));

      foreach($guestCompanionsOrdered as $index => $companion) {
        SessionService::attachCompanion($companion,
          $sessions->whereIn('id', $sessionIdsByCompanion[$index]));
      }

      // Send subscribe confirmation email
      EmailService::sendSubscribeConfirmEmail($event, $guest);

      SessionService::logSessionDebug("Api\GuestsController::registerNominative[before return]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_saved')
      ]);

    }
    else { // new guest
      // from here all validation passed

      // Create guest and companions
      $guest = GuestService::registerGuest($request->all(), $event);
      $companions = GuestService::registerGuestCompanions($request->all(), $guest, $event);
      
      // Send session full notification message
      $this->sendFullSessionEmailForNominative(collect($request->companionsData), $sessions,
        $guestSessionsIds, $guest, $event, $company);

      // Register / Unregister to session
      SessionService::attachGuest($guest, $sessions->whereIn('id', $guestSessionsIds));
      
      foreach($companions as $index => $companion) {
        SessionService::attachCompanion($companion,
          $sessions->whereIn('id', $sessionIdsByCompanion[$index]));
      }

      // Send subscribe confirmation email
      EmailService::sendSubscribeConfirmEmail($event, $guest);

      SessionService::logSessionDebug("Api\GuestsController::registerNominative[before return2]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_created')
      ]);
    }
  }

  /**
   * Register new guest
   * only for public minisite
   */
  public function register($eventId, GuestRegisterRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::register"); // TODO: remove, for debug purpose

    $companions = $request->companions == null ? 0 : $request->companions;
    $event = Event::with('company')->findOrFail($eventId);
    $company = $event->company()->first();

    $sessionsPlaces = [];
    $sessions = collect([]);
    foreach($request->sessions as $sessionId => $places) {
      $session = Session::findOrFail($sessionId);
      $sessionsPlaces[] = [$session, $places];
      $sessions->push($session);
    }

    $orgGuest = $event->guests()->with('sessions')->active()->where('email', $request->email)->first();

    // Check if place remain in global event
    $fullEventResponse = $this->guestSubscribeEventFullResponse($event, $company, $orgGuest, $companions, $request->email);
    if ($fullEventResponse !== null) {
      return $fullEventResponse; // 403
    }

    // Check that he can register to all wanted sessions
    $fullSessions = SessionService::checkGuestCanRegisterToAll($orgGuest, $sessionsPlaces);
    if ($fullSessions->count() > 0) {
      return $this->guestSessionsFullResponse($orgGuest, $fullSessions); // 403
    }

    if ($orgGuest != null) { // Guest exists, update if not subscribed

      // Check that guest isn't allready subscribed
      if ($orgGuest->subscribed) {
        return $this->guestAllreadySubscribedResponse($orgGuest); // 403
      }

      // Update guest in DB
      $orgGuest->subscribed = true; // set subscribed
      GuestService::updateGuestAndCompanions($request->all(), $event, $orgGuest); // create companions here

      // Register to sessions
      $this->registerToSessionNotNominative($sessionsPlaces, $event, $company, $orgGuest);

      // Send subscribe confirmation email
      EmailService::sendSubscribeConfirmEmail($orgGuest->event()->first(), $orgGuest);

      // Temporary: Check data consistency after import
      VerifyExtendedFieldsConsistency::dispatch(null, $event, "Check after guest register, guest exists (Api\GuestsController->register)");

      SessionService::logSessionDebug("Api\GuestsController::register[before return]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $orgGuest,
        'message' => Lang::get('models.event.guest_registration_saved')
      ]);
    }
    else {
      $guest = GuestService::registerGuest($request->all(), $event);
      GuestService::registerGuestCompanions($request->all(), $guest, $event);

      // Register to sessions
      $this->registerToSessionNotNominative($sessionsPlaces, $event, $company, $guest);

      // Send confirmation mail
      EmailService::sendSubscribeConfirmEmail($event, $guest);

      // Deferred job to test validity of created guest's email
      VerifySingleMailValidity::dispatch($guest);

      // Temporary: Check data consistency after import
      VerifyExtendedFieldsConsistency::dispatch(null, $event, "Check after guest register, guest doesn't exists (Api\GuestsController->register)");

      SessionService::logSessionDebug("Api\GuestsController::register[before return2]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_created')
      ]);
    }
  }

  /**
   * Subscribe existing guest with nominative companions
   */
  public function subscribeNominative($eventId, $id, GuestSubscribeNominativeRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::subscribeNominative"); // TODO: remove, for debug purpose

    $companions = $request->companionsData == null ? 0 : count($request->companionsData);
    $event = Event::with('company', 'sessionGroups', 'sessionGroups.sessions', 'sessions')->findOrFail($eventId);
    $guest = $event->guests()->with('sessions')->active()->findOrFail($id);
    $company = $event->company()->first();

    // Check session groups consitency
    $guestSessionsIds = collect($request->sessions);
    $sessionIdsByCompanion = collect($request->companionsData)->pluck('sessions');

    $sessions = $event->sessions; // optimization

    // Event
    $fullEventResponse = $this->guestSubscribeEventFullResponse($event, $company, $guest, $companions, $guest->email);
    if ($fullEventResponse != null) {
      return $fullEventResponse; // 403
    }

    if (!$event->allow_subscription_update && $guest->subscribed) {
      return $this->subscriptionUpdateNotAllowedResponse();
    }

    // isn't need because they can subscribe in more than one sessions for group
    // if (!SessionGroupService::checkSessionGroupRegisteringUniq($guestSessionsIds, $sessionIdsByCompanion, $event)) {
    //   return $this->guestSessionGroupNotUniqResponse($guest); // 403
    // }

    // Check that he can register to all wanted sessions
    $fullSessions = SessionService::checkGuestAndNominativeCompanionsCanRegister($request, $guest);
    if ($fullSessions->count() > 0) {
      return $this->guestSessionsFullResponse($guest, $fullSessions); // 403
    }

    // From here all validation passed
    $wasSubscribed = $guest->subscribed;

    // Update guest in DB
    $guest->subscribed = true; // set subscribed
    $res = GuestService::updateGuestAndCompanions($request->all(), $event, $guest); // create companions here
    $guestCompanionsOrdered = $res['companions'];

    // Send session full notification message
    $this->sendFullSessionEmailForNominative(collect($request->companionsData), $sessions,
      $guestSessionsIds, $guest, $event, $company);

    // Register / Unregister to session
    SessionService::attachGuest($guest, $sessions->whereIn('id', $guestSessionsIds));

    foreach($guestCompanionsOrdered as $index => $companion) {
      SessionService::attachCompanion($companion,
        $sessions->whereIn('id', $sessionIdsByCompanion[$index]));
    }

    if ($wasSubscribed) {
      // Testing if we need to resend confirmation email (nominative companions changes)
      $resendConfirmationMail = array_key_exists('companionsData', $request->all()) && $event->nominative_companions;

      // If event is nominative: send email
      if($resendConfirmationMail){
        EmailService::sendSubscribeConfirmEmail($event, $guest);
      }

      SessionService::logSessionDebug("Api\GuestsController::subscribeNominative[before return]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_updated')
      ]);
    }
    else {
      // Send confirmation mail
      EmailService::sendSubscribeConfirmEmail($event, $guest);

      SessionService::logSessionDebug("Api\GuestsController::subscribeNominative[before return2]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_saved')
      ]);
    }
  }

  public function subscribe($event_id, $id, GuestSubscribeRequest $request)
  {
    SessionService::logSessionDebug("Api\GuestsController::subscribe"); // TODO: remove, for debug purpose

    $companions = $request->companions == null ? 0 : $request->companions;
    $event = Event::with('guests', 'company')->findOrFail($event_id);
    $guest = $event->guests()->with('sessions')->active()->findOrFail($id);
    $company = $event->company()->first();

    // Sessions
    $sessionsPlaces = [];
    foreach($request->sessions as $sessionId => $places) {
      $session = Session::findOrFail($sessionId);
      $sessionsPlaces[] = [$session, $places];
    }

    // Event
    if (!$event->allow_subscription_update && $guest->subscribed) {
      return $this->subscriptionUpdateNotAllowedResponse();
    }

    $fullEventResponse = $this->guestSubscribeEventFullResponse($event, $company, $guest, $companions, $guest->email);
    if ($fullEventResponse != null) {
      return $fullEventResponse; // 403
    }

    $fullSessions = SessionService::checkGuestCanRegisterToAll($guest, $sessionsPlaces);
    if ($fullSessions->count() > 0) {
      return $this->guestSessionsFullResponse($guest, $fullSessions); // 403
    }

    // From here all validation passed
    $wasSubscribed = $guest->subscribed;

    // Update guest in DB
    $guest->subscribed = true; // set subscribed
    GuestService::updateGuestAndCompanions($request->all(), $event, $guest); // create companions here

    // Register to sessions
    $this->registerToSessionNotNominative($sessionsPlaces, $event, $company, $guest);

    // Response

    // Temporary: Check data consistency
    VerifyExtendedFieldsConsistency::dispatch(null, $event, "Check after guest subscribe (Api\GuestsController->subscribe)");

    if ($wasSubscribed) {
      // Testing if we need to resend confirmation email (nominative companions changes)
      $resendConfirmationMail = array_key_exists('companionsData', $request->all()) && $event->nominative_companions;
      if ($resendConfirmationMail) {
          EmailService::sendSubscribeConfirmEmail($event, $guest);
      }

      SessionService::logSessionDebug("Api\GuestsController::subscribe[before return]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_updated')
      ]);
    }
    else {
      // Send confirmation mail
      EmailService::sendSubscribeConfirmEmail($event, $guest);

      SessionService::logSessionDebug("Api\GuestsController::subscribe[before return2]"); // TODO: remove, for debug purpose
      return response()->json([
        'guest' => $guest,
        'message' => Lang::get('models.event.guest_registration_saved')
      ]);
    }
  }

  /**
   * Register or unregister to sessions.
   * NOT for nominative companions
   * @param $sessionPlaces : array of session and wanted places
   */
  private function registerToSessionNotNominative(array $sessionsPlaces, Event $event, Company $company, Guest $guest)
  {
    foreach($sessionsPlaces as $sessionPlace) {
      $s = $sessionPlace[0]; // session
      $p = $sessionPlace[1]; // places
      if($p <= 0) {
        SessionService::unRegisterGuestAndCompanions($guest, $s);
      }
      else {
        if (SessionService::isSessionFullAfterSubscribe($guest, $s, $p)) {
          EmailService::sendSessionFullNotification($s, $event, $company, $guest);
        }
        SessionService::registerGuestAndCompanions($guest, $s, $p);
      }
    }
  }

  /**
   * Check and send email to organisator if session is full after subscribe/register
   * Work only for companions nominative functions
   */
  private function sendFullSessionEmailForNominative(Collection $companionsData, Collection $sessions,
      Collection $guestWantedSessionIds, Guest $guest, Event $event, Company $company)
  {
    $sessionIdsByCompanion = $companionsData->pluck('sessions');

    foreach($sessions as $session) {
      $places = $guestWantedSessionIds->first(function ($value, $key) use ($session) {
        return $session->id === $value;
      }) == null ? 0 : 1;

      foreach($sessionIdsByCompanion as $index => $sessions) {
        if (isset($sessions) && in_array($session->id, $sessions)) {
          $places = $places + 1;
        }
      }

      $previousSessionCount = $guest->countSessionPlaces($session);
      if($previousSessionCount !== $places)
      {
        if($places !== 0) {
          if (SessionService::isSessionFullAfterSubscribe($guest, $session, $places)) {      
            EmailService::sendSessionFullNotification($session, $event, $company, $guest);
          }
        }
      }
    }
  }

  public function solveDuplicate($event_id, $guest_id, Request $request)
  {
    $event = Event::where('id', $event_id)->first();
    $guest = Guest::where('id', $guest_id)->first();
    $event->guests()->whereNotIn('id', [$guest->id])->where(['email' => $guest->email, 'active' => 1])->update(['active' => 0]);
    return response()->json("OK", 200);
  }

  /**
   * Check if an email verification job exists
   * if true, then try to get results and return JobMailVerificationFeedback
   * if false, return null
   */
  public function checkEmailVerificationJobs($event_id)
  {
    $jobMailVerificationFeedback = JobMailVerificationFeedback::where([
      'event_id' => $event_id,
      'processed' => false,
      'finished' => false
    ])->first();

    if ($jobMailVerificationFeedback && $jobMailVerificationFeedback->neverbounce_job_id != null &&
        !$jobMailVerificationFeedback->processed) {
      // Check and update Neverbounce based validation
      UpdateBatchMailsVerificationStatus::dispatch($jobMailVerificationFeedback->neverbounce_job_id);
    }
    else if ($jobMailVerificationFeedback && $jobMailVerificationFeedback->nb_addresses === 0) {
      // Update databse based validation
      $jobMailVerificationFeedback->processed = true;
      $jobMailVerificationFeedback->finished = true;
      $jobMailVerificationFeedback->save();
    }

    if (!$jobMailVerificationFeedback) {
      // to keep the front-end in touch
      $jobMailVerificationFeedback = JobMailVerificationFeedback::where([
        'event_id' => $event_id,
        'finished' => false
      ])->first();
    }

    return response()->json($jobMailVerificationFeedback);
  }

  public function unCheckin(int $event_id, int $id)
  {
    $guest = Guest::active()->findOrFail($id);
    $guest->checkin = false;
    $guest->save();
    return response()->json($guest);
  }

  public function unCheckinSession(int $event_id, int $session_id, int $id)
  {
    $guest = Guest::active()->findOrFail($id);
    $session = Session::findOrFail($session_id);
    SessionService::checkinGuest($guest, $session, false);
    return response()->json($guest);
  }

  private function checkEventCanSubscribeGuest(Event $event, string $guestEmail, Company $company, int $companions = 0, int $allreadyReservedPlaces = 0)
  {
    if ($allreadyReservedPlaces > $companions + 1) {
      return false;
    }
    if (isset($event->nb_places)) {
      $nbSubscribed = $event->countGuestsSubscribedWithCompanions() - $allreadyReservedPlaces;
      // is the event full
      if ($nbSubscribed + 1 + $companions > $event->nb_places) {
        // Return error if event is full on subscribtions
        return true;
      }

      // is it the last place
      if ($nbSubscribed + 1 + $companions >= $event->nb_places) {
        EmailService::sendEventFullNotification($event, $guestEmail, $company);
      }
    }
    return false;
  }

  /**
   * Return response object if guest cannot subscribe, return null instead
   */
  private function guestSubscribeEventFullResponse(Event $event, Company $company, ?Guest $guest, int $wantedCompanions, string $guestEmail)
  {
    $nComp = $guest == null ? 0 : $guest->nbCompanions;
    $subscribed = $guest == null ? false : $guest->subscribed;
    $subscribedPlaces = ($nComp == null ? 0 : $nComp) + ($subscribed ? 1 : 0);
    if ($this->checkEventCanSubscribeGuest($event, $guestEmail, $company, $wantedCompanions, $subscribedPlaces)) {
      return response()->json([
        'message' => Lang::get('models.event.guests_limit_reached')
      ], 403);
    }
  }

  /**
   * Return response object describing full sessions
   */
  private function guestSessionsFullResponse(?Guest $guest, Collection $fullSessions)
  {
    return response()->json([
      'guest' => $guest,
      'message' => Lang::get('models.session.guest_no_more_place_in_sessions', [
        'sessions' => $fullSessions->implode(Session::localAttributeField('name'), ', ')
      ])
    ], 403);
  }

  private function guestAllreadySubscribedResponse(Guest $guest)
  {
    return response()->json([
      'guest' => $guest,
      'message' => Lang::get('models.event.guest_registration_allready_done')
    ], 403);
  }

  private function guestSessionGroupNotUniqResponse(Guest $guest)
  {
    return response()->json([
      'guest' => $guest,
      'message' => Lang::get('models.sessionGroups.consistency_not_respected')
    ], 403);
  }

  /**
   * Return 403 error with JSON error message
   */
  private function subscriptionUpdateNotAllowedResponse()
  {
    return response()->json([
      'message' => Lang::get('models.event.subscription_update_not_allowed')
    ], 403);
  }

}
