<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Config;
use App\Jobs\SendEmail;
use Illuminate\Support\Facades\Auth;

use DB;

use App\Guest;
use App\Companion;
use App\Event;
use App\Email;
use App\JobFeedback;
use App\Facades\JobFeedbackService;

use App\Http\Requests\Email\EmailRequest;
use App\Http\Requests\Email\EmailSendRequest;

use Illuminate\Http\Request;
use Session;
use Log;
use Lang;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use DbView;

class EmailsController extends Controller
{
  public function index($id)
  {
    $event = Event::with('emails')->findOrFail($id);
    $mails = $event->emails()->get();
    return response()->json($mails);
  }

  public function store($id, string $lang, EmailRequest $request)
  {

    // Restricting multilingual update to confirmation emails
    if($lang != 'fr'){
      return response()->json([
        'title' => [Lang::get('validation.cannot_save')]
      ], 422);
    }

    // The event concerned by this mail
    $event = Event::findOrFail($id);

    // New mail from this company with the data as content and mail title
    $data = $request->all();

    // No choice to that field by field : Laravel doesn't recognise the incoming format for boolean fields
    $mail = new Email();
    $mail->type = array_key_exists("type", $data) ? $data["type"] : null;
    $mail->joinICS = $data["joinICS"];
    $mail->joinTicket = $data["joinTicket"];
    $mail->sendWithDefaultMail = $data["sendWithDefaultMail"];

    if(isset($data['from_column'])){
      $mail->from_column = $data["from_column"];
    }

    // Storing only in french, multilangual will be supported later
    $mail->setTranslatedAttribute('title', 'fr', $data["title"]);
    $mail->setTranslatedAttribute('template', 'fr', $data["template"]);
    $mail->setTranslatedAttribute('html', 'fr', $data["html"]);
    $mail->sessions_style = $data['sessionStyle'];

    $event->emails()->save($mail);

    return response()->json($mail); //confirm with return (200)
  }

  public function update($event_id, $id, string $lang, EmailRequest $request)
  {

    $email = Email::findOrFail($id);

    // Restricting multilingual update to confirmation emails
    if(!$email->isConfirm && $lang != 'fr'){
      return response()->json([
        'title' => [Lang::get('validation.cannot_save')]
      ], 422);
    }

    $data = $request->all();
    $email->type = array_key_exists("type", $data) ? $data["type"] : null;
    $email->joinICS = $data["joinICS"];
    $email->joinTicket = $data["joinTicket"];
    $email->sendWithDefaultMail = $data["sendWithDefaultMail"];

    $email->from_column = isset($data['from_column']) ? $data["from_column"] : null;

    $email['template_' . $lang] = $request->template;
    $email['html_' . $lang] = $request->html;
    $email['title_' . $lang] = $request->title;
    $email->sessions_style = $data['sessionStyle'];

    $email->save();

    return response()->json($data); //confirm with return (200)
  }

  public function send($event_id, $email_id, EmailSendRequest $request)
  {
    Log::info("[EmailsController] [Send] Ask sending for email: ".$email_id);
    Log::info("[EmailsController] [Send] Retrieve Informations");
    $user = Auth::user();
    $email = Email::with('event', 'event.company')->findOrFail($email_id);
    $event = $email->event;
    $company = $event->company;

    // Check if a job is allready running
    if (JobFeedbackService::isOneRunning($email)) {
      return response()->json("Running Job", 429);
    }

    $data = $request->all();
    $guests = Guest::whereIn('id', $data["guests"])->get();
    $companions = Companion::whereIn('id', $data["companions"])->with('guest')->get();
    $mailMode = $data["mailMode"];
    $joinICS = $data["joinICS"];

    if (isset($data["fromColumn"])) {
      $fromColumn = $data["fromColumn"];
    }
    else {
      $fromColumn = "null";
    }

    Log::info("[EmailsController] [Send] Test password validity");
    // Test password validity
    if (Hash::check($data["password"], $user->password)) {

      Log::info("[EmailsController] [Send] Create JobFeedback");
      // Creating the jobfeedback object to have information
      $jobFeedback = new JobFeedback();
      $jobFeedback->event_id = $event->id;
      $jobFeedback->mail_id = $email->id;
      $jobFeedback->user_id = $user->id;
      $jobFeedback->nb_guests = $guests->count() + $companions->count();
      if ($fromColumn != "null") {
        $jobFeedback->from_column = $fromColumn;
        $jobFeedback->save();
        Log::info("[EmailsController] [Send] Dispatch Job with from_column");
        SendEmail::dispatch($guests, $companions, $email, $joinICS, $mailMode, $jobFeedback, $fromColumn);
      }
      else {
        $jobFeedback->save();
        Log::info("[EmailsController] [Send] Dispatch Job without from_column");
        SendEmail::dispatch($guests, $companions, $email, $joinICS, $mailMode, $jobFeedback);
      }

      Log::info("[EmailsController] [Send] Respond 200 to front-end");
      return response()->json("Mails successfully sent", 200);

    }
    else {
      return response()->json("Wrong password", 401); // Incorrect password
    }
  }

  public function resendInvalid($event_id, $email_id, Request $request){

    $job = JobFeedback::with('shipment')->findOrFail($request['jobFeedbackId']);

    if($job->shipment){
      $newJobFeedback = JobFeedback::setToResend($job);
      
      [$guests, $companions] = $job->shipment->failedGuestAndCompanions();

      $newJobFeedback->nb_guests = $guests->count() + $companions->count();
  
      $email = Email::with('event', 'event.company')->findOrFail($email_id);

      $data = $request->all();
  
      $newJobFeedback->save();
      $job->resended = $newJobFeedback->id;
      $job->timestamps = false;
      $job->save();

      SendEmail::dispatch($guests, $companions, $email, $data["joinICS"], $data["mailMode"], $newJobFeedback);
    }

    return response()->json("Mails successfully sent", 200);
  }
}