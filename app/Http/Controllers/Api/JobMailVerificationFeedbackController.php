<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\JobMailVerificationFeedback;

use Illuminate\Http\Request;

class jobMailVerificationFeedbackController extends Controller
{
  public function index($event_id)
  {
    $jobMailVerificationFeedback = jobMailVerificationFeedback::where('event_id')->findOrFail($event_id);
    return response()->json($jobMailVerificationFeedback);
  }
}
