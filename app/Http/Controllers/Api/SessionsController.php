<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Guest;
use App\Event;
use App\Session;
use App\Companion;

use App\Facades\SessionService;

class SessionsController extends Controller
{
  public function index(int $eventId)
  {
    $sessions = Session::with('sessionGroup')->where('event_id', $eventId)->get()->toArray();
    return response()->json(array_map(function ($session) {
      unset($session['guests']);
      unset($session['companions']);
      return $session;
    }, $sessions));
  }

  public function indexOfGuest(int $guestId)
  {
    $guest = Guest::with('sessions', 'companions', 'companions.sessions')->findOrFail($guestId);

    // Add totalGuestPlaces info (count places with companions)
    $sessions = SessionService::buildSessionListWithTotalGuestPlaces($guest);

    $sessions = array_map(function ($session) {
      unset($session['guests']);
      unset($session['companions']);
      return $session;
    }, $sessions);

    return response()->json(array_values($sessions));
  }

  public function checkin(int $eventId, int $sessionId, string $hash)
  {
    $event = Event::with('guests', 'companions', 'sessions')->findOrFail($eventId);
    $session = $event->sessions()->findOrFail($sessionId);
    $guest = $event->guests()->with('sessions')->where('accessHash', $hash)->first();

    if ($guest === NULL) {
      $companion = $event->companions()->with('sessions', 'guest')->where('access_hash', $hash)->firstOrFail(); // 404 if not found
      // Companion
      if ($companion->isCheckinToSession($session)) {
        return response()->json([
          'type' => 'companion',
          'data' => $companion
        ], 409);
      }

      $checkinSuccess = SessionService::checkinCompanion($companion, $session, !$event->nominative_companions);
      if (!$checkinSuccess) { // no more places, ticket unvalid
        return response()->json([
          'type' => 'companion',
          'data' => $companion
        ], 404);
      }

      // Companion subscription to sessions may have changed (place stolen), we need to reask database
      return response()->json([
        'type' => 'companion',
        'data' => Companion::with('sessions', 'guest')->findOrFail($companion->id)
      ]);
    }
    else {
      // Guest
      if (!$guest->isSubscribedToSession($session)) {
        return response()->json([
          'type' => 'guest',
          'data' => $guest
        ], 404);
      }

      if ($guest->isCheckinToSession($session)) {
        return response()->json([
          'type' => 'guest',
          'data' => $guest
        ], 409);
      }

      SessionService::checkinGuest($guest, $session);

      return response()->json([
        'type' => 'guest',
        'data' => $guest
      ]);
    }
  }
}
