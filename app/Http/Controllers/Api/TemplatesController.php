<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Template;
use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests\TemplateUpdateRequest;
use App\Http\Requests\TemplateCreateRequest;

use Session;
use Log;

class TemplatesController extends Controller
{
    public function index($companyId)
    {
        $company = Company::findOrFail($companyId);
        $templates = $company->templates()->get();
        return response()->json($templates);
    }

    public function store(TemplateCreateRequest $request, $company_id)
    {
        $company = Company::with('templates')->findOrFail($company_id);

        $template = new Template();
        $template->name = $request->name;
        $template->template = $request->template;
        $template->template_type = $request->template_type;
        $template->sessions_styles = $request->sessions_styles;
        
        $company->templates()->save($template);

        return response()->json($template);
    }

    public function update(TemplateUpdateRequest $request, $company_id, $template_id)
    {
        $template = Template::findOrFail($template_id);

        $template->template = $request->template;
        $template->sessions_styles = $request->sessions_styles;
        $template->save();

        return response()->json("Template updated successfully.", 200);
    }

    public function show($company_id, $template_id)
    {
        $template = Template::findOrFail($template_id);
        return response()->json($template);
    }

    public function destroy($company_id, $template_id)
    {
      $template = Template::findOrFail($template_id);
      $template->delete();
      return response()->json("Template successfully deleted.", 200);
    }
}
