<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Guest;
use App\Companion;
use App\Event;

use App\Facades\EventService;

use App\Http\Requests\Event\EventRemoveExtFieldRequest;

use Illuminate\Http\Request;

class EventsController extends Controller
{
  public function index()
  {
    $user = Auth::user();
    if ($user->isAdmin()) {
      return response()->json(Event::active()->actualAndFutur()->orderBy('start_date', 'ASC')->get());
    }

    $company = $user->company()->with('events')->firstOrFail();
    return response()->json($company->events()->active()->actualAndFutur()->orderBy('start_date', 'ASC')->get());
  }

  public function stats($id)
  {
    $event = Event::with('guests', 'guests.companions')->findOrFail($id);

    $stats = [];

    $invited = $event->guests->where('invited', true)->where('active', true);
    $stats['invited'] = $invited->count();
    $stats['invitedCompanions'] = Companion::whereIn('guest_id', $invited->pluck('id'))->count();
  
    $imported = $event->guests->where('active', true);
    $stats['imported'] = $imported->count();
    $stats['companions'] = Companion::whereIn('guest_id', $imported->pluck('id'))->count();

    $subscribed = $event->guests->where('subscribed', true)->where('declined', false)->where('active', true);
    $stats['subscribed'] = $subscribed->count();
    $stats['subscribedCompanions'] = Companion::whereIn('guest_id', $subscribed->pluck('id'))->count();

    $declined = $event->guests->where('declined', true)->where('active', true);
    $stats['declined'] = $declined->count();
    $stats['declinedCompanions'] = Companion::whereIn('guest_id', $declined->pluck('id'))->count();

    $checkin = $event->guests->where('checkin', '>', 0)->where('active', true);
    $stats['checkin'] = $checkin->count();
    $stats['checkinCompanions'] = Companion::whereIn('guest_id', $checkin->pluck('id'))->where('checkin', true)->count();

    return response()->json($stats);
  }

  public function show($id)
  {
    return response()->json(Event::findOrFail($id));
  }

  public function extendedFields($id)
  {
    return response()->json(Event::findOrFail($id)->extendedFileds);
  }

  /**
   * Return 409 if guest or companion allready checkin
   * 404 if not found
   */
  public function checkin(int $id, string $hash)
  {
    $event = Event::with('guests', 'companions')->findOrFail($id);
    $guest = $event->guests()->with('sessions')->where('accessHash', $hash)->first();

    if ($guest === NULL) {
      $companion = $event->companions()->with('sessions', 'guest')->where('access_hash', $hash)->firstOrFail(); // 404 if not found
      // Companion

      if ($companion->checkin) {
        return response()->json([
          'type' => 'companion',
          'data' => $companion
        ], 409);
      }

      $companion->checkin = true;
      $companion->save();

      return response()->json([
        'type' => 'companion',
        'data' => $companion
      ]);
    }
    else {
      // Guest

      if ($guest->checkin) {
        return response()->json([
          'type' => 'guest',
          'data' => $guest
        ], 409);
      }

      $guest->checkin = true;
      $guest->save();

      return response()->json([
        'type' => 'guest',
        'data' => $guest
      ]);
    }
  }

  public function removeExtendedField($eventId, EventRemoveExtFieldRequest $request)
  {
    $event = Event::with('guests', 'guests.companions')->findOrFail($eventId);

    $result = EventService::removeExtendedField($event, $request->field);

    return response()->json($result);
  }
}
