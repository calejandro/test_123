<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\JobFeedback;
use App\Facades\JobFeedbackService;
use App\Event;

use App\Shipment;
use Excel;

use Illuminate\Http\Request;

class JobFeedbacksController extends Controller
{
    public function index($event_id, $email_id)
    {
        $runningJobs = JobFeedback::where(['mail_id' => $email_id, 'finished' => false])->get();
        $finishedJobs = JobFeedback::where(['mail_id' => $email_id, 'finished' => true])->orderBy('updated_at', 'desc')->get();
        return response()->json(["running" => $runningJobs, "finished" => $finishedJobs]);
     }

     public function exportShipment($email_id, $job_id)
     {
        $job = JobFeedback::with('shipment')->findOrFail($job_id);
        $succesfulLoad = json_decode($job->shipment->succesful);
        $failedLoad = json_decode($job->shipment->failed);
        return JobFeedbackService::exportToXLS($succesfulLoad, $failedLoad);
     }
}
