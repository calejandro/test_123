<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SurveyForm;
use App\Event;
use App\Guest;
use App\SurveyResponse;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\SurveyFormRequest;
use App\Http\Requests\SurveyFormReplyRequest;

use App\Facades\SanitizeService;

use Log;

class SurveyFormController extends Controller
{
    public function show($event_id)
    {
        $event = Event::with('surveyForm')->findOrFail($event_id);
        return response()->json($event->surveyForm()->first());
    }

    // This is exactly the same content as the show method but not the same goal and could change
    public function get($event_id)
    {
        $event = Event::with('surveyForm')->findOrFail($event_id);
        return response()->json($event->surveyForm()->first());
    }

    public function update($event_id, SurveyFormRequest $request)
    {
        $event = Event::with('surveyForm')->findOrFail($event_id);
        $surveyForm = $event->surveyForm()->first();
        $data = $request->all();

        $surveyForm->label_color = $data["label_color"];

        $surveyForm->update($data);

        return response()->json($surveyForm);
    }

    public function results($event_id)
    {
        $surveyForm = SurveyForm::with('surveyQuestions')->where('event_id', $event_id)->firstOrFail();
        $questions = $surveyForm->surveyQuestions()->get();
        return response()->json($questions);
    }

    public function reply($event_id, $guest_id, SurveyFormReplyRequest $request)
    {
        $event = Event::with('surveyForm')->findOrFail($event_id);
        $guest = Guest::findOrFail($guest_id);
        $surveyForm = $event->surveyForm()->first();
        $guest->checkActive();

        $requestData = $request->all();

        // Sanitize
        foreach ($requestData['surveyFields'] as $key => &$value) {
            $value['label'] = SanitizeService::removeBalises($value['label']);
        }

        $event->addSurveyFileds($requestData['surveyFields']);
        $event->save();

        foreach ($requestData['surveyFields'] as $key => &$value) {
            if (!empty($value['value'])) {
                $guest->addSurveyResponse($surveyForm, $key, $value['value']);
            }
        }

        return response()->json($guest);
    }
}
