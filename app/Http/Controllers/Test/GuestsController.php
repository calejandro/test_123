<?php

namespace App\Http\Controllers\Test;

use App\Guest;
use App\Event;

use App\Http\Controllers\Controller;

use App\Facades\GuestService;

use Faker;
use Request;

class GuestsController extends Controller
{
  public function importSmoke(int $id, Request $request)
  {
    $event = Event::with('guests')->findOrFail($id);

    $faker = Faker\Factory::create('fr_FR');

    $rawGuests = [];

    for ($i=0; $i<($request->n ?? 20); $i++) {
      $rawGuests[] = [
        'prenom' => $faker->firstName,
        'nom' => $faker->lastName,
        'mail' => $faker->email,
        'adresse' => $faker->address,
        'tel' => $faker->phoneNumber,
        'job' => $faker->jobTitle,
        'company' => $faker->company
      ];
    }

    $guests = GuestService::mapRawGuests($event, $rawGuests, [
      'prenom' => 'firstname',
      'nom' => 'lastname',
      'mail' => 'email'
    ], []);

    Guest::insert($guests);

    if($guests[0]['extendedFields'] != null) {
      $extFields = json_decode($guests[0]['extendedFields'], true);
      $event->addExtendedFileds(array_keys($extFields));
      $event->save();
    }

    return response()->json($event->guests()->active()->get());
  }
}
