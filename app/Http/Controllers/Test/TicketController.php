<?php

namespace App\Http\Controllers\Test;

use App\Event;

use App\Http\Controllers\Controller;

use App\Facades\TemplateService;

class TicketController extends Controller
{
  public function overview(int $eventId)
  {
    $event = Event::findOrFail($eventId);
    $demo = true;
    $guest = null;
    $hash = 'DemoQrCode';
    $qrcode = TemplateService::generateCheckinQrCodeImage(TemplateService::generateCheckinQrCode($hash));

    // Getting the specific ticket template as string to call correct blade template
    $ticket_type = $event->ticket()->first()->ticketType()->first();
    $ticket_template = $ticket_type === NULL ? 'render' : $ticket_type->template;
    
    return view('tickets.layouts.' . $ticket_template, compact('event', 'guest', 'qrcode', 'demo'))->render(); // Generate ticket HTML page
  }
}
