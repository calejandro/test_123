<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Session;
use App\i18n\LocalizableModel;

use App\Http\Requests\Session\CreateUpdateRequest;

use Auth;
use Log;

class SessionController extends Controller
{
    /**
     * Avoid using route binding to optimize requests with nested relationships
     */
    public function index(int $eventId)
    {
        $this->authorize('index', Session::class);
        $event = Event::with('sessions', 'sessions.sessionGroup')->with(['sessionGroups' => function($q) {
            return $q->orderBy(LocalizableModel::localAttributeField('name'), 'ASC');
        }])->findOrFail($eventId);

        $minimumCategoriesSubscriptions = $event->sessionGroups->filter(function ($value, $key) {
            return $value->mandatory;
        })->count();

        return view('sessions.index', [
            'sessions' => $event->sessions,
            'event' => $event,
            'minimumCategoriesSubscriptions' => $minimumCategoriesSubscriptions
        ]);
    }

    public function create(int $eventId)
    {
        $this->authorize('create', Session::class);
        $event = Event::with(['sessionGroups' => function($q) {
            return $q->orderBy(LocalizableModel::localAttributeField('name'), 'ASC');
        }])->findOrFail($eventId);

        return view('sessions.create', [
            'event' => $event,
            'sessionGroups' => $event->sessionGroups
        ]);
    }

    public function store(CreateUpdateRequest $request, Event $event)
    {
        $this->authorize('create', Session::class);

        $session = new Session($request->all());
        $event->sessions()->save($session);
        return redirect()->route('events.sessions.index', [$event]);
    }

    public function destroy(Event $event, Session $session)
    {
        $this->authorize('destroy', Session::class);

        $user = Auth::user();
        Log::info("[DELETE SESSION DEBUG] [DELETE SESSION] [SessionController::destroy] User " . $user->id . " delete session " . $session->id); // For debug pupose

        $session->delete();
        return redirect()->route('events.sessions.index', [$event]);
    }

    public function edit(int $eventId, Session $session)
    {
        $this->authorize('edit', Session::class);
        $event = Event::with(['sessionGroups' => function($q) {
            return $q->orderBy(LocalizableModel::localAttributeField('name'), 'ASC');
        }])->findOrFail($eventId);

        return view('sessions.edit', [ 
            'event' => $event,
            'session' => $session,
            'sessionGroups' => $event->sessionGroups
        ]);
    }

    public function update(CreateUpdateRequest $request, Event $event, Session $session)
    {
        $this->authorize('edit', Session::class);

        $session->update($request->all());
        return redirect()->route('events.sessions.index', [$event]);
    }
}
