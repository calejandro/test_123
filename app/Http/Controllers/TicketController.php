<?php

namespace App\Http\Controllers;

use App\Company;
use App\Event;
use App\Ticket;
use App\TicketType;
use App\Guest;
use App\Companion;

use App\Facades\TemplateService;
use App\Facades\TicketService;

use Illuminate\Http\Request;

use Image;
use Session;
use Log;
use Lang;

class TicketController extends Controller
{

  public function generateDemoTicket(Event $event, $lang)
  {
    // $pdf = TicketService::generateTicketPDF($event, Guest::find(6), true, $lang);
    $pdf = TicketService::generateTicketPDF($event, null, true, $lang);
    return $pdf->download('ticket.pdf');
  }

  public function generateTicket(Event $event, Guest $guest)
  {
    $pdf = TicketService::generateTicketPDF($event, $guest);
    return $pdf->download('ticket.pdf'); // Returns the pdf *file*
  }


  public function generateCompanionTicket(Event $event, Companion $companion)
  {
    $pdf = TicketService::generateCompanionTicketPDF($event, $companion->guest, $companion, $companion->id);
    return $pdf->download('ticket.pdf'); // Returns the pdf *file*
  }


  public function edit(Event $event)
  {
    $ticket_types = TicketType::all()->pluck('template', 'id')->map(function ($template, $key) {
        return trans('models.ticket.ticket_type_' . $template); // Getting ticket name text value translated
    });
    $ticket = $event->ticket()->firstOrFail();
    $ticket->description_fr =  str_replace("<br/>", "\r\n", $ticket->description_fr); // remove <br>
    $ticket->description_en =  str_replace("<br/>", "\r\n", $ticket->description_en); // remove <br>
    $ticket->description_de =  str_replace("<br/>", "\r\n", $ticket->description_de); // remove <br>
    return view('tickets.edit', compact('event', 'ticket', 'ticket_types'));
  }

  public function update(Request $request, Event $event, Ticket $ticket)
  {
    // Delete previously uploaded image if needed
    if(!$request->input("keep_image_1") OR $request->input("keep_image_1") != 1){
      if(file_exists(public_path($ticket->image_1))){
        unlink(public_path($ticket->image_1));
      }
      $ticket->image_1 = null;
    }

    if(!$request->input("keep_image_2") OR $request->input("keep_image_2") != 1){
      if(file_exists(public_path($ticket->image_2))){
        unlink(public_path($ticket->image_2));
      }
      $ticket->image_2 = null;
    }

    $ticket->update([
      'sup_info_field' => $request->sup_info_field == '' ? null : $request->sup_info_field, // deprecated since v5.8.1
      'type_id' => $request->type_id,
      'description_fr' => str_replace("\r\n", "<br/>", $request->description_fr), // add <br>
      'description_en' => str_replace("\r\n", "<br/>", $request->description_en), // add <br>
      'description_de' => str_replace("\r\n", "<br/>", $request->description_de), // add <br>
    ]);

    $image_1 = $request->file('image_1');
    $image_2 = $request->file('image_2');

    if($image_1 != null){
      $name = time() . '.' . $image_1->getClientOriginalExtension();
      Image::make($image_1->getRealPath())->resize(300, null, function ($constraint) {
        $constraint->aspectRatio();
      })->save(public_path('/uploads/images/ticket_image_1/'.$name));

      $ticket->image_1 = '/uploads/images/ticket_image_1/' . $name;
    }
    if($image_2 != null){
      $name = time() . '.' . $image_2->getClientOriginalExtension();
      Image::make($image_2->getRealPath())->resize(1200, null, function ($constraint) {
        $constraint->aspectRatio();
      })->save(public_path('/uploads/images/ticket_image_2/'.$name));

      $ticket->image_2 = '/uploads/images/ticket_image_2/' . $name;
    }

    $ticket->save();

    Session::flash('flash_message', 'Ticket updated!');
    return redirect()->route('events.ticket.edit', [$event]);
  }
}
