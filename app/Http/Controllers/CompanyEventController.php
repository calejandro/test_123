<?php

namespace App\Http\Controllers;

use App\Type;
use App\Event;
use App\Company;
use App\Category;
use App\Email;
use App\SubscribeForm;
use App\SurveyForm;
use App\Minisite;
use App\Guest;
use App\Http\Requests\Event\EventSessionControls;
use App\Utils\StringUtil;

use App\Http\Requests\EventCreateRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Validator;
use Session;

class CompanyEventController extends Controller
{
  /**
   * Display active and future events
   */
  public function index(Request $request, Company $company)
  {
    $this->authorize('index', Event::class);

    $events = Event::where('company_id', $company->id)
      ->active()
      ->actualAndFutur();

    // Test whether or not a filter is applied
    if (isset($request->categories)) {
      // Get events in function of filter
      $filter_categories = $request->categories;
      $events = $events->whereHas('categories', function ($q) use ($filter_categories){
          $q->whereIn('categories.id', $filter_categories);
        });
    }
    $events =  $events->orderBy('start_date', 'ASC')->get();

    return view('events.index', compact(['company', 'events', 'filter_categories']));
  }

  /**
   * Display past events
   */
  public function past(Request $request, Company $company)
  {
    $this->authorize('index', Event::class);

    $events = Event::where('company_id', $company->id)->active()->past();

    // Test whether or not a filter is applied
    if (isset($request->categories)) {
      // Get events in function of filter
      $filter_categories = $request->categories;
      $events = $events->whereHas('categories', function ($q) use ($filter_categories){
        $q->whereIn('categories.id', $filter_categories);
      });
    }
    $events =  $events->orderBy('start_date', 'ASC')->get();

    return view('events.past', compact(['company', 'events', 'filter_categories']));
  }

  public function duplicate(Company $company, Event $event)
  {
    $this->authorize('create', Event::class);

    $newEvent = $event->replicate();
    $newEvent->advanced_subscription_form_mode = 1; // Let's make formbuilder disappear in the future
    $newEvent->name_fr = "[Copie] " . $event->name_fr;
    $newEvent->name_de = "[Kopie] " . $event->name_de;
    $newEvent->name_en = "[Copy] " . $event->name_en;
    $newEvent->extendedFileds = null;
    $newEvent->push();

    // add Categories relations
    foreach($event->categories as $category){
      $newEvent->categories()->attach($category);
    }

    // replicate relations
    $newEvent->replicateEmails($event->emails()->get());
    $newEvent->replicateSurveyForm($event->surveyForm()->first());
    $newEvent->replicateSubscribeForm($event->subscribeForm()->first());
    $newEvent->replicateTicket($event->ticket()->first());
    $newEvent->replicateSessionsAndSessionGroups($event->sessions()->with('sessionGroup')->get());

    $minisite = Minisite::where('event_id', $event->id)->first();
    if (!empty($minisite)) {
      $newEvent->replicateMinisite($minisite);
    }
    $newEvent->setDefaultController($event->controller()->first()->password, false);

    return redirect()->route('companies.events.index', [$company]);
  }

  public function create(Company $company)
  {
    $this->authorize('create', Event::class);

    $categories = Category::where('company_id', $company->id)->orderBy('name')->get()->pluck('name', 'id');
    $types = Type::all()->pluck(Type::localAttributeField('name'), 'id');
    $edit_mode = 0;
    return view('events.create', compact(['company', 'categories', 'edit_mode', 'types']));
  }

  public function store(EventCreateRequest $request, Company $company)
  {
    $this->authorize('create', Event::class);

    $data = $request->all();

    $ev = new Event($data);
    $ev->public_minisite = $request->get('public_minisite', 0);
    $ev->allow_subscription_update = $request->get('allow_subscription_update', 0);
    $ev->advanced_minisite_mode = $request->get('advanced_minisite_mode', 0);
    $ev->advanced_subscription_form_mode = $request->get('advanced_subscription_form_mode', 1);
    $ev->nominative_companions = $request->get('nominative_companions', 0);

    if($request->get('public_minisite', 0)) {
      $ev->visible_captcha = $request->has('visible_captcha');
    }else{
      $ev->visible_captcha = false;
    }

    // Languages TODO: Change the system for link between langs and contents
    $ev->language_fr = $request->get('language_fr', 0);
    $ev->language_en = $request->get('language_en', 0);
    $ev->language_de = $request->get('language_de', 0);

    $evSaved = $company->events()->save($ev);

    $type = Type::findOrFail($data['type_id']);
    $type->events()->save($evSaved);

    // Save m2m categories for this event
    if (isset($data['categories'])) {
      $ev->categories()->sync($data['categories']);
    }

    // Set default relation values
    $evSaved->setDefaultConfirmMail(!$company->hasCompleteSMTPConfig());
    $evSaved->setDefaultFullEventMail(!$company->hasCompleteSMTPConfig());
    $evSaved->setDefaultSubscribeForm();
    $evSaved->setDefaultSurveyForm();
    $evSaved->setDefaultController(StringUtil::randomString());
    $evSaved->setDefaultTicket();

    Session::flash('flash_message', 'Event created!');
    return redirect()->route('companies.events.index', [$company]);
  }

  public function show(Company $company, Event $event)
  {
    $this->authorize('show', Event::class); // Test if current user has access to this feature

    $failedGuests = $event->guests()->where(['valid_email' => false, 'active' => true])->get();
    return view('events.show', compact(['company', 'event', 'failedGuests']));
  }

  public function edit(Company $company, Event $event)
  {
    $this->authorize('edit', Event::class);

    $categories = Category::where('company_id', $company->id)->orderBy('name')->get()->pluck('name', 'id');
    $types = Type::all()->pluck(Type::localAttributeField('name'), 'id');
    $edit_mode = 1;
    return view('events.edit', compact(['company', 'event', 'categories', 'edit_mode', 'types']));
  }

  public function update(EventCreateRequest $request, Company $company, Event $event)
  {
    $this->authorize('edit', Event::class);

    $data = $request->all();
    $event->update($data);
    $event->public_minisite = $request->get('public_minisite', 0);
    $event->allow_subscription_update = $request->get('allow_subscription_update', 0);
    $event->advanced_minisite_mode = $request->get('advanced_minisite_mode', 0);

    if($request->get('public_minisite', 0)) {
      $event->visible_captcha = $request->has('visible_captcha');
    }else{
      $event->visible_captcha = false;
    }

    if ($event->guests()->active()->count() <= 0) {
      $event->nominative_companions = $request->get('nominative_companions', 0);
    }

    // Languages TODO: Change the system for link between langs and contents
    $event->language_fr = $request->get('language_fr', 0);
    $event->language_en = $request->get('language_en', 0);
    $event->language_de = $request->get('language_de', 0);

    $type = Type::findOrFail($data['type_id']);
    $type->events()->save($event);

    // Save m2m categories for this event
    if (isset($data['categories'])) {
      $event->categories()->sync($data['categories']);
    }

    Session::flash('flash_message', 'Event updated!');
    return redirect()->route('companies.events.show', [$company, $event]);
  }

  public function destroy(Company $company, Event $event)
  {
    $this->authorize('destroy', Event::class);

    $event->unActivate();
    Session::flash('flash_message', 'Event deleted!');
    return redirect()->route('companies.events.index', [$company]);
  }

  public function updateSessionsControls(EventSessionControls $request, Event $event){
    $this->authorize('edit', Event::class);

    $event->mandatory_sessions = $request->has('mandatory_sessions');
    $event->allowed_session_subscription = $request->allowed_session_subscription;
    $event->save();

    return redirect()->route('events.sessions.index', [$event]);
  }

  public function updateMandatoryCategorySessionControl(EventSessionControls $request, Event $event){
    $this->authorize('edit', Event::class);
    
    if($request->maximum_categories_subscriptions)
    {
      $mandatoryCategories = $event->sessionGroups->where('mandatory', true)->count();
      $request->validate([
        'maximum_categories_subscriptions' => ['sometimes',
        function($attribute, $value, $fail) use ($mandatoryCategories) {
            if ($mandatoryCategories > 0 && $mandatoryCategories > $value) {
                return $fail(Lang::get('validation.custom.mandatory_category_subscription_lock'));
            }
          }
        ]
      ]);
    }
    
    $event->maximum_categories_subscriptions = $request->maximum_categories_subscriptions;
    $event->one_mandatory_category_session = $request->has('one_mandatory_category_session');
    $event->save();

    return redirect()->route('events.sessions.index', [$event]);
  }

}
