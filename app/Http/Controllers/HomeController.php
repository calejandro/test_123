<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }


    public function changeLocal(Request $request)
    {
      $language = $request->input('lang');

      if(in_array($language, config('app.locales'))){
        Session::put('locale', $language);
        App::setLocale($language);
      }

      return redirect()->back();
    }
}
