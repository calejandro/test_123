<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Company;
use App\User;
use App\Permission;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use Session;
use Log;

class CompanyUserController extends Controller
{
    public function index(Company $company)
    {
        $this->authorize('index', User::class);

        $users = $company->users()->notController()->get();
        return view('users.index', compact(['company', 'users']));
    }

    public function create(Company $company)
    {
        $this->authorize('create', User::class);

        return view('users.create', compact('company'));
    }

    public function store(Company $company, UserCreateRequest $request)
    {
        $this->authorize('create', User::class);

        $data = $request->all();

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);

        $company->users()->save($user);

        // Set default permissions for user (all by default)
        $user->setDefaultPermissions();

        Session::flash('flash_message', 'User added!');

        return redirect()->route('companies.users.index', [$company]);
    }

    public function edit(Company $company, User $user)
    {
        $this->authorize('edit', User::class);

        $user->password = null;
        return view('users.edit', compact(['company', 'user']));
    }

    public function update(Company $company, $id, UserUpdateRequest $request)
    {
        $this->authorize('edit', User::class);
        $data = $request->all();

        $user = User::findOrFail($id);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);

        $user->save();

        Session::flash('flash_message', 'User updated!');
        return redirect()->route('companies.users.index', [$company]);
    }

    public function destroy(Company $company, $id)
    {
        $this->authorize('destroy', User::class);

        User::destroy($id);
        Session::flash('flash_message', 'User deleted!');
        return redirect()->route('companies.users.index', [$company]);
    }

    /**
     * Admin only
     */
    public function editPermissions($companyId, $userId)
    {
        $company = Company::findOrFail($companyId);

        // Permissions grouped for clarity TODO: this could be cleaner and managed differently but hey it works
        // We have a permission that is always true and we dont want to display delete perms (4, 10, 15, 19) : this could be handled more properly
        $users_permissions = Permission::where('name', 'LIKE', '%users%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $events_permissions = Permission::where('name', 'LIKE', '%events%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $guests_permissions = Permission::where('name', 'LIKE', '%guests%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $emails_permissions = Permission::where('name', 'LIKE', '%emails%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $minisite_permissions = Permission::where('name', 'LIKE', '%minisite%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $session_permissions = Permission::where('name', 'LIKE', '%sessions%')->whereNotIn('id', [7, 4, 10, 15, 19])->get();
        $other_permissions = Permission::whereIn('id', [5, 11, 23])->get();

        $user = User::findOrFail($userId);
        return view('users.edit_permissions', compact([
            'company', 
            'users_permissions',
            'events_permissions',
            'guests_permissions',
            'emails_permissions',
            'minisite_permissions',
            'session_permissions',
            'other_permissions', 
            'user'
        ]));
    }

    /**
     * Admin only
     */
    public function updatePermissions($company_id, $id, Request $request)
    {
        $company = Company::findOrFail($company_id);
        $u = User::findOrFail($id);
        $u->permissions()->detach(); // Delete all permissions before attaching new
        if (!is_null($request->input('permissions'))) {
            $newPermissions = Permission::whereIn('id', $request->input('permissions'))->get();
            $allPermissions = Permission::all();

            if ($newPermissions->where('name', 'users-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'users-delete')->first());
            }
            if ($newPermissions->where('name', 'events-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'events-delete')->first());
            }
            if ($newPermissions->where('name', 'guests-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'guests-delete')->first());
            }
            if ($newPermissions->where('name', 'emails-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'emails-delete')->first());
            }
    
            $u->permissions()->attach($newPermissions);
        }
        $u->permissions()->attach(7);
        return redirect()->route('companies.users.edit', [$company->id, $id]);
    }

}
