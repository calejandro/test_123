<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Request;
use View;

use App\Event;
use App\Company;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $currentCompany = null;

    public function __construct() {
        
        $params = Request::route()->parameters();
        
        if(array_key_exists('company', $params)){
            $this->currentCompany = Company::findOrFail($params['company']);
        }
        else if(array_key_exists('event', $params)){
            $this->currentCompany = Event::findOrFail($params['event'])->company;
        }

        View::share('currentCompany', $this->currentCompany);
    }  
}
