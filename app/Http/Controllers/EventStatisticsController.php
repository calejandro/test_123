<?php

namespace App\Http\Controllers;

use App\Event;
use App\Guest;
use App\Company;
use App\Category;
use App\Email;
use App\SubscribeForm;
use Illuminate\Http\Request;

use App\Http\Requests\EventCreateRequest;

use Excel;
use Lang;

class EventStatisticsController extends Controller
{
    public function index(Request $request, Company $company)
    {
        $event_stats = $this->eventStatistics($company);
        $events_past_count = $event_stats['events_past_count'];
        $events_actual_count = $event_stats['events_actual_count'];
        $events_futur_count = $event_stats['events_futur_count'];
        $events_count = $event_stats['events_count'];

        $guests_statistics = $this->guestsStatistics($company);

        $events_categories = $this->categoriesStatistics($company);

        return view('events.statistics.index', compact([
            'company',
            'events_past_count',
            'events_actual_count',
            'events_futur_count',
            'events_count',
            'guests_statistics',
            'events_categories'
        ]));
    }

    /**
     * Create XLS file
     */
    public function export(Company $company)
    {
        $event_stats = $this->eventStatistics($company);
        $guests_statistics = $this->guestsStatistics($company);
        $events_categories = $this->categoriesStatistics($company);

        global $formated_categories;
        $formated_categories = [];
        foreach($events_categories as $k => $v) {
            $formated_categories[] = [$k, ''.$v];
        }

        global $exp_event;
        foreach($event_stats as $k => $v) {
            $exp_event[] = [Lang::get('models.event.statistic.'.$k), ''.$v];
        }

        global $exp_guests;
        foreach($guests_statistics as $k => $v) {
            $exp_guests[] = [Lang::get('models.event.statistic.'.$k), ''.$v];
        }

        // Create XLS
        return Excel::create('statistics', function($excel) {
            $excel->sheet('exported', function($sheet) {
                global $exp_event;
                global $exp_guests;
                global $formated_categories;
                // Sheet manipulation
                $sheet->rows($exp_event);

                $sheet->rows([['']]);
                $sheet->rows($exp_guests);

                $sheet->rows([['']]);
                $sheet->rows([[Lang::get('models.category.Categories')]]);
                $sheet->rows($formated_categories);
            });
        })->download('xls');
    }

    private function eventStatistics(Company $company)
    {
        $events_past_count = $company->events()->active()->past()->count();
        $events_actual_count = $company->events()->active()->actual()->count();
        $events_futur_count = $company->events()->active()->futur()->count();
        $events_count = $events_past_count + $events_actual_count + $events_futur_count;

        return [
            'events_past_count' => $events_past_count,
            'events_actual_count' => $events_actual_count,
            'events_futur_count' => $events_futur_count,
            'events_count' => $events_count
        ];
    }

    private function guestsStatistics(Company $company)
    {
        $events_ids =  $company->events()->active()->pluck('id');
        return [
            'guests_invited_count' => Guest::whereIn('event_id', $events_ids)->where(['active' => true, 'invited' => true])->count(),
            'guests_subscribed_count' => Guest::whereIn('event_id', $events_ids)->where(['active' => true, 'subscribed' => true])->count(),
            'guests_checkin_count' => Guest::whereIn('event_id', $events_ids)->where(['active' => true, 'checkin' => true])->count(),
            'guests_count' => Guest::whereIn('event_id', $events_ids)->where('active', true)->count(),
            'guests_declined_count' => Guest::whereIn('event_id', $events_ids)->where('declined', True)->active()->count()
        ];
    }

    private function categoriesStatistics(Company $company)
    {
        $events_categories = [];
        $categories = $company->categories()->with('events')->get();

        foreach($categories as $category){
            $events_categories[$category->name] = $category->events()->active()->count();
        }

        return $events_categories;
    }
}
