<?php

namespace App\Http\Controllers;

use Image;
use App\Minisite;
use App\Page;
use App\Event;
use App\Guest;
use File;
use App\Companion;
use Illuminate\Http\Request;

use App\Http\Requests\MinisiteCreateRequest;
use App\Facades\MinisiteService;

use App\Utils\LocaleUtil;

class MinisiteController extends Controller
{
  public function index(Event $event)
  {
    // verify 
    $path = public_path('source/'.'subfolder-' . $event->company()->get()->first()->id, 0, '/');
    if(!File::exists($path)) {
      // mkdir($path, 0755, true);
      File::makeDirectory($path, $mode = 0755, true, true);
    }

    $this->authorize('index', Minisite::class);

    $event->load('company', 'company.templates');

    $company = $event->company;
    $minisite = Minisite::where('event_id', $event->id)->with('pages', 'pages.pageType')->get()->first();
    $accessHash = $event->getDemoHash();

    //change it to select the templates for minisites only
    //$company->load('templates');
    $templates = $company->templates;

    return view('minisite.index', compact(['company', 'event', 'minisite', 'accessHash', 'templates']));
  }

  public function create(Event $event)
  {
    $this->authorize('create', Minisite::class);

    $company = $event->company()->get();
    $minisite = Minisite::where('event_id', $event->id)->get()->first();

    # If a minisite already exists
    if ($minisite != null) {
      $edit_mode = true;
      return redirect()->route('events.minisites.edit', ['event' => $event->id, 'minisite' => $minisite->id, 'edit_mode' => $edit_mode]);
    }
    else {
      $edit_mode = false;
      return view('minisite.create', compact(['company', 'event', 'edit_mode']));
    }
  }

  public function store(MinisiteCreateRequest $request, Event $event)
  {
    $this->authorize('create', Minisite::class);
    $data = $request->all();

    $minisite = new Minisite($data);

    $minisite->event_id = $event->id;

    $bgImg = $request->file('site_background_image');
    $bottomImg = $request->file('bottom_image_path');
    $topImg = $request->file('site_top_image');
    
    if($bgImg != null){
      $name = time() . '.' . $bgImg->getClientOriginalExtension();
      MinisiteService::saveBackgroundImage($bgImg, $name);
      $minisite->site_background_image = '/uploads/images/bg_img/' . $name;
    }
    if($bottomImg != null){
      $name = time() . '.' . $bottomImg->getClientOriginalExtension();
      MinisiteService::saveBottomImage($bottomImg, $name);
      $minisite->bottom_image_path = '/uploads/images/bottom_img/' . $name;
    }
    if($topImg != null){
      $name = time() . '.' . $topImg->getClientOriginalExtension();
      MinisiteService::saveTopImage($topImg, $name);
      $minisite->site_top_image = '/uploads/images/top_img/' . $name;
    }

    $minisite->save();
    return redirect()->route('events.minisites.index', [$event]);
  }

  public function redirectNewUrl($slug, $minisiteId, $hash, $lang='fr')
  {
    $minisite = Minisite::findOrFail($minisiteId);
    return redirect()->route('minisite_subdomain_show', [
      'slug' => $minisite->slug,
      'hash' => $hash,
      'lang' => $lang
    ]);
  }

  /**
   * Show minisite
   * If minisite is public $hash may be null
   */
  public function show($slug, $hash=null, $lang=null)
  {
    $minisite = Minisite::where('slug', $slug)->with('event', 'pages')->firstOrFail();
    $pages = $minisite->pages()->orderBy('order_id', 'ASC')->get();
    $event = $minisite->event()->with('surveyForm')->first();

    $setLang = $event->getLangsAttribute()[0];

    // TODO: DRY this code with PageController@show
    if(in_array($hash, ['fr', 'de', 'en'])) { // avoid to have double $lang in URL
      $setLang = $hash;
      $hash = null;
    }

    if($pages->count() <= 0) {
      return view('minisite.under_construction');
    }

    //$demo = $event->isValidDemoHash($hash);  // Check hash if Private minisite
    $guest = Guest::with('event', 'event.surveyForm')->where('accessHash', $hash)->active()->first();
    $companion = Companion::where('access_hash', $hash)->first();

    $page = $minisite->pages()->accessibleByGuest()->orderBy('order_id', 'ASC')->first();
    if (isset($companion)) {
      $page = $minisite->pages()->accessibleByCompanion()->orderBy('order_id', 'ASC')->first();
    }

    if (!$event->active || !isset($page)) {
      abort(404, 'Page not found');
    }

    if (!MinisiteService::verifyPageAccess($event, $page, $guest, $companion, $hash)) {
      return view('subscribeForms.notallowed');
    }

    $demo = $event->isValidDemoHash($hash);

    // Defining minisite lang
    if(is_null($lang) && isset($guest) && !is_null($guest->language) && in_array($guest->language, $event->getLangsAttribute())){
      $lang = $guest->language;
    }else if(is_null($lang)){
      $lang = $setLang;
    }

    LocaleUtil::changeLocal($lang);

    // Survey form
    $surveyForm = $event->surveyForm()->first();

    // Subscribe form
    $subscribeForm = $event->subscribeForm()->first();

    $minAttributes = Guest::getMinAttributes();

    return view('minisite.show', compact([
      'minisite', 
      'pages', 
      'page', 
      'event', 
      'surveyForm', 
      'subscribeForm', 
      'guest', // can be null !
      'companion', // can be null ! 
      'hash', 
      'lang',
      'minAttributes',
      'demo'
    ]));
  }

  public function edit(Event $event, Minisite $minisite)
  {
    $this->authorize('edit', Minisite::class);

    $company = $event->company()->get();
    $edit_mode = true;
    return view('minisite.edit', compact(['company', 'event', 'minisite', 'edit_mode']));
  }

  public function update(MinisiteCreateRequest $request, Event $event, Minisite $minisite)
  {
    $this->authorize('edit', Minisite::class);
    $data = $request->except(['keep_bg_img_file', 'keep_bottom_img_file', 'keep_top_img_file']);

    // We do not keep the file, we have to delete it
    if(!$request->input("keep_bg_img_file") OR $request->input("keep_bg_img_file") != 1){
      if(file_exists(public_path($minisite->site_background_image))){
        unlink(public_path($minisite->site_background_image));
      }
      $minisite->site_background_image = null;
    }

    if(!$request->input("keep_bottom_img_file") OR $request->input("keep_bottom_img_file") != 1){
      if(file_exists(public_path($minisite->bottom_image_path))){
        unlink(public_path($minisite->bottom_image_path));
      }
      $minisite->bottom_image_path = null;
    }

    if(!$request->input("keep_top_img_file") OR $request->input("keep_top_img_file") != 1){
      if(file_exists(public_path($minisite->site_top_image))){
        unlink(public_path($minisite->site_top_image));
      }
      $minisite->site_top_image = null;
    }

    $minisite->update($data);

    $bgImg = $request->file('site_background_image');
    $bottomImg = $request->file('bottom_image_path');
    $topImg = $request->file('site_top_image');
    
    if($bgImg != null){
      $name = time() . '.' . $bgImg->getClientOriginalExtension();
      MinisiteService::saveBackgroundImage($bgImg, $name);
      $minisite->site_background_image = '/uploads/images/bg_img/' . $name;
    }
    if($bottomImg != null){
      $name = time() . '.' . $bottomImg->getClientOriginalExtension();
      MinisiteService::saveBottomImage($bottomImg, $name);
      $minisite->bottom_image_path = '/uploads/images/bottom_img/' . $name;
    }
    if($topImg != null){
      $name = time() . '.' . $topImg->getClientOriginalExtension();
      MinisiteService::saveTopImage($topImg, $name);
      $minisite->site_top_image = '/uploads/images/top_img/' . $name;
    }

    $minisite->save();

    return redirect()->route('events.minisites.index', [$event]);
  }
}
