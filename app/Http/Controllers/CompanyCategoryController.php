<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests\CategoryCreateRequest;

use Session;

class CompanyCategoryController extends Controller
{
    public function index(Company $company)
    {
        $categories = $company->categories;
        return view('categories.index', compact(['company', 'categories']));
    }

    public function create(Company $company)
    {
        return view('categories.create', compact('company'));
    }

    public function store(Company $company, CategoryCreateRequest $request)
    {
        $data = $request->all();

        $cat = new Category;
        $cat->name = $data["name"];
        $cat->description = $data["description"];

        $company->categories()->save($cat);

        Session::flash('flash_message', 'Category added!');

        return redirect()->route('companies.categories.index', [$company]);
    }

    public function edit(Company $company, Category $category)
    {
        return view('categories.edit', compact(['company', 'category']));
    }

    public function update(CategoryCreateRequest $request, Company $company, Category $category)
    {
        $data = $request->all();

        $category->name = $data["name"];
        $category->description = $data["description"];

        $category->save();

        Session::flash('flash_message', 'Category updated!');
        return redirect()->route('companies.categories.index', [$company]);
    }

    public function destroy(Company $company, Category $category)
    {
        $category->delete();
        Session::flash('flash_message', 'Category deleted!');
        return redirect()->route('companies.categories.index', [$company]);
    }
}
