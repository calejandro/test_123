<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
    public function changelog()
    {
        return view('admin.dashboard.changelog');
    }
}
