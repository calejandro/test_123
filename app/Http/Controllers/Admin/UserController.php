<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use App\Permission;
use Illuminate\Http\Request;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereDoesntHave('company')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $data = $request->all();

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);

        $user->save();

        Session::flash('flash_message', 'User added!');

        return redirect()->route('admins.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $user->password = null;

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UserUpdateRequest $request)
    {
        $data = $request->all();

        $user = User::findOrFail($id);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);

        $user->save();

        Session::flash('flash_message', 'User updated!');
        return redirect()->route('admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('flash_message', 'User deleted!');
        return redirect()->route('admins.index');
    }

    /**
     * Admin edit permissions
     */
    public function editPermissions($userId)
    {

        // Permissions grouped for clarity TODO: this could be cleaner and managed differently but hey it works
        // We have a permission that is always true and we dont want to display delete perms (4, 10, 15, 19) : this could be handled more properly
        $users_permissions = Permission::where('name', 'LIKE', '%users%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $events_permissions = Permission::where('name', 'LIKE', '%events%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $guests_permissions = Permission::where('name', 'LIKE', '%guests%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $emails_permissions = Permission::where('name', 'LIKE', '%emails%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $minisite_permissions = Permission::where('name', 'LIKE', '%minisite%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $session_permissions = Permission::where('name', 'LIKE', '%sessions%')->whereNotIn('id', [4, 10, 15, 19])->get();
        $other_permissions = Permission::whereIn('id', [5, 11, 23])->get();

        $user = User::findOrFail($userId);

        return view('admin.users.edit_permissions', compact([
            'company', 
            'users_permissions',
            'events_permissions',
            'guests_permissions',
            'emails_permissions',
            'minisite_permissions',
            'session_permissions',
            'other_permissions', 
            'user'
        ]));

    }

    /**
     * Admin edit permissions
     */
    public function updatePermissions($id, Request $request)
    {
        $u = User::findOrFail($id);
        $u->permissions()->detach(); // Delete all permissions before attaching new

        if (!is_null($request->input('permissions'))) {
            $newPermissions = Permission::whereIn('id', $request->input('permissions'))->get();
            $allPermissions = Permission::all();

            if ($newPermissions->where('name', 'users-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'users-delete')->first());
            }
            if ($newPermissions->where('name', 'events-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'events-delete')->first());
            }
            if ($newPermissions->where('name', 'guests-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'guests-delete')->first());
            }
            if ($newPermissions->where('name', 'emails-create')->count() > 0) {
                $newPermissions->push($allPermissions->where('name', 'emails-delete')->first());
            }

            $u->permissions()->attach($newPermissions);
        }

        $u->save();
        return redirect()->route('admins.edit', [$id]);
    }

}
