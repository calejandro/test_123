<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use Illuminate\Http\Request;
use Session;
use File;

use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $companies = Company::where('active', true)
            ->orderBy('companyName', 'asc')
            ->paginate(25);
        return view('admin.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CompanyCreateRequest $request)
    {
        $requestData = $request->all();

        $company_id = Company::create($requestData)->id;

        // Create the subfolder of images for minisite
        $path = public_path().'/source/subfolder-' . $company_id;
        File::makeDirectory($path, $mode = 0777, true, true);

        Session::flash('flash_message', 'Company added!');

        return redirect('companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $company = Company::findOrFail($id);

        return view('admin.companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);

        return view('admin.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, CompanyUpdateRequest $request)
    {
        $requestData = $request->all();

        $company = Company::findOrFail($id);
        $company->update($requestData);

        Session::flash('flash_message', 'Company updated!');

        return redirect('companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $company = Company::where('id', $id)->first();
        $company->active = false;
        $company->save();

        Session::flash('flash_message', 'Company successfully deleted!');

        return redirect('companies');
    }


    public function lock($id)
    {
        $this->setLockCompany($id, true);
        Session::flash('flash_message', 'Company locked!');
        return redirect('companies');
    }

    public function unlock($id)
    {
        $this->setLockCompany($id, false);
        Session::flash('flash_message', 'Company unlocked!');
        return redirect('companies');
    }

    private function setLockCompany($companyId, $lockState)
    {
        $company = Company::findOrFail($companyId);
        $company->lock = $lockState;
        return $company->save();
    }
}
