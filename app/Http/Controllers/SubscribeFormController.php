<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Facades\TemplateService;

use App\Companion;
use App\Guest;
use App\Event;
use App\SubscribeForm;
use Illuminate\Http\Request;

use DbView;

class SubscribeFormController extends Controller
{
    public function edit(Event $event)
    {
        $accessHash = $event->getDemoHash();
        $event->load('company', 'company.templates');
        $templates = $event->company->MinisiteTemplates();


        return view('subscribeForms.edit', compact(['event', 'accessHash', 'templates']));
    }

    /**
     *  @param $hash: guest hash
    */
    public function show($hash)
    {
        $event = Event::findWithHash($hash);
        $guest = null;

        if ($event == null) {
            $guest = Guest::where('accessHash', $hash)->active()->first();
            if (!isset($guest)) {

                $companion = Companion::where('access_hash', $hash)->whereHas('guest', function ($q) {
                    $q->where('active', true);
                })->with('guest', 'guest.event')->first();

                if (isset($companion)) {
                    return redirect()->route('events.subscribeForm.showCompanion', [$hash]);
                }
                else {
                    return view('subscribeForms.notallowed');
                }
            }
            else {
                $event = $guest->event()->first();
            }
        }

        if (!$event->active) {
            abort(404, 'Page not found');
        }

        if($event->hasMinisite() && $event->advanced_minisite_mode) {
            return redirect($this->minisiteUrl($event, $hash));
        }

        $subscribeForm = $event->subscribeForm()->first();

        // Define whether the sub form to inject is simple (formbuilder) or advanced (surveyjs)
        $eventSubscrForm = html_entity_decode($subscribeForm->formHtmlWithAdditions);
        if ($event->advanced_subscription_form_mode) {
            $eventSubscrForm = html_entity_decode($subscribeForm->advancedFormHtmlWithAdditions($guest));
        }

        if (isset($guest)) {
            // guest
            $viewBody = TemplateService::generate($subscribeForm->html, [
                'firstname' => $guest->firstname,
                'lastname' => $guest->lastname,
                'email' => $guest->lastname,
                'additionalFields' => $guest->extendedFields,
                'eventSubscrForm' => $eventSubscrForm,
                'map' => html_entity_decode(TemplateService::generateMap($event->address, 450)),
                'bigMap' => html_entity_decode(TemplateService::generateMap($event->address, 650)),
                'littleMap' => html_entity_decode(TemplateService::generateMap($event->address, 250))
            ]);
        }
        else {
            // demo
            $viewBody = TemplateService::generate($subscribeForm->html, [
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'email' => 'email',
                'additionalFields' => [],
                'eventSubscrForm' => $eventSubscrForm,
                'map' => html_entity_decode(TemplateService::generateMap($event->address, 450)),
                'bigMap' => html_entity_decode(TemplateService::generateMap($event->address, 650)),
                'littleMap' => html_entity_decode(TemplateService::generateMap($event->address, 250))
            ]);
        }

        $nbEmptyPlaces = $event->empty_places;
        $minAttributes = Guest::getMinAttributes();

        return view('subscribeForms.show', compact('event', 'subscribeForm', 'guest', 'companion', 'viewBody', 'nbEmptyPlaces', 'minAttributes'));
    }

    /**
     *  @param $hash: companion hash
    */
    public function showCompanion(string $hash)
    {
        $companion = Companion::where('access_hash', $hash)->whereHas('guest', function ($q) {
            $q->where('active', true);
        })->with('guest', 'guest.event')->firstOrFail();

        $event = $companion->guest->event;

        if (!$event->active) {
            abort(404, 'Page not found');
        }

        if ($event->hasMinisite() && $event->advanced_minisite_mode) {
            return redirect($this->minisiteUrl($event, $hash));
        }

        $subscribeForm = $event->subscribeForm()->first();

        return view('subscribeForms.showCompanion', compact('companion', 'event', 'subscribeForm'));
    }

    private function minisiteUrl(Event $event, string $hash)
    {
        $minisite = $event->minisite()->first();
        $url = $minisite->subscribeFormLink($hash);
        if ($url === null) {
            $url = $minisite->indexLink($hash);
        }
        return $url;
    }
}
