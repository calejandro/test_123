<?php

namespace App\Http\Controllers;

use App\Event;
use App\Session;
use App\SessionGroup;

use App\Http\Requests\SessionGroup\CreateUpdateRequest as SessionGroupsCreateUpdateRequest;

use Illuminate\Http\Request;

use Auth;
use Illuminate\Support\Facades\Lang;
use Log;

class SessionGroupController extends Controller
{
    public function create(Event $event)
    {
        $this->authorize('create', Session::class);

        $mandatoryCategories = $event->sessionGroups->where('mandatory', true)->count();

        return view('sessionGroups.create', [
            'event' => $event,
            'mandatoryCategories' => $mandatoryCategories
        ]);
    }

    public function store(SessionGroupsCreateUpdateRequest $request, Event $event)
    {
        $this->authorize('create', Session::class);

        if($request->has('mandatory') && $event->maximum_categories_subscriptions)
        {
            $mandatoryCategories = $event->sessionGroups->where('mandatory', true)->count();
            $request->validate([
                'mandatory' => ['sometimes',
                    function($attribute, $value, $fail) use ($mandatoryCategories, $event) {
                        if ($mandatoryCategories + 1 > $event->maximum_categories_subscriptions) {
                            return $fail(Lang::get('validation.custom.mandatory_category_subscription_lock_group'));
                        }
                    }
                ]
            ]);
        }

        $sg = new SessionGroup($request->all());
        $event->sessionGroups()->save($sg);
        return redirect()->route('events.sessions.index', [$event]);
    }

    public function edit(Event $event, SessionGroup $sessionGroup)
    {
        $this->authorize('edit', Session::class);

        return view('sessionGroups.edit', [
            'event' => $event,
            'sessionGroup' => $sessionGroup
        ]);
    }

    public function update(SessionGroupsCreateUpdateRequest $request, Event $event, SessionGroup $sessionGroup)
    {
        $this->authorize('edit', Session::class);

        if($request->has('mandatory') && $event->maximum_categories_subscriptions)
        {
            $mandatoryCategories = $event->sessionGroups->where('mandatory', true)->count();
            $request->validate([
                'mandatory' => ['sometimes',
                    function($attribute, $value, $fail) use ($mandatoryCategories, $event) {
                        if ($mandatoryCategories + 1 > $event->maximum_categories_subscriptions) {
                            return $fail(Lang::get('validation.custom.mandatory_category_subscription_lock_group'));
                        }
                    }
                ]
            ]);
        }

        $sessionGroup->update($request->all());
        $sessionGroup->mandatory = $request->has('mandatory');
        $sessionGroup->save();
        return redirect()->route('events.sessions.index', [$event]);
    }

    public function destroy(Event $event, SessionGroup $sessionGroup)
    {
        $this->authorize('destroy', Session::class);

        $user = Auth::user();
        Log::info("[DELETE SESSION DEBUG] [DELETE SESSION GROUP] [SessionGroupController::destroy] User " . $user->id . " delete session group " . $sessionGroup->id); // For debug pupose

        $sessionGroup->sessions()->withTrashed()->update(['session_group_id' => null]);
        $sessionGroup->delete();

        return redirect()->route('events.sessions.index', [$event]);
    }
}