<?php

namespace App\Http\Controllers;

use App\Event;

use Auth;

class CheckinController extends Controller
{
    public function index(?Event $event)
    {
        $controllerEvent = Auth::user()->controller ? Auth::user()->event : null;
        return view('checkin.index', [
            'event' => is_null($event->id) ? $controllerEvent : $event
        ]);
    }
}
