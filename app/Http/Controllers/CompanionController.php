<?php

namespace App\Http\Controllers;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Companion;
use App\Event;
use App\Guest;

use App\Facades\CompanionService;

use App\Http\Requests\Companion\CompanionRequest;


class CompanionController extends Controller
{
  public function edit(Event $event, Companion $companion)
  {
    if (!Auth::user()->isController()) {
      $this->authorize('edit', Guest::class);
    }

    $companion->guest->checkActive();

    return view('companions.edit', compact(['event', 'companion']));
  }

  public function update(Event $event, Companion $companion, CompanionRequest $request)
  {
    if (!Auth::user()->isController()) {
      $this->authorize('edit', Guest::class);
    }

    $companion->guest->checkActive();

    $companion->update($request->all());

    return redirect()->route('events.guests.index', [$event]);
  }
}
