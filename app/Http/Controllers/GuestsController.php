<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Guest;
use App\Event;
use App\Session;
use App\SessionGroup;
use App\Companion;
use App\JobMailVerificationFeedback;

use Illuminate\Http\Request;

use Log;
use Auth;
use Excel;
use Lang;
use Carbon\Carbon;

use App\Http\Requests\Guest\GuestRequest;

use App\Facades\SessionService;
use App\Facades\GuestService;
use App\Facades\EmailValidationService;
use App\Jobs\VerifySingleMailValidity;
use App\Jobs\VerifyBatchMailsValidity;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class GuestsController extends Controller
{
  /**
   * Display conflicts
   */
  public function duplicates(Event $event)
  {
    $duplicate_blocs = GuestService::getDuplicates($event);
    return view('guests.solve_duplicates_conflicts', compact(['event', 'duplicate_blocs']));
  }

  public function index(Event $event)
  {
    $this->authorize('index', Guest::class);

    $surveyFields = $event->getSurveyFieldsList();
    $failedGuests = $event->guests()->where([
      'valid_email' => false,
      'active' => true
    ])->get();

    $failedEmailValidation = $event->verificationJobs()->where('error', true)->get();

    $duplicates_detected = GuestService::hasGuestsDuplicateEmails($event);
    $emailValidationRunning = EmailValidationService::isOneRunning($event);
    $hasRegisteredCompanions = Guest::has('companions')->where(['active' => 1, 'event_id' => $event->id])->count() > 0;

    return view('guests.index', compact(['event', 'surveyFields', 'failedGuests', 'duplicates_detected', 'emailValidationRunning', 'failedEmailValidation', 'hasRegisteredCompanions']));
  }

  public function create(Event $event)
  {
    $this->authorize('create', Guest::class);

    $locales = [];
    $locales['-'] = '-';
    $locales = array_merge($locales, Guest::EXPLICIT_LOCALES);

    return view('guests.create', compact(['event', 'locales']));
  }

  public function store(Event $event, GuestRequest $request)
  {
    $this->authorize('create', Guest::class);

    $requestData = $request->all();

    $guest = new Guest;
    $guest->firstname = $requestData['firstname'];
    $guest->lastname = $requestData['lastname'];
    $guest->email = $requestData['email'];
    $guest->company = $requestData['company'];
    $guest->invited = $requestData['invited'];
    $guest->checkin = $requestData['checkin'];
    $guest->language = $requestData['language'] == '-' ? null : $requestData['language'];

    if (isset($requestData['extended_fields'])) {
      $guest->extendedFields = $requestData['extended_fields'];
    }

    $event->guests()->save($guest);

    // Deferred job to test validity of created guest's email
    VerifySingleMailValidity::dispatch($guest);

    return redirect()->route('events.guests.index', [$event]);
  }

  public function edit(Event $event, $id)
  {
    if (!Auth::user()->isController()) {
      $this->authorize('edit', Guest::class);
    }

    $locales = [];
    $locales['-'] = '-';
    $locales = array_merge($locales, Guest::EXPLICIT_LOCALES);

    $guest = Guest::findOrFail($id);
    $guest->checkActive();

    return view('guests.edit', compact(['event', 'guest', 'locales']));
  }

  public function update(Event $event, $id, GuestRequest $request)
  {
    if (!Auth::user()->isController()) {
      $this->authorize('edit', Guest::class);
    }

    $guest = Guest::findOrFail($id);
    $guest->checkActive();

    $guest->update([
      'firstname' => $request->firstname,
      'lastname' => $request->lastname,
      'company' => $request->company,
      'email' => $request->email,
      'language' => $request->language == '-' ? null : $request->language,
      'extendedFields' => $request->extended_fields,
      'invited' => $request->invited,
      'checkin' => $request->checkin,
      'valid_email' => null // Guest mail valid back to null to recontrol it on next send
    ]);

    // Deferred job to test validity of created guest's email
    VerifySingleMailValidity::dispatch($guest);

    return redirect()->route('events.guests.index', [$event]);
  }

  public function destroy(Event $event, $id)
  {
    $this->authorize('destroy', Guest::class);

    $guest = Guest::findOrFail($id);
    $guest->active = false;
    $guest->save();

    return redirect()->route('events.guests.index', [$event]);
  }


  public function import(Event $event, Request $request)
  {
    $importRows = Excel::selectSheetsByIndex(0)
      ->load($request->file('import_guests'), function($reader) {})
      ->ignoreEmpty()
      ->get();

    $importRows = GuestService::cleanXLSImport($importRows);

    $rows = $importRows->toArray();
    $importColumns = [];
    if (count($rows) > 0) {
      $importColumns = array_keys($rows[0]); // Contains the distinct columns including required ones
    }

    $importErrors = [];
    if (count($rows) > 0 && count(array_keys($rows[0])) > config('app.max_xls_columns_accept')) {
      $importErrors[] = Lang::get('interface.guests_import.column.maximum_reached', [
        'max' => config('app.max_xls_columns_accept')
      ]);
    }

    return view('guests.import', compact(['event', 'importRows', 'importColumns']))->withErrors($importErrors);
  }

  /**
   * Export guests to XLS format
   */
  public function export(Event $event)
  {
    $guests = $event->guests()->active()->get();
    GuestService::normalizeExtendedFields($guests, $event->extendedFileds ?? []);
    $sessions=Session::where('event_id',$event->id)->get()->toArray();
    $sessionsGroup=SessionGroup::where('event_id',$event->id)->get()->toArray();

    // $guests = $event->guests()->with('surveyResponses', 'sessions', 'companions.sessions')->active()->get()->toArray();
    $guests = $event->guests()->with('surveyResponses', 'companions')->active()->get()->toArray();
    $eventSurveyFields = $event->getSurveyFieldsList();

    return GuestService::exportExcel($guests, $eventSurveyFields, $sessions, $sessionsGroup,  $event->extendedFileds ?? []);
  }

  public function exportCompanions(Event $event)
  {
    $companions = Companion::with('sessions', 'guest')->whereHas('guest', function ($query) use($event){
      $query->where(['active' => 1, 'event_id' => $event->id]);
    })->get()->toArray();
    $sessions = $event->sessions()->get();
    return GuestService::exportCompanionsExcel($companions, $sessions);

  }

  public function checkin(string $hash)
  {
    $companion = NULL;
    $guest = Guest::with('sessions', 'event')->where('accessHash', $hash)->first();

    if ($guest === NULL) {
      $companion = Companion::with('guest')->where('access_hash', $hash)->firstOrFail();
      $guest = $companion->guest;
    }

    $event = $guest->event;
    $sessions = $event->sessions()->get();

    $errorMessage = NULL;
    $warningMessage = NULL;

    if (!$guest) {
      $errorMessage = Lang::get('models.guest.checkin.InvitInvalidUnknownGuest');
    }
    else {
      if (!$guest->invited) { // not invited to event
        $warningMessage = Lang::get('models.guest.checkin.WarningNotInvited');
      }
      if (!$guest->subscribed) { // not subscribed to event
        $warningMessage = Lang::get('models.guest.checkin.WarningNotSubscribed');
      }

      if($companion) {
        if ($companion->checkin) { // not registered to session
          $warningMessage = Lang::get('models.guest.checkin.TicketAllreadyScanned');
        }
      }
      else {
        if ($guest->checkin) { // not registered to session
          $warningMessage = Lang::get('models.guest.checkin.TicketAllreadyScanned');
        }
      }
    }

    return view('guests.checkin', [
      'guest' => $guest,
      'event' => $event,
      'sessions' => $sessions,
      'companion' => $companion,
      'hash' => $hash,
      'error_message' => $errorMessage,
      'warning_message' => $warningMessage
    ]);
  }

  public function confirmCheckin(Request $request, string $hash)
  {
    $companion = NULL;
    $event = NULL;
    $guest = Guest::with('sessions', 'event')->where('accessHash', $hash)->first();

    $confirmedCheckin = false;

    if ($guest === NULL) {
      $companion = Companion::with('guest')->where('access_hash', $hash)->firstOrFail();
      $guest = $companion->guest;
      $event = $guest->event;

      if (!$companion->checkin) {
        $companion->checkin = true;
        $companion->save();
        $confirmedCheckin = true;
      }
    }
    else {
      $event = $guest->event;
      if (!$guest->checkin) {
        $guest->checkin = true;
        $guest->save();
        $confirmedCheckin = true;
      }
    }

    return view('guests.checkin', compact(['confirmedCheckin', 'event', 'guest']));
  }

  public function checkinSession(Session $session, $hash)
  {
    $companion = NULL;
    $event = $session->event()->firstOrFail();

    $guest = $event->guests()->with('sessions')->where('accessHash', $hash)->first();

    if($guest === NULL){
      $companion = Companion::where('access_hash', $hash)->firstOrFail();
      $guest = $companion->guest;
    }

    $sessions = $event->sessions()->get();

    $sessionCheckinCount = $guest->countSessionCheckin($session);
    $sessionPlacesCount = $guest->countSessionPlaces($session);

    $errorMessage = NULL;
    $warningMessage = NULL;

    if (!$guest) {
      $errorMessage = Lang::get('models.guest.checkin.InvitInvalidUnknownGuest');
    }
    else {
      if (!$guest->invited) { // not invited to event
        $warningMessage = Lang::get('models.guest.checkin.WarningNotInvited');
      }
      if (!$guest->subscribed) { // not subscribed to event
        $warningMessage = Lang::get('models.guest.checkin.WarningNotSubscribed');
      }
    }

    return view('guests.checkin', [
      'companion' => $companion,
      'guest' => $guest,
      'error_message' => $errorMessage,
      'warning_message' => $warningMessage,
      'event' => $event,
      'sessions' => $sessions,
      'hash' => $hash,
      'session' => $session,
      'sessionPlacesCount' => $sessionPlacesCount,
      'sessionCheckinCount' => $sessionCheckinCount
    ]);
  }

  /**
  * Method called in POST by clicking on confirm entry on checkin page.
  *
  * Subscribes guest OR accompanion to session by swapping accompanions entries
  * if needed in order to keep a correct track of attending vs effective checkins.
  *
  */
  public function confirmCheckinSession(Session $session, Request $request)
  {
    $guest = NULL;
    $companion = NULL;

    // Getting guest and/or companion depending on the case
    if (isset($request->guest_id)) {
      $guest = Guest::with(['sessions', 'event'])->findOrFail($request->guest_id);
    }
    else {
      $companion = Companion::findOrFail($request->companion_id);
      $guest = $companion->guest;
    }

    $event = $guest->event;

    $confirmedCheckin = false;

    if(isset($request->companion_id)) { // Subscribing a COMPANION
      $companionSession = $companion->sessions()->where('sessions.id', $session->id)->first();

      if (!($companionSession != null && $companionSession->pivot->checkin)) {
        $confirmedCheckin = SessionService::checkinCompanion($companion, $session, !$event->nominative_companions);
      }
    }
    else { // Subscribing a GUEST
      $guestSession = $guest->sessions()->where('sessions.id', $session->id)->first();

      // If guest not yet subscribed to this session, we subscribe it !
      if ($guestSession != null && !$guestSession->pivot->checkin) {
        SessionService::checkinGuest($guest, $session);
        $confirmedCheckin = true;
      }
    }

    return view('guests.checkin', compact(['confirmedCheckin', 'event', 'guest']));
  }

  /**
   * Relaunch email validation and remove failed jobs
   */
  public function validateEmails(Event $event)
  {
    EmailValidationService::removeFailed($event);

    // Building base jobVerificationFeedback object
    // Create even if no guest needs to be validated, to ensure front-end will ask guestlist updated
    $verificationFeedback = new JobMailVerificationFeedback();
    $verificationFeedback->event_id = $event->id;
    $verificationFeedback->save();

    $guests = $event->guests()->emailValidity(Guest::EMAIL_VALIDITY['unchecked'])->get();

    // Trigger job to validate list of emails, job is created inside it
    VerifyBatchMailsValidity::dispatch($guests->toArray(), $event->id, $verificationFeedback);

    return redirect()->route('events.guests.index', [$event]);
  }

  /**
   * Guest decline invitation
   * - Session subscriptions deleted
   * - Companions deleted
   */
  public function decline(string $hash)
  {
    $guest = Guest::where('accessHash', $hash)->firstOrFail();
    $event = $guest->event()->first();

    if (!$guest) {
      return view('guests.decline_feedback', [
        'errorMessage' => Lang::get('models.guest.checkin.InvitInvalidUnknownGuest'), 
        'event' => $event, 
        'guest' => $guest
      ]);
    }
    else {
      // Automatically detach session on set attribut
      $guest->declined = true; // if true, will remove companions and sessions
      $guest->save();
      return view('guests.decline_feedback', compact(['event', 'guest']));
    }
  }
}
