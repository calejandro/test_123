<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\StoreSmtpCompanyConfig;
use Lang;

class CompanySmtpController extends Controller
{
    public function edit(Company $company)
    {
        if(!in_array('configurations-update', Auth::user()->permissions()->get()->pluck('name')->toArray())) {
          return view('errors.403', compact(['company']));
        }
        return view('companies.edit', compact(['company']));
    }

    public function update(StoreSmtpCompanyConfig $request, Company $company)
    {
        $data = $request->all();
        $company->smtp_host = $data["smtp_host"];
        $company->smtp_port = $data["smtp_port"];
        $company->smtp_from_mail_address = $data["smtp_from_mail_address"];
        $company->smtp_from_name = $data["smtp_from_name"];
        //$company->smtp_encryption = $data["smtp_encryption"];
        $company->smtp_username = $data["smtp_username"];
        $company->smtp_password = Crypt::encrypt($data["smtp_password"]);
        $company->save();
        return redirect()->route('companies.config', [$company])->with('message', Lang::get('interface.config_updated'));
    }
}
