<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Facades\TemplateService;

use Mail;

use App\Guest;
use App\Email;
use App\EmailType;
use App\Event;
use App\Facades\SessionService;
use App\Template;
use App\Minisite;
use App\JobFeedback;
use App\JobMailVerificationFeedback;
use App\Session as SessionModel;

use App\Utils\MergeTagsUrlBuilder;

use Illuminate\Http\Request;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Mail\Mailer;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;

class EmailsController extends Controller
{
  public function index(Event $event)
  {
    $this->authorize('index', Email::class);

    $emailsInvit = $event->emails()
      ->where('type', EmailType::INVITATION)
      ->where('active', true)
      ->get();

    $confirmTypes = [EmailType::CONFIRMATION];
    if ($event->nominative_companions) {
      $confirmTypes[] = EmailType::COMPANIONS_CONFIRMATION;
    }

    $emailsConfirm = $event->emails()
      ->whereIn('type', $confirmTypes)
      ->get();

    $emailEventFull = $event->emails()
      ->where('type', EmailType::EVENT_FULL_NOTIFICATION)
      ->first();

    $emailsOther = $event->emails()
      ->whereNull('type')
      ->where('active', true)
      ->get();

    return view('emails.index', compact(['event', 'emailsInvit', 'emailsConfirm', 'emailsOther', 'emailEventFull']));
  }

  /**
   * Create Email of "Other" type
   */
  public function createOther(Event $event)
  {
    $this->authorize('create', Email::class);

    $event->load('company', 'company.templates', 'minisite');

    $company = $event->company;

    $email = new Email();
    $email->joinICS = false;
    $email->joinTicket = false;
    $email->sendWithDefaultMail = !$company->hasCompleteSMTPConfig();
    $email->sended = 0;
    $email->event_id = $event->id;
    $email->sessions_style = null;

    $template = null;
    $templates = $company->EmailTemplates();

    $mailJobNotAlreadyRunning = true; // mailjob cannot be running on creation
    $verificationJobNotAlreadyRunning = JobMailVerificationFeedback::where([
      'finished' => false,
      'event_id' => $event->id
    ])->get()->isEmpty();

    $minisite = $event->minisite;
    $extendedFields = json_encode(Guest::getExtendedFieldListForEvent($event));

    $lang = config('app.fallback_locale');

    return view('emails.edit', compact([
      'lang',
      'event',
      'email',
      'templates',
      'template',
      'company',
      'minisite',
      'extendedFields',
      'mailJobNotAlreadyRunning',
      'verificationJobNotAlreadyRunning'
    ]));
  }

  /**
   * Create Email of "Invitation" type
   */
  public function createInvit(Event $event)
  {
    $this->authorize('create', Email::class);

    $event->load('company', 'company.templates', 'minisite');

    $company = $event->company;

    $email = new Email();
    $email->type = EmailType::INVITATION;
    $email->joinICS = false;
    $email->joinTicket = false;
    $email->sendWithDefaultMail = !$company->hasCompleteSMTPConfig();
    $email->sended = 0;
    $email->event_id = $event->id;
    $email->sessions_style = null;

    $template = null;
    $templates = $company->EmailTemplates();

    $mailJobNotAlreadyRunning = true; // mail job cannot be already running on creation
    $verificationJobNotAlreadyRunning = JobMailVerificationFeedback::where([
      'finished' => false,
      'event_id' => $event->id
    ])->get()->isEmpty();

    $minisite = $event->minisite;
    $extendedFields = json_encode(Guest::getExtendedFieldListForEvent($event));

    $lang = config('app.fallback_locale');

    return view('emails.edit', compact([
      'lang',
      'event',
      'email',
      'template',
      'templates',
      'company',
      'minisite',
      'extendedFields',
      'mailJobNotAlreadyRunning',
      'verificationJobNotAlreadyRunning'
    ]));
  }

  /**
   * Show email preview
   */
  public function render(Event $event, Email $email, $lang='fr', $display="desktop")
  {

    $renderMail = $this->renderTemplate($event, $email, $lang);
    $showMobile = $display === "mobile";

    return view('emails.render', compact(['event', 'email', 'lang', 'renderMail', 'showMobile']));
  }

  public function renderTemplate(Event $event, Email $email, $lang="fr")
  {
    $guest = $event->guests()->where('active', true)->first();
    if ($guest == null) {
      $renderMail = TemplateService::generatePreview($email->getTranslatedAttribute('html', $lang), $event, $email);
    }
    else {
      $isCompanion = ($email->type === EmailType::COMPANIONS_CONFIRMATION);
      
      $sessions = SessionService::sessionsGroupedByCategory($guest);
      
      $renderMail = TemplateService::generate($email->getTranslatedAttribute('html', $lang), [
        'firstname' => $guest->firstname,
        'lastname' => $guest->lastname,
        'email' => $guest->email,
        'language' => $guest->language,
        'additionalFields' => $guest->extendedFields,
        'accessHash' => $guest->accessHash,
        'decline#url' => TemplateService::buildMergeTagUrl($event, $guest->accessHash, 'decline', $isCompanion),
        'minisite#url' => TemplateService::buildMergeTagUrl($event, $guest->accessHash, 'minisite', $isCompanion),
        'subscribe#url' => TemplateService::buildMergeTagUrl($event, $guest->accessHash, 'subscribe', $isCompanion),
        'survey#url' => TemplateService::buildMergeTagUrl($event, $guest->accessHash, 'survey', $isCompanion),
        'qrcode' => TemplateService::generateCheckinQrCodeImage(TemplateService::generateCheckinQrCode($guest->accessHash)),
        'sessions' => TemplateService::buildSessionsElementPreview($event, $email, $sessions),
      ]);
    }

    return $renderMail;
  }

  public function edit(Event $event, Email $email, string $lang='fr')
  {
    $this->authorize('edit', Email::class);
    $this->checkActive($email);

    $event->load('company', 'company.templates', 'minisite');

    $template = $email->getTranslatedAttribute('template', $lang);
    $template = str_replace('&quot;', '\&quot;', $template);
    $email->template = null;

    $email->setAttribute('title_i18n', $email->getTranslatedAttribute('title', $lang));
    $email->setAttribute('template_i18n', $template);
    $email->setAttribute('html_i18n', $email->getTranslatedAttribute('html', $lang));

    $company = $event->company;
    $templates = $company->EmailTemplates();
    $minisite = $event->minisite;

    $mailJobNotAlreadyRunning = JobFeedback::where([
      'finished' => false,
      'mail_id' => $email->id
    ])->get()->isEmpty();

    $verificationJobNotAlreadyRunning = JobMailVerificationFeedback::where([
      'finished' => false,
      'event_id' => $event->id
    ])->get()->isEmpty();

    $extendedFields = json_encode(Guest::getExtendedFieldListForEvent($event));
    return view('emails.edit', compact([
      'event',
      'email',
      'templates',
      'template',
      'company',
      'minisite',
      'extendedFields',
      'mailJobNotAlreadyRunning',
      'verificationJobNotAlreadyRunning',
      'lang'
    ]));
  }

  public function update(Event $event, Email $email, Request $request)
  {
    $this->checkActive($email);

    $requestData = $request->all();
    $email->update($requestData);

    return redirect('emails');
  }

  public function destroy(Event $event, Email $email)
  {
    $this->authorize('destroy', Email::class);

    if($email->isConfirm) {
      Session::flash('flash_message', 'Cannot delete confirm email');
    }
    else{
      $email->active = false;
      $email->save();
      Session::flash('flash_message', 'Email deleted!');
    }

    return redirect()->route('events.emails.index', [$event->id]);
  }

  public function duplicate(Event $event, Email $email){
      $email->replicate($except = null, $addCopyText = true);
      return redirect()->route('events.emails.index', [$event->id]);
  }

  private function checkActive(Email $email)
  {
    if(!$email->active){
      throw new ModelNotFoundException('Ressource not found');
    }
  }
  
}
