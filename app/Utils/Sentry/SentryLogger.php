<?php

namespace App\Utils\Sentry;

use App;
use App\Utils\BuiltIns;
use Config;
use Session;

use Exception;

/**
 * Util to communicate with sentry
 */
final class SentryLogger
{
    private function __construct() {}

    public static function logMessage(string $errorMessage, array $extra)
    {
        app('sentry')->captureMessage($errorMessage, $extra);
    }

    public static function logException(Exception $exception, array $extra)
    {
        app('sentry')->captureException($exception, $extra);
    }
}
