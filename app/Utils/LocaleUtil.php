<?php

namespace App\Utils;

use App;
use App\Utils\BuiltIns;
use Config;
use Session;

/**
 * Util to set i18n local
 */
final class LocaleUtil
{
    private function __construct() {}

    public static function changeLocal(string $lang)
    {
        if(in_array($lang, config('app.locales'))){
            Session::put('locale', $lang);
            App::setLocale($lang);
            BuiltIns::setcookie('lang', $lang, time()+3600, "/");
        }
    }
}
