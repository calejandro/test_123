<?php

namespace App\Utils;

use App\JobMailVerificationFeedback;

/**
 * Neverbounce API abstraction
 */
final class Neverbounce
{
    private function __construct() {}

    private static function setApiKey()
    {
        \NeverBounce\Auth::setApiKey(config('app.neverbounce_token'));
    }

    public static function jobCreate(array $inputs, JobMailVerificationFeedback $verificationFeedback)
    {
        self::setApiKey();
        return \NeverBounce\Jobs::create(
            $inputs,
            \NeverBounce\Jobs::SUPPLIED_INPUT,
            '[Eventwise Platform] Import Verification. Job Feedback #' . $verificationFeedback->id,
            false, // run_sample
            true, // autoparse 
            true // autostart
        );
    }

    public static function jobResults(int $jobId, int $page)
    {
        self::setApiKey();
        return \NeverBounce\Jobs::results($jobId, [
            'items_per_page' => 1000,
            'page' => $page
        ]);
    }

    public static function jobStatus(int $jobId)
    {
        self::setApiKey();
        return \NeverBounce\Jobs::status($jobId);
    }

    public static function singleCheck(string $email)
    {
        self::setApiKey();
        return \NeverBounce\Single::check($email, true, true); // email, (addressinfo), (creditsinfo), (maxexecution)
    }
}
