<?php

namespace App\Utils;

final class StringUtil
{
    private function __construct() {}

    public static function randomString(int $length = 8)
    {
        $length = ($length < 4) ? 4 : $length;
        return bin2hex(random_bytes(($length-($length%2))/2));
    }
}
