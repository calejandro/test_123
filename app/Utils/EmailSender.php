<?php

namespace App\Utils;

use \Config;
use Lang;

use App\Email;
use App\Guest;
use App\Company;
use App\Companion;
use App\Event;

use App\Facades\TicketService;
use App\Facades\TemplateService;
use App\Facades\EmailService;
use App\Facades\SessionService;
use Illuminate\Mail\Mailer;

/**
 * Util for email sending
 */
final class EmailSender
{
    private function __construct() {}

    private static function attachGuestTickets(&$message, Guest $guest, Event $event)
    {
        $pdf = TicketService::generateTicketPDF($event, $guest);
        $message->attachData($pdf->output(), Lang::get('models.guest.ticketFileName', [], $guest->language) . ".pdf"); // If QrCode in mail body, we need a ticket too
        $i=0;
        foreach ($guest->companions()->get() as $companion) {
            $i++;
            $pdfCompanion = TicketService::generateCompanionTicketPDF($event, $guest, $companion);
            $message->attachData($pdfCompanion->output(), Lang::get('models.companion.ticketFileName', ['n' => $i], $guest->language) . ".pdf");
        }
    }

    private static function attachCompanionTicket(&$message, Guest $guest, Companion $companion, Event $event)
    {
        $pdfCompanion = TicketService::generateCompanionTicketPDF($event, $guest, $companion);
        $message->attachData($pdfCompanion->output(), Lang::get('models.guest.ticketFileName', [], $guest->language) .  ".pdf");
    }

    private static function setSubjectAndBody(&$message, ?string $firstname, 
        ?string $lastname, ?string $company, ?string $destEmail, ?string $language, ?array $extendedFields, string $accessHash,
        string $qrCode, Email $email, Event $event, $isCompanion = false, $sessions = [], $companion_id = null)
    {
        // Setting mail content depending on guest language
        $definedMailHtml = $email->html;
        $definedMailTitle = $email->title;
    
        // For the moment, only confirm mail support multilingual
        if ($email->isConfirm) {
            $definedMailHtml = $email->getTranslatedAttributeWithFallback('html', $language, $event->getLangsAttribute());
            $definedMailTitle = $email->getTranslatedAttributeWithFallback('title', $language, $event->getLangsAttribute());
        }

        $htmlMail = TemplateService::generate($definedMailHtml, [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'company' => $company,
            'email' => $destEmail,
            'language' => $language,
            'additionalFields' => $extendedFields,
            'accessHash' => $accessHash,
            'qrcode' => $qrCode,
            'decline#url' => TemplateService::buildMergeTagUrl($event, $accessHash, 'decline', $isCompanion),
            'minisite#url' => TemplateService::buildMergeTagUrl($event, $accessHash, 'minisite', $isCompanion),
            'subscribe#url' => TemplateService::buildMergeTagUrl($event, $accessHash, 'subscribe', $isCompanion),
            'survey#url' => TemplateService::buildMergeTagUrl($event, $accessHash, 'survey', $isCompanion),
            'sessions' => TemplateService::buildSessionsElement($event, $email, $sessions)
        ]);
    
        $message->subject($definedMailTitle)->setBody($htmlMail, 'text/html');
    }

    private static function setFrom(&$message, Company $company, ?string $fromColumn, ?array $extendedFields)
    {
        // Default mail from, then testing
        $mailFromAddress = Config::get('mail.from.address');
        $mailFromName = Config::get('mail.from.name');

        // Mail FROM
        if ($company->smtp_from_name != null){
            $mailFromAddress = $company->smtp_from_mail_address;
            $mailFromName = $company->smtp_from_name;
        }

        // Then if column we override config
        if (!is_null($fromColumn) && is_array($extendedFields)){
            if (array_key_exists($fromColumn, $extendedFields) && $extendedFields[$fromColumn] != "") {
                $mailFromName = $extendedFields[$fromColumn];
            }
        }

        // Final message from config
        $message->from($mailFromAddress, $mailFromName);
    }

    public static function sendGuestMail(Mailer $mailer, Guest $guest, Email $email, Event $event, Company $company, bool $containsQrCode, 
        ?string $fromColumn, bool $joinICS, bool $joinTicket)
    {    
        $mailer->send([], [], function($message) use ($guest, $email, $event, $company, 
                $containsQrCode, $fromColumn, $joinICS, $joinTicket) {
    
            // setting a language default
            $language = isset($guest->language) ? $guest->language : $event->getLangsAttribute()[0];

            $sessions = SessionService::sessionsGroupedByCategory($guest);

            self::setSubjectAndBody($message, 
                $guest->firstname, $guest->lastname, $guest->company, $guest->email, $language, $guest->extendedFields, $guest->accessHash,
                $containsQrCode ? '<img style="display:block; width:100%; max-width:200px; margin: 0 auto;" src="' . $message->embedData(TemplateService::generateCheckinQrCode($guest->accessHash), "qr.png") . '">' : '', 
                $email, $event, false,
                $sessions);
        
            $message->to($guest->email);
    
            self::setFrom($message, $company, $fromColumn, $guest->extendedFields);
        
            // Attached ICS calendar file if needed
            if ($joinICS) {
                $message->attachData(EmailService::generateICSFile($event, $guest), "invitation_calendar.ics");
            }
        
            // Attach pdf ticket file if needed
            if ($joinTicket) {
                self::attachGuestTickets($message, $guest, $event);
            }
        
            // Attach qrcode image file if mail body contains it
            if ($email->isConfirm && $containsQrCode) {
                $message->attachData(TemplateService::generateCheckinQrCode($guest->accessHash), "invitation_qrcode.png"); // Needed to display inside mail body
            }
        });
    }

    public static function sendCompanionMail(Mailer $mailer, Companion $companion, Guest $guest, Email $email, Event $event, Company $company, bool $containsQrCode, 
        ?string $fromColumn, bool $joinICS, bool $joinTicket)
    {    
        $mailer->send([], [], function($message) use ($companion, $guest, $email, $event, $company, 
                $containsQrCode, $fromColumn, $joinICS, $joinTicket) {

            $sessions = SessionService::sessionsGroupedByCategory($companion);
    
            self::setSubjectAndBody($message, 
                $companion->firstname, $companion->lastname, $companion->company, $companion->email, $guest->language, [], $companion->access_hash,
                $containsQrCode ? '<img style="display:block; width:100%; max-width:200px; margin: 0 auto;" src="' . $message->embedData(TemplateService::generateCheckinQrCode($companion->access_hash), "qr.png") . '">' : '', 
                $email, $event, true, $sessions, $companion->id);
        
            $message->to($companion->email);
    
            self::setFrom($message, $company, $fromColumn, []);
        
            // Attached ICS calendar file if needed
            if ($joinICS) {
                $message->attachData(EmailService::generateICSFile($event, $guest), "invitation_calendar.ics"); // TODO for companion ?
            }
        
            // Attach pdf ticket file if needed
            if ($joinTicket) {
                self::attachCompanionTicket($message, $guest, $companion, $event);
            }
        
            // Attach qrcode image file if mail body contains it
            if ($email->isConfirm && $containsQrCode) {
                $message->attachData(TemplateService::generateCheckinQrCode($companion->access_hash), "invitation_qrcode.png"); // Needed to display inside mail body
            }
        });
    }

}
