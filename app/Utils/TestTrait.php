<?php

namespace App\Utils;

use App\Company;
use App\User;
use App\Permission;
use App\Event;
use App\EmailType;
use DateTime;

use Log;

trait TestTrait {
    /**
     * Set "ActingAs" and create new user
     */
    private function setTestBoy(Company $company, iterable $permissions)
    {
        $perms = Permission::whereIn('name', $permissions)->get();
        $u = factory(User::class, 1)
            ->create()
            ->each(function ($u) use ($company, $perms) {
                $company->users()->save($u);
                $u->permissions()->attach($perms->pluck('id'));
            })[0];

        $this->actingAs($u);
        return $u;
    }

    private function getAllPermsNamesExcept(String $exept)
    {
        return Permission::where('name', '!=', $exept)->pluck('name');
    }

    private function assertUserExists(array $rawUser, $id = null)
    {
        if ($id != null) {
            $u = User::find($id);
        }
        else {
            $u = User::where('email', $rawUser['email'])->first();
        }

        $this->assertTrue($u->name === $rawUser['name']);
        $this->assertTrue($u->email === $rawUser['email']);
    }

    private function assertEventsSame(array $rawEvent, Event $event)
    {
        // assert Same
        $this->assertTrue($rawEvent['name_fr'] === $event->name_fr);
        $this->assertTrue((array_key_exists('description_fr', $rawEvent) ? $rawEvent['description_fr'] : null) === $event->description_fr);
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $event->start_date);
        $this->assertTrue($rawEvent['start_date'] === $d->format('d/m/Y H:i'));
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $event->end_date);
        $this->assertTrue($rawEvent['end_date'] === $d->format('d/m/Y H:i'));
        $this->assertTrue($rawEvent['nb_places'] === $event->nb_places);
        $this->assertTrue($rawEvent['address'] === $event->address);
        $this->assertTrue($rawEvent['type_id'] === $event->type->id);
        $this->assertTrue($rawEvent['advanced_minisite_mode'] == $event->advanced_minisite_mode);
        
        $this->assertTrue((array_key_exists('organisator_email', $rawEvent) ? $rawEvent['organisator_email'] : null) === $event->organisator_email);
        $this->assertTrue((array_key_exists('organisator_lastname', $rawEvent) ? $rawEvent['organisator_lastname'] : null) === $event->organisator_lastname);
        $this->assertTrue((array_key_exists('organisator_firstname', $rawEvent) ? $rawEvent['organisator_firstname'] : null) === $event->organisator_firstname);
    }

    private function assertEventsCopy(Event $eventSrc, Event $event)
    {
        // assert Same
        $rawEvent = $eventSrc->toArray();
        $this->assertTrue((array_key_exists('description', $rawEvent) ? $rawEvent['description'] : null) === $event->description);
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $event->start_date);
        $d2 = DateTime::createFromFormat('Y-m-d H:i:s', $rawEvent['start_date']);
        $this->assertTrue($d2->format('d/m/Y H:i:s') === $d->format('d/m/Y H:i:s'));
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $event->end_date);
        $d2 = DateTime::createFromFormat('Y-m-d H:i:s', $rawEvent['end_date']);
        $this->assertTrue($d2->format('d/m/Y H:i:s') === $d->format('d/m/Y H:i:s'));
        $this->assertTrue($rawEvent['nb_places'] === $event->nb_places);
        $this->assertTrue($rawEvent['address'] === $event->address);
        $this->assertTrue($rawEvent['type_id'] === $event->type->id);
        $this->assertTrue($rawEvent['advanced_minisite_mode'] == $event->advanced_minisite_mode);
        
        $this->assertTrue((array_key_exists('organisator_email', $rawEvent) ? $rawEvent['organisator_email'] : null) === $event->organisator_email);
        $this->assertTrue((array_key_exists('organisator_lastname', $rawEvent) ? $rawEvent['organisator_lastname'] : null) === $event->organisator_lastname);
        $this->assertTrue((array_key_exists('organisator_firstname', $rawEvent) ? $rawEvent['organisator_firstname'] : null) === $event->organisator_firstname);
    
        // assert Dependencies
        $this->assertTrue($eventSrc->controller->password === $event->controller->password);

        if($eventSrc->ticket != null) {
            $this->assertTrue($eventSrc->ticket->id != $event->ticket->id); // assert this is a copy
            $this->assertTrue($eventSrc->ticket->description === $event->ticket->description); // assert same content
        }
    }

    private function assertEventValid(Event $event)
    {
        // assert Valid
        $this->assertTrue($event->emails()->where('type', EmailType::CONFIRMATION)->count() === 1);
        $this->assertTrue($event->emails()->where('type', EmailType::EVENT_FULL_NOTIFICATION)->count() === 1);
        $this->assertTrue($event->emails()->where('type', EmailType::COMPANIONS_CONFIRMATION)->count() === 1);
        $this->assertTrue($event->emails()->count() === 3);

        // Emails
        $this->assertTrue($event->controller != null);

        // Controller
        $this->assertTrue($event->controller != null);

        // Subscribe form
        $this->assertTrue($event->subscribeForm->formTemplate != null);
        $this->assertTrue($event->subscribeForm->formHtml != null);

        // Survey form
        $this->assertTrue($event->surveyForm->formTemplate != null);
        $this->assertTrue($event->surveyForm->formHtml != null);
    }

    private function assertModelsSame($a, $b, $key = 'id')
    {
        $expected = $a->pluck($key);
        $ids = $b->pluck($key);
        $this->assertTrue(count($ids->diff($expected)) === 0);
        $this->assertTrue(count($expected->diff($ids)) === 0);
    }
}