<?php

namespace App\Utils;

define('TESTING', env('TESTING', false));

class BuiltIns
{
    public static function setcookie($l, $s, $t, $p)
    {
        if (!TESTING) {
            return setcookie($l, $s, $t, $p);
        }
        else {
            return true;
        }
    }

    public static function isUnitTestRunning() 
    {
        return TESTING;
    }
}
