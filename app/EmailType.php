<?php

namespace App;

abstract class EmailType
{
    const INVITATION = 'INVITATION';
    const CONFIRMATION = 'CONFIRMATION';
    const COMPANIONS_CONFIRMATION = 'COMPANIONS_CONFIRMATION';
    const EVENT_FULL_NOTIFICATION = 'EVENT_FULL_NOTIFICATION';

    static function all() : array
    {
        return [self::INVITATION, self::CONFIRMATION, self::COMPANIONS_CONFIRMATION, self::EVENT_FULL_NOTIFICATION];
    }
}
