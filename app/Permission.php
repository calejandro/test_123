<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name : varchar(255)
 * - description : varchar(255)
 */
class Permission extends Model
{
  protected $table = 'permissions';

  protected $primaryKey = 'id';
}
