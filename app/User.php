<?php

namespace App;

use App\Permission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Attributes
 * - id : int(10) unsigned
 * - company_id : int(10) unsigned
 * - name : varchar(255)
 * - email : varchar(255)
 * - password : varchar(255)
 * - remember_token : varchar(255)
 * - created_at : Date
 * - updated_at : Date
 * - event_id : int(10) unsigned
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $appends = ['controller'];

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setDefaultPermissions()
    {
      $permissions = Permission::all();
      $this->permissions()->detach($permissions);
      $this->permissions()->attach($permissions);
    }

    public function setDefaultControllerPermissions()
    {
      $permissions = Permission::whereIn('id', [12, 13, 14])->get();
      $this->permissions()->detach($permissions);
      $this->permissions()->attach($permissions);
    }

    public function isController()
    {
        return $this->event_id != null;
    }

    public function isAdmin()
    {
        return $this->company_id === null;
    }

    // Accessors

    public function getControllerAttribute()
    {
      return $this->isController();
    }

    // Queries

    public function scopeNotController($query)
    {
        return $query->whereNull('event_id');
    }

    // Relations

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
