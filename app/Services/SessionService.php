<?php

namespace App\Services;

use App\Session;
use App\Guest;
use App\Companion;

use App\Http\Requests\Guest\GuestSubscribeNominativeRequest;

use Illuminate\Support\Collection;

use Auth;
use Log;

class SessionService
{
    /**
     * Log for debug purpose only.
     * TODO: remove
     */
    public function logSessionDebug($call)
    {
        $user = Auth::user();
        $sessions = Session::withTrashed()->orderBy('id', 'asc')->pluck('id');
        $idPrev = null;
        foreach ($sessions as $id) {
            if ($idPrev !== null) {
                if ($id !== $idPrev+1) {
                    // hole
                    Log::info("[DELETE SESSION DEBUG] [SESSIONS LOGS] [SessionService::logSessionDebug] [" . $call . "] session hole discoverd, id should be " . ($idPrev+1));
                }
            }
            $idPrev = $id;
        }
    }

    /**
     * Verify that enough places remains in the wanted sessions for guest and nominative companions
     * @param $orgGuest: If provided, will check with allready subscribed sessions
     * @return collection of full sessions
     */
    public function checkGuestAndNominativeCompanionsCanRegister(GuestSubscribeNominativeRequest $request, ?Guest $orgGuest) : Collection
    {
        $sessionIdPlaces = [];
        foreach ($request->sessions as $sessionId) {
            $sessionIdPlaces[$sessionId] = 1;
        }
        if (!is_null($request->companionsData)) {
            foreach ($request->companionsData as $companionData) {
                if (array_key_exists('sessions', $companionData)) {
                    foreach ($companionData['sessions'] as $sessionId) {
                        if (array_key_exists($sessionId, $sessionIdPlaces)) {
                            $sessionIdPlaces[$sessionId] = $sessionIdPlaces[$sessionId] + 1;
                        }
                        else {
                            $sessionIdPlaces[$sessionId] = 1;
                        }
                    }
                }
            }
        }

        $sessions = Session::whereIn('id', array_keys($sessionIdPlaces))->get();
        $sessionPlaces = [];
        foreach ($sessionIdPlaces as $sessionId => $places) {
            $sessionPlaces[] = [
                $sessions->firstWhere('id', $sessionId),
                $places
            ];
        }

        return $this->checkGuestCanRegisterToAll($orgGuest, $sessionPlaces);
    }

    /**
     * Register guest to session if place remains and guest isn't allready subscribed to a session
     * Accept only to subscribe on not allready subscribed session
     * Return false if guest want to unsubscribe or reduce his places
     * 
     * @param $guest
     * @param $sessionsPlaces: [[Session, int], [Session, int], ...]
     */
    public function checkGuestCanRegisterWithoutChange(Guest $guest, array $sessionsPlaces) : bool
    {
        foreach ($sessionsPlaces as $sessionPlace) {
            $session = $sessionPlace[0];
            $places = $sessionPlace[1];

            $registeredSession = $guest->sessions()->where('sessions.id', $session->id)->first(); // TODO:companions: Change with "ALLSESSIONREGISTERED WITH COMPANIONS"

            if ($registeredSession != null && $registeredSession->pivot->places > $places) {
                return false;
            }
        }  
        return true;
    }

    /**
     * Only check place remaining
     * check with companions
     * @param $guest: if null => 0 place reserved
     * @param $sessionsPlaces: [[Session, int], [Session, int], ...]
     */
    public function checkGuestCanRegisterToAll(?Guest $guest, array $sessionsPlaces) : Collection
    {
        $fullSessions = collect([]);
        foreach($sessionsPlaces as $sessionPlace) {
            $wantedSession = $sessionPlace[0];
            $nbPlacesWanted = $sessionPlace[1];
            $nbPlacesRegistered = $guest == null ? 0 : $guest->countSessionPlaces($wantedSession);

            if (!$this->checkGuestCanRegister($wantedSession, $nbPlacesWanted, $nbPlacesRegistered)) {
                $fullSessions->push($wantedSession);
            }
        }
        return $fullSessions;
    }

    /**
     * Only check place remaining
     */
    public function checkGuestCanRegister(Session $session, int $places, int $reservedPlaces=0) : bool
    {
        return $session->placesReserved + $places - $reservedPlaces <= $session->places;
    }

    /**
     * Return true if session will be full after subscribe with companions
     */
    public function isSessionFullAfterSubscribe(Guest $guest, Session $session, $places) : bool
    {
        $reservedPlaces = $guest->countSessionPlaces($session);
        return $session->placesReserved + $places - $reservedPlaces >= $session->places;
    }


    /*
    * Verify if an user want unregister
    * @param $sessionsPlaces: [[Session, int], [Session, int], ...]
    */
    public function checkGuestTryToUnRegister(Guest $guest, array $sessionsPlaces) : bool
    {
        foreach($sessionsPlaces as $sessionPlace) {
            if ($this->checkGuestTryToRemovePlaces($guest, $sessionPlace[0], $sessionPlace[1])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if guest try to remove place for him or for companions
     */
    public function checkGuestTryToRemovePlaces(Guest $guest, Session $session, int $places) : bool
    {
        $reservedPlaces = $guest->countSessionPlaces($session);
        return $places < $reservedPlaces;
    }

    /**
     * Check guest is regiustered to a session. WITHOUT companions
     */
    public function checkGuestIsRegistered(Guest $guest, Session $session) : bool
    {
        return $guest->sessions()->where('sessions.id', $session->id)->first() != null;
    }

    /**
     * Detach all sessions and keep only provided
     * No places check, use "checkGuestAndNominativeCompanionsCanRegister" or else
     * @param $guest: Guest
     * @param $session: Session[]
     */
    public function attachGuest(Guest $guest, Collection $sessions)
    {
        $guest->sessions()->detach();
        foreach ($sessions as $session) {
            $session->guests()->attach($guest->id);
        }
    }

    /**
     * Detach all sessions and keep only provided
     * No places check, use "checkGuestAndNominativeCompanionsCanRegister" or else
     * @param $companion: Companion
     * @param $session: Session[]
     */
    public function attachCompanion(Companion $companion, Collection $sessions)
    {
        $companion->sessions()->detach();
        foreach ($sessions as $session) {
            $session->companions()->attach($companion->id);
        }
    }

    /**
     * Register the guest in the session with all this companions
     * if guest is allready subscribed, then update the existing
     * This function can remove companions session registering
     * if Guest ask more places than it allready has companions, then it's refused
     * can return false!
     */
    public function registerGuestAndCompanions(Guest $guest, Session $session, int $places) : bool
    {
        $reservedPlaces = $guest->countSessionPlaces($session);
        $wantedPlaces = $places - $reservedPlaces;

        if ($wantedPlaces > 0) { // he want more places

            // Check that we have enough places for all
            if (!$this->checkGuestCanRegister($session, $places, $reservedPlaces)) {
                return false;
            }

            // Check that guest has enough companions
            if ($guest->companions()->count() + 1 < $places) {
                return false;
            }

            // Attach himself if it isn't done
            $guestSession = $guest->sessions()->where('sessions.id', $session->id)->first();
            if ($guestSession == null) {
                $session->guests()->attach($guest->id);
                $wantedPlaces = $wantedPlaces - 1;
            }

            // Attach as many more companions as necessary
            foreach ($guest->companions()->unRegisteredToSession($session)->get() as $companion) {
                if ($wantedPlaces > 0) {
                    $session->companions()->attach($companion->id);
                    $wantedPlaces = $wantedPlaces - 1;
                }
                else {
                    break;
                }
            }
        }
        else if ($wantedPlaces < 0) { // he want less places

            $unWantedPlaces = abs($wantedPlaces);
            // Detach as many companions as necessary, guest in last
            foreach ($session->companions()->where('guest_id', $guest->id)->get() as $companion) {
                if ($unWantedPlaces > 0) {
                    $session->companions()->detach($companion->id);
                    $unWantedPlaces = $unWantedPlaces - 1;
                }
                else {
                    break;
                }
            }

            // Deteach guest if it is necessary
            if($unWantedPlaces > 0) {
                $session->guests()->detach($guest->id);
            }
        }

        return true;
    }

    /**
     * Unregister guest and all his companions
     */
    public function unRegisterGuestAndCompanions(Guest $guest, Session $session)
    {
        $session->guests()->detach($guest->id);
        foreach ($session->companions()->where('guest_id', $guest->id)->get() as $companion) {
            $session->companions()->detach($companion->id);
        }
    }

    /**
     * Checkin (or uncheckin) guest to a Session. NOT companions
     */
    public function checkinGuest(Guest $guest, Session $session, bool $checkin = true)
    {
        if ($this->checkGuestIsRegistered($guest, $session)) {
            // update checkin
            $session->guests()->updateExistingPivot($guest->id, [
                'checkin' => $checkin
            ]);
        }
        else {
            // create relation
            $session->guests()->attach($guest->id, [
                'places' => 0,
                'checkin' => $checkin
            ]);
        }
    }

    public function stolePlace(Companion $new, Companion $old, Session $session, bool $checkin=true)
    {
        $session->companions()->detach($old->companion_id);
        $session->companions()->attach($new->id, [
            'checkin' => $checkin
        ]);
    }

    /**
     * Checkin a companion to a session
     * If param $stole == true: A companion can Stole a place to another (or the guest) if the other hasn't allready checkin
     * @param $stole: if true: permit places stoling
     * @return bool: if false: No more places.
     */
    public function checkinCompanion(Companion $companion, Session $session, bool $stole) : bool
    {
        $guest = $companion->guest;
        $companionSession = $companion->sessions()
            ->wherePivot('session_id', '=', $session->id)
            ->first();

        if ($companionSession != NULL && $companionSession->pivot->checkin) { // Allready checkin
            return true;
        }
        else if ($companionSession != NULL) {
            // The relation exists, let's checkin
            $session->companions()->updateExistingPivot($companion->id, [
                'checkin' => true
            ]);
            return true;
        }
        else {
            if (!$stole) {
                return false;
            }

            // Look for a place
            $otherCompanions = $guest->companions()->where('id', '<>', $companion->id)->whereHas('notCheckinSessions', function ($q) use ($session) {
                $q->where('sessions.id', $session->id);
            })->get();

            if ($otherCompanions->count() > 0) {
                // Stole to a companion
                $this->stolePlace($companion, $otherCompanions->first(), $session, true);
                return true;
            }
            else {
                // Stole to a guest
                $guestSession = $guest->sessions()->where('sessions.id', $session->id)->first();
                if ($guestSession === null) {
                    // Guest hasn't place
                    return false;
                }
                else if ($guestSession->pivot->checkin <= 0) {
                    // Guest has a place to stole
                    $session->guests()->detach($guest->id);
                    $session->companions()->attach($companion->id, [
                        'checkin' => true
                    ]);
                    return true;
                }
                else {
                    // Guest is allready checkin
                    return false;
                }
            }
        }
    }

    /**
     * Cancel checkin of a companion to a session
     */
    public function unCheckinCompanion(Companion $companion, Session $session)
    {
        if ($companion->isCheckinToSession($session)) {
            $session->companions()->updateExistingPivot($companion->id, [
                'checkin' => false
            ]);
        }
    }

    /**
     * Checkin (or uncheckin) guest to a Session with companions
     * 
     * WARNING: no relation will be created. The reserved places can't be surpassed !
     * Will return false if no places remains
     */
    public function checkinGuestAndCompanion(Guest $guest, Session $session, int $checkin)
    {
        if ($checkin > 0) {
            // Check checkin is possible
            $notCheckinCompanions = $guest->companions()->registeredToSession($session)->notCheckinToSession($session)->get();
            $nbNotCheckinPlaces = ($this->checkGuestIsRegistered($guest, $session) && !$guest->isCheckinToSession($session)) ? 1 : 0;
            $nbNotCheckinPlaces = $nbNotCheckinPlaces + $notCheckinCompanions->count();

            if ($checkin > $nbNotCheckinPlaces) {
                return false;
            }

            // Add checkin
            // Checkin Guest if it isn't done
            if ($this->checkGuestIsRegistered($guest, $session) && !$guest->isCheckinToSession($session)) {
                $guest->sessions()->updateExistingPivot($session->id, [
                    'checkin' => true
                ]);
                $checkin = $checkin - 1;
            }

            // Checkin companions
            foreach ($notCheckinCompanions as $companion) {
                if ($checkin > 0) {
                    $companion->sessions()->updateExistingPivot($session->id, [
                        'checkin' => true
                    ]);
                    $checkin = $checkin - 1;
                }
                else {
                    break;
                }
            }
        }
        else if ($checkin < 0) {
            // Remove checkin
            $checkin = abs($checkin);

            // Check uncheckin is possible
            $checkinCompanions = $guest->companions()->checkinToSession($session)->get();
            $nbCheckinPlaces = ($this->checkGuestIsRegistered($guest, $session) && $guest->isCheckinToSession($session)) ? 1 : 0;
            $nbCheckinPlaces = $nbCheckinPlaces + $checkinCompanions->count();

            if ($checkin > $nbCheckinPlaces) {
                return false;
            }

            // Uncheckin Guest if it is done
            if ($this->checkGuestIsRegistered($guest, $session) && $guest->isCheckinToSession($session)) {
                $guest->sessions()->updateExistingPivot($session->id, [
                    'checkin' => false
                ]);
                $checkin = $checkin - 1;
            }

            // Uncheckin companions
            foreach ($checkinCompanions as $companion) {
                if ($checkin > 0) {
                    $companion->sessions()->updateExistingPivot($session->id, [
                        'checkin' => false
                    ]);
                    $checkin = $checkin - 1;
                }
                else {
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Build session array of guest and companion with participation
     */
    public function buildSessionListWithTotalGuestPlaces(Guest $guest) : array
    {
        // Add totalGuestPlaces info (count places with companions)
        $sessions = [];
        foreach($guest->sessions as $session) {
            $session->hasGuestPlace = true;
            $session->totalGuestPlaces = 1;
            $session->subscribedCompanions = [];
            $sessions[$session->id] = $session;
        }

        foreach($guest->companions()->with('sessions')->get() as $companion) {
            foreach($companion->sessions as $compSession) {
                if(array_key_exists($compSession->id, $sessions)) {
                    $sessions[$compSession->id]->totalGuestPlaces = $sessions[$compSession->id]->totalGuestPlaces + 1;
                    $comps = $sessions[$compSession->id]->subscribedCompanions;
                    $comps[] = $companion->id;
                    $sessions[$compSession->id]->subscribedCompanions = $comps; 
                }
                else {
                    $compSession->totalGuestPlaces = 1;
                    $compSession->subscribedCompanions = [$companion->id];
                    $compSession->hasGuestPlace = false;
                    $sessions[$compSession->id] = $compSession;
                }
            }
        }

        return array_values($sessions);
    }

    public function sessionsGroupedByCategory($person)
    {
        $sessions = $person->sessions()->with('sessionGroup')->get()->groupBy(function ($item) {
            if (!$item['sessionGroup']) return '';
            return $item['sessionGroup']->name;
        });
        $sessions = array_sort($sessions, function ($value, $key) {
            //dump($value);
            return $key;
        });

        return collect($sessions);
    }
}
