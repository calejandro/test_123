<?php

namespace App\Services;

use App\Event;

use DB;
use Log;

/**
 * Service for "Event" ressource
 */
class EventService
{
    /**
     * Delete extendedField on Event and all Guests and All Companions
     */
    public function removeExtendedField(Event $event, String $field): bool
    {
        $index = array_search($field, $event->extendedFileds);
        if ($index !== FALSE) {
            $ext = $event->extendedFileds;
            unset($ext[$index]);

            $event->guests()->update([
                'extendedFields' => DB::raw("JSON_REMOVE(extendedFields, '$.\"$field\"')")
            ]);

            $event->companions()->toBase()->update([
                'companions.extended_fields' => DB::raw("JSON_REMOVE(extended_fields, '$.\"$field\"')"),
                'companions.updated_at' => now() // necessary to specify to avoid ambiguous update
            ]);

            $event->extendedFileds = $ext;
            $event->save();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Check event extended fields consitency with all guests
     * Rules: 
     * 1. The guests can have less fields than event
     * 2. Event must have at least all fields in guests extended fields
     */
    public function verifyExtendedFieldsConsitency(Event $event): bool
    {
        $guests = $event->guests()->active()->get();

        $guestsFields = [];
        foreach($guests as $guest) {
            if (!is_null($guest->extendedFields)) {
                $guestsFields = array_merge($guestsFields, $guest->extendedFields);
            }
        }
        $guestsFields = array_keys($guestsFields);

        $error = count(array_diff($guestsFields, $event->extendedFileds)) > 0;

        if($error) {
            Log::error("[EventService] verifyExtendedFieldsConsitency issue, guestsFields:" . 
                implode(",", $guestsFields) . ", eventFields: " . 
                implode(",", $event->extendedFileds));
        }

        return $error;
    }
}
