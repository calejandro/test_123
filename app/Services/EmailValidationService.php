<?php

namespace App\Services;

use DB;
use Log;
use GuzzleHttp\Client; // For the neverbounce API call
use Carbon\Carbon;

use App\JobMailVerificationFeedback;
use App\VerifiedEmailAddress;
use App\Guest;
use App\Event;

use App\Utils\Neverbounce;
use App\Utils\Sentry\SentryLogger;

/**
* Service to validate email (with Neverbounce)
* TODO: remove any dependencies to Guests
*/
class EmailValidationService
{
    const MAPPING = [
        "invalid" => 0,
        "valid" => 1,
        "catchall" => 2,
        "unknown" => 3,
        "disposable" => 0
    ]; // Keys depending on the neverbounce API

    public function isOneRunning(Event $event)
    {
        return $event->verificationJobs()->where('finished', false)->count() > 0;
    }

    /**
     * remove "error" status of all validation
     */
    public function removeFailed(Event $event)
    {
        $event->verificationJobs()->where('error', true)->update(['error' => false]);
    }

    public function mapMailValidityResultToCodeValue($result)
    {
      $mapping = self::MAPPING;
  
      $finalValidity = 0;
  
      if (array_key_exists($result, $mapping)) {
        $finalValidity = $mapping[$result];
      }
  
      return $finalValidity;
    }

    /**
    * Check the status of a current batch mail verification distant job (Neverbounce)
    *
    * This function is a heartbeat periodically checking the status of a running
    * distant batch email address verification job. If the job is finished, we
    * trigger the processBatchMailsVerification function to process the results
    * and update the given mail addresses.
    *
    * @see processBatchMailsVerification
    *
    * @param Guest $guest
    *   The new create guest to whom we want to verify the mail address
    *
    * @return bool If true the job has finished, if false it is pending
    */
    public function updateBatchMailsVerificationStatus(int $neverbounceJobId) : bool
    {
      try {
        $result = Neverbounce::jobStatus($neverbounceJobId);
        return $result->job_status == "complete";
      }
      catch(\Exception $e) {
        Log::channel('verification')->critical('[EmailValidationService] Exception on calling NeverBounce API. '. $e);
        SentryLogger::logException($e, [
            'message' => '[EmailValidationService] Exception on calling NeverBounce API.'
        ]);
        return false;
      }
    }

    /**
    * Verify the mail address of a given group of guests.
    *
    * The verification is done by triggering a distant verification job on the
    * Neverbounce API. The job is then processed by the distant service. We need
    * to periodically check the state of the distant job before getting the results
    * once it will be finished. The periodic check is done through another job.
    *
    * @see getEmailAddressAlreadyVerified
    * @see mapMailValidityResultToCodeValue
    *
    * @param iterable<Guest> $guests
    *   Array of guests containing the mail addresses to validate.
    * @param JobMailVerificationFeedback $id
    *   The id of the concerned verification job feedback in the DB.
    */
    public function verifyBatchEmailsValidity(array $filteredGuests, $eventId, JobMailVerificationFeedback $verificationFeedback)
    {
      Log::channel('verification')->info('[EmailValidationService] Triggering a batch email verification for ' . count($filteredGuests) . ' addresses');

      // If we have at least one address to validate
      if(count($filteredGuests) > 0) {

        Log::channel('verification')->info('[EmailValidationService] [verifyBatchEmailsValidity] Calling Neverbounce API to validate batch mail addresses.');

        if (config('app.disable_guests_email_validation')) {
            Log::channel('verification')->info('[EmailValidationService] WARNING: Validation in dev mode');
            $verificationFeedback->nb_addresses = count($filteredGuests);
            $verificationFeedback->save();
            return null;
        }

        try{
            // Create minimal input
            $mapInput = function($guest) {
                return [
                    'email' => $guest['email']
                ];
            };
            $inputs = array_map($mapInput, $filteredGuests);

            Log::channel('verification')->info('[EmailValidationService] [verifyBatchEmailsValidity] input: ');

            $result = Neverbounce::jobCreate($inputs, $verificationFeedback);

            $verificationFeedback->nb_addresses = count($filteredGuests);
            $verificationFeedback->neverbounce_job_id = $result->job_id;
            $verificationFeedback->save();

            //Log::channel('verification')->info('[EmailValidationService] [verifyBatchEmailsValidity] Neverbounce responded with : ' . var_dump($result));
            Log::channel('verification')->info('[EmailValidationService] [verifyBatchEmailsValidity] Called Neverbounce API to validate batch mail addresses. Results will be fetched later.');

        }
        catch(\Exception $e) {
            // Mark job as done to avoid any issues
            $verificationFeedback->nb_addresses = count($filteredGuests);
            $verificationFeedback->error = true;
            $verificationFeedback->finished = true;
            $verificationFeedback->processed = true;
            $verificationFeedback->save();

            Log::channel('verification')->critical('[EmailValidationService] Exception on calling batch NeverBounce API.' . $e);

            SentryLogger::logException($e, [
                'message' => '[EmailValidationService] Exception on calling NeverBounce API.'
            ]);
        }
      }
      else{
        $verificationFeedback->nb_addresses = count($filteredGuests);
        $verificationFeedback->save();

        Log::channel('verification')->info('[EmailValidationService] No addresses to validate.');
      }
    }

    /*
    * @email: mail address to check if already verified
    */
    protected function singleEmailAddressAlreadyVerified($guest)
    {
        // We can hit the DB since it is a simple manual entry added through form
        $verifiedAddress = VerifiedEmailAddress::where('email', strtolower($guest->email))
            ->whereDate('created_at', '>=', Carbon::now()->subMonths(3));
        if($verifiedAddress->exists()){
            Log::channel('verification')->info('[EmailValidationService] [singleEmailAddressAlreadyVerified] Address already verified previously less than three monthes ago.');
            $guest->valid_email = $verifiedAddress->first()->valid_email; // Applying the mail validity to guest
            $guest->save();
            return true;
        }
        else{
            return false;
        }
    }

    /**
    * Verify the mail address of a single given guest.
    *
    * The verification is done through a Neverbounce API call after we tested if
    * the concerned mail address was already checked or not before.
    *
    * @see singleEmailAddressAlreadyVerified
    * @see mapMailValidityResultToCodeValue
    *
    * @param Guest $guest
    *   The new create guest to whom we want to verify the mail address
    */
    public function verifySingleEmailAddressValidity(Guest $guest)
    {
        $finalValidity = $this->mapMailValidityResultToCodeValue('unknown'); // Pessimistic default

        if(!$this->singleEmailAddressAlreadyVerified($guest)){
            Log::channel('verification')->info('[EmailValidationService] (guest: ' . $guest->id . ') [validateSingleEmailAddress] Calling Neverbounce API to validate single mail address.');
            try{
                $result = Neverbounce::singleCheck($guest->email);

                if (isset($result->result)) {
                    $finalValidity = $this->mapMailValidityResultToCodeValue($result->result);
                }
                else {
                    Log::channel('verification')->critical('[EmailValidationService] [validateSingleEmailAddress] Neverbounce returned "null".');
                }

                $guest->valid_email = $finalValidity;

                $verifAddress = new VerifiedEmailAddress;
                $verifAddress->email = $guest->email;
                $verifAddress->valid_email = $finalValidity;
                $verifAddress->save();

                $guest->save();
            }
            catch(\Exception $e){
                Log::channel('verification')->critical('[EmailValidationService] Exception on calling NeverBounce API for single mail validation.');

                SentryLogger::logException($e, [
                    'message' => '[EmailValidationService] Exception on calling NeverBounce API.'
                ]);
            }
        }
    }

    
    /*
    * Check if some addresses of incoming imported guests are already verified.
    *
    * This function gets all the previously verified mail addresses from until three
    * weeks ago and update the guests having an address already previously verified.
    * This function returns an array containing only the guests which have a not
    * already validated address.
    *
    *
    * @param Guest $guests
    *   Array of incoming guests that needs to be validated (or not).
    */
    public function getEmailAddressAlreadyVerified($guests): array
    {
        // We get the already verified addresses
        $verifiedAddresses = VerifiedEmailAddress::whereDate('created_at', '>=', Carbon::now()->subMonths(3))
            ->whereIn('email', $guests->pluck('email')->map(function ($email) {
                return strtolower($email);
            }))
        ->get();

        Log::channel('verification')->info('[EmailValidationService] [getEmailAddressAlreadyVerified] List of asked addresses verified in DB: ' . $verifiedAddresses->count());

        // This is a mapping between verified address and its validity
        $emailsValidity = $verifiedAddresses->pluck('valid_email', 'email'); //email (string) => validity (int)

        // Update the valid_email status of all guests with email
        foreach (self::MAPPING as $text => $mapping) {
            $mappingEmails = $emailsValidity->filter(function ($value, $key) use ($mapping) {
                return $value == $mapping;
            })->keys()->all();

            if (count($mappingEmails) > 0) {
                // check email in lower case
                Guest::whereIn(DB::raw('lower(email)'), $mappingEmails)->update(['valid_email' => $mapping]);
            }
        }

        return $guests->filter(function ($value, $key) use ($verifiedAddresses) {
            // return all email not in verifiedAddresses
            return $verifiedAddresses->where('email', strtolower($value->email))->count() <= 0; // collection query (not sql)
        })->toArray();
    }

    /**
     * Retrieve all Neverbounce results for a job
     */
    public function getNeverbounceJobResults(int $jobId)
    {
      $query = null;
      $results = [];
      $page = 0;

      do {
        $page = $page + 1;
        $query = Neverbounce::jobResults($jobId, $page);
        $page = $query->query['page'];
        $results = array_merge($results, $query->results);

        Log::channel('verification')->info('[EmailValidationService] [getNeverbounceJobResults] Page '. $page .' retrieved (total pages: '. $query->total_pages .'), results: ' . count($query->results));

      } while ($query->total_pages > $page);

      Log::channel('verification')->info('[EmailValidationService] [getNeverbounceJobResults] All results: ' . count($results));
      return $results;
    }

}
