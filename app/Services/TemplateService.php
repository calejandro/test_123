<?php

namespace App\Services;

use App\Email;
use DbView;
use App\Event;
use App\Guest;
use App\Minisite;
use App\Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Faker;

class TemplateService
{   

    // Used by buildMergeTagUrl to generate merge tag urls
    private $urlTypeToDefaultRoutesMapping = [
        "decline" => "events.guests.decline",
        "survey" => "events.surveyForm.show",
        "subscribe" => "events.subscribeForm.show",
        "minisite" => "events.subscribeForm.show" // Same as sub for simple sites
    ];

    /**
     * Render template with fake data
     */
    public function generatePreview($template, Event $event, Email $email)
    {
        $faker = Faker\Factory::create('fr_FR');

        $testSessions = new Session();
        $testSessions->name_en = 'Test Session EN';
        $testSessions->name_fr = 'Test Session DE';
        $testSessions->name_de = 'Test Session FR';
        $testSessions->places = 22;
        $sessions = collect(["" => [$testSessions]]);
        
        return TemplateService::generate($template, [
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'email' => $faker->email,
            'additionalFields' => [],
            'accessHash' => $event->getDemoHash(),
            'qrcode' => TemplateService::generateCheckinQrCodeImage(TemplateService::generateCheckinQrCode($event->getDemoHash())),
            'sessions' => TemplateService::buildSessionsElementPreview($event, $email, null)
        ], true);
    }

    /**
    *   Generate template
    *   @param $ressource: ressource containing the field
    *   @param $ressourceField: field containing the html/blade template
    *   @param $with: array containing key, value of data to insert
    *   @param $previewMode: will show empty extFields values in template
    *   @return generated HTML
    */
    public function generate($template, $with, $previewMode=false)
    {
        /**try
        {
            return DbView::make($ressource)->field($ressourceField)->with($with)->render();
        }
        catch(\Exception $e)
        {
            return 'Ooups une erreur s\'est glissée dans le template!';
        }**/

        return TemplateService::generateCustom($template, TemplateService::transformBindings($with), $previewMode);
    }

    /**
     * take a list as:
     * [
     *      'key' => 'value',
     *      'key' => 'value',
     *      'key' => [
     *          'key_nested' => 'value',
     *          'key_nested' => 'value'
     *       ]
     * ]
     * 
     * and transform it to:
     * [
     *      'key' => 'value',
     *      'key' => 'value',
     *      'key['key_nested']' => 'value',
     *      'key['key_nested']' => 'value'
     * ]
     */
    public function transformBindings($bindings)
    {
        $parsed = [];
        foreach($bindings as $key => $value)
        {
            if(is_array($value))
            {
                foreach($value as $nestedKey => $nestedValue)
                {
                    $v = $nestedValue;
                    if(is_array($nestedValue)) {
                        $v = json_encode($nestedValue);
                    }
                    // $parsed[$key.'['.$nestedKey.']'] = $v;
                    $parsed[$nestedKey] = $v;
                }
            }
            else
            {
                $parsed[$key] = $value;
            }
        }
        return $parsed;
    }

    private function retrieveBinding($value, $with)
    {
        if(preg_match('/\'\H*\'/', $value))
        {
            return str_replace("'", '', $value);
        }
        else
        {
            if(array_key_exists($value, $with))
            {
                return $with[$value];
            }
            else
            {
                return $value;
            }
        }
    }

    /**
     * Cond:
     * [[IF value == 'value' THEN 'value3']]
     * binding: [[value]], [[value['key']]]
     */
    public function generateCustom($template, $with, $previewMode = false)
    {
        $parsedTemplate = $template;

        // "IF" binding
        $condMatches = [];
        $pattern = '/\[\[\s*IF\s+\H+\s+==\s+\H+\s+THEN\s+\H+\s*\]\]/';
        preg_match_all($pattern, $parsedTemplate, $condMatches);
        if(count($condMatches) > 0){
            
            foreach($condMatches[0] as $match)
            {
                $data = explode(' ', $match);
                $v1 = TemplateService::retrieveBinding($data[1], $with);
                $v2 = TemplateService::retrieveBinding($data[3], $with);
                $do = TemplateService::retrieveBinding(str_replace(']]', '', $data[5]), $with);

                if($v1 === $v2)
                {
                    $parsedTemplate = str_replace($match, $do, $parsedTemplate);
                }
                else
                {
                    $parsedTemplate = str_replace($match, '', $parsedTemplate);
                }
            }
        }

        // key binding
        foreach($with as $key => $value)
        {
            $parsedTemplate = str_replace('[['.$key.']]', $value, $parsedTemplate);
        }

        if (!$previewMode) {
            $condMatches = [];
            $pattern = '/\[\[[^(\]\])]*\]\]/';
            preg_match_all($pattern, $parsedTemplate, $condMatches);
            foreach($condMatches[0] as $match) {
                $parsedTemplate = str_replace($match, '', $parsedTemplate);
            }
        }

        return $parsedTemplate;
    }

    /**
    *   Genereate maps HTML
    */
    public function generateMap($address, $height)
    {
        return '<iframe 
                src="https://www.google.com/maps/embed/v1/place?key=' . config('app.google_map_access_token') . '&q=' . $address . '" 
                width="100%" 
                height="' . $height . '"
                frameborder="0" 
                style="border:0" 
                allowfullscreen></iframe>';
    }

    public function generateCheckinQrCode(string $accessHash)
    {
        // fixed string url to avoid strange error (double escaping) with route()
        return QrCode::format('png')
            ->size(200)
            ->generate((config('app.https') ? 'https://' : 'http://') .config('app.url').'/guests/checkin/'.$accessHash);
    }

    public function generateCheckinQrCodeImage($qrCode)
    {
        return html_entity_decode('<img style="display:block; margin: 0 auto; width:100%; max-width:200px" src="data:image/png;base64, ' . base64_encode($qrCode) . '">');
    }

    /*
    *
    * Function used to build urls that are injected in emails when generated (template).
    * Injected link happen when one of the tags [[xxxx#url]] is used.
    *
    */
    public function buildMergeTagUrl(Event $event, String $accessHash, String $urlType, $isCompanion = false)
    {
        if($urlType === "decline"){ // Direct return because decline url is always the same and unconditional
            return (config('app.https') ? 'https://' : 'http://') . config('app.url') . route($this->urlTypeToDefaultRoutesMapping[$urlType], ['hash' => $accessHash], false);
        }

        if($event->advanced_minisite_mode){ // Is an advanced minisite
            if(isset($event->minisite)){ // Minisite exists
                // Generating link for advanced minisite
                if($urlType === "minisite"){
                    return $event->minisite->indexLink($accessHash);
                }else if($urlType === "subscribe"){ // subscribe link
                    return $event->minisite->subscribeFormLink($accessHash, true, $isCompanion);
                }else if($urlType == "survey"){
                    return $event->minisite->surveyFormLink($accessHash);
                }
            }else{
                return ""; // Empty link since minisite does not exist
            }

        }

        // Simple minisite cases
        return (config('app.https') ? 'https://' : 'http://') . config('app.url') . route($this->urlTypeToDefaultRoutesMapping[$urlType], ['hash' => $accessHash], false);
    }

    public function buildSessionsElement($event, $email, $sessions){
        if(!$sessions || $sessions->count() < 1) return "";
        
        if($email->sessions_style){
            $cssStyleTitle = TemplateService::css_array_to_css(json_decode($email->sessions_style)->title);
            $cssStyleCategory = TemplateService::css_array_to_css(json_decode($email->sessions_style)->category);
            $cssStyleSessions = TemplateService::css_array_to_css(json_decode($email->sessions_style)->sessions);
        }else{
            $cssStyleTitle = "";
            $cssStyleCategory = "";
            $cssStyleSessions = "";
        }

        return view('emails_templates.sessions.subscription_recap', [
            'sessions' => $sessions,
            'event' => $event,
            'title_style' => $cssStyleTitle,
            'category_style' => $cssStyleCategory,
            'sessions_style' => $cssStyleSessions,
          ])->render();
    }

    public function buildSessionsElementPreview($event, $email, $sessions){
        if(!$sessions || $sessions->count() < 1)
        {
            $testSessions = new Session();
            $testSessions->name_en = 'Test Session EN';
            $testSessions->name_fr = 'Test Session DE';
            $testSessions->name_de = 'Test Session FR';
            $testSessions->places = 22;
            $sessions = collect(["Test category" => [$testSessions], "" => [$testSessions]]);
        }

        return TemplateService::buildSessionsElement($event, $email, $sessions);
    }

    /**
   * converts an object into a string of property:value; format.
   *
   * @param array $rules
   *   An array of CSS rules in the form of:
   *   array('selector'=>array('property' => 'value')). Also supports selector
   *   nesting, e.g.,
   *   array('selector' => array('selector'=>array('property' => 'value'))).
   *
   * @return string A CSS string of rules. This is not wrapped in <style> tags.
   * @source http://matthewgrasmick.com/article/convert-nested-php-array-css-string
   */
  function css_array_to_css($rules, $indent = 0) {
    if(!$rules) return '';
    $css = '';

    foreach ($rules as $key => $value) {
        $css .= "$key: $value;";
    }

    return $css;
  }
}
