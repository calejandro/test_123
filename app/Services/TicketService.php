<?php

namespace App\Services;

use App\Event;
use App\Ticket;
use App\Guest;
use App\Companion;
use App\Facades\SessionService;
use App\Facades\TemplateService;

use PDF;
use DB;
use Log;

/**
 * Service for "Ticket" ressource
 */
class TicketService
{
    /**
     * Generate and returns PDF ticket with access QrCode
     */
    public function generateTicketPDF(Event $event, ?Guest $guest, bool $demo=false, $language=null)
    {
      $hash = $demo ? 'DemoQrCode' : $guest->accessHash;
      $qrcode = TemplateService::generateCheckinQrCodeImage(TemplateService::generateCheckinQrCode($hash));

      // Getting the specific ticket template as string to call correct blade template
      $ticketType = $event->ticket()->first()->ticketType()->first();
      $ticketTemplate = $ticketType === NULL ? 'render' : $ticketType->template;

      $company = null;

      // Defining the company field (depending on the sup_info_field)
      if ($demo) {
        $company = "COMPANY";
      }
      elseif (is_null($event->ticket->sup_info_field)) {
        $company = $guest->company;
      }
      elseif ($guest->extendedFields != null &&
        array_key_exists($event->ticket->sup_info_field, $guest->extendedFields)) {
        // Compatibility code, deprecated since v5.8.1
        $company = $guest->extendedFields[$event->ticket->sup_info_field];
      }

      // Defining the ticket language
      $lang = is_null($language) ? $event->getLangsAttribute()[0] : $language;
      if(!$demo && !is_null($guest) && !is_null($guest->language) && in_array($guest->language, $event->getLangsAttribute())){
        $lang = $guest->language;
      }

      $sessions = $demo ? 
        collect([]) : 
        // $guest->sessionsGroupedByCategory();
        SessionService::sessionsGroupedByCategory($guest);

        

      $html = view('tickets.layouts.' . $ticketTemplate, [
        'demo' => $demo,
        'event' => $event,
        'qrcode' => $qrcode,
        'guest' => $guest,
        'email' => $demo ? "exemple@mail.com" : $guest->email,
        'firstname' => $demo ? "Exemple" : $guest->firstname,
        'lastname' => $demo ? "Exemple" : $guest->lastname,
        'extendedFields'  => $demo ? [] : $guest->extendedFields,
        'company' => $company,
        'sessions' => $sessions,
        'lang' => $lang
      ])->render(); // Generate ticket HTML page

      return PDF::loadHtml($html)->setPaper('a4'); // Returns dompdf object (not yet a file !)
    }

    public function generateCompanionTicketPDF(Event $event, Guest $guest, Companion $companion)
    {
      $qrcode = TemplateService::generateCheckinQrCodeImage(TemplateService::generateCheckinQrCode($companion->access_hash));

      // Getting the specific ticket template as string to call correct blade template
      $ticket_type = $event->ticket()->first()->ticketType()->first();
      $ticket_template = $ticket_type === NULL ? 'render' : $ticket_type->template;

      $firstname = $guest->firstname;
      $lastname = $guest->lastname;
      $email = $guest->email;
      $company = "";

      if($event->nominative_companions){
        $firstname = $companion->firstname;
        $lastname = $companion->lastname;
        $email = $companion->email;
        $company = $companion->company != null ? $companion->company : "";
      }

      // Defining the ticket language
      $lang = config('app.fallback_locale');
      if(!is_null($guest) && !is_null($guest->language)){
        $lang = $guest->language;
      }

      // $sessions = $companion->sessionsGroupedByCategory();
      $sessions = SessionService::sessionsGroupedByCategory($companion);

      $html = view('tickets.layouts.' . $ticket_template, [
        'demo' => false,
        'event' => $event,
        'qrcode' => $qrcode,
        'guest' => $guest,
        'email' => $email,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'company' => $company,
        'extendedFields'  => $guest->extendedFields,
        'sessions' => $sessions,
        'lang' => $lang
      ])->render(); // Generate ticket HTML page

      return PDF::loadHtml($html)->setPaper('a4'); // Returns dompdf object (not yet a file !)
    }
}
