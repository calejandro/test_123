<?php

namespace App\Services;

use Illuminate\Support\Collection;

use App\Companion;
use App\Guest;
use App\Event;
use DB;

use App\Http\Requests\Companion\CompanionRequest;
use App\Http\Requests\Guest\GuestsFilterRequest;
use App\Http\Requests\Guest\GuestsFilterPaginateRequest;

/**
 * Service for "Companion" ressource
 */
class CompanionService
{
    /**
     * Update companion and add new extended fields to event list
     */
    public function update(Companion $companion, Event $event, CompanionRequest $request) : Companion
    {
        $companion->firstname = isset($request->firstname) ? $request->firstname : $companion->firstname;
        $companion->lastname = isset($request->lastname) ? $request->lastname : $companion->lastname;
        $companion->email = isset($request->email) ? $request->email : $companion->email;
        $companion->company = isset($request->company) ? $request->company : $companion->company;

        if (isset($request->extended_fields)) {
            $event->addExtendedFileds(array_keys($request->extended_fields));
            $companion->extended_fields = isset($companion->extended_fields) ? 
                array_merge($companion->extended_fields, $request->extended_fields) : $request->extended_fields;
            $event->save();
        }

        if (isset($request->checkin)) {
            $companion->checkin = $request->checkin;
        }

        $companion->save();
        return $companion;
    }

    /**
     * Search guest with filter params
     * Default return order by guest_id and lastname
     * @return Collection : guests
     */
    public function search(Event $event, /*GuestsFilterRequest*/ $request, ?Collection $guestIds) : Collection
    {
        if (!($request instanceof GuestsFilterRequest || $request instanceof GuestsFilterPaginateRequest)) {
            throw new \InvalidArgumentException('$request must be a string or a GuestsFilterRequest object.');
        }

        $textFilterFields = ['firstname', 'lastname', 'email', 'company'];
        $booleanFilterFields = ['checkin'];

        // We do not want companion lines when event is not nominative
        if(!$event->nominative_companions){
            return collect([]);
        }

        // Companion is always considered as subscribed
        if ($request->has('subscribed') && !is_null($request->input('subscribed')) && !$request->input('subscribed')) {
            return collect([]);
        }

        // Companion cannot declined
        if ($request->has('declined') && !is_null($request->input('declined')) && $request->input('declined')) {
            return collect([]);
        }

        // Companions cannot be invited
        if ($request->has('invited') && !is_null($request->input('invited')) && $request->input('invited')) {
            return collect([]);
        }

        // Companion has not email validity check
        if ($request->has('valid_email') && !is_null($request->valid_email)) {
            if (intval($request->valid_email) !== Guest::EMAIL_VALIDITY['unknown']) {
                return collect([]);
            }
        }

        // Companions can't respond to survey
        if ($request->has('surveyFields')) {
            foreach ($request->surveyFields as $key => $value) {
                if (!empty($value)) {
                    return collect([]);
                }
            }
        }

        // Building a query form request parameters
        $companions = (new Companion)->newQuery();

        $companions->with('guest');

        // Pre filtering only from this event and from active guest
        $companions->whereHas('guest', function($q) use ($event) {
            $q->where('event_id', $event->id)->where('active', true);
        });

        // Filtering with boolean status filters (invited, subscribed, declined)
        foreach ($booleanFilterFields as $field) {
            if ($request->has($field) && !is_null($request->input($field))){
                $companions->where($field, $request->input($field) ? 1 : 0);
            }
        }
    
        // Filtering with base fields
        foreach ($textFilterFields as $field) {
            if ($request->has($field) && !empty($request->input($field))) {
                $companions->where($field, 'like', '%'. $request->input($field) . '%');
            }
        }

        // Filtering with guest fields
        if ($request->has('mainGuest')) {
            $companions->whereHas('guest', function($q) use ($request) {
                $firstname = $request->input('mainGuest.firstname');
                $lastname = $request->input('mainGuest.lastname');
                $email = $request->input('mainGuest.email');
                $company = $request->input('mainGuest.company');

                if (!is_null($firstname)) {
                    $q->where('firstname', 'like', '%'.$firstname.'%');
                }
                if (!is_null($lastname)) {
                    $q->where('lastname', 'like', '%'.$lastname.'%');
                }
                if (!is_null($email)) {
                    $q->where('email', 'like', '%'.$email.'%');
                }
                if (!is_null($company)) {
                    $q->where('company', 'like', '%'.$company.'%');
                }
            });
        }

        if ($request->has('language') && !is_null($request->input('language')) && $request->input('language') !== "all") {
            $companions->whereHas('guest', function($q) use ($request) {
                $q->where('language', $request->input('language'));
            });
        }

        // Filtering with extended fields
        if ($request->has('extendedFields') && sizeof($request->input('extendedFields'))) {
            foreach ($request->input('extendedFields') as $extField => $value) {
                if (!is_null($value)) {
                    // Double arrow to handle unquoted json query
                    $companions->where('extended_fields->>' . $extField, 'like', $value . '%');
                }
            }
        }

        // Session filters
        if ($request->has('sessions.subscribed')) {
            foreach ($request->input('sessions.subscribed') as $id => $wanted) {
                if (json_decode($wanted)) {
                    $companions->whereHas('sessions', function ($query) use ($id) {
                        $query->where('sessions.id', $id);
                    });
                }
                else {
                    $companions->whereDoesntHave('sessions', function ($query) use ($id) {
                        $query->where('sessions.id', $id);
                    });
                }
            }
        }
    
        if ($request->has('sessions.checkin') && count(array_keys($request->input('sessions.checkin'))) > 0) {

            foreach ($request->input('sessions.checkin') as $id => $wanted) {
                $checkin = json_decode($wanted);

                if (!($checkin === true || $checkin === false)) {
                    continue;
                }

                if ($checkin) {
                    $companions->whereHas('sessions', function ($query) use ($id) {
                        $query->where('sessions.id', $id)
                            ->where('session_companion.checkin', 1);
                    });
                }
                else {
                    $companions->whereHas('sessions', function ($query) use ($id) {
                        $query->where('sessions.id', $id)
                            ->where('session_companion.checkin', 0);
                    });
                }
            }
        }

        if (isset($guestIds)) {
            $companions->whereIn('guest_id', $guestIds);
        }

        return $companions->orderBy('lastname', 'asc')->orderBy('guest_id', 'asc')->get();
    }
}
