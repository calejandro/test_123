<?php

namespace App\Services;

use Lang;

use App\Email;
use App\JobFeedback;
use Excel;

/**
 * Service for "JobFeedback" ressource
 */
class JobFeedbackService
{
    /**
     * Check if a JobFeedback is running for an email
     * @return bool
     */
    public function isOneRunning(Email $email) : bool
    {
        $count = $email->jobFeedbacks()
            ->where('finished', false)
            ->count();
        return $count > 0;
    }

    public function exportToXLS($successful, $failed){
        $report = [];
        
        foreach ($successful as $person){

            $succPers = [
                Lang::get('validation.attributes.lastName') => $person->lastname,
                Lang::get('validation.attributes.firstName') => $person->firstname,
                Lang::get('validation.attributes.email') => $person->email,
                Lang::get('validation.attributes.date') => $person->date->date,
                Lang::get('validation.attributes.state') => Lang::get('validation.attributes.success')
            ];
            $report[] = $succPers;
        }

        for($i = 0; $i < count($failed); $i++){
            $person = $failed[$i];
            $f = [
                Lang::get('validation.attributes.lastName') => $person->lastname,
                Lang::get('validation.attributes.firstName') => $person->firstname,
                Lang::get('validation.attributes.email') => $person->email,
                Lang::get('validation.attributes.date') => $person->date->date,
                Lang::get('validation.attributes.state') => Lang::get('validation.attributes.failure')
            ];
            if($person->failure->code > 0){
                $fail = "validation.error_codes." . $person->failure->code;
                $f[Lang::get('validation.attributes.failure')] = Lang::get($fail);
            }
            elseif($person->failure->message == "ignored"){
                $f[Lang::get('validation.attributes.failure')] = Lang::get('validation.attributes.ignored');
            }else{
                $f[Lang::get('validation.attributes.failure')] = $person->failure->message;
            }

            $report[] = $f;
        }

        return Excel::create('export', function($excel) use ($report) {
            $excel->sheet('exported', function($sheet) use ($report) {
                $sheet->fromArray($report);
            });
        })->download('xls');
    }
}
