<?php

namespace App\Services;

class SanitizeService
{
    /**
     * Remove all in <...> or &lt;...&gt;
     */
    public function removeBalises(String $text)
    {
        return preg_replace('/(&lt;([^&gt;]+)&gt;|<([^>]+)>)/i', "", $text);
    }
}
