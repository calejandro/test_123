<?php

namespace App\Services;

use App\Jobs\SendEmail;
use App\Jobs\SendNotificationEmail;
use App\Jobs\SendCompanionConfirmEmail;

use App\Email;
use App\Event;
use App\Guest;
use App\Session;
use App\Company;
use App\JobFeedback;
use App\EmailType;

use Log;
use Datetime;

use App\Utils\Sentry\SentryLogger;

/**
 * Service for "Email" ressource
 */
class EmailService
{
    /**
     * Send subscribtion confirmation email for a guest
     */
    public function sendSubscribeConfirmEmail(Event $event, Guest $guest)
    {
        $jobFeedback = new JobFeedback();
        $jobFeedback->event_id = $event->id;
        $jobFeedback->nb_guests = 1;
        $jobFeedback->save();

        try {
            $mailConfirm = Email::where([
                'type' => EmailType::CONFIRMATION, 
                'event_id' => $event->id
            ])->first(); // Get confirmation email

            $jobFeedback->mail_id = $mailConfirm->id;
            $jobFeedback->save();

            SendEmail::dispatch(collect([$guest]), collect([]), $mailConfirm, $mailConfirm->joinICS,
                $mailConfirm->sendWithDefaultMail ? 1 : null, $jobFeedback, $mailConfirm->from_column);

            // Send companions confirm email
            if ($event->nominative_companions) {
                $companions = $guest->companions()->with('guest')->get();

                $companionMailConfirm = Email::where([
                    'type' => EmailType::COMPANIONS_CONFIRMATION, 
                    'event_id' => $event->id
                ])->first();

                foreach($companions as $companion) {
                    $jb = new JobFeedback();
                    $jb->event_id = $event->id;
                    $jb->nb_guests = 1;
                    $jb->save();

                    SendCompanionConfirmEmail::dispatch($companion, $companionMailConfirm, $jb);
                }
            }
        }
        catch(\Exception $e) {
            Log::info("Error on auto confirmation mail sending after subscription " . $e);
            SentryLogger::logException($e, [
                "message" => "Error on auto confirmation mail sending after subscription"
            ]);
        }
    }

    /**
     * Send event full notification
     */
    public function sendEventFullNotification(Event $event, string $guestEmail, Company $company)
    {
        $receiverEmail = $this->getSystemNotificationReceiver($event, $company);
        try {
            SendNotificationEmail::dispatch($receiverEmail, 'event_places_limit_reached', [
                'company' => $company->companyName, 
                'event' => $event->name,
                'guest' => $guestEmail
            ]);
        }
        catch (\Exception $e) {
            Log::info("Error on auto confirmation mail sending after subscription " . $e);
        }
    }

    public function sendSessionFullNotification(Session $session, Event $event, Company $company, Guest $guest)
    {
        $receiverEmail = $this->getSystemNotificationReceiver($event, $company);
        SendNotificationEmail::dispatch($receiverEmail, 'session_places_limit_reached', [
            'company' => $company->companyName, 
            'event' => $event->name,
            'session' => $session->name,
            'guest' => $guest->email
        ]);
    }

  /**
   *  This method generates a complete .ics file related to this event. It contains
   *  informations such as event description, dates, title, organizer and even url when
   *  a minisite does exist.
   * 
   *  @return rendered ics file
   */
  public function generateICSFile(Event $event, Guest $guest=null){

    $tz = 'UCT';
    $dtz = new \DateTimeZone($tz);

    // Attached ICS generation
    $vEvent = new \Eluceo\iCal\Component\Event();

    // Defining ICS file language
    $lang = $event->getLangsAttribute()[0];
    if(!is_null($guest) && !is_null($guest->language) && in_array($guest->language, $event->getLangsAttribute(), true)){
        $lang = $guest->language;
    }

    // Add dtz arg in date properties to take back the timezone and uncomment last line
    $vEvent->setDtStart(new Datetime($event->start_date))
      ->setDtEnd(new Datetime($event->end_date))
      ->setSummary($event->getTranslatedAttribute('name', $lang))
      ->setLocation($event->address)
      ->setUseUtc(false);

    // Generating and setting event description string
    $evDescription = $event->getTranslatedAttribute('description', $lang) != "" ? $event->getTranslatedAttribute('description', $lang) : "";

    // Adding minisite link if possible
    $evDescription .= ($event->advanced_minisite_mode == True && $event->public_minisite == True && $event->minisite != null) ? "\n\n" . $event->minisite->accessLink($lang) : "";
    $vEvent->setDescription($evDescription);

    // Setting organizer if specified
    if($event->organisator_email != ""){
      $organizer = new \Eluceo\iCal\Property\Event\Organizer("MAILTO:" . $event->organisator_email);
      $vEvent->setOrganizer($organizer);
    }
    
    $vCalendar = new \Eluceo\iCal\Component\Calendar("Eventwise");
    $vCalendar->addComponent($vEvent);

    $vTimeZone = new \Eluceo\iCal\Component\Timezone($tz);
    return $vCalendar->render();
  }

    private function getSystemNotificationReceiver(Event $event, Company $company)
    {
        return $event->organisator_email != "" ? $event->organisator_email : $company->email;
    }
}
