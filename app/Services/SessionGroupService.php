<?php

namespace App\Services;

use Illuminate\Support\Collection;

use App\Event;

class SessionGroupService
{
    /**
     * Ensure guest or companion try to register in two session in the same group
     * @param $guestSessionsIds : [sessionId, sessionId, sessionId]
     * @param $sessionIdsByCompanion : [
    *                      companionIndex: [sessionId, sessionId, sessionId], 
    *                      companionIndex: [sessionId, sessionId, sessionId]
     * ]
     */
    public function checkSessionGroupRegisteringUniq(Collection $guestSessionsIds, Collection $sessionIdsByCompanion, 
        Event $event) : bool
    {
        $groups = $event->sessionGroups()->with('sessions')->get();
        foreach ($groups as $group) {
            $sessionIds = $group->sessions->pluck('id');

            if (count($sessionIds->intersect($guestSessionsIds)) > 1) {
                return false;
            }

            foreach ($sessionIdsByCompanion as $index => $companionSessionIds) {
                if (count($sessionIds->intersect($companionSessionIds)) > 1) {
                    return false;
                }
            }
        }

        return true;
    }
}
