<?php

namespace App\Services;

use Log;
use Image;
use App\Minisite;
use App\Event;
use App\Page;
use App\Guest;
use App\Companion;

/**
 * Service for "Minisite" ressource
 */
class MinisiteService
{
    private function saveImage($img, $path, $size, $name)
    {
        Image::make($img->getRealPath())->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
          })->save(public_path($path . '/' . $name));
    }

    public function saveBackgroundImage($img, $name)
    {
        $this->saveImage($img, '/uploads/images/bg_img/', Minisite::MINISITE_BG_IMG_MAX_SIZE, $name);
    }

    public function saveBottomImage($img, $name)
    {
        $this->saveImage($img, '/uploads/images/bottom_img', Minisite::MINISITE_TOP_BOTTOM_IMG_MAX_SIZE, $name);
    }

    public function saveTopImage($img, $name)
    {
        $this->saveImage($img, '/uploads/images/top_img/', Minisite::MINISITE_TOP_BOTTOM_IMG_MAX_SIZE, $name);
    }

    /**
     * Assert guest / companion or demoHash has access to the wanted page
     * @param $foundGuest : founded guest for the hash, can be null
     * @param $foundCompanion : founded companion for the hash, can be null
     */
    public function verifyPageAccess(Event $event, Page $page, ?Guest $foundGuest, 
        ?Companion $foundCompanion, ?string $hash) : bool
    {
        if(!isset($foundGuest) && (!isset($foundCompanion) || !$foundCompanion->guest->active)
            && !$event->public_minisite && !$event->isValidDemoHash($hash)) {
            return false;
        }
        else if(isset($foundGuest)) {
            if ($foundGuest->event_id !== $event->id) {
                return false;
            }
        }
        else if(isset($foundCompanion)) {
            if ($foundCompanion->guest->event_id !== $event->id) {
                return false;
            }
        }
    
        if ($page->pageType->isTypeCompanionSubscribeForm() && !isset($foundCompanion) && !$event->isValidDemoHash($hash)) {
            return false;
        }

        return true;
    }
}
