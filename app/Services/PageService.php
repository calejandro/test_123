<?php

namespace App\Services;

use App\Minisite;
use App\Page;

use DB;

/**
 * Service for "Page" ressource
 */
class PageService
{
    /**
     * Usefull if a page is deleted
     * All pages after the deleted page needs top be updated to decrease -1 they order (fix the hole)
     */
    public function decreaseOrderAfter(Minisite $minisite, Page $page)
    {
        $minisite->pages()->where('order_id', '>', $page->order_id)->update([
            'order_id'=> DB::raw('order_id-1')
        ]);
    }

    /**
     * Invert order_id
     */
    public function invertOrder(Page $pageA, Page $pageB)
    {
        $orderIdA = $pageA->order_id;
        $orderIdB = $pageB->order_id;

        $pageA->order_id = $orderIdB;
        $pageB->order_id = $orderIdA;

        $pageA->save();
        $pageB->save();
    }
}
