<?php

namespace App\Services;

use DB;
use Carbon\Carbon;
use Excel;
use Lang;
use Illuminate\Support\Collection;

use App\Companion;
use App\Guest;
use App\Event;
use App\SessionGuest;
use App\SessionCompanion;
use App\JobMailVerificationFeedback;
use App\VerifiedEmailAddress;
use App\SurveyQuestion;

use App\Http\Requests\Guest\GuestsFilterRequest;
use App\Http\Requests\Guest\GuestsFilterPaginateRequest;

use App\Facades\EmailValidationService;

use App\Jobs\SendNotificationEmail;

use Log;
use GuzzleHttp\Client; // For the neverbounce API call
use Faker;

/**
* Service for "Guest" ressource
*/
class GuestService
{
  /**
   * Checkin a guest with n places, if the guest hasn't enought Companions then it return false
   */
  public function checkinGuest(Guest $guest, int $places): bool
  {
    $companionsUnCheckin = $guest->companions()->where('checkin', false)->get();
    $emptyPlaces = $guest->checkin ? $companionsUnCheckin->count() : $companionsUnCheckin->count() + 1;
    if ($emptyPlaces < $places) {
      return false;
    }

    if (!$guest->checkin) {
      $guest->checkin = true;
      $guest->save();
      $places = $places - 1;
    }

    foreach ($companionsUnCheckin as $companion) {
      if ($places <= 0) {
        return true;
      }
      else {
        $companion->checkin = true;
        $companion->save();
        $places = $places - 1;
      }
    }
    return true;
  }

  /**
   * Get guests duplicates grouped by emails
   */
  public function getDuplicates(Event $event) : iterable
  {
    $duplicateBlocs = [];
    $dupEmails = $this->getEmailsDuplicates($event);
    $guests = $event->guests()->active()->whereIn('email', $dupEmails)->get();

    foreach ($dupEmails as $email) {
      $duplicateBlocs[] = $guests->filter(function ($value) use ($email) {
        return $value->email == $email;
      })->values();
    }

    return $duplicateBlocs;
  }

  /**
   * Retrieve all duplicate emails in guest list for an event
   */
  public function getEmailsDuplicates(Event $event) : iterable
  {
      return $event->guests()
          ->active()
          ->select('email')
          ->groupBy('email')
          ->havingRaw('COUNT(*) > 1')
          ->get()
          ->pluck('email');
  }

  /**
   * Check if guests of an event has duplicates
   */
  public function hasGuestsDuplicateEmails(Event $event): bool
  {
      $duplicatesEmails = $this->getEmailsDuplicates($event);
      return $duplicatesEmails->count() > 0;
  }

  /*
  * Update existing guest and companion
  * Create companions if no id is provided, else update companions
  * @return [
  *   'guest' => Guest,
  *   'companions' => Companion[], // final list of companions with Ids, in same order as provided
  *   'updatedCompanions' => Companion[], // companions updated, order not preserved
  *   'createdCompanions' => Companion[], // companions created, order not preserved
  *   'deletedCompanions' => Companion[] // companions deleted, order not preserved
  * ]
  */
  public function updateGuestAndCompanions($requestData, Event $event, Guest $guest, $replaceExtFieldsHard = false) : array
  {
    // Send notification mail if guest changed
    if ((array_key_exists('firstname', $requestData) && $requestData['firstname'] != $guest->firstname) ||
        (array_key_exists('lastname', $requestData) && $requestData['lastname'] != $guest->lastname) ||
        (array_key_exists('email', $requestData) && $requestData['email'] != $guest->email) ||
        (array_key_exists('company', $requestData) && $requestData['company'] != $guest->company)) {
      try {
        $company = $event->company;
        $receiverEmail = $event->organisator_email != "" ? $event->organisator_email : $company->email;
        $guestBefore = '' . $guest->firstname . ' ' .  $guest->lastname . ' ( ' . $guest->email . ', company: ' . $guest->company . ' )';
        $guestAfter = '' . (array_key_exists('firstname', $requestData) ? $requestData['firstname'] : $guest->firstname) . ' ' .
                          (array_key_exists('lastname', $requestData) ? $requestData['lastname'] : $guest->lastname) . ' ( ' .
                          (array_key_exists('email', $requestData) ? $requestData['email'] : $guest->email)  . ', company: ' .
                          (array_key_exists('company', $requestData) ? $requestData['company'] : $guest->company)
                          . ' )';

        SendNotificationEmail::dispatch($receiverEmail, 'guest_infos_modification', [
          'company' => $company->companyName,
          'event' => $event->name,
          'guest_before' => $guestBefore,
          'guest_after' => $guestAfter
        ]);
      }
      catch (\Exception $e) {
        Log::info("ERROR ON AUTO MAIL SENDING AFTER SUBSCRIBPTION " . $e);
      }
    }

    $companions = [];
    $updatedCompanions = [];
    $createdCompanions = [];
    $deletedCompanions = [];

    // Updating companions or nominative companions if needed
    if (isset($requestData['companions']) || isset($requestData['companionsData'])) {

      if ($event->nominative_companions && isset($requestData['companionsData'])) {
        // nominative
        $res = $guest->addUpdateDeleteNominativeCompanions($requestData['companionsData']);
        $companions = $res['companions'];
        $updatedCompanions = $res['updatedCompanions'];
        $createdCompanions = $res['createdCompanions'];
        $deletedCompanions = $res['deletedCompanions'];
      }
      else {
        // no nominative case
        $toInsertCompanions = $requestData['companions'] - $guest->nbCompanions;

        if ($toInsertCompanions < 0) {
          $deletedCompanions = $guest->removeCompanions(abs($toInsertCompanions));
        }
        else {
          $createdCompanions = $guest->addCompanions($toInsertCompanions);
        }
      }
    }

    if(array_key_exists('firstname', $requestData)){ $guest->firstname = $requestData['firstname']; }
    if(array_key_exists('lastname', $requestData)){ $guest->lastname = $requestData['lastname']; }
    if(array_key_exists('email', $requestData)){ $guest->email = $requestData['email']; }
    if(array_key_exists('language', $requestData)){ $guest->language = $requestData['language']; }
    if(array_key_exists('company', $requestData)){ $guest->company = $requestData['company']; }
    if(array_key_exists('extendedFields', $requestData) && ($requestData['extendedFields'] !== null || $replaceExtFieldsHard)){
      if ($replaceExtFieldsHard) {
        $guest->extendedFields = $requestData['extendedFields'];
      }
      else {
        if ($guest->extendedFields === null) {
          $guest->extendedFields = $requestData['extendedFields'];
        }
        else {
          $guest->extendedFields = array_replace($guest->extendedFields, $requestData['extendedFields']);
        }
      }

      $event->addExtendedFileds(array_keys($requestData['extendedFields']));
      $event->save();
    }

    $guest->save();

    return [
      'guest' => $guest,
      'companions' => $companions,
      'updatedCompanions' => $updatedCompanions,
      'createdCompanions' => $createdCompanions,
      'deletedCompanions' => $deletedCompanions
    ];
  }

  /**
   * Register new guest from the subscribe form
   * the created guest is subscribed
   */
  public function registerGuest($requestData, Event $event) : Guest
  {
    $guest = new Guest();
    $guest->subscribed = true;
    $guest->email = $requestData['email'];
    if(array_key_exists('firstname', $requestData)) { $guest->firstname = $requestData['firstname']; }
    if(array_key_exists('lastname', $requestData)) { $guest->lastname = $requestData['lastname']; }
    if(array_key_exists('language', $requestData)) { $guest->language = $requestData['language']; }
    if(array_key_exists('company', $requestData)) { $guest->company = $requestData['company']; }
    if(array_key_exists('extendedFields', $requestData)) {
      $guest->extendedFields = $requestData['extendedFields'];
      $event->addExtendedFileds(array_keys($requestData['extendedFields']));
      $event->save();
    }

    $event->guests()->save($guest);

    return $guest;
  }

  /**
   * Register companions of guest
   * Handle either nominative companions or non-nominative companions
   * @return Companion[] ordered by $requestData index
   */
  public function registerGuestCompanions(array $requestData, Guest $guest, Event $event) : array
  {
    if ($event->nominative_companions && array_key_exists('companionsData', $requestData)){
      return $guest->addUpdateDeleteNominativeCompanions($requestData['companionsData'])['companions'];
    }
    else if(array_key_exists('companions', $requestData)){
      return $guest->addCompanions($requestData['companions']);
    }
  }

  /**
  * Check if any difference exists in guest from extendedFields
  */
  private function extendedFieldsDifferences(iterable $guests, array $extendedFields): bool
  {
    foreach ($guests as $g) {
      if (is_null($g->extendedFields)) {
        return true;
      }
      if (count(array_diff($extendedFields, array_keys($g->extendedFields))) > 0 || count(array_diff(array_keys($g->extendedFields), $extendedFields)) > 0) {
        return true;
      }
    }
    return false;
  }

  /**
  * Normalize Guests (Update) extended fields with Event base extended fields list
  * Save normalized guest
  * Does not return updated guests !
  */
  public function normalizeExtendedFields(iterable $guests, array $extendedFields)
  {
    if ($extendedFields != null && $this->extendedFieldsDifferences($guests, $extendedFields)) {
      foreach ($guests as $g) {
        $update = false;
        $arrExt = $g->extendedFields ?: [];

        // Add missing fields of event in guest
        foreach ($extendedFields as $key) {
          if (is_null($g->extendedFields) || !in_array($key, array_keys($g->extendedFields), true)) {
            // Add the field
            $arrExt[$key] = "";
            $update = true;
          }
          else if(isset($g->extendedFields[$key]) && is_array($g->extendedFields[$key])) {
            // Convert array to string
            $arrExt[$key] = implode(", ", $g->extendedFields[$key]);
            $update = true;
          }
        }

        // Remove field in guest that is not in event
        // Too dangerous to do that

        if ($update) {
          $g->update(['extendedFields' => $arrExt]);
        }
      }
    }
  }

  /**
  * Create Excel file with guests
  * WARN: guests extended fields must be normalized before !
  * @param $eventSessions: sessions with sessionGroup
  */
  public function exportExcel(iterable $guests, iterable $eventSurveyFields, iterable $eventSessions,iterable $sessionsGroup, iterable $eventExtFields): Excel
  {
    // Formating Email
    $guestsExport = [];
    foreach ($guests as $guest) {

      $subscribedAtFormated = '';
      if(array_key_exists('subscribed_at', $guest) && $guest['subscribed_at'] != null) {
        $subscribedAtFormated = Carbon::createFromFormat('Y-m-d H:i:s', $guest['subscribed_at'])->timezone('Europe/Paris')->format('d.m.Y H:i');
      }

      $gExp = [
        Lang::get('validation.attributes.first_name') => $guest['firstname'],
        Lang::get('validation.attributes.last_name') => $guest['lastname'],
        Lang::get('validation.attributes.email') => $guest['email'],
        Lang::get('validation.attributes.company') => $guest['company'],
        Lang::get('interface.attributes.valid_email.valid_email') =>  Lang::get('interface.attributes.valid_email.valid_email_'.$guest['valid_email']),
        Lang::get('validation.attributes.invited') => $guest['invited'] ? 'true' : 'false',
        Lang::get('validation.attributes.subscribed') => $guest['subscribed'] ? 'true' : 'false',
        Lang::get('validation.attributes.subscribedAt') => $subscribedAtFormated,
        Lang::get('validation.attributes.checkin') => "".$guest['checkin'],
        Lang::get('validation.attributes.companions') => "".$guest['nbCompanions'],
        Lang::get('validation.attributes.declined') => $guest['declined'] ? 'true' : 'false',
      ];

      foreach ($eventExtFields as $key) {
        if (array_key_exists($key, $guest['extendedFields'])) {
          $value = $guest['extendedFields'][$key];
          if (is_array($guest['extendedFields'][$key])) {
            $gExp[$key . "_"] = implode(',', $value);
          }
          else {
            $gExp[$key . "_"] = $value;
          }
        }
        else {
          $gExp[$key . "_"] = ""; // empty value
        }
      }

      // Add missing survey fields to guest
      foreach ($eventSurveyFields as $surveyField) {
        $res = "";

        if (array_key_exists('survey_responses', $guest)) {
          foreach ($guest['survey_responses'] as $response) {
            if ($response['name'] == $surveyField) {
              if(is_array($response['value'])) {
                $res = implode(',', $response['value']);
              }
              else{
                $res = $response['value'];
              }
              break;
            }
          }
        }
        $gExp[$surveyField . "_"] = $res;
      }


      // Add sessions subscriptions and checkin

      foreach($eventSessions as $session) {
        $totalSessionSubscriptions = 0;
        $totalSessionCheckins = 0;

        // $sessions = array_filter($guest['sessions'], function ($s) use ($session) {
        //   return $s['pivot']['session_id'] == $session->id;
        // });

        $sessions = SessionGuest::where('guest_id',$guest['id'])->where('session_id',$session['id'])->get()->toArray();

        $guestSession = count($sessions) > 0 ? array_pop($sessions) : null;
        // $totalSessionSubscriptions += $guestSession != null ? $guestSession['pivot']['places'] : 0;
        $totalSessionSubscriptions += $guestSession != null ? $guestSession['places'] : 0;
        // $totalSessionCheckins += $guestSession != null ? $guestSession['pivot']['checkin'] : 0;
        $totalSessionCheckins += $guestSession != null ? $guestSession['checkin'] : 0;

        // Todo if companions, count the subscribeds to session
        if(count($guest['companions']) > 0){
          foreach($guest['companions'] as $companion){ // TODO: perf ugly
               $companionSessions = SessionCompanion::where('companion_id',$companion['id'])->where('session_id',$session['id'])->get()->toArray();
        //     $companionSessions = array_filter($companion['sessions'], function ($c) use ($session) {
        //       return $c['pivot']['session_id'] == $session->id;
        //     });
            
            $companionSession = count($companionSessions) > 0 ? array_pop($companionSessions) : null;
            $totalSessionSubscriptions += $companionSession != null ? 1 : 0;
        //     $totalSessionCheckins += $companionSession != null ? $companionSession['pivot']['checkin'] : 0;
            $totalSessionCheckins += $companionSession != null ? $companionSession['checkin'] : 0;

          }
        }

        $totalSubscription = $totalSessionSubscriptions == 0 ? "0" : strval($totalSessionSubscriptions);
        $totalCheckin = $totalSessionCheckins == 0 ? "0" : strval($totalSessionCheckins);

        // if($session->session_group_id !== null) {
        if($session['session_group_id'] !== null) {
          $sessionGroup = array_filter($sessionsGroup, function ($s) use ($session) {
                  return $s['id'] == $session['session_group_id'];
                });
          $sessionGroup1 = count($sessionGroup) > 0 ? array_pop($sessionGroup) : null;
          // $gExp[$session->sessionGroup->name . "_" . $session->name . "_subscribe"] = $totalSubscription;
          $gExp[$sessionGroup1['name_fr'] . "_" . $session['name'] . "_subscribe"] = $totalSubscription;
          // $gExp[$session->sessionGroup->name . "_" . $session->name . "_checkin"] = $totalCheckin;
          $gExp[$sessionGroup1['name_fr']   . "_" . $session['name']. "_checkin"] = $totalCheckin;
        }
        else {
          $gExp[$session['name'] . "_subscribe"] =  $totalSubscription;
          $gExp[$session['name'] . "_checkin"] = $totalCheckin;
        }
      }

      $guestsExport[] = $gExp;
    }

    // Create XLS
    return Excel::create('export', function($excel) use ($guestsExport) {
      $excel->sheet('exported', function($sheet) use ($guestsExport) {
        // Sheet manipulation
        $sheet->fromArray($guestsExport);
      });
    })->download('xls');
  }

  /*
  * Exporting all companions form guest list.
  * Exported data is minimalist and stuck to base fields (firstname, lastname, email)
  */
  public function exportCompanionsExcel(iterable $companions, iterable $eventSessions): Excel{

    $companionsExport = [];


    foreach($companions as $companion){

      $gExp = [
        trans_choice('models.guest.companion', 1) . " " . Lang::get('validation.attributes.first_name') => $companion['firstname'],
        trans_choice('models.guest.companion', 1) . " " . Lang::get('validation.attributes.last_name') => $companion['lastname'],
        trans_choice('models.guest.companion', 1) . " " . Lang::get('validation.attributes.email') => $companion['email'],
        trans_choice('models.guest.companion', 1) . " " . Lang::get('validation.attributes.company') => $companion['company'],
        trans_choice('models.guest.companion', 1) . " " . Lang::get('validation.attributes.checkin') => strval($companion['checkin']),
        ' ' => '  ',
        trans_choice('models.guest.guest', 1) . " " . Lang::get('validation.attributes.first_name') => $companion['guest']['firstname'],
        trans_choice('models.guest.guest', 1) . " " . Lang::get('validation.attributes.last_name') => $companion['guest']['lastname'],
        trans_choice('models.guest.guest', 1) . " " . Lang::get('validation.attributes.email') => $companion['guest']['email'],
        trans_choice('models.guest.guest', 1) . " " . Lang::get('validation.attributes.companions') => $companion['guest']['nbCompanions'],
        '   ' => '  ',
      ];


      foreach($eventSessions as $session) {
        
        $sessions = array_filter($companion['sessions'], function ($s) use ($session) {
          return $s['pivot']['session_id'] == $session->id;
        });
        $companionSession = count($sessions) > 0 ? array_pop($sessions) : null;
        $gExp[$session->name . "_subscribe"] = $companionSession != null ? "1" : "0";
        $gExp[$session->name . "_checkin"] = $companionSession != null ? strval($companionSession['pivot']['checkin']) : "0";
      }

      $companionsExport[] = $gExp;
    }
   

    // Create XLS
    return Excel::create('export', function($excel) use ($companionsExport) {
      $excel->sheet('exported', function($sheet) use ($companionsExport) {
        // Sheet manipulation
        $sheet->fromArray($companionsExport);
      });
    })->download('xls');
  }


  public function mapRawGuests(Event $event, array $rawGuests, array $mapping, array $ignored): array
  {
    $guests = [];

    // Create guest mapped array
    foreach ($rawGuests as $row) {
      $guests[] = self::mapRawGuest($row, $mapping, $ignored, $event->id, $event->extendedFileds);
    }

    return $guests;
  }

  /**
  * Format RAW guest with given mapping
  * return raw mapped guest
  * @param $mapping : array with association of rawGuest field to guest's field
  */
  public function mapRawGuest(array $rawGuest, array $mapping, array $ignored, int $event_id, ?array $extendedFields): array
  {
    $guest = [];
    foreach($mapping as $key => $value) {
      // $key = actual key in guests
      // $value = db key to map

      if (($extendedFields != null &&
      in_array($value, $extendedFields) &&
      !in_array($value, ['firstname', 'lastname', 'email']))
      || is_numeric($value)) {
        $setValue = isset($rawGuest[$key]) ? $rawGuest[$key] : "";
        $guest['extendedFields'][$value] = $setValue;
      }
      else if($value == 'language'){
        if(isset($rawGuest[$key])){
          $guest[$value] = strtolower($rawGuest[$key]);
        }else{
          $guest[$value] = NULL;
        }
      }
      else {
        $guest[$value] = isset($rawGuest[$key]) ? $rawGuest[$key] : null;
      }

        unset($rawGuest[$key]);
      }

    $guest['event_id'] = $event_id;

    if (!array_key_exists('extendedFields', $guest)) {
      $guest['extendedFields'] = [];
    }

    // Remove ignored and unmapped fields
    foreach ($ignored as $ignoreName) {
      unset($rawGuest[$ignoreName]);
    }

    // ExtendedFields
    $guest['extendedFields'] = array_merge($rawGuest, $guest['extendedFields']);
    $guest['extendedFields'] = json_encode($guest['extendedFields']);

    // Generate accessHash
    $hashfname = isset($guest['firstname']) ? $guest['firstname'] : "";
    $hashlname = isset($guest['lastname']) ? $guest['lastname'] : "";
    $guest['accessHash'] = Guest::generateAccessHash($hashfname, $hashlname, $guest['email']);

    return $guest;
  }

  /**
  * Trim emails in guests array
  * The guests must be in array format
  */
  public function trimEmails(array $guests): array
  {
    foreach($guests as $i => $guest) {
      $guests[$i]['email'] = trim($guest['email'], " \t\n\r\0\x0B\xC2\xA0"); // trim special characters too
      if($guests[$i]['email'] === '') {
        $guests[$i]['email'] = null;
      }
    }

    return $guests;
  }

  /**
   * This maps any supported language
   *
   */
  public function cleanLanguages(array $guests){
    foreach($guests as $i => $guest) {

      $langValue = isset($guests[$i]['language']) ? $guests[$i]['language'] : "";

      if(!empty($langValue) && strlen($langValue) == 2 && !in_array($langValue, config('app.locales'))){
        $guests[$i]['language'] = null;
      }

      if(!empty($langValue) && strlen($langValue) > 2 && preg_match('/^((fra|fre)|(eng|ang)|(all|ger|deu))/', $langValue)){
        if(preg_match('/^(fra|fre)/', $langValue)){
          $guests[$i]['language'] = 'fr';
        }else if(preg_match('/^(eng|ang)/', $langValue)){
          $guests[$i]['language'] = 'en';
        }else if(preg_match('/^(all|ger|de)/', $langValue)){
          $guests[$i]['language'] = 'de';
        }else{
          $guests[$i]['language'] = null; // Ignoring all unwanted languages
        }

      }
    }
    return $guests;
  }

  public function anonymise(iterable $guests)
  {
    // Inspired by http://www.jonhaworth.com/articles/php/generate-random-email-addresses
    $tlds = ["com", "net", "ch", "org", "info"];
    $char = "0123456789abcdefghijklmnopqrstuvwxyz";

    // Randomizing each guest data
    foreach ($guests as $guest) {
      $ulen = mt_rand(5, 10);
      $dlen = mt_rand(7, 17);

      // Firstname
      $fn = '';
      for ($i = 1; $i <= $ulen; $i++) { $fn .= substr($char, mt_rand(0, strlen($char)), 1); }

      // Lastname
      $ln = '';
      for ($i = 1; $i <= $dlen; $i++) { $ln .= substr($char, mt_rand(0, strlen($char)), 1);}

      $mail = $fn . "@" . $ln . "." . $tlds[mt_rand(0, (sizeof($tlds)-1))];

      $guest->firstname = ucfirst($fn);
      $guest->lastname = ucfirst($ln);
      $guest->email = $mail;
      $guest->extendedFields = [];
      $guest->save();
    }
  }


  /**
  * Process the results of a batch mail verification job.
  *
  * This function simply updates the mail_valid status of the concerned guests.
  * This function closes the concerned JobMailVerificationFeedback object and
  * add the validated addresses to the master table containing all validated
  * addresses.
  *
  * @see generateClient
  * @see requestNeverbounceAPI
  *
  * @param neverbounce iterable results
  */
  public function processBatchMailsVerification($results)
  {
    Log::channel('verification')->info('[GuestService] processBatchMailsVerification start with ' . count($results) . ' results');

    foreach ($results as $key => $verification) {
      Guest::where(DB::raw('lower(email)'), strtolower($verification['data']['email']))
        ->update([
          'valid_email' => EmailValidationService::mapMailValidityResultToCodeValue($verification['verification']->result)
        ]);

      // Creating the top table level address entry
      $verifAddress = new VerifiedEmailAddress;
      $verifAddress->email = strtolower($verification['data']['email']);
      $verifAddress->valid_email = EmailValidationService::mapMailValidityResultToCodeValue($verification['verification']->result);
      $verifAddress->save();
    }

    Log::channel('verification')->info('[GuestService] processBatchMailsVerification finished with ' . count($results) . ' updates.');
  }

  /**
   * Remove all empty row from XLS import
   */
  public function cleanXLSImport(Collection $content) : Collection
  {
    $filterFunc = function($guest) {
      $keys = $guest->keys();
      foreach($keys as $key) {
        if ($guest[$key] != null) {
          return true;
        }
      }
      return false;
    };
    return $content->filter($filterFunc)->values();
  }

  public function generateDemoGuest(Event $event) : Guest
  {
    $faker = Faker\Factory::create('fr_FR');
    $guest = new Guest();
    $guest->firstname = $faker->firstName;
    $guest->lastname = $faker->lastName;
    $guest->email = $faker->email;
    $guest->accessHash = $event->getDemoHash();
    $guest->event_id = $event->id;

    return $guest;
  }

  /**
   * Search guest with filter params
   * Default return order by lastname
   * @return Collection : guests
   */
  public function search(Event $event, /*GuestsFilterRequest*/ $request) : Collection
  {
    if (!($request instanceof GuestsFilterRequest || $request instanceof GuestsFilterPaginateRequest)) {
      throw new \InvalidArgumentException('$request must be a string or a GuestsFilterRequest object.');
    }

    // Guest has no mainGuest
    if ($request->has('mainGuest') && 
        (!is_null($request->input('mainGuest.firstname')) ||
        !is_null($request->input('mainGuest.lastname')) ||
        !is_null($request->input('mainGuest.email')) || 
        !is_null($request->input('mainGuest.company')) )) {
      return collect([]);
    }

    $textFilterFields = ['firstname', 'lastname', 'email', 'company'];
    $booleanFilterFields = ['invited', 'subscribed', 'declined'];

    // Building a query form request parameters
    $guests = (new Guest)->newQuery();

    $guests->with('sessions', 'surveyResponses');

    // Pre filtering only active guests and from this event
    $guests->where(['active' => '1', 'event_id' => $event->id]);

    // Filtering guests with base fields
    foreach ($textFilterFields as $field) {
      if ($request->has($field) && !empty($request->input($field))) {
        $guests->where($field, 'like', '%'. $request->input($field) . '%');
      }
    }

    if ($request->has('valid_email') && $request->valid_email !== NULL) {
      if (intval($request->valid_email) === Guest::EMAIL_VALIDITY['unknown']) {
        // Unknown code is used for unchecked email
        $guests->whereNull('valid_email');
      }
      else if (intval($request->valid_email) === Guest::EMAIL_VALIDITY['catchall']) {
        // Unknown email are showed the same way as catchall
        $guests->whereIn('valid_email', [
          Guest::EMAIL_VALIDITY['catchall'], 
          Guest::EMAIL_VALIDITY['unknown']
        ]);
      }
      else {
        $guests->where('valid_email', intval($request->valid_email));
      }
    }

    if ($request->has('language') && !is_null($request->input('language')) && $request->input('language') !== "all") {
      $guests->where('language', $request->input('language'));
    }

    // Filtering guests with boolean status filters (invited, subscribed, declined)
    foreach ($booleanFilterFields as $field) {
      if ($request->has($field) && !is_null($request->input($field))){
        $guests->where($field, $request->input($field) ? 1 : 0);
      }
    }

    // Filtering guests with extended fields
    if ($request->has('extendedFields') && sizeof($request->input('extendedFields'))) {
      foreach ($request->input('extendedFields') as $extField => $value) {
        if (!is_null($value)) {
          // Double arrow to handle unquoted json query
          $guests->where('extendedFields->>' . $extField, 'like', $value . '%');
        }
      }
    }

    // Has companions filter
    if ($request->has('companions') && !is_null($request->input('companions'))) {
      if ($request->input('companions')) {
        $guests->has('companions');
      }
      else {
        $guests->doesntHave('companions');
      }
    }

    // Checkin filter
    if ($request->has('checkin') && !is_null($request->input('checkin'))) {
      if($event->nominative_companions) {
        $guests->where('checkin', $request->input('checkin') ? 1 : 0);
      }
      else {
        // Checkin of companions applied on guest
        $checkinValue = $request->input('checkin') ? 1 : 0;

        if ($checkinValue) {
          $guests->where(function ($q) use ($checkinValue) {
            $q->where('checkin', $checkinValue)
              ->orWhereHas('companions', function($query) use ($checkinValue) {
                $query->where('checkin', $checkinValue);
              });
          });
        }
        else {
          $guests->where('checkin', $checkinValue)
            ->whereDoesntHave('companions', function($query) use ($checkinValue) {
              $query->where('checkin', true);
            });
        }
       
      }
    }

    // Sessions filter
    if ($request->has('sessions.subscribed') && count(array_keys($request->input('sessions.subscribed'))) > 0) {

      foreach ($request->input('sessions.subscribed') as $id => $wanted) {
        $subscribed = json_decode($wanted);

        if (!($subscribed === true || $subscribed === false)) {
          continue;
        }

        if ($subscribed) {
          if ($event->nominative_companions) {
            $guests->whereHas('sessions', function ($query) use ($id) {
              $query->where('sessions.id', $id);
            });
          }
          else {
            $guests->where(function ($q) use ($id) {
              $q->whereHas('sessions', function ($query) use ($id) { // Is related to session
                $query->where('sessions.id', $id);
              })->orWhereHas('companions.sessions', function ($query) use ($id) { // OR has a companion related to session
                $query->where('sessions.id', $id);
              });
            });
          }
        }
        else {
          if ($event->nominative_companions) {
            $guests->whereDoesntHave('sessions', function ($query) use ($id) {
              $query->where('sessions.id', $id);
            });
          }
          else {
            $guests->where(function ($q) use ($id) {
              $q->whereDoesntHave('sessions', function ($query) use ($id) { // Is not related to session
                $query->where('sessions.id', $id);
              })->where(function ($q) use ($id) {
                $q->whereDoesntHave('companions.sessions', function ($query) use ($id) { // Has no companions related to session
                  $query->where('sessions.id', $id);
                })->orWhere(function($q) use ($id) { // OR don't have any companion
                  $q->doesntHave('companions');
                });
              });
            });
          }
        }
      }
    }

    if ($request->has('sessions.checkin') && count(array_keys($request->input('sessions.checkin'))) > 0) {
      
      foreach ($request->input('sessions.checkin') as $id => $wanted) {
        $checkin = json_decode($wanted); // "true" / "false"

        if (!($checkin === true || $checkin === false)) {
          continue;
        }

        if ($checkin) {
          if($event->nominative_companions) {
            $guests->whereHas('sessions', function ($query) use ($id) {
              $query->where('sessions.id', $id)
                ->where('session_guest.checkin', '>=', 1);
            });
          }
          else {
            $guests->where(function ($q) use ($id) {
              $q->whereHas('sessions', function ($query) use ($id) { // Is checkin
                $query->where('sessions.id', $id)
                  ->where('session_guest.checkin', '>=', 1);
              })->orWhereHas('companions.sessions', function ($query) use ($id) { // Or one of his companions is checkin
                $query->where('sessions.id', $id)
                  ->where('session_companion.checkin', '>=', 1);
              });
            });
          }
        }
        else {
          if($event->nominative_companions) {
            $guests->whereHas('sessions', function ($query) use ($id) {
              $query->where('sessions.id', $id)
                ->where('session_guest.checkin', '<', 1);
            });
          }
          else {
            // Guests AND Companions MUST not have checkin in sessions
            $guests->where(function ($q) use ($id) {
                $q->whereHas('sessions', function ($query) use ($id) { // Isn't checkin in session
                  $query->where('sessions.id', $id)
                    ->where('session_guest.checkin', '<', 1);
                })->orWhere(function($q) use ($id) {
                  $q->whereDoesntHave('sessions', function ($query) use ($id) { // OR isn't related to session
                    $query->where('sessions.id', $id);
                  });
                });
              })
              ->where(function ($q) use ($id) {
                $q->whereHas('companions.sessions', function ($query) use ($id) { // Don't have companion checkin in session
                  $query->where('sessions.id', $id)
                    ->where('session_companion.checkin', '<', 1);
                })->orWhere(function($q) use ($id) { // OR don't have any companion related to session
                  $q->whereDoesntHave('companions.sessions', function ($query) use ($id) {
                    $query->where('sessions.id', $id);
                  });
                })->orWhere(function($q) use ($id) { // OR don't have any companion
                  $q->doesntHave('companions');
                });
              });
          }
        }
      }
    }

    // Survey responses
    if ($request->has('surveyFields')) {
      $questions = SurveyQuestion::where('survey_form_id', $event->surveyForm->id)->get()->pluck('id', 'name'); // [ 'name' => 'id', ... ]
      foreach ($request->surveyFields as $key => $value) {
        if (!empty($value)) {
          $guests->whereHas('surveyResponses', function ($query) use ($questions, $value, $key) {
            $qId = $questions[$key];
            $query->where('survey_question_id', $qId)->where('value', 'like', '%'.$value.'%');
          });
        }
      }
    }

    return $guests->orderBy('lastname', 'asc')->get();
  }

}
