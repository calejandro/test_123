<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - finished : boolean // Job is completed
 * - processed: boolean // Job is running
 * - error : boolean
 * - nb_adresses : int(11)
 * - nb_valid_addresses : int(11)
 * - nb_invalid_addresses : int(11)
 * - nb_catchall_addresses : int(11)
 * - nb_unknown_addresses : int(11)
 * - nb_disposable_addresses : int(11)
 * - nb_duplicate_addresses : int(11)
 * - event_id : int(11)
 * - neverbounce_job_id : int(11)
 */
class JobMailVerificationFeedback extends Model
{
    protected $table = 'jobMailVerificationFeedbacks';

    protected $primaryKey = 'id';

    protected $guarded = [];

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }
}
