<?php

namespace App;

use App\I18n\LocalizableModel;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Attributes
 * - id : int(10) unsigned
 * - session_group_id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name_fr : varchar(255)
 * - name_de : varchar(255)
 * - name_en : varchar(255)
 * - places : int
 */
class Session extends LocalizableModel
{
    use SoftDeletes;

    protected $table = 'sessions';

    protected $primaryKey = 'id';

    protected $fillable = ['name_fr', 'name_en', 'name_de', 'places', 'session_group_id'];

    protected $appends = ['placesReserved', 'placesCheckin'];

    protected $with = ['guests', 'companions']; // eager loading necessary for $appends

    protected $localizable = [
        'name'
    ];

    /**
     * Replicate copy and save
     */
    public function replicate(array $except = null, $addCopyText = false)
    {
        $newSession = $this->copy($addCopyText);
        $newSession->save();
    }

    public function copy($addCopyText = true){
        $newSession = new Session();
        $newSession->name_fr = ($addCopyText ? "[Copie] " : "") . $this->name_fr;
        $newSession->name_en = ($addCopyText ? "[Copy] " : "") . $this->name_en;
        $newSession->name_de = ($addCopyText ? "[Kopie] " : "") . $this->name_de;
        $newSession->places = $this->places;
        return $newSession;
    }

    // Accessors

    public function getPlacesReservedAttribute()
    {
        $guests = $this->guestsSubscribed()->get();
        $companions = $this->companions()->active()->get();
        return $guests->count() + $companions->count();
    }

    public function getPlacesCheckinAttribute()
    {
        $checkins = 0;
        $guests = $this->guestsCheckin()->get();
        foreach ($guests as $guest) {
            if($guest->pivot->checkin) {
                $checkins = $checkins + 1;
            }
        }

        $companions = $this->companions()->active()->get();
        foreach ($companions as $companion) {
            if($companion->pivot->checkin) {
                $checkins = $checkins + 1;
            }
        }
        return $checkins;
    }

    // Queries

    /**
     * Return all subscription
     * Warning a subscription can contain multiple place
     */
    public function scopeGuestsSubscribed($query)
    {
        return $this->guests()->active();
    }

    public function scopeGuestsCheckin($query)
    {
        return $this->guests()->wherePivot('checkin', '>', 0)->active();
    }

    // Relations

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function sessionGroup()
    {
        return $this->belongsTo('App\SessionGroup');
    }

    public function guests()
    {
        return $this->belongsToMany('App\Guest', 'session_guest')
            ->withPivot('checkin', 'places')
            ->withTimestamps();
    }

    public function companions()
    {
        return $this->belongsToMany('App\Companion', 'session_companion')
            ->withPivot('checkin')
            ->withTimestamps();
    }
}
