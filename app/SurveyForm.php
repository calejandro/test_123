<?php

namespace App;

use App\I18n\LocalizableModel;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - visible : boolean
 * - template : longtext
 * - html : longtext
 * - formTemplate : longtext
 * - formHtml : longtext
 * - event_id : int(10) unsigned
 * - label_color : int(11)
 * - submit_text_fr : string
 * - submit_text_en : string
 * - submit_text_de : string
 * - submit_text_es : string
 * - submit_color_bg : string // CSS color code
 * - submit_color_text : string // CSS color code
 */
class SurveyForm extends LocalizableModel
{
    protected $table = 'surveyForms';
    protected $primaryKey = 'id';

    protected $fillable = [
      'visible', 'template', 'html', 'formTemplate', 'formHtml', 'advancedFormHtml', 'advancedFormTemplate',
      'submit_text_fr', 'submit_text_en', 'submit_text_de', 'submit_text_es', 'submit_color_bg', 'submit_color_text'
    ];

    protected $localizable = [
      'submit_text'
    ];

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function surveyQuestions()
    {
      return $this->hasMany('App\SurveyQuestion');
    }

    public function surveyResponses()
    {
      return $this->hasManyThrough('App\SurveyResponse', 'App\SurveyQuestion');
    }
}
