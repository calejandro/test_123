<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - value : JSON
 * - survey_question_id : int(10) unsigned
 * - guest_id : int(10) unsigned
 */
class SurveyResponse extends Model
{
  protected $table = 'surveyResponses';

  protected $hidden = ['guest_id', 'survey_question_id'];

  protected $primaryKey = 'id';

  protected $appends = ['name'];

  protected $fillable = ['value'];

  protected $casts = [
    'value' => 'array'
  ];

  // Accessors

  public function getNameAttribute()
  {
    return $this->surveyQuestion()->first()->name;
  }

  // Relations

  public function guest()
  {
    return $this->belongsTo('App\Guest');
  }

  public function surveyQuestion()
  {
    return $this->belongsTo('App\SurveyQuestion');
  }

  public function scopeFromActiveGuest($query)
  {
    return $query->whereHas('guest', function ($g) {
      $g->active();
    });
  }
}
