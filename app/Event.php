<?php

namespace App;

use App\I18n\LocalizableModel;

use App\Email;
use App\SurveyForm;
use App\SubscribeForm;
use App\SurveyQuestion;
use App\User;
use App\Minisite;
use App\Event;
use App\Ticket;
use App\EmailType;

use Illuminate\Support\Collection;

use Lang;
use Datetime;
use Illuminate\Support\Facades\Hash;

use Log;

/**
 * Attributes
 * - id : int(10) unsigned
 * - name : varchar(255)
 * - description : text
 * - start_date : Date
 * - end_date : Date
 * - nb_places : bigint(20)
 * - address : varchar(255)
 * - company_id : int(10) unsigned
 * - active : boolean
 * - created_at : Date
 * - updated_at : Date
 * - extendedFileds : JSON
 * - type_id : int(10) unsigned
 * - advanced_minisite_mode : boolean
 * - organisator_firstname : varchar(255)
 * - organisator_lastname : varchar(255)
 * - organisator_email : varchar(255)
 * - language_fr : boolean
 * - language_de : boolean
 * - language_en : boolean
 * - public_minisite : boolean
 * - companions_limit : int(19) unisigned, default: 0
 * - nominative_companions : boolean
 * - allow_subscription_update : boolean, default: true
 */
class Event extends LocalizableModel
{
  protected $table = 'events';
  protected $primaryKey = 'id';

  protected $fillable = ['name_fr', 'description_fr', 'name_de', 'description_de',
    'name_en', 'description_en', 'start_date', 'end_date',
    'nb_places', 'address', 'company_id', 'active', 'category_id', 'extendedFileds',
    'organisator_firstname', 'organisator_lastname', 'organisator_email', 'advanced_minisite_mode',
    'companions_limit', 'nominative_companions', 'allow_subscription_update',
    'mandatory_sessions', 'allowed_session_subscription', 'one_mandatory_category_session', 'maximum_categories_subscriptions',
    'visible_captcha'
  ];

  protected $casts = [
    'extendedFileds' => 'array'
  ];

  protected $localizable = ['name', 'description'];

  /*protected $dates = [
    'start_date',
    'end_date'
  ];*/

  protected $appends = [
    'minisite_demo_url', 'survey_form_url', 'subscribe_form_url', 'companion_subscribe_form_url', 'empty_places', 'langs'
  ];

  /*
  * Simply returns int of active guests with subscribed = true
  */
  public function countGuestsSubscribed()
  {
    return $this->guests()->active(true)->where('subscribed', true)->count();
  }

  /*
  * Returns int sum of active guests with subscribed = true + their companions
  */
  public function countGuestsSubscribedWithCompanions()
  {
    $guestSubscribed = $this->countGuestsSubscribed();
    $guests = $this->guests()->active(true)->where('subscribed', true)->with('companions')->get();
    $companions = 0;

    foreach($guests as $guest) {
      $companions = $companions + $guest->companions()->count();
    }

    return $guests->count() + $companions;
  }

  /*
  * Simply returns number of remaining places considering companions
  */
  private function countEmptyPlaces()
  {
    return $this->nb_places - $this->countGuestsSubscribedWithCompanions();
  }

  public function availableLanguages()
  {
    // TODO: Everything of this is ugly but won't stay
    $langs = [];
    if($this->language_fr){
      $langs[] = "fr";
    }
    if($this->language_de){
      $langs[] = "de";
    }
    if($this->language_en){
      $langs[] = "en";
    }
    return $langs;
  }

  public function hasMinisite()
  {
    $minisite = $this->minisite()->first();
    return !is_null($minisite);
  }

  public function addExtendedFileds($newFields)
  {
    Log::channel('debug')->info('[Event] addExtendedFileds, $newFields:'); // TODO: Log for #412 remove it
    Log::channel('debug')->info($newFields); // TODO: Log for #412 remove it

    Log::channel('debug')->info('[Event] addExtendedFileds, (before adding) $this->extendedFileds:'); // TODO: Log for #412 remove it
    Log::channel('debug')->info($this->extendedFileds); // TODO: Log for #412 remove it

    if($this->extendedFileds != null)
    {
      $this->extendedFileds = array_unique(array_merge($this->extendedFileds, $newFields));
    }
    else
    {
      $this->extendedFileds = array_unique($newFields);
    }

    Log::channel('debug')->info('[Event] addExtendedFileds, (after adding) $this->extendedFileds:'); // TODO: Log for #412 remove it
    Log::channel('debug')->info($this->extendedFileds); // TODO: Log for #412 remove it
  }

  /**
  *   @param $newFields = array (
  *     'value-1' =>
  *       array (
  *         'name' => 'value-1',
  *         'value' => 'a',
  *         'label' => 'Champ de Saisie Texte',
  *         'type' => 'text'
  *       ),
  */
  public function addSurveyFileds($newFields)
  {
    foreach($newFields as $field => $values) {
      $questions = $this->surveyForm()->first()->surveyQuestions()->where('name', $field);
      if($questions->count() <= 0) { // no question exists
        $question = new SurveyQuestion();
        $question->name = $field;
        $question->type = $values['type'];
        $question->label = $values['label'];
        $this->surveyForm()->first()->surveyQuestions()->save($question);
      }
      else {
        $question = $questions->first();
        // never update the name
        $question->type = $values['type'];
        $question->label = $values['label'];
        $question->save();
      }
    }
  }

  public function getDemoHash()
  {
    return base64_encode($this->start_date->format('Y-m-d H:i:s').'&'.$this->id.'&'.$this->company_id.'&'.$this->end_date->format('Y-m-d H:i:s').'&'.$this->organisator_email);
  }

  public static function findWithHash($hash)
  {
    $infos = explode('&', base64_decode($hash));
    if(count($infos) !== 5) {
      return null;
    }

    $event = Event::findOrFail($infos[1]);
    if($infos[1] === ''.$event->id && $infos[2] === ''.$event->company_id) {
      return $event;
    }
    else {
      return null;
    }
  }

  public function isValidDemoHash($hash)
  {
    $infos = explode('&', base64_decode($hash));

    if(count($infos) !== 5) {
      return false;
    }

    return ($infos[0] === $this->start_date->format('Y-m-d H:i:s') &&
      intval($infos[1]) === $this->id &&
      intval($infos[2]) === $this->company_id &&
      $infos[3] === $this->end_date->format('Y-m-d H:i:s') &&
      $infos[4] === ''.$this->organisator_email
    );
  }

  public function getSurveyFieldsList()
  {
    return $this->surveyForm()->first()->surveyQuestions()->pluck('name');
  }

  public function getDefaultController()
  {
    return User::where('event_id', $this->id)->get()->first();
  }

  /**
   * Create default confirmation emails for guests and companions (only if event is nominative)
   */
  public function setDefaultConfirmMail($sendWithDefault = true)
  {
    $mail = new Email();
    $mail->title_fr = Lang::choice('models.email.confirmEmail', 1, [], 'fr');
    $mail->title_en = Lang::choice('models.email.confirmEmail', 1, [], 'en');
    $mail->title_de = Lang::choice('models.email.confirmEmail', 1, [], 'de');
    $mail->type = EmailType::CONFIRMATION;
    $mail->joinICS = false;
    $mail->sendWithDefaultMail = $sendWithDefault;

    $this->emails()->save($mail);

    $mailCompanion = new Email();
    $mailCompanion->title_fr = Lang::choice('models.email.confirmEmailCompanions', 1, [], 'fr');
    $mailCompanion->title_en = Lang::choice('models.email.confirmEmailCompanions', 1, [], 'en');
    $mailCompanion->title_de = Lang::choice('models.email.confirmEmailCompanions', 1, [], 'de');
    $mailCompanion->type = EmailType::COMPANIONS_CONFIRMATION;
    $mailCompanion->joinICS = false;
    $mailCompanion->sendWithDefaultMail = $sendWithDefault;
    $this->emails()->save($mailCompanion);
  }

  public function setDefaultFullEventMail($sendWithDefault = true)
  {
    $mail = new Email();
    $mail->title_fr = Lang::choice('models.email.fullEvent', 1, [], 'fr');
    $mail->title_en = Lang::choice('models.email.fullEvent', 1, [], 'en');
    $mail->title_de = Lang::choice('models.email.fullEvent', 1, [], 'de');
    $mail->joinICS = false;
    $mail->type = EmailType::EVENT_FULL_NOTIFICATION;
    $mail->sendWithDefaultMail = $sendWithDefault;
    $this->emails()->save($mail);
  }

  public function setDefaultSubscribeForm()
  {
    // Default subscribe form
    $sf = new SubscribeForm();

    $defaultTemplate = <<<EOT
    [{"type":"text","required":true,"label":"Prénom","placeholder":"Entrez votre prénom","className":"form-control","name":"firstname","subtype":"text"},{"type":"text","required":true,"label":"Nom de famille","placeholder":"Entrez votre nom","className":"form-control","name":"lastname","subtype":"text"},{"type":"text","subtype":"email","required":true,"label":"Email","placeholder":"Entrez votre email","className":"form-control","name":"email"}]
EOT;
    $defaultHtml = <<<EOT
    <div class="rendered-form"><div class="fb-text form-group field-firstname"><label for="firstname" class="fb-text-label">Prénom<span class="fb-required">*</span></label><input placeholder="Entrez votre prénom" class="form-control" name="firstname" id="firstname" required="required" aria-required="true" type="text"></div><div class="fb-text form-group field-lastname"><label for="lastname" class="fb-text-label">Nom de famille<span class="fb-required">*</span></label><input placeholder="Entrez votre nom" class="form-control" name="lastname" id="lastname" required="required" aria-required="true" type="text"></div><div class="fb-text form-group field-email"><label for="email" class="fb-text-label">Email<span class="fb-required">*</span></label><input placeholder="Entrez votre email" class="form-control" name="email" id="email" required="required" aria-required="true" type="email"></div></div>
EOT;

    $sf->formTemplate = $defaultTemplate;
    $sf->formHtml = $defaultHtml;

    $this->subscribeForm()->save($sf);
  }


  public function setDefaultSurveyForm()
  {
    // Default survey form
    $svf = new SurveyForm();

    $svf->formTemplate = <<<EOT
    [{"type":"radio-group","label":"Question 1","name":"question1","values":[{"label":"Réponse 1","value":"reponse1"},{"label":"Réponse 2","value":"reponse2"}]},{"type":"checkbox-group","label":"Question 2","name":"question2","values":[{"label":"Réponse 1","value":"reponse1"},{"label":"Réponse 2","value":"reponse2"},{"label":"Réponse 3","value":"reponse3"}]},{"type":"button","subtype":"submit","label":"Valider","className":"form-control btn btn-primary","name":"submit","style":"primary"}]
EOT;

    $svf->formHtml = <<<EOT
    <div class="rendered-form"><div class="fb-radio-group form-group field-question1"><label for="question1" class="fb-radio-group-label">Question 1</label><div class="radio-group"><div class="radio"><input name="question1" id="question1-0" value="reponse1" type="radio"><label for="question1-0">Réponse 1</label></div><div class="radio"><input name="question1" id="question1-1" value="reponse2" type="radio"><label for="question1-1">Réponse 2</label></div></div></div><div class="fb-checkbox-group form-group field-question2"><label for="question2" class="fb-checkbox-group-label">Question 2</label><div class="checkbox-group"><div class="checkbox"><input name="question2[]" id="question2-0" value="reponse1" type="checkbox"><label for="question2-0">Réponse 1</label></div><div class="checkbox"><input name="question2[]" id="question2-1" value="reponse2" type="checkbox"><label for="question2-1">Réponse 2</label></div><div class="checkbox"><input name="question2[]" id="question2-2" value="reponse3" type="checkbox"><label for="question2-2">Réponse 3</label></div></div></div><div class="fb-button form-group field-submit"><button type="submit" class="form-control btn btn-primary" name="submit" style="primary" id="submit">Valider</button></div></div>
EOT;

    $this->surveyForm()->save($svf);
  }


  public function setDefaultTicket(){
    $ticket = new Ticket();
    $ticket->event_id = $this->id;
    $ticket->save();
  }

  public function replicateEmails($emails){
    foreach($emails as $email){
      $newEmail = $email->copy($addCopyText = false);
      $newEmail->event_id = $this->id;
      $newEmail->save();
    }
  }

  public function replicateSurveyForm(SurveyForm $surveyform)
  {
    $newSurveyForm = new SurveyForm();
    $newSurveyForm->visible = $surveyform->visible;
    $newSurveyForm->template = $surveyform->template;
    $newSurveyForm->html = $surveyform->html;
    $newSurveyForm->formTemplate = $surveyform->formTemplate;
    $newSurveyForm->formHtml = $surveyform->formHtml;
    $newSurveyForm->advancedFormTemplate = $surveyform->advancedFormTemplate;
    $newSurveyForm->advancedFormHtml = $surveyform->advancedFormHtml;
    $newSurveyForm->event_id = $this->id;
    $newSurveyForm->label_color = $surveyform->label_color;
    $newSurveyForm->submit_text_fr = $surveyform->submit_text_fr;
    $newSurveyForm->submit_text_en = $surveyform->submit_text_en;
    $newSurveyForm->submit_text_de = $surveyform->submit_text_de;
    $newSurveyForm->submit_text_es = $surveyform->submit_text_es;
    $newSurveyForm->submit_color_bg = $surveyform->submit_color_bg;
    $newSurveyForm->submit_color_text = $surveyform->submit_color_text;
    return $newSurveyForm->save();
  }

  public function replicateSubscribeForm(SubscribeForm $subscribeform)
  {
    $newSubscribeForm = new SubscribeForm();
    $newSubscribeForm->visible = $subscribeform->visible;
    $newSubscribeForm->template = $subscribeform->template;
    $newSubscribeForm->html = $subscribeform->html;
    $newSubscribeForm->formTemplate = $subscribeform->formTemplate;
    $newSubscribeForm->formHtml = $subscribeform->formHtml;
    $newSubscribeForm->advancedFormTemplate = $subscribeform->advancedFormTemplate;
    $newSubscribeForm->advancedFormHtml = $subscribeform->advancedFormHtml;
    $newSubscribeForm->label_color = $subscribeform->label_color;
    $newSubscribeForm->allow_basefields_modification = $subscribeform->allow_basefields_modification;
    $newSubscribeForm->submit_text_fr = $subscribeform->submit_text_fr;
    $newSubscribeForm->submit_text_en = $subscribeform->submit_text_en;
    $newSubscribeForm->submit_text_de = $subscribeform->submit_text_de;
    $newSubscribeForm->submit_text_es = $subscribeform->submit_text_es;
    $newSubscribeForm->submit_color_bg = $subscribeform->submit_color_bg;
    $newSubscribeForm->submit_color_text = $subscribeform->submit_color_text;
    $newSubscribeForm->event_id = $this->id;
    return $newSubscribeForm->save();
  }

  public function replicateMinisite(Minisite $minisite)
  {
    $newMinisite = $minisite->replicate();

    // assure that no same slug exists
    $i = 1;
    do {
      // limit 63
      $newSlug = (strlen($minisite->slug) >= 62) ? substr($minisite->slug, 0, -3).'-'.$i : $minisite->slug.'-'.$i;
      $newMinisite->slug = $newSlug;
      $i = $i + 1;
    } while (Minisite::where('slug', $newSlug)->count() > 0);

    $newMinisite->event_id = $this->id;
    $newMinisite->save();

    $pages = $minisite->pages()->get();

    foreach($pages as $page){
      $newPage = $page->replicate();
      $newPage->minisite_id = $newMinisite->id;
      $newPage->save();
    }
  }

  public function replicateTicket(Ticket $ticket)
  {
    $newTicket = new Ticket();
    $newTicket->event_id = $this->id;
    $newTicket->image_1 = $ticket->image_1;
    $newTicket->image_2 = $ticket->image_2;
    $newTicket->description_fr = $ticket->description_fr;
    $newTicket->description_en = $ticket->description_en;
    $newTicket->description_de = $ticket->description_de;
    $newTicket->sup_info_field = $ticket->sup_info_field; // deprecated since v5.8.1
    $newTicket->type_id = $ticket->type_id;
    $newTicket->save();
  }

  public function replicateSessionsAndSessionGroups(Collection $sessions)
  {
    $sessionGroupsReplicated = []; //  ORIGINAL_REPLICATED_SESSION_GROUP_ID => NEW_SESSION_GROUP

    foreach ($sessions as $session) {

      $newSession = $session->copy($addCopyText = false);
      $newSession->event_id = $this->id;

      if ($session->sessionGroup != null) {
        $newSessionGroup = null;
        if (isset($session->sessionGroup)) {
          if (isset($sessionGroupsReplicated[$session->sessionGroup->id])) {
            $newSessionGroup = $sessionGroupsReplicated[$session->sessionGroup->id];
          }
          else {
            $newSessionGroup = $session->sessionGroup->replicate();
            $newSessionGroup->event_id = $this->id;
            $newSessionGroup->save();
            $sessionGroupsReplicated[$session->sessionGroup->id] = $newSessionGroup;
          }

          $newSession->session_group_id = $newSessionGroup->id;
        }
      }

      $newSession->save();
    }
  }

  /**
   * "Delete" Event
   */
  public function unActivate()
  {
    $this->active = false;
    $minisite = $this->minisite()->first();
    if ($minisite != null) {
      $minisite->slug = null;
      $minisite->save();
    }
    $this->save();
  }

  /**
   * return subscribeform url for a guest access hash
   */
  public function subscribeFormLinkWithHash(String $accessHash, ?Minisite $minisite, $companion = false)
  {
    if ($this->advanced_minisite_mode && !is_null($minisite) && !is_null($minisite->subscribeFormLink($accessHash, true, $companion))) {
      return $minisite->subscribeFormLink($accessHash, true, $companion);
    }
    else if(!$this->advanced_minisite_mode) {
      return route('events.subscribeForm.show', [$accessHash]);
    }
    return null;
  }

  // Accessors

  public function getLangsAttribute()
  {
    $langs = [];
    if ($this->language_fr) {
      $langs[] = 'fr';
    }
    if ($this->language_en) {
      $langs[] = 'en';
    }
    if ($this->language_de) {
      $langs[] = 'de';
    }
    if ($this->language_es) {
      $langs[] = 'es';
    }
    return $langs;
  }

  public function getExplicitLangsAttribute()
  {
    $langs = [];
    if ($this->language_fr) {
      $langs['fr'] = 'Français (FR)';
    }
    if ($this->language_en) {
      $langs['en'] = 'English (EN)';
    }
    if ($this->language_de) {
      $langs['de'] = 'Deutsch (DE)';
    }
    if ($this->language_es) {
      $langs['es'] = 'Español (ES)';
    }
    return $langs;
  }


  public function getEmptyPlacesAttribute($date)
  {
    return $this->countEmptyPlaces();
  }

  public function getStartDateAttribute($date)
  {
    $date = new \Carbon\Carbon($date);
    return $date;
  }

  public function getEndDateAttribute($date)
  {
    $date = new \Carbon\Carbon($date);
    return $date;
  }

  public function getMinisiteDemoUrlAttribute()
  {
    $minisite = $this->minisite()->first();
    if ($this->advanced_minisite_mode && !is_null($minisite)) {
      return $minisite->accessLink($this->getDemoHash());
    }
    return null;
  }

  public function getSurveyFormUrlAttribute()
  {
    $minisite = $this->minisite()->first();
    if ($this->advanced_minisite_mode && !is_null($minisite)) {
      return $minisite->surveyFormLink($this->getDemoHash());
    }
    else {
      return url('/')."/survey/".$this->getDemoHash()."?editable=0";
    }
  }

  public function getSubscribeFormUrlAttribute()
  {
    $minisite = $this->minisite()->first();
    if ($this->advanced_minisite_mode && !is_null($minisite)) {
      return $minisite->subscribeFormLink($this->getDemoHash());
    }
    else {
      return url('/')."/subscribe/".$this->getDemoHash()."?editable=0";
    }
  }

  public function getCompanionSubscribeFormUrlAttribute()
  {
    $minisite = $this->minisite()->first();
    if ($this->advanced_minisite_mode && !is_null($minisite)) {
      return $minisite->subscribeFormLink($this->getDemoHash(), true, true);
    }
    else {
      return url('/')."/subscribe/".$this->getDemoHash()."?editable=0";
    }
  }

  // Mutators

  public function setStartDateAttribute($date)
  {
    $this->attributes['start_date'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i:s');
  }

  public function setEndDateAttribute($date)
  {
    $this->attributes['end_date'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i:s');
  }

  // Queries

  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }

  public function scopePast($query)
  {
    return $query->whereDate('end_date', '<', date("Y-m-d"));
  }

  public function scopeFutur($query)
  {
    return $query->whereDate('start_date', '>', date("Y-m-d"));
  }

  public function scopeActual($query)
  {
    $today = date("Y-m-d H:i:s");
    return $query
    ->whereDate('end_date', '>=', $today)
    ->whereDate('start_date', '<=', $today);
  }

  public function scopeActualAndFutur($query)
  {
    return $query->whereDate('end_date', '>=', date("Y-m-d"));
  }

  public function setDefaultController($password, $hashPassword = true)
  {
    $con = new User();
    $con->name = "Ctrl_" . $this->id . $this->company->get()->first()->id;
    $con->email = "ctrl_" . $this->id . $this->company->get()->first()->id . "@eventwise.ch";
    $con->company_id = $this->company()->get()->first()->id;
    $con->event_id = $this->id;
    $con->password = $hashPassword ? Hash::make($password) : $password;
    $con->setDefaultControllerPermissions();
    $con->save();
  }

  // Relations

  public function type(){
    return $this->belongsTo('App\Type');
  }

  public function company()
  {
    return $this->belongsTo('App\Company');
  }

  public function subscribeForm()
  {
    return $this->hasOne('App\SubscribeForm');
  }

  public function surveyForm()
  {
    return $this->hasOne('App\SurveyForm');
  }

  public function minisite()
  {
    return $this->hasOne('App\Minisite');
  }

  public function categories()
  {
    return $this->belongsToMany('App\Category', 'event_category');
  }

  public function sessionGroups()
  {
    return $this->hasMany('App\SessionGroup');
  }

  public function sessions()
  {
    return $this->hasMany('App\Session');
  }

  public function guests()
  {
    return $this->hasMany('App\Guest');
  }

  public function companions()
  {
    return $this->hasManyThrough('App\Companion', 'App\Guest');
  }

  public function emails()
  {
    return $this->hasMany('App\Email');
  }

  public function controller()
  {
    return $this->hasOne('App\User');
  }

  public function verificationJobs()
  {
    return $this->hasMany('App\JobMailVerificationFeedback');
  }

  public function ticket()
  {
    return $this->hasOne('App\Ticket');
  }
}
