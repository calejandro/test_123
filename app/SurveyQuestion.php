<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name : varchar(255)
 * - survey_form_id : int(10) unsigned
 * - label : varchar(255)
 * - type : varchar(255)
 */
class SurveyQuestion extends Model
{
  protected $table = 'surveyQuestions';
  protected $primaryKey = 'id';
  protected $appends = ['responses'];

  protected $fillable = ['name', 'type', 'label'];

  // Accessors

  public function getResponsesAttribute()
  {
    $responses = $this->surveyResponses()->fromActiveGuest()->pluck('value')->toArray();

    $flat = [];
    foreach ($responses as $response) {
      if (is_array($response)) {
        foreach ($response as $nested) {
          $flat[] = $nested;
        }
      } else if (is_bool($response)) {
        $flat[] = $response ? "true" : "false";
      } else {
        $flat[] = $response;
      }
    }

    return array_count_values($flat);
  }

  // Relations

  public function surveyForm()
  {
    return $this->belongsTo('App\SurveyForm');
  }

  public function surveyResponses()
  {
    return $this->hasMany('App\SurveyResponse');
  }
}
