<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id   : int(10) unsigned
 * - name : varchar(255)
 * - description : text
 * - company_id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 */
class Category extends Model
{
  protected $table = 'categories';
  protected $primaryKey = 'id';

  protected $fillable = ['name', 'description', 'event_id']; // mass-assignable attributes

  // Relations

  public function company()
  {
    return $this->belongsTo('App\Company');
  }

  public function events()
  {
    return $this->belongsToMany('App\Event', 'event_category');
  }
}
