<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $checkin
 * @property string $created_at
 * @property int $guest_id
 * @property int $places
 * @property int $session_id
 * @property string $updated_at
 */
class SessionGuest extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'session_guest';

    /**
     * @var array
     */
    protected $fillable = ['checkin', 'created_at', 'guest_id', 'places', 'session_id', 'updated_at'];

}
