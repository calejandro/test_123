<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id   : int(10) unsigned
 * - companyName : varchar(255)
 * - firstname : varchar(255)
 * - lastname : varchar(255)
 * - phone : varchar(255)
 * - email : varchar(255)
 * - created_at : Date
 * - updated_at : Date
 * - lock : boolean
 * - smtp_host : varchar(255)
 * - smtp_port : int(11)
 * - smtp_from_mail_address : varchar(255)
 * - smtp_from_name : varchar(255)
 * - smtp_encryption : varchar(255)
 * - smtp_username : varchar(255)
 * - smtp_password : varchar(255)
 * - active : boolean
 */
class Company extends Model
{
  protected $table = 'companies';
  protected $primaryKey = 'id';

  protected $fillable = ['companyName', 'firstname', 'lastname', 
    'phone', 'email', 'smtp_host', 'smtp_port', 'smtp_from_mail_address', 
    'smtp_from_name', 'smtp_encryption', 'smtp_encryption', 'smtp_password'];

  protected $hidden = ['users', 'categories', 'events', 'templates'];

  public function hasCompleteSMTPConfig()
  {
    return $this->smtp_host != null && $this->smtp_port != null &&
      $this->smtp_from_mail_address != null && $this->smtp_from_name != null &&
      $this->smtp_password != null;
  }

  // Relations

  public function users()
  {
    return $this->hasMany('App\User');
  }

  public function categories()
  {
    return $this->hasMany('App\Category');
  }

  public function events()
  {
    return $this->hasMany('App\Event');
  }

  public function templates()
  {
    return $this->hasMany('App\Template');
  }

  public function MinisiteTemplates(){
    return $this->templates()->where("template_type", "MINISITE")->get();
  }

  public function EmailTemplates(){
    return $this->templates()->where("template_type", "EMAIL")->get();
  }
}
