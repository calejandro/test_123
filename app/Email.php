<?php

namespace App;

use App\I18n\LocalizableModel;
use App\EmailType;

/**
 * Attributes
 * - id : int(10) unsigned
 * - event_id : int(10) unsigned
 * - title : varchar(255)

 * - isInvit : boolean // /!\ will be deleted, remplaced by type
 * - isConfirm : boolean // /!\ will be deleted, remplaced by type
 * - isEventFullNotification : boolean // /!\ will be deleted, remplaced by type
 *
 * - type : string nullable (enum of EmailType) // null is "other"

 * - joinICS : boolean
 * - joinTicket: boolean
 * - sendWithDefaultMail : boolean
 * - sended : int(10) unsigned
 * - active : boolean
 * - template : longtext
 * - created_at : Date
 * - updated_at : Date
 * - html_fr : longtext
 * - html_en : longtext
 * - html_de : longtext

 * - from_column : varchar(255)
 */
class Email extends LocalizableModel
{
    protected $table = 'emails';
    protected $primaryKey = 'id';

    protected $fillable = ['title_fr', 'title_en', 'title_de', 'joinICS', 'joinTicket', 'sendWithDefaultMail', 
      'sended', 'template_fr', 'template_en', 'template_de', 'html_fr', 'html_en', 'html_de', 'from_column', 'type',
      'sessions_style'];

    protected $localizable = ['title', 'template', 'html'];

    protected $appends = ['isInvit', 'isConfirm', 'isEventFullNotification'];
    
    // Accessors

    public function getIsInvitAttribute() : bool
    {
      return $this->type === EmailType::INVITATION;
    }

    public function getIsConfirmAttribute() : bool
    {
      return $this->type === EmailType::CONFIRMATION ||
        $this->type === EmailType::COMPANIONS_CONFIRMATION;
    }

    public function getIsEventFullNotificationAttribute() : bool
    {
      return $this->type === EmailType::EVENT_FULL_NOTIFICATION;
    }

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function jobFeedbacks()
    {
      return $this->hasMany('App\JobFeedback', 'mail_id');
    }

    /**
     * Replicate copy and save
     */
    public function replicate(array $except = null, $addCopyText = false)
    {
      $newEmail = $this->copy($addCopyText);
      $newEmail->save();
    }

    /**
     * Copy create a perfect copy (except id) of the ressource without
     * saving it
     */
    public function copy($addCopyText = true) : Email
    {
      $newEmail = new Email();
      $newEmail->title_fr = ($addCopyText ? "[Copie] " : "") . $this->title_fr;

      $newEmail->type = $this->type;
      $newEmail->joinICS = $this->joinICS;
      $newEmail->joinTicket = $this->joinTicket;
      $newEmail->from_column = $this->from_column;
      $newEmail->sendWithDefaultMail = $this->sendWithDefaultMail;
      $newEmail->sended = 0;
      $newEmail->template_fr = $this->template_fr;
      $newEmail->html_fr = $this->html_fr;

      if($newEmail->isConfirm){
        $newEmail->title_en = ($addCopyText ? "[Copy] " : "") . $this->title_en;
        $newEmail->title_de = ($addCopyText ? "[Kopie] " : "") . $this->title_de;

        $newEmail->template_en = $this->template_en;
        $newEmail->html_en = $this->html_en;
        $newEmail->template_de = $this->template_de;
        $newEmail->html_de = $this->html_de;
      }

      $newEmail->active = $this->active;
      $newEmail->event_id = $this->event_id;

      return $newEmail;
    }

    public function hasContentInLang($lang){
      return !is_null($this->getTranslatedAttribute('title', $lang)) && !is_null($this->getTranslatedAttribute('html', $lang));
    }

}
