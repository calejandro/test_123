<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SurveyResponse;
use App\Event;
use App\Exceptions\GuestPartialUpdateException;

use Hash;

/**
 * Attributes
 * - id : int(10) unsigned
 * - guest_id : int(10) unsigned
 * - checkin: boolean
 * - access_hash : varchar(255)
 * - created_at : Date
 * - updated_at : Date
 * - firstname: varchar(255)
 * - lastname: varchar(255)
 * - email : varchar(255)
 * - company : varchar(255)
 * - extended_fields : json
 */
class Companion extends Model
{
    protected $table = 'companions';
    protected $primaryKey = 'id';

    protected $casts = [
        'extended_fields' => 'array'
    ];

    protected $fillable = ['firstname', 'lastname', 'company', 'email', 'extended_fields', 'checkin'];

    protected $with = ['guest']; // eager loading necessary for attributes accessors

    public function isSubscribedToSession(?Session $session)
    {
        if ($session == null) {
            return false;
        }
        $companionSession = $this->sessions()->where('sessions.id', $session->id)->first();
        return $companionSession != null;
    }

    public function isCheckinToSession(?Session $session)
    {
        if ($session == null) {
            return false;
        }
        $companionSession = $this->sessions()->where('sessions.id', $session->id)->first();
        if ($companionSession == null) {
            return false;
        }
        return $companionSession != null && $companionSession->pivot->checkin > 0;
    }

    // Override
    public static function create(array $attributes = [])
    {
        $attributes['access_hash'] = Guest::generateAccessHash('firstname_'.$attributes['guest_id'], 
            'lastname_'.$attributes['guest_id'], 'companion_'.$attributes['guest_id'].'@mail.com');
        $model = static::query()->create($attributes);
        return $model;
    }

    public function save(array $options = [])
    {
        if ($this->access_hash === NULL) {
            $this->access_hash = Guest::generateAccessHash('firstname_'.$this->guest_id, 
            'lastname_'.$this->guest_id, 
            'companion_'.$this->guest_id.'@mail.com');
        }
        parent::save($options);
    }

    // Accessors
    public function getActiveAttribute()
    {
        return $this->guest()->active;
    }

    // Queries
    public function scopeUnRegisteredToSession($query, Session $session)
    {
        $registeredCompanionsIds = $session->companions()->get()->pluck('id');
        return $query->whereNotIn('id', $registeredCompanionsIds);
    }

    public function scopeRegisteredToSession($query, Session $session)
    {
        $registeredCompanionsIds = $session->companions()->get()->pluck('id');
        return $query->whereIn('id', $registeredCompanionsIds);
    }

    public function scopeCheckinToSession($query, Session $session)
    {
        $checkinCompanionsIds = $session->companions()->wherePivot('checkin', true)->get()->pluck('id');
        return $query->whereIn('id', $checkinCompanionsIds);
    }

    public function scopeNotCheckinToSession($query, Session $session)
    {
        $checkinCompanionsIds = $session->companions()->wherePivot('checkin', true)->get()->pluck('id');
        return $query->whereNotIn('id', $checkinCompanionsIds);
    }

    /**
     * Active state based on guest active attribut
     */
    public function scopeActive($query)
    {
        return $query->join('guests', 'companions.guest_id', 'guests.id')
            ->where('guests.active', true);
    }

    // Relations
    public function guest()
    {
        return $this->belongsTo('App\Guest');
    }

    public function sessions()
    {
        return $this->belongsToMany('App\Session', 'session_companion')
            ->withPivot('checkin') // boolean
            ->withTimestamps();
    }
    
    public function checkinSessions()
    {
        return $this->belongsToMany('App\Session', 'session_companion')
            ->wherePivot('checkin', true) // boolean
            ->withTimestamps();
    }

    public function jobFeedbacks()
    {
        return $this->belongsToMany('App\JobFeedback');
    }

    public function notCheckinSessions()
    {
        return $this->belongsToMany('App\Session', 'session_companion')
            ->wherePivot('checkin', false) // boolean
            ->withTimestamps();
    }
}
