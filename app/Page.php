<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - minisite_id : int(10) unsigned
 * - page_type_id : int(10) unsigned
 * - lang : varchar(255) DEPRECATED
 * - order_id : int(11)
 * - name_fr : varchar(255)
 * - name_de : varchar(255)
 * - name_en : varchar(255)
 * - content_fr : longtext
 * - content_de : longtext
 * - content_en : longtext
 * - slug_fr : varchar(255)
 * - slug_de : varchar(255)
 * - slug_en : varchar(255)
 */
class Page extends Model // TODO: extend i18n and remove "getNameAttribute()"
{
    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function availableLanguages()
    {
      // TODO: all of this is ugly
      $langs = [];
      if ($this->content_fr != "" || !is_null($this->content_fr)){
        $langs[] = "fr";
      }
      if ($this->content_de != "" || !is_null($this->content_de)){
        $langs[] = "de";
      }
      if ($this->content_en != "" || !is_null($this->content_en)){
        $langs[] = "en";
      }

      $evnt_languages = $this->minisite->event->availableLanguages();

      return array_intersect($langs, $evnt_languages);
    }

    // Accessors

    // TODO: remove when i18n Implementation done
    public function getNameAttribute()
    {
      if ($this->name_fr != null) {
        return $this->name_fr;
      }
      else if ($this->name_en != null) {
        return $this->name_en;
      }
      else if ($this->name_de != null) {
        return $this->name_de;
      }
    }

    // Queries

    public function scopeAccessibleByGuest($query)
    {
      return $query->whereHas('pageType', function($q) {
        $q->accessibleByGuest();
      });
    }
    
    public function scopeAccessibleByCompanion($query)
    {
      return $query->whereHas('pageType', function($q) {
        $q->accessibleByCompanion();
      });
    }

    // Relations

    public function pageType()
    {
      return $this->belongsTo('App\PageType', 'page_type_id');
    }

    public function minisite()
    {
      return $this->belongsTo('App\Minisite');
    }
}
