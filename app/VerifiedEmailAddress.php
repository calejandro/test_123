<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - email : varchar(255)
 * - valid_email : int(11)
 */
class verifiedEmailAddress extends Model
{
  protected $table = 'verifiedEmailAddresses';

  protected $primaryKey = 'id';
}
