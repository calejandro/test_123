<?php

if (! function_exists('formatIntervalDates')) {
    function formatIntervalDates(\Carbon\Carbon $dateFrom, \Carbon\Carbon $dateTo)
    {
        if ($dateFrom->format('Ymd') === $dateTo->format('Ymd')) { // Same day
            return $dateFrom->format('d.m.Y H:i').' - '.$dateTo->format('H:i');
        }
        else {
            return $dateFrom->format('d.m.Y H:i').' - '.$dateTo->format('d.m.Y H:i');
        }
    }
}