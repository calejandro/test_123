<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// use ChrisKonnertz\DeepLy\DeepLy;

class i18nUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'i18n:update {src=fr}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update languages files from a src';

    protected $deepLy = null;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->deepLy = new DeepLy();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $srcLang = $this->argument('src');

        $languages = array_diff(['de', 'fr', 'en'], [$srcLang]);
        $files = ['auth', 'interface', 'models', 'pagination', 'passwords', 'validation'];
        foreach ($languages as $lang) {
            foreach ($files as $file) {
                $pathTo = './resources/lang/'.$lang.'/'.$file.'.php';
                $from = require('./resources/lang/'.$srcLang.'/'.$file.'.php');
                $to = require($pathTo);

                $this->saveFile($to,  $pathTo . '.old'); // save old files
                $translated = $this->translate($from, $to, $srcLang, $lang, $pathTo);
                $this->saveFile($translated, $pathTo . '.translated'); // save translations
            }
        }
    }

    private function translate($arrayFrom, $arrayTo, $langFrom, $langTo)
    {
        if (is_array($arrayFrom)) {
            foreach ($arrayFrom as $key => $value) {
                $arrayTo[$key] = $this->translate($value, $arrayTo[$key], $langFrom, $langTo);
            }
        }
        else { // string
            if ($arrayTo == null || $arrayTo == $arrayFrom) {

                echo "translate ?";
                echo $arrayFrom;
                echo $langFrom;
                echo $langTo;
                $t = $this->deepLy->translate($arrayFrom, strtoupper($langFrom), strtoupper($langTo));
                echo "translate OK !";
                return $t;
            }
            else {
                return $arrayTo; // do not translate
            }
            
        }
        return $arrayTo;
    }

    private function transText($src, $from, $to)
    {
        return $this->deepLy->translate($arrayFrom, strtoupper($langFrom), strtoupper($langTo));
    }

    private function saveFile($array, $path)
    {
        $content = "<?php return " . $this->varExport($array) . ";";
        file_put_contents($path, $content);
    }

    private function varExport($var, $indent="")
    {
        switch (gettype($var)) {
            case "string":
                return "'" . addcslashes($var, "\\\$\"\r\n\t\v\f") . "'";
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                         . ($indexed ? "" : $this->varExport($key) . " => ")
                         . $this->varExport($value, "$indent    ");
                }
                return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, TRUE);
        }
    }
}
