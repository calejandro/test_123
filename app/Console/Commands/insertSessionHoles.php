<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Session;

class insertSessionHoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:insertSessionHoles {eventid=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $deepLy = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $eventId = $this->argument('eventid');

        $sessions = Session::withTrashed()->orderBy('id', 'asc')->pluck('id');
        $idPrev = null;
        foreach ($sessions as $id) {
            if ($idPrev !== null) {
                if ($id !== $idPrev+1) {
                    // hole
                    $holeId = $idPrev+1;

                    // insert hole

                    $session = new Session();
                    $session->id = $holeId;
                    $session->event_id = $eventId;
                    $session->places = 0;
                    $session->name_en = 'hole session';
                    $session->name_fr = 'hole session';
                    $session->name_de = 'hole session';
                    $session->save();
                }
            }
            $idPrev = $id;
        }
       
    }
}
