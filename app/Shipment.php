<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Guest;
use App\Companion;

/**
 * Attributes
 * - id   : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - failed : json
 * - succesful : json
 * - failed_count : int(11)
 * - succesful_count : int(11)
 * - job_id : bigint(20) unsigned
 */

class Shipment extends Model
{
    protected $table = 'shipments';
    protected $primaryKey = 'id';

    protected $fillable = ['failed', 'succesful', 'failed_count', 'succesful_count'];

    public function failedGuestAndCompanions(){      
      $invalid = json_decode($this->failed);

      $invalidFromGuests = array_filter($invalid, function($v, $k){
        return $v->type == "guest";
      }, ARRAY_FILTER_USE_BOTH);
      
      $invalidFromCompanion = array_filter($invalid, function($v, $k){
        return $v->type = "companion";
      }, ARRAY_FILTER_USE_BOTH);
      
      $guests = Guest::whereIn('id', array_pluck($invalidFromGuests, 'person_id'))->get();
      $companions = Companion::whereIn('id', array_pluck($invalidFromCompanion, 'person_id'))->with('guest')->get();

      return [$guests, $companions];
    }

    // Relations

    public function jobFeedback()
    {
      return $this->belongsTo('App\JobFeedback');
    }
}
