<?php

namespace App\Policies;
use Log;
use App\User;
use App\Permission;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of events for his company.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user){
      return in_array('users-view', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can create events.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user){
        return in_array('users-create', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user){
        return in_array('users-update', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function destroy(User $user){
        return in_array('users-delete', $user->permissions()->get()->pluck('name')->toArray());
    }
}
