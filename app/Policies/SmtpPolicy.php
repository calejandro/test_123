<?php

namespace App\Policies;
use Log;
use App\User;
use App\Permission;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class SmtpPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of events for his company.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user){
      return in_array('configurations-update', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can create events.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user){
        return in_array('configurations-update', $user->permissions()->get()->pluck('name')->toArray());
    }
}
