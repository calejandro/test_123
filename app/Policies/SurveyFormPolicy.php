<?php

namespace App\Policies;
use Log;
use App\User;
use App\Permission;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyFormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user){
        return in_array('survey-update', $user->permissions()->get()->pluck('name')->toArray());
    }


}
