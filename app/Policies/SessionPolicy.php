<?php

namespace App\Policies;
use Log;
use App\User;
use App\Session;
use App\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of events for his company.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user){
      return in_array('sessions-view', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can see the overview of the events of his company.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function show(User $user){
      return in_array('sessions-overview', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can create events.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user){
        return in_array('sessions-create', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user){
        return in_array('sessions-update', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function destroy(User $user){
        return in_array('sessions-delete', $user->permissions()->get()->pluck('name')->toArray());
    }
}
