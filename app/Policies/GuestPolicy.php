<?php

namespace App\Policies;
use Log;
use App\User;
use App\Permission;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class GuestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of events for his company.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user){
      return in_array('guests-view', $user->permissions()->get()->pluck('name')->toArray()) || $user->isController();
    }

    /**
     * Determine whether the user can create events.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user){
        return in_array('guests-create', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user){
        return in_array('guests-update', $user->permissions()->get()->pluck('name')->toArray());
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function destroy(User $user){
        return in_array('guests-delete', $user->permissions()->get()->pluck('name')->toArray());
    }
}
