<?php

namespace App;

use App\I18n\LocalizableModel;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - visible : boolean
 * - template : longtext
 * - html : longtext
 * - formTemplate : longtext
 * - formHtml : longtext
 * - advancedFormTemplate : longtext
 * - advancedFormHtml : longtext
 * - advanced_form_companion_template : longtext
 * - advanced_form_companion_html : longtext
 * - event_id : int(10) unsigned
 * - label_color : int(11)
 * - allow_basefields_modification : boolean (default true)
 * - companion_lastname_required : boolean (default true)
 * - companion_firstname_required : boolean (default true)
 * - companion_company_required : boolean (default false)
 * - submit_text_fr : string
 * - submit_text_en : string
 * - submit_text_de : string
 * - submit_text_es : string
 * - submit_color_bg : string // CSS color code
 * - submit_color_text : string // CSS color code
 */
class SubscribeForm extends LocalizableModel
{
  protected $table = 'subscribeForms';
  protected $primaryKey = 'id';

  protected $fillable = [
    'visible', 'template', 'html', 'formTemplate', 'formHtml', 'advancedFormTemplate', 'advancedFormHtml', 
    'advanced_form_companion_template', 'advanced_form_companion_html',
    'submit_text_fr', 'submit_text_en', 'submit_text_de', 'submit_text_es', 'submit_color_bg', 'submit_color_text',
    'companion_lastname_required', 'companion_firstname_required', 'companion_company_required'
  ];

  protected $localizable = [
    'submit_text'
  ];

  // Accessors

  /**
   * Return formHtml with necessary balises for VueJS execution.
   * Doesn't include JS dependencies
   */
  public function getFormHtmlWithAdditionsAttribute()
  {
    // Will inject form html and then companions / send form button component in the [[eventSubscrForm]] tag
    return $this->formHtml . '<div id="app-subscribe-form-view"><subscribe-form></subscribe-form></div>';
  }

    /**
   * Balises for VueJS advanced surveyJS form execution.
   * Doesn't include JS dependencies
   */
  public function advancedFormHtmlWithAdditions(?Guest $guest)
  {
    $guestProp = '';
    if (isset($guest)) {
      $guestProp = ":guest='".$guest->toJson()."'";
    }

    $event = Event::findOrFail($this->event_id);

    // Will inject advanced form component in simple minisites in the [[eventSubscrForm]] tag
    return '<div id="app-subscribe-form-view">
      <advanced-subscribe-form
        :event-id="'. $this->event_id .'"'. $guestProp .'
        :visible-captcha="'.$event->visible_captcha.'"
        :allow-base-field-modification="'. $this->allow_basefields_modification .'"
      ></advanced-subscribe-form>
    </div>';
  }

  // Relations

  public function event()
  {
    return $this->belongsTo('App\Event');
  }
}
