<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Guest;
use App\JobMailVerificationFeedback;
use App\Facades\GuestService;
use App\Facades\EmailValidationService;
use Log;

class VerifyBatchMailsValidity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // laravel queue timeout param
    public $timeout = 5 * 60 * 60; // 5h

    protected $arrGuests;
    protected $eventId;
    protected $verificationFeedback;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $arrGuests, int $eventId, JobMailVerificationFeedback $verificationFeedback)
    {
        $this->arrGuests = $arrGuests;
        $this->eventId = $eventId;
        $this->verificationFeedback = $verificationFeedback;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', $this->timeout);

        $guests = Guest::active()
            ->where('event_id', $this->eventId)
            ->whereIn('email', array_column($this->arrGuests, 'email'))
            ->get();

        Log::channel('verification')->info('[VerifyBatchMailsValidity] Retrieve allready verified guests emails');
        $filteredGuests = EmailValidationService::getEmailAddressAlreadyVerified($guests);
        Log::channel('verification')->info('[VerifyBatchMailsValidity] ' . count($filteredGuests) . ' addresses to check (all addresses to validate: ' .count($guests->unique('email')).')');
        EmailValidationService::verifyBatchEmailsValidity($filteredGuests, $this->eventId, $this->verificationFeedback);
    }
}
