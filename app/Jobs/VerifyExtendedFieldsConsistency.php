<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Guest;
use App\Event;
use App\User;

use App\Facades\EventService;

use App\Utils\Sentry\SentryLogger;

use Log;

class VerifyExtendedFieldsConsistency implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // laravel queue timeout param
    public $timeout = 0.5 * 60 * 60; // 30min

    private $user;
    private $event;
    private $lastActionMessage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(?User $user, Event $event, string $lastActionMessage)
    {
        $this->user = $user;
        $this->event = $event;
        $this->lastActionMessage = $lastActionMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', $this->timeout);

        Log::info('[VerifyExtendedFieldsConsistency] handle');
        
        if (EventService::verifyExtendedFieldsConsitency($this->event)) {
            SentryLogger::logMessage('[VerifyExtendedFieldsConsistency] Extended fields consistency issue', [
                "lastActionMessage" => $this->lastActionMessage,
                "event" => $this->event->id,
                "user" => $this->user
            ]);
            Log::error('[VerifyExtendedFieldsConsistency] handle error');
        }
        else {
            Log::info('[VerifyExtendedFieldsConsistency] handle ok');
        }
    }
}
