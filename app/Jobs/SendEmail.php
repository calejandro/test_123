<?php

namespace App\Jobs;

use \Config;
use Datetime;
use Mail;
use GuzzleHttp\Client; // For the neverbounce API call
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Crypt;

use Log;
use Illuminate\Support\Collection;

use App\Email;
use App\Guest;
use App\Companion;
use App\Facades\EmailService;

use Lang;

use App\Utils\EmailSender;

use Illuminate\Mail\Mailer;
use Illuminate\View\Compilers\BladeCompiler;

use App\Utils\Sentry\SentryLogger;
use Carbon\Carbon;
use DbView;
use Exception;

class SendEmail implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  // laravel queue timeout param
  public $timeout = 5 * 60 * 60; // 5h

  protected $guestList;       // Guests to whom the mail will be sent
  protected $companionList;       // Guests to whom the mail will be sent
  protected $email;           // The concerned mail
  protected $event;           // The related event
  protected $company;         // The concerned company

  protected $joinICS;         // Set if yes or no ics calendar file must be attached
  protected $joinTicket;      // Set if pdf ticket must be attached to mail
  protected $containsQrCode;  // Set if yes or no the mail contains a qrcode
  protected $mailmode;        // Set the sending mode (personal SMTP server or Sponsorizes)
  protected $from_column;     // Extended field column name to use for from_name

  protected $nbSentMails;     // Successfully sent mails counter
  protected $client;          // Client for  neverbouce request

  protected $jobFeedback;     // Feedback object for this particular job
  protected $sendMail;        // For safety tests when logic is modified
  protected $blockCatchall;   // Hard define if we must block the Catchall addresses or not

  protected $mailValidMapping = ["invalid" => 0, "valid" => 1, "catchall" => 2, "unknown" => 3, "disposable" => 0]; // Keys depending on the neverbounce API
  protected $unknown_address_validation_retry_attempts;

  private function logInfo($message)
  {
    Log::channel('email')->info('[SendEmail] (email: '. $this->email->id.', jobFeedback: '. $this->jobFeedback->id .') '.$message);
  }

  private function logError($message, $title)
  {
    $this->logInfo($message);

    try {
      SentryLogger::logMessage('[SendEmail] '.$title, [
        "message" => $message,
        "email" => $this->email->id,
        "jobFeedback" => $this->jobFeedback->id
      ]);

      Log::channel('slack')->critical('[SendEmail] (email: '. $this->email->id.', jobFeedback: '. $this->jobFeedback->id .') '.$message);
    }
    catch(\Exception $exception) {
      $this->logInfo('Cannot log to slack');
      SentryLogger::logException($exception, []);
    }
  }

  /**
  * Create a new job instance.
  *
  * @return void
  */
  public function __construct(Collection $guestsList, Collection $companionsList, $email, $joinICS, $mailmode, $jobFeedback, $from_column = null)
  {
    $this->guestList = $guestsList;
    $this->companionList = $companionsList;
    $this->email = $email;
    $this->event = $this->email->event()->first();
    $this->company = $this->event->company()->first();
    $this->joinICS = $joinICS;
    $this->joinTicket = $this->email->joinTicket;
    $this->mailmode = $mailmode;
    $this->jobFeedback = $jobFeedback;

    $this->logInfo('Construct SendEmail Job');
    $this->logInfo('Event: '.$this->event->name.', '.$this->event->start_date.', '.$this->event->end_date.', '.$this->event->address);

    $this->containsQrCode = strpos($this->email->html, "[[qrcode]]") !== false ? true : false;
    $this->from_column = $from_column;

    $this->nbSentMails = 0;

    if($this->joinICS) {
      $this->logInfo(EmailService::generateICSFile($this->event));
    }

    // This can be toggled in urgency
    $this->sendMail = true;
    $this->blockCatchall = false;
  }


  /**
  *
  * Return true if less than 5% of catchall addresses in *all* guests list (5% is hardcoded for the moment and defined by eventwise)
  */
  public function mailingListIsSane($guestList)
  {
    $totalGuests = $this->event->guests()->where('active', 1)->get()->count(); // We want to test against ALL guests
    $catchallGuests = $guestList->where('valid_email', 2)->count();
    $catchallProportion = ($catchallGuests / $totalGuests) * 100; // Returns proportion of catchall in percent
    $this->jobFeedback->update(['catchall_proportion' => $catchallProportion]); // Updating feedback catchall proportion
    return $catchallProportion <= 5.0;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle(Mailer $mailer)
  {
    ini_set('max_execution_time', $this->timeout);

    try {
      $this->logInfo('[handle] Starting job');
      if ($this->company->smtp_host != null && empty($this->mailmode)) {
        $this->logInfo('[handle] Using company config: host:'.$this->company->smtp_host.', port:'.$this->company->smtp_port);

        if ($this->company->smtp_port == 587) {
          $transport = new \Swift_SmtpTransport($this->company->smtp_host, $this->company->smtp_port, "tls");
        }
        else {
          $transport = new \Swift_SmtpTransport($this->company->smtp_host, $this->company->smtp_port);
        }

        $transport->setUsername($this->company->smtp_username);
        $transport->setPassword(Crypt::decrypt($this->company->smtp_password));
        $this->jobFeedback->update([
          'mail_host' => $this->company->smtp_host,
          'mail_port' => $this->company->smtp_port,
          'mail_username' => $this->company->smtp_username
        ]);
      }
      else {
        $this->logInfo('[handle] Using Eventwise config: host:' . Config::get('mail.host') . ', port:' . Config::get('mail.port'));

        $transport = new \Swift_SmtpTransport(Config::get('mail.host'), Config::get('mail.port'), "tls");
        $transport->setUsername(Config::get('mail.username'));
        $transport->setPassword(Config::get('mail.password'));
        $this->jobFeedback->update([
          'mail_host' => Config::get('mail.host'),
          'mail_port' => Config::get('mail.port'),
          'mail_username' => Config::get('mail.username')
        ]);
      }

      $smtp = new \Swift_Mailer($transport);
      $mailer->setSwiftMailer($smtp);

      $this->logInfo('[handle] Guests: ' . $this->guestList->pluck('id')->implode(',') . ' Companions: ' . $this->companionList->pluck('id')->implode(','));

      $guestList = null;
      $invalidShipments = [];

      if(!$this->email->isConfirm) {
        // Generating new guest list set in function of catchall proportion
        if ($this->mailingListIsSane($this->guestList) || !$this->blockCatchall) {

          $this->logInfo('[handle] Guests before validation: ' . $this->guestList->count());

          $guestList = $this->guestList->whereIn('valid_email', [
            $this->mailValidMapping["valid"], 
            $this->mailValidMapping["catchall"], 
            $this->mailValidMapping["unknown"]
          ]);
          
          $invalidGuestList = $this->guestList->whereIn('valid_email', $this->mailValidMapping["invalid"]);

          // Add null validation
          foreach ($this->guestList as $guest) {
            if ($guest->valid_email === NULL) {
              $guestList->push($guest);
            }
          }

          foreach ($invalidGuestList as $invalidGuest){
            if ($invalidGuest->valid_email === 0){
              $invalidShipments = $this->addToIgnoredShipment($invalidShipments, $invalidGuest);
            }
          }

          $this->logInfo('[handle] Guests after validation: ' . $guestList->count()); 

          $this->jobFeedback->update(['catchall_addresses_sent' => true]); // Updating feedback sent mails
        }
        else {
          $guestList = $this->guestList->whereIn('valid_email', [$this->mailValidMapping["valid"]]);
          $this->logInfo('[handle] Confirmation: no validation');

          foreach ($this->guestList as $guest) {
            if ($guest->valid_email !== $this->mailValidMapping["valid"]) {
              $invalidShipments = $this->addToIgnoredShipment($invalidShipments, $guest);
            }
          }
        }
      }
      else {
        $guestList = $this->guestList;
      }

      // Attach guests and companions to jobFeedback
      if ($guestList != null) {
        $this->jobFeedback->guests()->attach($guestList->pluck('id'));
      }
      $this->jobFeedback->companions()->attach($this->companionList->pluck('id'));

      $validShipments = [];

      // After all, we send. This is a security test to not go past the checked guest
      if ($this->sendMail) {

        $sendingError = false;

        // Send guests emails
        foreach ($guestList as $guest) {
          try {
            $this->sendGuestMail($mailer, $guest);
            array_push($validShipments, $this->createShipmentState($guest, "guest"));
          }
          catch(\Swift_TransportException $ste) {
            $this->logError('[handle] Cannot send email to guest (Swift_TransportException) : ' . $guest->id, '[handle] Swift_TransportException');
            Log::channel('email')->info("Swift_TransportException, possible causes: Wrong SMTP configuration, Connexion refused (password, user or sender email), Too many requests, ...");
            $sendingError = true;

            $failure = $this->parseSwiftErrorCode($ste);
            array_push($invalidShipments, $this->createShipmentState($guest, "guest", $failure));
          }
          catch(\Throwable $exception) { // Throwable handle Exception and Error
            $this->logError('[handle] Cannot send email to guest : ' . $guest->id, '[handle] Exception');
            Log::channel('email')->info($exception);
            $sendingError = true;
            
            $newException["code"] = $exception->getCode() ? $exception->getCode() : 0 ;
            $newException["message"] = $exception->getMessage();
            array_push($invalidShipments, $this->createShipmentState($guest, "guest", $newException));
          }
        }

        // Send companions emails
        foreach ($this->companionList as $companion) {
          try {
            $this->sendCompanionMail($mailer, $companion);
            array_push($validShipments, $this->createShipmentState($companion, "companion"));
          }
          catch(\Swift_TransportException $ste) {
            $this->logError('[handle] Cannot send email to companion (Swift_TransportException) : ' . $companion->id, '[handle] Swift_TransportException');
            Log::channel('email')->info("Swift_TransportException, possible causes: Wrong SMTP configuration, Connexion refused (password, user or sender email), Too many requests, ...");
            $sendingError = true;

            $failure = $this->parseSwiftErrorCode($ste);
            array_push($invalidShipments, $this->createShipmentState($companion, "companion", $failure));
          }
          catch(\Throwable $exception) { // Throwable handle Exception and Error
            $this->logError('[handle] Cannot send email to companion : ' . $companion->id, '[handle] Exception');
            Log::channel('email')->info($exception);
            $sendingError = true;
           
            $newException["code"] = $exception->getCode() ? $exception->getCode() : 0 ;
            $newException["message"] = $exception->getMessage();
            array_push($invalidShipments, $this->createShipmentState($companion, "companion", $newException));
          }
        }

        Email::where('id', $this->email->id)->update([
          'sended' => $this->email->sended + $this->nbSentMails
        ]);

        $this->jobFeedback->update([
          'nb_ignored_mails' => $this->jobFeedback->nb_guests - $this->nbSentMails, // Updating feedback ignored mails
          'finished' => true,  // Updating feedback finished state
          'join_ics' => $this->joinICS,
          'nb_mails_sent' => $this->nbSentMails,
          'error' => $sendingError
        ]);
        $this->jobFeedback->saveShipmentState($invalidShipments, $validShipments);

        $this->logInfo('[handle] Finished');

      } // End of security test
    }
    catch(\Throwable $exception) {
        $this->failed($exception);
    }
  }

  private function sendGuestMail(Mailer $mailer, Guest $guest)
  {
    $this->logInfo('[SendMail] Sending email for guest: ' . $guest->id);
  
    EmailSender::sendGuestMail($mailer, $guest, $this->email, $this->event, $this->company, $this->containsQrCode, 
      $this->from_column, $this->joinICS, $this->joinTicket);

    $this->logInfo('[SendMail] email for guest: ' . $guest->id . ' sent');

    $this->nbSentMails += 1;

    // Set the guest to invited when the type of mail is invitation
    if ($this->email->isInvit) {
      $guest->invited = true;
      $guest->save();
    }
  }

  private function sendCompanionMail(Mailer $mailer, Companion $companion)
  {
    $this->logInfo('[SendMail] Sending email for companion: ' . $companion->id);
  
    EmailSender::sendCompanionMail($mailer, $companion, $companion->guest, $this->email, $this->event, $this->company, $this->containsQrCode, 
      $this->from_column, $this->joinICS, $this->joinTicket);

    $this->logInfo('[SendMail] email for companion: ' . $companion->id . ' sent');

    $this->nbSentMails += 1;
  }

  public function failed(\Throwable $exception)
  {
    $this->jobFeedback->update([
      'error' => true, // Updating feedback error state
      'finished' => true, // Updating feedback finished state
      'nb_mails_sent' => $this->nbSentMails
    ]);
    $this->logError('[handle] Job failed', '[handle] Send failed');

    if($exception instanceof \Swift_TransportException) {
      Log::channel('email')->info("Swift_TransportException, possible causes: Wrong SMTP configuration, Connexion refused (password, user or sender email), Too many requests, ...");
    }
    else {
      Log::channel('email')->info($exception);
    }
  }

  private function createShipmentState($person, $type, $failure = null){
    $state = [
      'lastname' => $person->lastname,
      'firstname' => $person->firstname,
      'email' => $person->email,
      'person_id' => $person->id,
      'type' => $type,
      'date' => Carbon::now()
    ];

    if($failure)
      $state['failure'] = $failure;

    return $state;
  }

  private function parseSwiftErrorCode($swiftException){
    $failure = [];
    $failure["message"] = strtok($swiftException->getMessage(), "\n");
    $failure["code"] = 0;

    try{
      if($swiftException->getCode() == 0){
        $tmp_msg = [];
        preg_match('/"([^"]{3})"/' , $failure["message"], $tmp_msg);
  
        foreach ($tmp_msg as $key => $value) {
          if (strlen($value) === 3) {
              $failure["code"] = (int) str_replace('"', '', $tmp_msg[1]);
              break;
            }
        }
  
      }else{
        $failure["code"] = $swiftException->getCode();
      }

    }catch(Exception $e){

    }

    return $failure;
  }

  private function addToIgnoredShipment($shipment, $guest){
    $ignored["code"] = 0;
    $ignored["message"] = "ignored";
    array_push($shipment, $this->createShipmentState($guest, "guest", $ignored));
    return $shipment;
  }
}
