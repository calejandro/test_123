<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Guest;
use App\JobMailVerificationFeedback;
use App\Facades\EmailValidationService;

class VerifySingleMailValidity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // laravel queue timeout param
    public $timeout = 5 * 60 * 60; // 5h

    protected $guest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Guest $guest)
    {
        $this->guest = $guest;

        // Building base jobVerificationFeedback object
        $jobVerificationFeedback = new JobMailVerificationFeedback();
        $jobVerificationFeedback->nb_addresses = 1; // We are in the single verif job :)
        $jobVerificationFeedback->event_id = $this->guest->event_id;
        $jobVerificationFeedback->processed = true;
        $jobVerificationFeedback->finished = true; // TODO: End of job should be triggered after actual job
        $jobVerificationFeedback->save();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', $this->timeout);
        EmailValidationService::verifySingleEmailAddressValidity($this->guest);
    }
}
