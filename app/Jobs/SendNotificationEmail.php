<?php

namespace App\Jobs;

use \Config;
use Datetime;
use Mail;
use GuzzleHttp\Client; // For the neverbounce API call
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Log;
use App\Email;
use App\Guest;
use App\JobFeedback;
use App\Facades\TemplateService;
use App\Utils\Sentry\SentryLogger;

use Illuminate\Mail\Mailer;
use Illuminate\View\Compilers\BladeCompiler;

use DbView;

class SendNotificationEmail implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  // laravel queue timeout param
  public $timeout = 5 * 60 * 60; // 5h

  protected $receiver;
  protected $notification_type;           // The concerned mail
  protected $data;
  protected $smtp;

  private function logInfo($message) {
    Log::channel('email')->info('[SendNotificationEmail] (receiver: '. $this->receiver .', type: '. $this->notification_type .') '.$message);
  }

  private function logError($message) {
    try {
      SentryLogger::logMessage('[SendNotificationEmail] '.$message, [
        "message" => $message
      ]);

      Log::channel('slack')->critical('[SendNotificationEmail] (email: (receiver: '. $this->receiver .', type: '. $this->notification_type .') '.$message);
    }
    catch(\Exception $exception) {
      $this->logInfo('Cannot log to slack');
      $this->logInfo($message);
      SentryLogger::logException($exception, []);
    }
  }

  /**
  * Create a new job instance.
  *
  * @return void
  */
  public function __construct($receiver, $notification_type, $data = null)
  {
    $this->logInfo('Construct SendNotificationEmail Job');

    $this->receiver = $receiver;
    $this->notification_type = $notification_type;
    $this->data = $data;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle(Mailer $mailer)
  {
    ini_set('max_execution_time', $this->timeout);

    $this->logInfo('[handle] Start');
    try{
      // Setting up the mail configuration
      $transport = new \Swift_SmtpTransport(Config::get('mail.host'), Config::get('mail.port'));
      $transport->setUsername(Config::get('mail.username'));
      $transport->setPassword(Config::get('mail.password'));
      $smtp = new \Swift_Mailer($transport);
      $mailer->setSwiftMailer($smtp);

      $notificationMail = '';
      $notificationSubject = '';
      $mReceiver = $this->receiver;

      if($this->notification_type == 'guest_infos_modification') {
        $notificationMail = view('emails_templates.notifications.guest_infos_modification', [
          'companyName' => $this->data['company'],
          'eventName' => $this->data['event'], 
          'guestBefore' => $this->data['guest_before'], 
          'guestAfter' => $this->data['guest_after']
        ]);
        $notificationSubject = 'Notification Eventwise | Changement des informations de base d\'un invité';
      }
      else if($this->notification_type == 'event_places_limit_reached') {
        $notificationMail = view('emails_templates.notifications.event_places_limit_reached', [
          'companyName' => $this->data['company'],
          'eventName' => $this->data['event']
        ]);
        $notificationSubject = 'Notification Eventwise | Nombre de places limite atteint pour un événement';
      }
      else if($this->notification_type == 'session_places_limit_reached') {
        $notificationMail = view('emails_templates.notifications.session_places_limit_reached', [
          'companyName' => $this->data['company'],
          'eventName' => $this->data['event'],
          'sessionName' => $this->data['session']
        ]);
        $notificationSubject = 'Notification Eventwise | Nombre de places limite atteint pour une session';
      }
      else {
        $notificationMail = '';
        $notificationSubject = 'Notification Eventwise | Changement des informations de base d\'un invité';
      }

      $mailer->send([], [], function($message) use($notificationSubject, $notificationMail, $mReceiver) {
        $message->to($mReceiver);
        $message->subject($notificationSubject)->setBody($notificationMail, 'text/html');
      });
      $this->logInfo('[handle] Sent');

    }catch(\Exception $exception){
      $this->logError('[handle] Failed');
      $this->logError($exception);
    }
  }

}
