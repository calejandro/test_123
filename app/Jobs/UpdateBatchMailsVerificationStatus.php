<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Guest;
use App\JobMailVerificationFeedback;
use App\Facades\EmailValidationService;
use App\Facades\GuestService;
use Log;

use App\Utils\Sentry\SentryLogger;

class UpdateBatchMailsVerificationStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // laravel queue timeout param
    public $timeout = 5 * 60 * 60; // 5h

    protected $neverbounceJobId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($neverbounceJobId)
    {
      $this->neverbounceJobId = $neverbounceJobId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', $this->timeout);

        $finished = EmailValidationService::updateBatchMailsVerificationStatus($this->neverbounceJobId);
        $jobfeedback = JobMailVerificationFeedback::where('neverbounce_job_id', $this->neverbounceJobId)->firstOrFail();

        if (config('app.disable_guests_email_validation')) {
            $jobfeedback->processed = true;
            $jobfeedback->finished = true; // Mark Job totally finished
            $jobfeedback->save();
        }

        if ($finished && !$jobfeedback->processed && !$jobfeedback->finished) {
            $jobfeedback->processed = true; // Mark Job result in proccess
            $jobfeedback->save();

            try {
                $results = EmailValidationService::getNeverbounceJobResults($this->neverbounceJobId);
                GuestService::processBatchMailsVerification($results);
            }
            catch(\Exception $e) {
                $verificationFeedback->error = true;

                SentryLogger::logException($e, [
                    'message' => '[UpdateBatchMailsVerificationStatus] Exception on retrieving results from NeverBounce API.',
                    'neverbounce_job_id' => $this->neverbounceJobId
                ]);
            }

            $jobfeedback->finished = true; // Mark Job totally finished
            $jobfeedback->save();
        }
    }
}
