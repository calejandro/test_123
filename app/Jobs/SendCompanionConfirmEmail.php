<?php

namespace App\Jobs;

use \Config;
use Datetime;
use Mail;
use GuzzleHttp\Client; // For the neverbounce API call
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Crypt;

use Log;
use App\Email;
use App\Companion;
use App\JobFeedback;

use App\Utils\EmailSender;

use Illuminate\Mail\Mailer;
use Illuminate\View\Compilers\BladeCompiler;

use App\Utils\Sentry\SentryLogger;

use DbView;

class SendCompanionConfirmEmail implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  // laravel queue timeout param
  public $timeout = 5 * 60 * 60; // 5h

  protected $companion;       // Companion to whom the mail will be sent
  protected $guest;
  protected $email;           // The concerned mail
  protected $event;           // The related event
  protected $company;         // The concerned company
  protected $jobFeedback;     // Feedback object for this particular job

  private function logInfo($message)
  {
    Log::channel('email')->info('[SendCompanionConfirmEmail] (email: '. $this->email->id.', jobFeedback: '. $this->jobFeedback->id .') '.$message);
  }

  private function logError($message, $title)
  {
    $this->logInfo($message);

    try {
      SentryLogger::logMessage('[SendCompanionConfirmEmail] '.$title, [
        "message" => $message,
        "email" => $this->email->id,
        "jobFeedback" => $this->jobFeedback->id
      ]);

      Log::channel('slack')->critical('[SendCompanionConfirmEmail] (email: '. $this->email->id.', jobFeedback: '. $this->jobFeedback->id .') '.$message);
    }
    catch(\Exception $exception) {
      $this->logInfo('Cannot log to slack');
      SentryLogger::logException($exception, []);
    }
  }

  /**
  * Create a new job instance.
  *
  * @return void
  */
  public function __construct(Companion $companion, Email $email, JobFeedback $jobFeedback)
  {
    $this->companion = $companion;
    $this->guest = $companion->guest;
    $this->email = $email;
    $this->event = $this->email->event()->first();
    $this->company = $this->event->company()->first();
    $this->jobFeedback = $jobFeedback;

    $this->logInfo('Construct SendCompanionConfirmEmail Job');
    $this->logInfo('Event: '.$this->event->name.', '.$this->event->start_date.', '.$this->event->end_date.', '.$this->event->address);
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle(Mailer $mailer)
  {
    ini_set('max_execution_time', $this->timeout);

    if ($this->email->isAttributeEmpty('html')) {
      return; // do not send empty companion confirm email
    }

    try {

      $this->logInfo('[handle] Starting job');
      if ($this->company->smtp_host != null && empty($this->mailmode)) {
        $this->logInfo('[handle] Using company config: host:'.$this->company->smtp_host.', port:'.$this->company->smtp_port);

        if ($this->company->smtp_port == 587) {
          $transport = new \Swift_SmtpTransport($this->company->smtp_host, $this->company->smtp_port, "tls");
        }
        else {
          $transport = new \Swift_SmtpTransport($this->company->smtp_host, $this->company->smtp_port);
        }

        $transport->setUsername($this->company->smtp_username);
        $transport->setPassword(Crypt::decrypt($this->company->smtp_password));
        $this->jobFeedback->update([
          'mail_host' => $this->company->smtp_host,
          'mail_port' => $this->company->smtp_port,
          'mail_username' => $this->company->smtp_username
        ]);
      }
      else {
        $this->logInfo('[handle] Using Eventwise config: host:' . Config::get('mail.host') . ', port:' . Config::get('mail.port'));

        $transport = new \Swift_SmtpTransport(Config::get('mail.host'), Config::get('mail.port'), "tls");
        $transport->setUsername(Config::get('mail.username'));
        $transport->setPassword(Config::get('mail.password'));

        $this->jobFeedback->update([
          'mail_host' => Config::get('mail.host'),
          'mail_port' => Config::get('mail.port'),
          'mail_username' => Config::get('mail.username')
        ]);
      }

      $smtp = new \Swift_Mailer($transport);
      $mailer->setSwiftMailer($smtp);

      $this->jobFeedback->companions()->attach($this->companion->id);

      $sendingError = false;

      try {
        $containsQrCode = strpos($this->email->html, "[[qrcode]]") !== false ? true : false;
    
        EmailSender::sendCompanionMail($mailer, $this->companion, $this->guest, $this->email, $this->event, $this->company, 
          $containsQrCode, $this->email->from_column, $this->email->joinICS, $this->email->joinTicket);
      }
      catch(\Swift_TransportException $ste) {
        $this->logError('[handle] Cannot send email to companion (Swift_TransportException) : ' . $this->companion->id, '[handle] Swift_TransportException');
        Log::channel('email')->info("Swift_TransportException, possible causes: Wrong SMTP configuration, Connexion refused (password, user or sender email), Too many requests, ...");
        $sendingError = true;
      }
      catch(\Exception $exception) {
        $this->logError('[handle] Cannot send email to companion : ' . $this->companion->id, '[handle] Exception');
        Log::channel('email')->info($exception);
        $sendingError = true;
      }


      Email::where('id', $this->email->id)->update([
        'sended' => $this->email->sended + 1
      ]);
  
      $this->jobFeedback->update([
        'nb_ignored_mails' => 0,
        'finished' => true,  // Updating feedback finished state
        'join_ics' => $this->email->joinICS,
        'nb_mails_sent' => 1,
        'error' => $sendingError
      ]);
      $this->logInfo('[handle] Finished');

    }
    catch(\Exception $exception) {
        $this->failed($exception);
    }
  }

  public function failed(\Exception $exception)
  {
    $this->jobFeedback->update([
      'error' => true, // Updating feedback error state
      'finished' => true, // Updating feedback finished state
      'nb_mails_sent' => 0
    ]);
    $this->logError('[handle] Job failed', '[handle] Send failed');

    if($exception instanceof \Swift_TransportException) {
      Log::channel('email')->info("Swift_TransportException, possible causes: Wrong SMTP configuration, Connexion refused (password, user or sender email), Too many requests, ...");
    }
    else {
      Log::channel('email')->info($exception);
    }
  }
}
