<?php

namespace App;

use App\I18n\LocalizableModel;

/**
 * Attributes
 * - id : int(10) unsigned
 * - event_id : int(10) unsigned
 * - name : varchar(255)
 * - description : text
 * - sup_info_field: varchar(255) // deprecated since v5.8.1
 * - created_at : Date
 * - updated_at : Date
 */
class Ticket extends LocalizableModel
{
  protected $table = 'tickets';

  protected $primaryKey = 'id';

  protected $fillable = ['image_1', 'image_2', 'description_fr', 'description_en', 'description_de',  'sup_info_field', 'type_id']; // mass-assignable attributes

  protected $localizable = ['description'];

  // Relations
  public function ticketType()
  {
    return $this->belongsTo('App\TicketType', 'type_id');
  }

  public function event()
  {
      return $this->belongsTo('App\Event');
  }
}
