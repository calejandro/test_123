<?php

namespace App\Exceptions;

use Exception;

class GuestPartialUpdateException extends Exception
{
    public $ignoredFields = [];

    public function __construct($ignoredFields) {
        parent::__construct();

        $this->ignoredFields = $ignoredFields;
    }

    public function __toString() {
        return "Guest has not been fully updated, cause of security constaint. Ignored fields: " . implode(", ", $this->ignoredFields); 
    }
}