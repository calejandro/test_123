<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property boolean $checkin
 * @property int $companion_id
 * @property string $created_at
 * @property int $session_id
 * @property string $updated_at
 */
class SessionCompanion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'session_companion';

    /**
     * @var array
     */
    protected $fillable = ['checkin', 'companion_id', 'created_at', 'session_id', 'updated_at'];

}
