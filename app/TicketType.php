<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name : varchar(255)
 * - description : varchar(255)
 * - template: le fichier blade correspondant
 */
class TicketType extends Model
{
  protected $table = 'ticket_types';

  protected $primaryKey = 'id';

  // Relations
  public function tickets()
  {
    return $this->hasMany('App\Ticket');
  }
}
