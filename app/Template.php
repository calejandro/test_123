<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name : varchar(255)
 * - template : longtext
 * - company_id : int(10) unsigned
 */
class Template extends Model
{
  protected $table = 'templates';

  protected $primaryKey = 'id';

  protected $fillable = ['name', 'template', 'template_type', 'sessions_styles']; // active

  protected $casts = [
    'template' => 'array',
  ];

  // Relations

  public function company()
  {
    return $this->belongsTo('App\Company');
  }
}
