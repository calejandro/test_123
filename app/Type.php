<?php

namespace App;

use App\I18n\LocalizableModel;

/**
 * Attributes
 * - id : int(10) unsigned
 * - name_fr : varchar(255)
 * - description_fr : text
 * - name_en : varchar(255)
 * - description_en : text
 * - name_de : varchar(255)
 * - description_de : text
 * - name_es : varchar(255)
 * - description_es : text
 * - created_at : Date
 * - updated_at : Date
 */
class Type extends LocalizableModel
{
  protected $table = 'types';
  protected $primaryKey = 'id';

  protected $localizable = [
    'name', 'description'
  ];

  // Relations

  public function events()
  {
    return $this->hasMany('App\Event');
  }
}
