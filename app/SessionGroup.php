<?php

namespace App;

use App\I18n\LocalizableModel;

/**
 * Attributes
 * - id : int(10) unsigned
 * - event_id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name_fr : varchar(255)
 * - name_de : varchar(255)
 * - name_en : varchar(255)
 */
class SessionGroup extends LocalizableModel
{
    protected $table = 'session_groups';

    protected $primaryKey = 'id';

    protected $fillable = ['name_fr', 'name_en', 'name_de', 'mandatory', 'allowed_session_subscriptions'];

    protected $localizable = [
        'name'
    ];

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function sessions()
    {
        return $this->hasMany('App\Session');
    }
}
