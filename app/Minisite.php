<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - title_fr : varchar(255)
 * - slug : varchar(255)
 * - event_id : int(10) unsigned
 * - site_background_color : varchar(255)
 * - site_background_image : varchar(255)
 * - menu_font_size : varchar(255)
 * - menu_font_color : varchar(255)
 * - menu_background_color : varchar(255)
 * - content_background_color : varchar(255)
 * - bottom_image_path : varchar(255)
 * - title_font_color : varchar(255)
 * - menu_item_hover_font_color : varchar(255)
 * - menu_item_hover_background_color : varchar(255)
 * - menu_item_active_font_color : varchar(255)
 * - menu_item_active_background_color : varchar(255)
 * - site_top_image : varchar(255)
 * - title_font_size : int(10) unsigned
 * - title_de : varchar(255)
 * - title_en : varchar(255)
 * - title_font : varchar(255)
 * - menu_font : varchar(255)
 */
class Minisite extends Model
{

    public const MINISITE_BG_IMG_MAX_SIZE = 2000;
    public const MINISITE_TOP_BOTTOM_IMG_MAX_SIZE = 910;

    protected $table = 'minisites';

    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $appends = [
      'default_survey_form_link',
      'default_subscribe_form_link',
      'index_link'
    ];

    /**
     * Available fonts for minisites
     * Add fallback fonts here if necessary
     */
    public static $availableFonts = [
      "Raleway" => "Raleway",
      "Arial" => "Arial",
      "\"Times New Roman\"" => "Times New Roman",
      "\"Courier New\"" => "Courier New",
      "\"Open Sans\", sans-serif" => "Open Sans"
    ];

    public static $availableAlignments = [
      "left" => "Left",
      "center" => "Center",
      "right" => "Right"
    ];

    public static $availableSizes = [
      "10" => "10%",
      "20" => "20%",
      "30" => "30%",
      "40" => "40%",
      "50" => "50%",
      "60" => "60%",
      "70" => "70%",
      "80" => "80%",
      "90" => "90%",
      "100" => "100%",
    ];

    public static $availableSpaces = [
      "10" => "10px",
      "20" => "20px",
      "30" => "30px",
      "40" => "40px",
      "50" => "50px",
      "60" => "60px",
      "70" => "70px",
      "80" => "80px",
      "90" => "90px",
      "100" => "100px",
    ];

    /**
     * Get link of the default index page
     * if $encodedUrl == false then the special characters will ne be replaced
     */
    public function indexLink(String $accessHash, $encodedUrl=true)
    {
      $url = (config('app.https') ? 'https://' : 'http://') . $this->slug . '.' .
        config('app.url') . route('minisite_subdomain_show', [$this->slug, $accessHash], false);
      
      if (!$encodedUrl) {
        $url = urldecode($url);
      }
      
      return $url;
    }

    /**
     * Get link of the default surveyForm page
     * if $encodedUrl == false then the special characters will ne be replaced
     */
    public function surveyFormLink(String $accessHash, $encodedUrl=true)
    {
      $typeSurvey = PageType::typeSurveyForm()->first();
      $page = $this->pages()->where('page_type_id', $typeSurvey->id)->first();
      if ($page == null) {
        return null;
      }

      $url = $this->pageLink($page, $accessHash);
      if (!$encodedUrl) {
        $url = urldecode($url);
      }
      return $url;
    }

    /**
     * Get link of the default subscribeForm page
     * if $encodedUrl == false then the special characters will ne be replaced
     */
    public function subscribeFormLink(String $accessHash, $encodedUrl=true, $companion=false)
    {
      $typeSubscribe = null;
      if ($companion) {
        $typeSubscribe = PageType::typeCompanionSubscribeForm()->first();
      }
      else {
        $typeSubscribe = PageType::typeSubscribeForm()->first();
      }

      $page = $this->pages()->where('page_type_id', $typeSubscribe->id)->first();
      if ($page == null) {
        return null;
      }

      $url = $this->pageLink($page, $accessHash);
      if (!$encodedUrl) {
        $url = urldecode($url);
      }
      return $url;
    }

    /**
     * Get link of a specific page
     * If the page doesn't exist in $lang, then fallback to other language
     */
    public function pageLink(Page $page, String $accessHash, $lang='fr')
    {
      $forceLang = null;
      $pageSlug = $page->{'slug_'.$lang};
      if ($pageSlug == null) {
        $pageSlug = $page->slug_en;
        $forceLang = 'en';
        if ($pageSlug == null) {
          $pageSlug = $page->slug_fr;
          $forceLang = 'fr';
          if ($pageSlug == null) {
            $pageSlug = $page->slug_de;
            $forceLang = 'de';
          }
        }
      }

      return (config('app.https') ? 'https://' : 'http://') . $this->slug . '.' .
          config('app.url') .
          route('minisite_page_subdomain_show', [$this->slug, $pageSlug, $page->id, $accessHash, $forceLang], false);
    }

    // Accessors

    /**
     * Generate link with "[[accessHash]]" to be replaced by the guest's token
     */
    public function getDefaultSurveyFormLinkAttribute()
    {
      return $this->surveyFormLink('[[accessHash]]', false);
    }

    /**
     * Generate link with "[[accessHash]]" to be replaced by the guest's token
     */
    public function getDefaultSubscribeFormLinkAttribute()
    {
      return $this->subscribeFormLink('[[accessHash]]', false);
    }

    /**
     * Generate link with "[[accessHash]]" to be replaced by the guest's token
     */
    public function getIndexLinkAttribute()
    {
      return $this->indexLink('[[accessHash]]', false);
    }

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function pages()
    {
      return $this->hasMany('App\Page');
    }

    public function accessLink(?string $accessHash = null, ?string $lang = null)
    {
      if ($accessHash != null) {
        $accessHash = '/'.$accessHash;
      }
      if ($lang != null) {
        $lang = '/'.$lang;
      }
      return (config('app.https') ? 'https://' : 'http://') . $this->slug.'.'.config('app.url').$accessHash.$lang;
    }
}
