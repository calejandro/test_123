<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SurveyResponse;
use App\Event;
use App\Companion;
use App\Exceptions\GuestPartialUpdateException;
use App\Facades\EmailService;

use App\Utils\StringUtil;

use Hash;

/**
 * Attributes
 * - id : int(10) unsigned
 * - event_id : int(10) unsigned
 * - firstname : varchar(255)
 * - lastname : varchar(255)
 * - company : varchar(255)
 * - email : varchar(255)
 * - extendedFields : JSON
 * - invited : boolean
 * - subscribed : boolean
 * - checkin : int(10) unsigned, default: 0
 * - active : boolean
 * - created_at : Date
 * - updated_at : Date
 * - accessHash : varchar(255)
 * - valid_email : int (0, 1, 2, 3) ["invalid" => 0, "valid" => 1, "catchall" => 2, "unknown" => 3]
 * - declined : boolean
 * - language : varchar(255) nullable, values: 'fr', 'en', 'de', null
 * - subscribed_at : Date
 */
class Guest extends Model
{
    public const EMAIL_VALIDITY = [
        "invalid" => 0,
        "valid" => 1,
        "catchall" => 2,
        "unknown" => 3,
        "unchecked" => NULL
    ];

    public const EXPLICIT_LOCALES =  [
        'fr' => 'Français (FR)',
        'en' => 'English (EN)',
        'de' => 'Deutsch (DE)'
    ];

    protected $table = 'guests';
    protected $primaryKey = 'id';

    protected $casts = [
        'extendedFields' => 'array'
    ];

    protected $dates = [
        'subscribed_at'
    ];

    protected $appends = ['survey', 'nbCompanions'];

    protected $fillable = ['firstname', 'lastname', 'email', 'company', 'extendedFields', 'invited', 'checkin', 'valid_email', 'language'];

    /**
     * Update the guest without changing the filled values
     * If filled values are not the same then return "false"
     * else return true
     */
    public function updateWithoutChange($rawGuest)
    {
        $notUpdated = [];
        if (array_key_exists('email', $rawGuest) && $this->email == null) {
            $this->email = $rawGuest['email'];
        }
        else if (array_key_exists('email', $rawGuest) && $this->email != $rawGuest['email']) {
            $notUpdated[] = 'email';
        }

        if (array_key_exists('firstname', $rawGuest) && $this->firstname == null) {
            $this->firstname = $rawGuest['firstname'];
        }
        else if (array_key_exists('firstname', $rawGuest) && $this->firstname != $rawGuest['firstname']) {
            $notUpdated[] = 'firstname';
        }

        if (array_key_exists('lastname', $rawGuest) && $this->lastname == null) {
            $this->lastname = $rawGuest['lastname'];
        }
        else if (array_key_exists('lastname', $rawGuest) && $this->lastname != $rawGuest['lastname']) {
            $notUpdated[] = 'lastname';
        }

        $nComp = $this->nbCompanions;
        if (array_key_exists('companions', $rawGuest) && $nComp <= $rawGuest['companions']) {
            // TODO #419 watch this for update of nominative comps. ANALYSE
            // Ici on ajoute les companions supplémentaires entrants
            $this->addCompanions($rawGuest['companions'] - $nComp);
        }
        else if (array_key_exists('companions', $rawGuest)) {
            $notUpdated[] = 'companions';
        }

        if (array_key_exists('extendedFields', $rawGuest)) {
            $extFields = $this->extendedFields;
            foreach ($rawGuest['extendedFields'] as $key => $value) { // insert only new field
                if (array_key_exists($key, $extFields) && ($extFields[$key] != null && $extFields[$key] != '') && $extFields[$key] != $value) {
                    // Testing if we have an array so the test of difference will change
                    if (is_array($extFields[$key])){
                      if (count($extFields[$key]) > count($value)){ // TODO test values and not length
                        $notUpdated[] = $key;
                      }
                      else{
                        $extFields[$key] = $value;
                      }
                    }
                    else{
                      $notUpdated[] = $key;
                    }
                }
                else {
                    $extFields[$key] = $value;
                }
            }
            $this->extendedFields = $extFields;
        }

        $this->save();


        if (count($notUpdated) > 0) {
            throw new GuestPartialUpdateException($notUpdated);
        }
    }

    /**
     * Create n new companions for a guest.
     * Those companions are non-nominatives (no firstname, lastname nor email)
     * @return Companion[]
     */
    public function addCompanions(int $n) : array
    {
        $companionsAdded = [];
        while ($n > 0) {
            $companion = new Companion();
            $companion->guest_id = $this->id;
            $companion->save();
            $companionsAdded[] = $companion;
            $n = $n - 1;
        }
        return $companionsAdded;
    }


    /**
     *  Add nominative companions to this guest.
     *  This is the equivalent of addCompanions but
     *  when the event has enabled nominative_companions.
     * @return [
     *   'companions' => Companion[], // final list of companions with Ids, in same order as provided
     *   'updatedCompanions' => Companion[], // companions updated, order not preserved
     *   'createdCompanions' => Companion[], // companions created, order not preserved
     *   'deletedCompanions' => Companion[] // companions deleted, order not preserved
     * ]
     */
    public function addUpdateDeleteNominativeCompanions(?array $companionsData): array
    {
        $companions = [];
        $updatedCompanions = [];
        $createdCompanions = [];
        $deletedCompanions = [];

        $oldCompanions = $this->companions()->get();
        $companionIdsToRemove = $oldCompanions->pluck('id');

        if ($companionsData != null) {
            foreach ($companionsData as $index => $companionData) {
                if (isset($companionData['id'])) {
                    $updated = $this->updateNominativeCompanion($companionData['id'], $companionData);
                    $companions[$index] = $updated;
                    $updatedCompanions[] = $updated;

                    $companionIdsToRemove = $companionIdsToRemove->filter(function($value, $key) use ($updated) {
                        return $value != $updated->id;
                    });
                }
                else {
                    $created = $this->addNominativeCompanion($companionData);
                    $companions[$index] = $created;
                    $createdCompanions[] = $created;
                }
            }
        }

        foreach ($companionIdsToRemove as $companionId) {
            $deleted = $this->companions()->findOrFail($companionId);
            $deletedCompanions[] = $deleted;
            $deleted->delete();
        }

        return [
            'companions' => $companions,
            'updatedCompanions' => $updatedCompanions,
            'createdCompanions' => $createdCompanions,
            'deletedCompanions' => $deletedCompanions
        ];
    }

    /**
     * Update nominative companion of guest
     */
    public function updateNominativeCompanion(int $id, array $companion): Companion
    {
        $uComp = $this->companions()->findOrFail($id);
        $uComp->firstname = isset($companion['firstname']) ? $companion['firstname'] : NULL;
        $uComp->lastname = isset($companion['lastname']) ? $companion['lastname'] : NULL;
        $uComp->email = $companion['email'];
        $uComp->company = isset($companion['company']) ? $companion['company'] : NULL;
        $uComp->save();

        return $uComp;
    }

    /**
     *  Add nominative companions to this guest.
     *  This is the equivalent of addCompanions but
     *  when the event has enabled nominative_companions.
     */
    public function addNominativeCompanion(array $companion): Companion
    {
        // New companions to create is incoming
        $newCompanion = new Companion();
        $newCompanion->guest_id = $this->id;
        $newCompanion->firstname = isset($companion['firstname']) ? $companion['firstname'] : NULL;
        $newCompanion->lastname = isset($companion['lastname']) ? $companion['lastname'] : NULL;
        $newCompanion->email = $companion['email'];
        $newCompanion->company = isset($companion['company']) ? $companion['company'] : NULL;
        $newCompanion->save();

        return $newCompanion;
    }

    /**
     * Count reserved places to a session for this guest and his companions
     */
    public function countSessionPlaces(?Session $session)
    {
        if ($session == null) {
            return 0;
        }
        $nbCompanionRegistered = $this->companions()->with('sessions')->get()->reduce(function($carry, $item) use ($session) {
            if ($carry == null) {
                return collect($item->sessions->where('id',  $session->id));
            }
            return $carry->concat($item->sessions->where('id',  $session->id));
        });
        $nbCompanionRegistered = ($nbCompanionRegistered == null) ? 0 : $nbCompanionRegistered->count(); // 0 to n
        $nbGuestSessionRegistered = $this->isSubscribedToSession($session) ? 1 : 0; // 0 to 1
        return $nbCompanionRegistered + $nbGuestSessionRegistered;
    }

    /**
     * Count checkin places to a session for this guest and his companions
     */
    public function countSessionCheckin(?Session $session)
    {
        if ($session == null) {
            return 0;
        }
        $companionsCheckin = $this->companions()->checkinToSession($session)->count();
        return $this->isCheckinToSession($session) ? $companionsCheckin + 1 : $companionsCheckin;
    }

    public function isCheckinToSession(?Session $session)
    {
        if ($session == null) {
            return 0;
        }
        $guestSession = $this->sessions()->where('sessions.id', $session->id)->first();
        if ($guestSession == null) {
            return false;
        }
        return $guestSession != null && $guestSession->pivot->checkin > 0;
    }

    public function isSubscribedToSession(?Session $session)
    {
        if ($session == null) {
            return false;
        }
        $guestSession = $this->sessions()->where('sessions.id', $session->id)->first();
        return $guestSession != null;
    }

    /**
     * Count checkin guests to the event
     */
    public function countCheckin()
    {
        $companionsCheckin = $this->companions()->where('checkin', true)->count();
        return $this->checkin ? $companionsCheckin + 1 : $companionsCheckin;
    }

    /**
     * Remove n companions starting by the most recently added (greatest ID)
     * @return Companion[] removed
     */
    public function removeCompanions(int $n) :  array
    {
        $deletedCompanions = [];
        $companions = $this->companions()->get();
        $i = 0;
        while ($i < $n && $companions->count() > $i) {
            $companionToDelete = $companions[($companions->count() - 1) - $i];
            $deletedCompanions[] = $companionToDelete;
            $companionToDelete->delete();
            $i = $i + 1;
        }
        return $deletedCompanions;
    }

    public function checkActive()
    {
        if (!$this->active) {
            throw new ModelNotFoundException('Ressource not found');
        }
    }

    public function addSurveyResponse($surveyForm, $key, $value)
    {
        $question = $surveyForm->surveyQuestions()->where('name', $key)->first();

        if ($question->surveyResponses()->where('guest_id', $this->id)->count() > 0) {
            $response = $question->surveyResponses()->where('guest_id', $this->id)->first();
        }
        else {
            $response = new SurveyResponse();
            $response->guest_id = $this->id;
            $response->survey_question_id = $this->event()->first()->surveyForm()->first()->surveyQuestions()->where('name', $key)->first()->id;
        }
        $response->value = $value;
        $response->save();
    }

    public static function getExtendedFieldListForEvent(Event $event)
    {
        $g = $event->guests()->where('active', 1)->first();
        if (!is_null($g) && !is_null($g->extendedFields)) {
            return array_keys($g->extendedFields);
        }
        else {
            return null;
        }
    }

    // Override

    public static function create(array $attributes = [])
    {
        $attributes['accessHash'] = Guest::generateAccessHash($attributes['firstname'], $attributes['lastname'], $attributes['email']);
        $model = static::query()->create($attributes);
        return $model;
    }

    public function save(array $options = [])
    {
        if($this->accessHash === NULL) {
            $this->accessHash = Guest::generateAccessHash($this->firstname, $this->lastname, $this->email);
        }
        parent::save($options);
    }

    // Accessors

    public function getSurveyAttribute()
    {
        return $this->surveyResponses()->get();
    }

    public function getNbCompanionsAttribute()
    {
        return $this->companions()->count();
    }

    /**
     * Return original attributes list of user
     */
    public static function getMinAttributes()
    {
        return ['firstname', 'lastname', 'company', 'email', 'language'];
    }

    public static function generateAccessHash($firstname, $lastname, $email)
    {
        return str_replace('/', '_', md5(StringUtil::randomString(8).':'.microtime().':'.$firstname.':'.$lastname.':'.$email));
    }

    // Mutators

    /**
     * Set subscribed_at automatically and remove declined
     */
    public function setSubscribedAttribute(bool $value)
    {
        $this->attributes['subscribed'] = $value;
        $this->attributes['subscribed_at'] = $value ? now() : null;

        if ($value) {
            $this->attributes['declined'] = false;
        }
    }

    /**
     * Set declined field of the guest
     * Warning : Delete all sessions subscriptions of the guest if declined
     * Warning : Delete all companions of the guest if declined
     */
    public function setDeclinedAttribute(bool $value)
    {
        $this->attributes['declined'] = $value;

        if ($value) {
            $this->companions()->delete();
            $this->sessions()->detach();
        }
    }

    // Queries

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeInvited($query)
    {
        return $query->where('invited', true);
    }

    public function scopeSubscribed($query)
    {
        return $query->where('subscribed', true);
    }

    public function scopeCheckin($query)
    {
        return $query->where('checkin', true);
    }

    public function scopeEmailValidity($query, ?int $validity)
    {
        if ($validity == null) {
            return $query->whereNull('valid_email');
        }
        else {
            return $query->where('valid_email', $validity);
        }
    }

    // Relations

    public function companions()
    {
      return $this->hasMany('App\Companion');
    }

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function surveyResponses()
    {
      return $this->hasMany('App\SurveyResponse');
    }

    public function surveyQuestions()
    {
        return $this->hasManyThrough('App\SurveyQuestion', 'App\SurveyResponse');
    }

    public function jobFeedbacks()
    {
      return $this->belongsToMany('App\JobFeedback');
    }

    public function sessions()
    {
        return $this->belongsToMany('App\Session', 'session_guest')
            ->withPivot('checkin')
            ->withPivot('places') // deprecated
            ->withTimestamps();
    }
}
