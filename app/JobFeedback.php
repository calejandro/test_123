<?php

namespace App;

use App\Shipment;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - finished : boolean
 * - error : boolean
 * - catchall_addresses_sent : boolean
 * - join_ics : boolean
 * - specific_config : boolean
 * - nb_guests : int(11)
 * - nb_tested_guests : int(11)
 * - nb_mails_sent : int(11)
 * - nb_ignored_mails : int(11)
 * - catchall_proportion : double
 * - from_column : varchar(255)
 * - mail_host : varchar(255)
 * - mail_port : varchar(255)
 * - mail_username : varchar(255)
 * - mail_from_address : varchar(255)
 * - mail_from_name : varchar(255)
 * - event_id : int(10) unsigned
 * - mail_id : int(10) unsigned
 * - user_id : int(10) unsigned
 */
class JobFeedback extends Model
{
    protected $table = 'jobFeedbacks';

    protected $primaryKey = 'id';

    protected $guarded = [];
    protected $appends = array('hasShipment');

    // Relations

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function mail()
    {
      return $this->belongsTo('App\Mail');
    }

    public function guests()
    {
      return $this->belongsToMany('App\Guest');
    }

    public function companions()
    {
      return $this->belongsToMany('App\Companion');
    }

    public function shipment()
    {
      return $this->hasOne('App\Shipment', 'jobfeedback_id');
    }

    public function getHasShipmentAttribute(){
      return $this->shipment != null;
    }

    /**
     * Save the shipment state of the JobFeedback in the Database
     */
    public function saveShipmentState(array $failedLoad, array $succesfulLoad)
    {
        $newShipment = new Shipment();
        $newShipment->failed = json_encode($failedLoad);
        $newShipment->succesful = json_encode($succesfulLoad);
        $newShipment->failed_count = count($failedLoad);
        $newShipment->succesful_count = count($succesfulLoad);
        $newShipment->jobfeedback_id = $this->id;
        $newShipment->save();   
    }

    public static function setToResend($oldJob)
    {
        $job = new JobFeedback();
        $job->event_id = $oldJob->event_id;
        $job->mail_id = $oldJob->mail_id;
        $job->user_id = $oldJob->user_id;
        $job->from_column = $oldJob->from_column;
        return $job;
    }
}
