<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Attributes
 * - id : int(10) unsigned
 * - created_at : Date
 * - updated_at : Date
 * - name : varchar(255)
 * - description : varchar(255)
 */
class PageType extends Model
{
  protected $table = 'page_types';

  protected $primaryKey = 'id';

  // Queries

  public function scopeAccessibleByGuest($query)
  {
    return $query->whereIn('name', ['Contenu', 'Sondage', 'Inscription']); // TODO: i18n
  }

  public function scopeAccessibleByCompanion($query)
  {
    return $query->whereIn('name', ['Contenu', 'Formulaire accompagnant']); // TODO: i18n
  }

  public function scopeTypeContent($query)
  {
    return $query->where('name', 'Contenu'); // TODO: i18n
  }

  public function scopeTypeSurveyForm($query)
  {
    return $query->where('name', 'Sondage'); // TODO: i18n
  }

  public function scopeTypeSubscribeForm($query)
  {
    return $query->where('name', 'Inscription'); // TODO: i18n
  }

  public function scopeTypeCompanionSubscribeForm($query)
  {
    return $query->where('name', 'Formulaire accompagnant'); // TODO: i18n
  }

  public function isTypeSubscribeForm()
  {
    return $this->name === 'Inscription'; // TODO: i18n
  }

  public function isTypeContent()
  {
    return $this->name === 'Contenu'; // TODO: i18n
  }

  public function isTypeCompanionSubscribeForm()
  {
    return $this->name === 'Formulaire accompagnant'; // TODO: i18n
  }

  public function isTypeSurveyForm()
  {
    return $this->name === 'Sondage'; // TODO: i18n
  }

  // Relations

  public function pages()
  {
    return $this->hasMany('App\Page');
  }
}
