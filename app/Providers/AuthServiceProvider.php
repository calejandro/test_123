<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Event;
use App\User;
use App\Guest;
use App\Email;
use App\Minisite;
use App\SurveyForm;
use App\Session;
use App\Http\Controllers\CompanySmtpController;

use App\Policies\EventPolicy;
use App\Policies\UserPolicy;
use App\Policies\GuestPolicy;
use App\Policies\MailPolicy;
use App\Policies\MinisitePolicy;
use App\Policies\SmtpPolicy;
use App\Policies\SurveyFormPolicy;
use App\Policies\SessionPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Event::class => EventPolicy::class,                     # Registering the event permission policy
        User::class => UserPolicy::class,                       # Registering the user permission policy
        Guest::class => GuestPolicy::class,                     # Registering the guest permission policy
        Email::class => MailPolicy::class,                      # Registering the mail permission policy
        Minisite::class => MinisitePolicy::class,               # Registering the minisite permission policy
        SurveyForm::class => SurveyFormPolicy::class,           # Registering the surveyform permission policy
        Session::class => SessionPolicy::class,                 # Registering the session permission policy
        // CompanySmtpController::class => SmtpPolicy::class,   # Registering the smtp configuration permission policy
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
