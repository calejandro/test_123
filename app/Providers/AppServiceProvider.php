<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

use App\Services\TemplateService;
use App\Services\SanitizeService;
use App\Services\SessionService;
use App\Services\SessionGroupService;
use App\Services\CompanyService;
use App\Services\CompanionService;
use App\Services\EmailService;
use App\Services\EmailValidationService;
use App\Services\EventService;
use App\Services\GuestService;
use App\Services\JobFeedbackService;
use App\Services\MinisiteService;
use App\Services\PageService;
use App\Services\PageTypeService;
use App\Services\SubscribeFormService;
use App\Services\SurveyFormService;
use App\Services\SurveyQuestionService;
use App\Services\SurveyResponseService;
use App\Services\TypeService;
use App\Services\UserService;
use App\Services\TicketService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapThree();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Appzcoder\CrudGenerator\CrudGeneratorServiceProvider');
        }

        $this->app->singleton('App\Services\TemplateService', function() {
            return new TemplateService();
        });

        $this->app->singleton('App\Services\SanitizeService', function() {
            return new SanitizeService();
        });

        $this->app->singleton('App\Services\SessionService', function() {
            return new SessionService();
        });

        $this->app->singleton('App\Services\SessionGroupService', function() {
            return new SessionGroupService();
        });

        $this->app->singleton('App\Services\CompanyService', function() {
            return new CompanyService();
        });

        $this->app->singleton('App\Services\EmailService', function() {
            return new EmailService();
        });

        $this->app->singleton('App\Services\EmailValidationService', function() {
            return new EmailValidationService();
        });

        $this->app->singleton('App\Services\EventService', function() {
            return new EventService();
        });

        $this->app->singleton('App\Services\GuestService', function() {
            return new GuestService();
        });

        $this->app->singleton('App\Services\CompanionService', function() {
            return new CompanionService();
        });

        $this->app->singleton('App\Services\JobFeedbackService', function() {
            return new JobFeedbackService();
        });

        $this->app->singleton('App\Services\MinisiteService', function() {
            return new MinisiteService();
        });

        $this->app->singleton('App\Services\PageService', function() {
            return new PageService();
        });

        $this->app->singleton('App\Services\PageTypeService', function() {
            return new PageTypeService();
        });

        $this->app->singleton('App\Services\SubscribeFormService', function() {
            return new SubscribeFormService();
        });

        $this->app->singleton('App\Services\SurveyFormService', function() {
            return new SurveyFormService();
        });

        $this->app->singleton('App\Services\SurveyQuestionService', function() {
            return new SurveyQuestionService();
        });

        $this->app->singleton('App\Services\SurveyResponseService', function() {
            return new SurveyResponseService();
        });

        $this->app->singleton('App\Services\TypeService', function() {
            return new TypeService();
        });

        $this->app->singleton('App\Services\UserService', function() {
            return new UserService();
        });

        $this->app->singleton('App\Services\TicketService', function() {
            return new TicketService();
        });
    }
}
