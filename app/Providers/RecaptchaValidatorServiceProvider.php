<?php

namespace App\Providers;

use ReCaptcha\ReCaptcha;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class RecaptchaValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Validator::extend('captcha', function ($attribute, $value, $parameters, $validator) {
            if (config('app.testing')) {
                return true; // always pass if app is in testing mode
            } else {
                $ip = request()->getClientIp();
                $recaptcha = new ReCaptcha(config('app.recaptcha_secret_key'), new \ReCaptcha\RequestMethod\CurlPost());
                $resp = $recaptcha->verify($value, $ip);

                return $resp->isSuccess();
            }
        });
    }
}
