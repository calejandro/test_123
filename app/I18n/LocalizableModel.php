<?php
 
namespace App\I18n;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use App;
use App\Event;

/**
 * LocalizableModel inspired by https://phraseapp.com/blog/posts/laravel-i18n-modelling-best-practices/
 */
abstract class LocalizableModel extends Model {
 
    /**
     * Localized attributes.
     *
     * @var array
     */
    protected $localizable = [];

    protected $fallbacks = ['fr', 'en', 'de']; // fallbacks in order

    protected $supported_locals = [
        'fr', 'en', 'de', 'es'
    ];

    /**
     * Whether or not to hide translated attributes e.g. name_en
     *
     * @var boolean
     */
    protected $hideLocaleSpecificAttributes = false;


    /**
     * Whether or not to append translatable attributes to array
     * output e.g. name
     *
     * @var boolean
     */
    protected $appendLocalizedAttributes = true;

    /**
     * Make a new translatable model
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        // We dynamically append localizable attributes to array output
        // and hide the localized attributes from array output
        foreach($this->localizable as $localizableAttribute) {
            if ($this->appendLocalizedAttributes) {
                $this->appends[] = $localizableAttribute;
            }

            if ($this->hideLocaleSpecificAttributes) {
                foreach($this->supported_locals as $locale) {
                    $this->hidden[] = $localizableAttribute.'_'.$locale;
                }
            }
        }

        parent::__construct($attributes);
    }
 
    /**
     * Magic method for retrieving a missing attribute.
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        // We determine the current locale and return the associated
        // locale-specific attribute e.g. name_en
        if (in_array($attribute, $this->localizable)) {
            return $this->getTranslatedAttributeWithFallback($attribute, App::getLocale(), $this->fallbacks);
        }
 
        return parent::__get($attribute);
    }

    public function hasTranslatedAttribute($attribute, $localCode): bool
    {
        return !empty($this->getTranslatedAttribute($attribute, $localCode));
    }

    public function getTranslatedAttribute($attribute, $localCode)
    {
        return $this->{$attribute.'_'.$localCode};
    }

    public function getTranslatedAttributeWithFallback($attribute, $localCode, $fallbackLocals)
    {
        $translated = $this->getTranslatedAttribute($attribute, $localCode);

        if ($translated === null || $translated === "") { // i18n fallback

            foreach ($fallbackLocals as $fallback) {
                $translated =  $this->getTranslatedAttribute($attribute, $fallback);
                if ($translated !== null && $translated !== "") {
                    return $translated;
                }
            }
            return null;
        }

        return $translated;
    }

    public function setTranslatedAttribute($attribute, $localCode, $value)
    {
        $this->{$attribute.'_'.$localCode} = $value;
    }

    public static function localAttributeField($attribute)
    {
        return $attribute.'_'.App::getLocale();
    }

    /*
    * Returns translated attribute or attribute in first lang available at event level
    *
    */
    public function getEventAwareTranslatedAttribute(string $attribute, Event $event)
    {
        $hasTranslatedAttribute = $this->hasTranslatedAttribute($attribute, App::getLocale());
        $localeInEvent = in_array(App::getLocale(), $event->getLangsAttribute());
        $localeHasContent = !is_null($this->getTranslatedAttribute($attribute, App::getLocale()));

        if ($hasTranslatedAttribute && $localeInEvent && $localeHasContent) {
            return $this->getTranslatedAttribute($attribute, App::getLocale());
        }
        else {
            if (count($event->getLangsAttribute()) === 0) {
                return $this->__get($attribute);
            }
            else {
                return $this->getTranslatedAttribute($attribute, $event->getLangsAttribute()[0]);
            }
        }
    }

    /**
     * Check the given attribut is empty (null or "") in all supported languages
     */
    public function isAttributeEmpty(string $attribute) : bool
    {
        return ($this->{$attribute.'_fr'} === null || $this->{$attribute.'_fr'} === "") &&
            ($this->{$attribute.'_de'} === null || $this->{$attribute.'_de'} === "") &&
            ($this->{$attribute.'_en'} === null || $this->{$attribute.'_en'} === "");
           
    }

    /**
     * Magic method for calling a missing instance method.
     * Necessary for JSON output
     *
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        // We handle the accessor calls for all our localizable attributes
        // e.g. getNameAttribute()
        foreach($this->localizable as $localizableAttribute) {
            if ($method === 'get'.Str::studly($localizableAttribute).'Attribute') {
                return $this->{$localizableAttribute};
            }
        }

        return parent::__call($method, $arguments);
    }
}