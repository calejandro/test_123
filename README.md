# Eventwise

## Authors

All right reserved to Sponsorize Sàrl, Genève, Rue des Pierres-du-Niton 17. (https://sponsorize.ch/)

Application develloped by HE-Arc Ingénierie. (https://www.he-arc.ch/)

## Dependencies

* PHP >= 7.1
* MySQL >= 5.7
* NPM = 8.12.0 (issues encuntered with newer versions)

The application is only approuved on systems:

* Debian 8
* Ubuntu 16.04.3 LTS

## Environment configuration

Following environments values needs to be provided:

```
APP_ENV, APP_KEY, APP_DEBUG, APP_LOG_LEVEL, APP_URL, APP_HTTPS, GOOGLE_MAP_ACCESS_TOKEN, NEVERBOUNCE_TOKEN, DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD, BROADCAST_DRIVER, CACHE_DRIVER, SESSION_DRIVER, QUEUE_DRIVER, MAIL_DRIVER, MAIL_HOST, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD, MAIL_ENCRYPTION, PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET
```

`.env.exemple` is an exemple file with these values.


Front-end configurations needs to be provided for email generate (server URL):

```
cp ./resources/assets/js/config/server.exemple.js ./resources/assets/js/config/server.js
nano resources/assets/js/config/server.js
```


## Deployment

### Deployment dependencies

* supervisor >= 3 is needed on production environnment.

`QUEUE_DRIVER` env variable must be setted with "database".

Please refear to https://laravel.com/docs/5.7/queues#supervisor-configuration for the supervisor configuration.

**Warning: the `--tries` parameters must be setted as `1`.**

### Deployment procedure

```
composer install
php artisan key:generate # run only one time
php artisan migrate
php artisan db:seed # run only one time
php artisan config:cache
php artisan queue:restart
npm run prod
```

## Development

For Windows users only, run with admin prompt:

```
npm install --global --production windows-build-tools
```

Compile the front-end:

```
npm install
npm run watch
```

In case of error with Windows, delete `./node_modules` and run: `npm install --no-bin-links`

# Run tests

Create `./.env.testing`, add `TESTING=true`.

## For Features + Dusk with docker

Ensure that `./run-test.sh` and `./wait-for` are in LF format.

```
npm run prod
docker-compose -f ./docker-tests.yml up
```

## For Features (HTTP + Unit) tests only

```
php artisan config:clear
php artisan migrate --env=testing
php artisan db:seed --env=testing
./vendor/bin/phpunit
```

## For Laravel Dusk tests (Selenium) only

`APP_URL` in `.env.testing` must be: `APP_URL=http://localhost:8000`

```
php artisan migrate --env=testing
php artisan serve --env=testing --quiet &
php artisan dusk --env=testing
```
