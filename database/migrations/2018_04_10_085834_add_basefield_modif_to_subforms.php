<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasefieldModifToSubforms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->boolean('allow_basefields_modification')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->dropColumn('allow_basefields_modification');
        });
    }
}
