<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitTextToSurveyFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
            $table->string('submit_text_fr')->default("Valider");
            $table->string('submit_text_en')->default("Validate and subscribe");
            $table->string('submit_text_de')->default("Validieren und Speichern");
            $table->string('submit_text_es')->default("Validar y registrar");

            $table->string('submit_color_bg')->default("#091d28");
            $table->string('submit_color_text')->default("#FFFFFF");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
            $table->dropColumn('submit_text_fr');
            $table->dropColumn('submit_text_en');
            $table->dropColumn('submit_text_de');
            $table->dropColumn('submit_text_es');

            $table->dropColumn('submit_color_bg');
            $table->dropColumn('submit_color_text');
        });
    }
}
