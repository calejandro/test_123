<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToEvent extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('events', function (Blueprint $table) {
      $table->integer('type_id')->unsigned()->nullable();
      $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade')->onUpdate('cascade');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('events', function (Blueprint $table) {
      $table->dropForeign('events_type_id_foreign');
      $table->dropColumn('type_id');
    });
  }
}
