<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->json('failed')->nullable();
            $table->json('succesful')->nullable();
            $table->integer('failed_count')->default(0);
            $table->integer('succesful_count')->default(0);
            $table->integer('jobfeedback_id')->unsigned();
            $table->foreign('jobfeedback_id')->references('id')->on('jobFeedbacks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
