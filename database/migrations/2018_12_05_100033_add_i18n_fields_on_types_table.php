<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddI18nFieldsOnTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->renameColumn('name', 'name_fr');
            $table->renameColumn('description', 'description_fr');
            $table->string('name_en')->nullable(); // nullables to avoid conflicts
            $table->text('description_en')->nullable();
            $table->string('name_de')->nullable();
            $table->text('description_de')->nullable();
            $table->string('name_es')->nullable();
            $table->text('description_es')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->renameColumn('name_fr', 'name');
            $table->renameColumn('description_fr', 'description');
            $table->dropColumn('name_en');
            $table->dropColumn('description_en');
            $table->dropColumn('name_de');
            $table->dropColumn('description_de');
            $table->dropColumn('name_es');
            $table->dropColumn('description_es');
        });
    }
}
