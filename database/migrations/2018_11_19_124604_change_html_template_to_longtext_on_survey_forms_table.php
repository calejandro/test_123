<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHtmlTemplateToLongtextOnSurveyFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
            $table->string('template', 4294967295)->change();
            $table->string('html', 4294967295)->change();
            $table->string('formTemplate', 4294967295)->change();
            $table->string('formHtml', 4294967295)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
            $table->text('template')->change();
            $table->text('html')->change();
            $table->text('formTemplate')->change();
            $table->text('formHtml')->change();
        });
    }
}
