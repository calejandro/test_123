<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('templates', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->string('name');
      $table->text('template')->nullable();
      $table->integer('company_id')->unsigned()->nullable();
      $table->foreign('company_id')->references('id')->on('companies');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('templates');
  }
}
