<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanionsRequirementsToSubscribeFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->boolean('companion_lastname_required')->default(true);
            $table->boolean('companion_firstname_required')->default(true);
            $table->boolean('companion_company_required')->default(false);         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->dropColumn('companion_lastname_required');
            $table->dropColumn('companion_firstname_required');
            $table->dropColumn('companion_company_required');
        });
    }
}
