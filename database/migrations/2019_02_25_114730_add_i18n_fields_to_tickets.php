<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddI18nFieldsToTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->renameColumn('description', 'description_fr');
            $table->text('description_en')->nullable(); // nullables to avoid conflicts
            $table->text('description_de')->nullable(); // nullables to avoid conflicts
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->renameColumn('description_fr', 'description');
            $table->dropColumn('description_en');
            $table->dropColumn('description_de');
        });
    }
}
