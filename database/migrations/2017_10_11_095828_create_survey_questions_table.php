<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveyQuestions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');

            $table->integer('survey_form_id')->unsigned()->nullable();
            $table->foreign('survey_form_id')->references('id')->on('surveyForms');

            $table->unique(array('name', 'survey_form_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('surveyQuestions');
        Schema::enableForeignKeyConstraints();
    }
}
