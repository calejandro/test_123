
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanionsSubscribeFormToSubscribeFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->longText('advanced_form_companion_template')->nullable();
            $table->longText('advanced_form_companion_html')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->dropColumn('advanced_form_companion_template');
            $table->dropColumn('advanced_form_companion_html');
        });
    }
}
