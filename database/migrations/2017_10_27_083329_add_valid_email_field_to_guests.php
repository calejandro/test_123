<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValidEmailFieldToGuests extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('guests', function (Blueprint $table) {
      $table->boolean('valid_email')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('guests', function (Blueprint $table) {
      $table->dropColumn('valid_email');
    });
  }
}
