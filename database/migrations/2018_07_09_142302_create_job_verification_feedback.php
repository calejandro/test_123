<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobVerificationFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobMailVerificationFeedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('finished')->default(false);
            $table->boolean('error')->default(0);
            $table->boolean('processed')->default(0);
            $table->integer('nb_addresses')->default(0);
            $table->integer('nb_valid_addresses')->default(0);
            $table->integer('nb_invalid_addresses')->default(0);
            $table->integer('nb_catchall_addresses')->default(0);
            $table->integer('nb_unknown_addresses')->default(0);
            $table->integer('nb_disposable_addresses')->default(0);
            $table->integer('nb_duplicate_addresses')->default(0);
            $table->integer('neverbounce_job_id')->default(0);

            $table->integer('event_id')->nullable()->unsigned();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobMailVerificationFeedbacks');
    }
}
