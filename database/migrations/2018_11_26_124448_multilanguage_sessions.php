<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultilanguageSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sessions', function (Blueprint $table) {
            
            // Titles
            $table->string('name_de')->nullable(true);
            $table->string('name_en')->nullable(true);

            // Rename name columns from default to FR
            $table->renameColumn('name', 'name_fr');
            
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sessions', function (Blueprint $table) {
            
            $table->dropColumn('name_de');
            $table->dropColumn('name_en');

            // Rename FR columns from default to FR
            $table->renameColumn('name_fr', 'name');

        });
    }
}
