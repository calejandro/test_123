<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugLangToMinisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
          $table->string('slug_de')->nullable(true);
          $table->string('slug_en')->nullable(true);

          $table->renameColumn('slug', 'slug_fr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
          $table->dropColumn('slug_de');
          $table->dropColumn('slug_en');

          $table->renameColumn('slug_fr', 'slug');
        });
    }
}
