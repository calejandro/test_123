<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveyResponses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->json('value');

            $table->integer('survey_question_id')->unsigned()->nullable();
            $table->foreign('survey_question_id')->references('id')->on('surveyQuestions');

            $table->integer('guest_id')->unsigned()->nullable();
            $table->foreign('guest_id')->references('id')->on('guests');

            $table->unique(array('guest_id', 'survey_question_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('surveyResponses');
        Schema::enableForeignKeyConstraints();
    }
}
