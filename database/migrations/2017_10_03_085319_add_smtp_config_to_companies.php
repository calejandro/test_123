<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmtpConfigToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('smtp_host')->nullable();
            $table->integer('smtp_port')->nullable();
            $table->string('smtp_from_mail_address')->nullable();
            $table->string('smtp_from_name')->nullable();
            $table->string('smtp_encryption')->nullable();
            $table->string('smtp_username')->nullable();
            $table->string('smtp_password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
          $table->dropColumn('smtp_host');
          $table->dropColumn('smtp_port');
          $table->dropColumn('smtp_from_mail_address');
          $table->dropColumn('smtp_from_name');
          $table->dropColumn('smtp_encryption');
          $table->dropColumn('smtp_username');
          $table->dropColumn('smtp_password');
        });
    }
}
