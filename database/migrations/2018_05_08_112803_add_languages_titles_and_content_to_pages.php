<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguagesTitlesAndContentToPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            // Titles
            $table->string('name_de')->nullable(true);
            $table->string('name_en')->nullable(true);

            // Contents
            $table->text('content_de')->nullable(true);
            $table->text('content_en')->nullable(true);

            // Rename FR columns from default to FR
            $table->renameColumn('name', 'name_fr');
            $table->renameColumn('content', 'content_fr');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {

          // Drop columns
          $table->dropColumn('name_de');
          $table->dropColumn('name_en');
          $table->dropColumn('content_de');
          $table->dropColumn('content_en');

          // Rename FR columns from default to FR
          $table->renameColumn('name_fr', 'name');
          $table->renameColumn('content_fr', 'content');

        });
    }
}
