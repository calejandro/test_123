<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinisitesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('minisites', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->string('title');
      $table->string('slug');

      $table->integer('event_id')->unsigned();
      $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');

      $table->string('site_background_color')->default("rgba(255, 255, 255, 1)");
      $table->string('site_background_image')->nullable();

      $table->string('menu_font_size')->default("12"); # pixels
      $table->string('menu_font_color')->default("rgba(0, 0, 0, 1)");
      $table->string('menu_background_color')->default("rgba(255, 255, 255, 1)");

      $table->string('content_background_color')->default("rgba(255, 255, 255, 1)");
      $table->string('bottom_image_path')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('minisites');
  }
}
