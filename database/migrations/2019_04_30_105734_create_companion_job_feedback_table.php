<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanionJobFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companion_job_feedback', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('companion_id')->unsigned()->nullable();
            $table->foreign('companion_id')->references('id')
                  ->on('companions')->onDelete('cascade');
      
            $table->integer('job_feedback_id')->unsigned()->nullable();
            $table->foreign('job_feedback_id')->references('id')
                  ->on('jobFeedbacks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companion_job_feedback');
    }
}
