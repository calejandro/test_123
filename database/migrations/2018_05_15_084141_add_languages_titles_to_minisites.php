<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguagesTitlesToMinisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
          $table->renameColumn('title', 'title_fr');
          $table->string('title_de')->nullable(true);
          $table->string('title_en')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
          $table->renameColumn('title_fr', 'title');
          $table->dropColumn('title_de');
          $table->dropColumn('title_en');
        });
    }
}
