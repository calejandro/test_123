<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LongtextForNewTemplateAndHtmlFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->string('template_en', 4294967295)->change();
            $table->string('template_de', 4294967295)->change();
            $table->string('html_en', 4294967295)->change();
            $table->string('html_de', 4294967295)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->text('template_en')->change();
            $table->text('template_de')->change();
            $table->text('html_en')->change();
            $table->text('html_de')->change();
        });
    }
}
