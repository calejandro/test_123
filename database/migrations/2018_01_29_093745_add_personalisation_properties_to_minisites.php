<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonalisationPropertiesToMinisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->string('title_font_color')->default("rgba(255, 255, 255, 1)");
            $table->string('menu_item_hover_font_color')->default("rgba(255, 255, 255, 1)");
            $table->string('menu_item_hover_background_color')->default("rgba(0, 0, 0, 1)");
            $table->string('menu_item_active_font_color')->default("rgba(255, 255, 255, 1)");
            $table->string('menu_item_active_background_color')->default("rgba(0, 0, 0, 1)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->dropColumn('title_font_color');
            $table->dropColumn('menu_item_hover_font_color');
            $table->dropColumn('menu_item_hover_background_color');
            $table->dropColumn('menu_item_active_font_color');
            $table->dropColumn('menu_item_active_background_color');
        });
    }
}
