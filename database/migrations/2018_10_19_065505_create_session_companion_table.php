<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionCompanionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_companion', function (Blueprint $table)
        {
            $table->integer('session_id')->unsigned()->nullable();
            $table->foreign('session_id')
                ->references('id')
                ->on('sessions')
                ->onDelete('cascade');

            $table->integer('companion_id')->unsigned()->nullable();
            $table->foreign('companion_id')
                ->references('id')
                ->on('companions')
                ->onDelete('cascade');

            $table->increments('id');
            $table->timestamps();

            $table->boolean('checkin')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_companion');
    }
}
