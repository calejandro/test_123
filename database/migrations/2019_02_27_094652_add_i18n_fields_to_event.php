<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddI18nFieldsToEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            // Title fields
            $table->renameColumn('name', 'name_fr');
            $table->string('name_en')->nullable(); // nullables to avoid conflicts
            $table->string('name_de')->nullable(); // nullables to avoid conflicts

            // Template fields
            $table->renameColumn('description', 'description_fr');
            $table->text('description_en')->nullable(); // nullables to avoid conflicts
            $table->text('description_de')->nullable(); // nullables to avoid conflicts
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            // Title fields
            $table->renameColumn('name_fr', 'name');
            $table->dropColumn('name_en');
            $table->dropColumn('name_de');

            // Template fields
            $table->renameColumn('description_fr', 'description');
            $table->dropColumn('description_en');
            $table->dropColumn('description_de');
        });
    }
}
