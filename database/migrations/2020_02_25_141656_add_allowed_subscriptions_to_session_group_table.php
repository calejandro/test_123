<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowedSubscriptionsToSessionGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session_groups', function (Blueprint $table) {
            $table->integer('allowed_session_subscriptions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session_groups', function (Blueprint $table) {
            $table->dropColumn('allowed_session_subscriptions');
        });
    }
}
