<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddI18nFieldsToEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emails', function (Blueprint $table) {

            // Title fields
            $table->renameColumn('title', 'title_fr');
            $table->string('title_en')->nullable(); // nullables to avoid conflicts
            $table->string('title_de')->nullable(); // nullables to avoid conflicts

            // Template fields
            $table->renameColumn('template', 'template_fr');
            $table->text('template_en')->nullable(); // nullables to avoid conflicts
            $table->text('template_de')->nullable(); // nullables to avoid conflicts

            // HTML fields
            $table->renameColumn('html', 'html_fr');
            $table->text('html_en')->nullable(); // nullables to avoid conflicts
            $table->text('html_de')->nullable(); // nullables to avoid conflicts
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emails', function (Blueprint $table) {

            // Title fields
            $table->renameColumn('title_fr', 'title');
            $table->dropColumn('title_en');
            $table->dropColumn('title_de');

            // Template fields
            $table->renameColumn('template_fr', 'template');
            $table->dropColumn('template_en');
            $table->dropColumn('template_de');
            
            // HTML fields
            $table->renameColumn('html_fr', 'html');
            $table->dropColumn('html_en');
            $table->dropColumn('html_de');  
        });
    }
}
