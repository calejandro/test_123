<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->string('name');
          $table->string('slug');
          $table->integer('minisite_id')->unsigned();
          $table->integer('page_type_id')->unsigned();
          $table->foreign('minisite_id')->references('id')->on('minisites')->onDelete('cascade');
          $table->foreign('page_type_id')->references('id')->on('page_types')->onDelete('cascade');
          $table->text('content');
          $table->string('lang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
