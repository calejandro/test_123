<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->boolean('isInvit')->default(false);
            $table->boolean('isConfirm')->default(false);
            $table->boolean('joinICS')->default(false);
            $table->boolean('sendWithDefaultMail')->default(false);
            $table->integer('sended')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->text('template')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails');
    }
}
