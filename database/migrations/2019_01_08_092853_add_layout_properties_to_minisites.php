<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLayoutPropertiesToMinisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->string('top_image_alignment')->default('center');
            $table->string('bottom_image_alignment')->default('center');
            $table->integer('top_image_size')->default(0);
            $table->integer('bottom_image_size')->default(0);
            $table->integer('title_menu_spacing')->default(10);
            $table->string('title_alignment')->default('left');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->dropColumn('top_image_alignment');
            $table->dropColumn('bottom_image_alignment');
            $table->dropColumn('top_image_size');
            $table->dropColumn('bottom_image_size');
            $table->dropColumn('title_menu_spacing');
            $table->dropColumn('title_alignment');
        });
    }
}
