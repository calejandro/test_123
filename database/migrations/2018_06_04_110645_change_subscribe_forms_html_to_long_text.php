<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubscribeFormsHtmlToLongText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->longText('html')->nullable()->change();
            $table->longText('formHtml')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribeForms', function (Blueprint $table) {
            $table->text('html')->nullable()->change();
            $table->text('formHtml')->nullable()->change();
        });
    }
}
