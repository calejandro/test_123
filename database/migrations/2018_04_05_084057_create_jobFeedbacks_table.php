<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobFeedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('finished')->default(false);
            $table->boolean('error')->default(0);
            $table->boolean('catchall_addresses_sent')->default(false);
            $table->boolean('join_ics')->default(false);
            $table->boolean('specific_config')->default(false);
            $table->integer('nb_guests')->default(0);
            $table->integer('nb_tested_guests')->default(0);
            $table->integer('nb_mails_sent')->default(0);
            $table->integer('nb_ignored_mails')->default(0);
            $table->float('catchall_proportion')->default(0.0);
            $table->string('from_column')->nullable();

            $table->string('mail_host')->nullable();
            $table->string('mail_port')->nullable();
            $table->string('mail_username')->nullable();
            $table->string('mail_from_address')->nullable();
            $table->string('mail_from_name')->nullable();

            // Relations
            $table->integer('event_id')->nullable()->unsigned();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->integer('mail_id')->nullable()->unsigned();
            $table->foreign('mail_id')->references('id')->on('emails')->onDelete('cascade');
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobFeedbacks');
    }
}
