<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveyForms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->boolean('visible')->default(0);
            $table->text('template')->nullable();
            $table->text('html')->nullable();
            $table->text('formTemplate')->nullable();
            $table->text('formHtml')->nullable();

            $table->integer('event_id')->unsigned()->nullable();
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveyForms');
    }
}
