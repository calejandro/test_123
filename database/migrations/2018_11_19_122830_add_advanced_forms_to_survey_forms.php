<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdvancedFormsToSurveyForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
          $table->longText('advancedFormTemplate', 4294967295)->nullable();
          $table->longText('advancedFormHtml', 4294967295)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveyForms', function (Blueprint $table) {
          $table->dropColumn('advancedFormTemplate');
          $table->dropColumn('advancedFormHtml');
        });
    }
}
