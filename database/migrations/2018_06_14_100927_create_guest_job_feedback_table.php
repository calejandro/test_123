<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestJobFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_job_feedback', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('guest_id')->unsigned()->nullable();
            $table->foreign('guest_id')->references('id')
                  ->on('guests')->onDelete('cascade');
      
            $table->integer('job_feedback_id')->unsigned()->nullable();
            $table->foreign('job_feedback_id')->references('id')
                  ->on('jobFeedbacks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_job_feedback');
    }
}
