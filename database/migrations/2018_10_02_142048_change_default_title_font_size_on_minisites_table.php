<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultTitleFontSizeOnMinisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->integer('title_font_size')->unsigned()->default(36)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->string('title_font_size')->default("18")->change();
        });
    }
}
