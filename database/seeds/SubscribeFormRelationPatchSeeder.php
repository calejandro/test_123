<?php

use Illuminate\Database\Seeder;
use App\Event;

class SubscribeFormRelationPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::doesnthave('subscribeForm')->get();
        $this->command->info('Try to fix ' . $events->count() . ' events');
        foreach($events as $event) {
            $event->setDefaultSubscribeForm();
            $this->command->info('Default subscribe form successfully added to event: ' . $event->id);
        }
    }
}
