<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeeder::class);
        $this->call(PermissionsSessionSeeder::class);

        $this->call(AdminSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(PageTypesSeeder::class);
        $this->call(TicketTypesSeeder::class);

        $this->call(CompaniesSeeder::class);
        $this->call(EventSeeder::class);

        // $this->call(EmailSeeder::class);
    }
}
