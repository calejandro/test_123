<?php

use Illuminate\Database\Seeder;

use App\Event;

class EventTitlesAndDescriptionsMultilingualDefaultsPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $events = App\Event::where(['active' => 1])->get();
        $this->command->info('Patching ' . $events->count() . ' events multilingual titles.');
        foreach($events as $event) {

            // Default existing events names will be same as fr
            if(is_null($event->name_de)){
                $event->name_de = $event->name_fr;
            }

            if(is_null($event->name_en)){
                $event->name_en = $event->name_fr;
            }
            
            // Default existing events descriptions will be same as fr
            if(is_null($event->description_de)){
                $event->description_de = $event->description_fr;
            }

            if(is_null($event->description_en)){
                $event->description_en = $event->description_fr;
            }
            
            $event->save();
            $this->command->info('Default multilingual titles and description set for : ' . $event->id);
        }

    }
}
