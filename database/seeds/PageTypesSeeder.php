<?php

use Illuminate\Database\Seeder;

class PageTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('page_types')->insert([
        [
          'name' => 'Contenu',
          'description' => 'Les pages destinées à afficher du contenu simple.',
        ],
        [
          'name' => 'Inscription',
          'description' => 'Pages contenant le formulaire d\'inscription.',
        ],
        [
          'name' => 'Sondage',
          'description' => 'Pages contenant le formulaire de sondage.',
        ],
        [
          'name' => 'Formulaire accompagnant',
          'description' => 'Formulaire de l\'accompagant, visible que par un lien d\'accès privé',
        ],
      ]);
    }
}
