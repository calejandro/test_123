<?php

use Illuminate\Database\Seeder;

class PermissionsSessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
        [
          'name' => 'sessions-view',
          'description' => 'Can view session overview'
        ],
        [
          'name' => 'sessions-create',
          'description' => 'Can create sessions'
        ],
        [
          'name' => 'sessions-update',
          'description' => 'Can update sessions'
        ],
        [
          'name' => 'sessions-delete',
          'description' => 'Can delete sessions'
        ]
      ]);
    }
}
