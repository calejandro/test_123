<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
        [
          'name' => 'users-view',
          'description' => 'Can view users list'
        ],
        [
          'name' => 'users-create',
          'description' => 'Can create users'
        ],
        [
          'name' => 'users-update',
          'description' => 'Can update users'
        ],
        [
          'name' => 'users-delete',
          'description' => 'Can delete users'
        ],
        [
          'name' => 'configurations-update', // Specific and not model related!
          'description' => 'Can change mail configuration'
        ],
        [
          'name' => 'events-overview', // Specific and not direct model related!
          'description' => 'Can see events overview'
        ],
        [
          'name' => 'events-view',
          'description' => 'Can view events list'
        ],
        [
          'name' => 'events-create',
          'description' => 'Can create events'
        ],
        [
          'name' => 'events-update',
          'description' => 'Can update events'
        ],
        [
          'name' => 'events-delete',
          'description' => 'Can delete events'
        ],
        [
          'name' => 'statistics-view',
          'description' => 'Can display statistics'
        ],
        [
          'name' => 'guests-view',
          'description' => 'Can view guests lists'
        ],
        [
          'name' => 'guests-create',
          'description' => 'Can create guests'
        ],
        [
          'name' => 'guests-update',
          'description' => 'Can update guests'
        ],
        [
          'name' => 'guests-delete',
          'description' => 'Can delete guests'
        ],
        [
          'name' => 'emails-view',
          'description' => 'Can view mails list'
        ],
        [
          'name' => 'emails-create',
          'description' => 'Can create mails'
        ],
        [
          'name' => 'emails-update',
          'description' => 'Can update mails'
        ],
        [
          'name' => 'emails-delete',
          'description' => 'Can delete mails'
        ],
        [
          'name' => 'minisite-view',
          'description' => 'Can view minisite overview'
        ],
        [
          'name' => 'minisite-create',
          'description' => 'Can create minisites'
        ],
        [
          'name' => 'minisite-update',
          'description' => 'Can update minisites'
        ],
        [
          'name' => 'survey-update',
          'description' => 'Can update survey form'
        ]
      ]);
    }
}
