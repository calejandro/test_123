<?php

use Illuminate\Database\Seeder;
use App\Event;

class UserControllerRelationPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::doesnthave('controller')->get();
        $this->command->info('Try to fix ' . $events->count() . ' events');
        foreach($events as $event) {
            $event->setDefaultController('123456');
            $this->command->info('Default user controller successfully added to event: ' . $event->id);
        }
    }
}
