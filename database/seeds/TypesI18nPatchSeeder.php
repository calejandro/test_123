<?php

use Illuminate\Database\Seeder;

use App\Type;

class TypesI18nPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $translations = [
        [
          'name_fr' => 'Autres',
          'description_fr' => 'Autres types',
          'name_en' => 'Other',
          'description_en' => 'Other types',
          'name_de' => 'Andere',
          'description_de' => 'Andere Typen',
          'name_es' => 'Otros',
          'description_es' => 'Otros tipos',
        ],
        [
          'name_fr' => 'Conférence',
          'description_fr' => 'Type conférence',
          'name_en' => 'Conference',
          'description_en' => 'Conference type',
          'name_de' => 'Konferenz',
          'description_de' => 'Konferenztyp',
          'name_es' => 'Conferencia',
          'description_es' => 'Tipo de conferencia',
        ],
        [
          'name_fr' => 'Expositions',
          'description_fr' => 'Type expositions',
          'name_en' => 'Exhibitions',
          'description_en' => 'Type of exhibitions',
          'name_de' => 'Ausstellung',
          'description_de' => 'Art der Ausstellungen',
          'name_es' => 'Exposiciones',
          'description_es' => 'Tipo de exposiciones',
        ],
        [
          'name_fr' => 'Foires',
          'description_fr' => 'Type foires',
          'name_en' => 'Forum',
          'description_en' => 'Forum type',
          'name_de' => 'Forum',
          'description_de' => 'Forumstyp',
          'name_es' => 'Foro',
          'description_es' => 'Tipo de foro',
        ],
        [
          'name_fr' => 'Gala',
          'description_fr' => 'Type gala',
          'name_en' => 'Gala',
          'description_en' => 'Autres types',
          'name_de' => 'Gala',
          'description_de' => 'Gala-Typ',
          'name_es' => 'Gala',
          'description_es' => 'Typo de Gala',
        ],
        [
          'name_fr' => 'Soirée',
          'description_fr' => 'Type soirée',
          'name_en' => 'Evening event',
          'description_en' => 'Evening event types',
          'name_de' => 'Abendveranstaltung',
          'description_de' => 'Abendtyp',
          'name_es' => 'Festejar',
          'description_es' => 'Tipo de noche',
        ],
        [
          'name_fr' => 'Séminaire',
          'description_fr' => 'Type séminaire',
          'name_en' => 'Seminar',
          'description_en' => 'Seminar type',
          'name_de' => 'Seminar',
          'description_de' => 'Seminartyp',
          'name_es' => 'Seminario',
          'description_es' => 'Tipo de seminario',
        ],
        [
          'name_fr' => 'Workshop',
          'description_fr' => 'Type workshop',
          'name_en' => 'Workshop',
          'description_en' => 'Workshop type',
          'name_de' => 'Workshop',
          'description_de' => 'Workshop-Typ',
          'name_es' => 'Workshop',
          'description_es' => 'Tipo de workshop',
        ],
      ];

      foreach($translations as $t) {
        $this->translateFromFr($t);
      }
    }

    function translateFromFr(array $a) {
      $e = Type::where('name_fr', $a['name_fr'])->first();
      $e->name_en = $a['name_en'];
      $e->description_en = $a['description_en'];
      $e->name_de = $a['name_de'];
      $e->description_de = $a['description_de'];
      $e->name_es = $a['name_es'];
      $e->description_es = $a['description_es'];
      $e->save();
    }
}
