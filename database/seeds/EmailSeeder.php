<?php

use Illuminate\Database\Seeder;

use App\EmailType;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emails')->insert([
            'title' => 'email invitation',
            'type' => EmailType::INVITATION,
            'joinICS' => false,
            'sendWithDefaultMail' => false,
            'sended' => 0
        ]);

        DB::table('emails')->insert([
            'title' => 'email confirmation',
            'type' => EmailType::CONFIRMATION,
            'joinICS' => false,
            'sendWithDefaultMail' => true,
            'sended' => NULL
        ]);

        DB::table('emails')->insert([
            'title' => 'email pub',
            'joinICS' => false,
            'sendWithDefaultMail' => false,
            'sended' => 2
        ]);
    }
}
