<?php

use Illuminate\Database\Seeder;
use App\Event;
use App\Ticket;

class TicketsPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::doesnthave('ticket')->get();
        $this->command->info('Adding default ticket to ' . $events->count() . ' events');
        foreach ($events as $event) {
          $ticket = new Ticket();
          $ticket->event_id = $event->id;
          $ticket->save();
          $this->command->info('Default ticket successfully created for event: ' . $event->id);
        }
    }
}
