<?php

use Illuminate\Database\Seeder;

class ChangeDefaultMinisiteTitleFontSizePatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $minisites = App\Minisite::all();
        $this->command->info('Try to fix ' . $minisites->count() . ' minisites');
        foreach($minisites as $minisite) {
            $minisite->title_font_size = 36;
            $minisite->save();
        }
    }
}
