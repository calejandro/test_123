<?php

use Illuminate\Database\Seeder;

use App\Event;
use App\Company;
use App\Type;

use App\Utils\StringUtil;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::all()->first();

        $ev = new Event();
        $ev->name_fr = 'Event public nominative';
        $ev->description_fr = 'Event public nominative';
        $ev->name_en = 'Event public nominative EN';
        $ev->description_en = 'Event public nominative EN';
        $ev->name_de = 'Event public nominative DE';
        $ev->description_de = 'Event public nominative DE';

        $ev->organisator_firstname = 'Mickey';
        $ev->organisator_lastname = 'Mouse';
        $ev->organisator_email = 'mickey@organisator.com';

        $ev->setStartDateAttribute("1/12/2019 12:00");
        $ev->setEndDateAttribute("12/12/2019 12:00");

        $ev->nb_places = 42;
        $ev->address = "test addr 1";

        $ev->public_minisite = true;
        $ev->advanced_minisite_mode = true;
        $ev->advanced_subscription_form_mode = true;
        $ev->nominative_companions = true;
        $ev->companions_limit = 3;

        $ev->language_fr = true;
        $ev->language_en = true;
        $ev->language_de = true;

        $evSaved = $company->events()->save($ev);

        $type = Type::all()->first();
        $type->events()->save($evSaved);

        // Set default relation values
        $evSaved->setDefaultConfirmMail(!$company->hasCompleteSMTPConfig());
        $evSaved->setDefaultFullEventMail(!$company->hasCompleteSMTPConfig());
        $evSaved->setDefaultSubscribeForm();
        $evSaved->setDefaultSurveyForm();
        $evSaved->setDefaultController(StringUtil::randomString());
        $evSaved->setDefaultTicket();
    }
}
