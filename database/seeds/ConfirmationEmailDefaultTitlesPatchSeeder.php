<?php

use Illuminate\Database\Seeder;

use App\Email;

class ConfirmationEmailDefaultTitlesPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emails = App\Email::where(['active' => 1, 'isConfirm' => 1])->get();
        $this->command->info('Patching ' . $emails->count() . ' confirmation emails');
        foreach($emails as $email) {
            $email->title_en = Lang::choice('models.email.confirmEmail', 1, [], 'en');
            $email->title_de = Lang::choice('models.email.confirmEmail', 1, [], 'de');
            $email->save();
            $this->command->info('Default confirmation email set for : ' . $email->id);
        }
    }
}
