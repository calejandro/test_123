<?php

use Illuminate\Database\Seeder;

class PageTypesCompanionPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('page_types')->insert([
        [
          'name' => 'Formulaire accompagnant',
          'description' => 'Formulaire de l\'accompagant, visible que par un lien d\'accès privé',
        ]
      ]);
    }
}
