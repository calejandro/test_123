<?php

use Illuminate\Database\Seeder;
use App\Event;
use App\Type;

class TypeRelationPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_id = App\Type::where('name', 'Autres')->first()->id;
        $events = App\Event::doesnthave('type')->get();
        $this->command->info('Try to fix ' . $events->count() . ' events');
        foreach($events as $event) {
            $event->type_id=$type_id;
            $event->save();
            $this->command->info('Default type successfully added to event: ' . $event->id);
        }
    }
}
