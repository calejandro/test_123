<?php

use Illuminate\Database\Seeder;

class TicketTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('ticket_types')->insert([
        [
          'name' => 'Défaut',
          'description' => 'Gabarit simple par défaut.',
          'template' => 'render'
        ],
        [
          'name' => 'Quatre quarts',
          'description' => 'Gabarit en quatre quarts distincts pliable.',
          'template' => 'four_quarters'
        ]
      ]);
    }
}
