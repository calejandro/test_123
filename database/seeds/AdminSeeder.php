<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Permission;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'change@me.com',
            'password' => bcrypt('changeme'),
        ]);

        $allPermissions = Permission::all();
        User::where('name', 'admin')->first()->permissions()->attach($allPermissions->pluck('id'));
    }
}
