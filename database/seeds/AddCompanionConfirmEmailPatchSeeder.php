<?php

use Illuminate\Database\Seeder;

use App\Email;
use App\Event;
use App\EmailType;


class AddCompanionConfirmEmailPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Event::all() as $event)
        {
            if ($event->emails()->where('type', EmailType::COMPANIONS_CONFIRMATION)->count() === 0) {
                $mailCompanion = new Email();
                $mailCompanion->title_fr = \Lang::choice('models.email.confirmEmailCompanions', 1, [], 'fr');
                $mailCompanion->title_en = \Lang::choice('models.email.confirmEmailCompanions', 1, [], 'en');
                $mailCompanion->title_de = \Lang::choice('models.email.confirmEmailCompanions', 1, [], 'de');
                $mailCompanion->type = EmailType::COMPANIONS_CONFIRMATION;
                $mailCompanion->joinICS = false;
                $mailCompanion->sendWithDefaultMail = true;
                $event->emails()->save($mailCompanion);
            } 
        }
    }
}
