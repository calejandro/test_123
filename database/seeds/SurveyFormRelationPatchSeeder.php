<?php

use Illuminate\Database\Seeder;
use App\Event;

class SurveyFormRelationPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::doesnthave('surveyForm')->get();
        $this->command->info('Try to fix ' . $events->count() . ' events');
        foreach($events as $event) {
            $event->setDefaultSurveyForm();
            $this->command->info('Default survey form successfully added to event: ' . $event->id);
        }
    }
}
