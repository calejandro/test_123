<?php

use Illuminate\Database\Seeder;

class MinisiteSlugFixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ms = App\Minisite::all();
        foreach($ms as $minisite) {
            $newSlug = null;
            if($minisite->slug_fr != null) {
                $newSlug = $minisite->slug_fr;
            }
            else if($minisite->slug_de != null) {
                $newSlug = $minisite->slug_de;
            }
            else if($minisite->slug_en != null) {
                $newSlug = $minisite->slug_en;
            }
            $newSlug = $newSlug . '-' . $minisite->id;
            $minisite->slug = $newSlug;
            $minisite->save();
        }
    }
}
