<?php

use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('companies')->insert([
        [
          'companyname' => 'Microsoft',
          'firstname' => 'Test',
          'lastname' => 'Test',
          'phone' => '0998888888',
          'email' => 'test@test.com'
        ],
        [
          'companyname' => 'HE-Arc',
          'firstname' => 'Test',
          'lastname' => 'Test',
          'phone' => '0998888888',
          'email' => 'he@arc.com'
        ],
        [
          'companyname' => 'Test',
          'firstname' => 'Test',
          'lastname' => 'Test',
          'phone' => '0998888888',
          'email' => 'test@test.com'
        ],
        [
          'companyname' => 'Apple',
          'firstname' => 'Test',
          'lastname' => 'Test',
          'phone' => '0998888888',
          'email' => 'test@test.com'
        ]
      ]);
    }
}
