<?php

use Illuminate\Database\Seeder;
use App\Facades\GuestService;
use App\Facades\SessionService;

class CompanionModelsPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guests = App\Guest::active()->where('companions', '>', 0)->get();
        $this->command->info('Try to fix ' . $guests->count() . ' guests');
        foreach ($guests as $guest) {

            $this->command->info('###########   Guest : ' . $guest->id);

            // Save info in variables to post-tests
            $originalEventCheckin = $guest->checkin;
            $originalNbCompanions = $guest->companions;
            $originalSessionPlaces = [];
            $originalSessionCheckin = [];

            $sessions = $guest->sessions()->get();
            foreach ($sessions as $session) {
                $originalSessionPlaces[$session->id] = $session->pivot->places;
                $originalSessionCheckin[$session->id] = $session->pivot->checkin;
            }


            // 1. Event
            
            // 1.1 Places
            $nbCompanions = $guest->companions;
            $guest->addCompanions($nbCompanions);
            $guest->companions = 0; // reset
            $this->command->info('Added ' . $nbCompanions . ' companions');
            
            // 1.2 Checkin
            if ($guest->checkin > 1) {
                $toCheckin = $guest->checkin - 1;  // Guest is allready checkin
                if($toCheckin > $nbCompanions) {
                    $toCheckin = $nbCompanions;
                }
                GuestService::checkinGuest($guest, $toCheckin);
                $this->command->info('(main entrance) Checkin ' . $toCheckin . ' companions');
            }

            if ($guest->checkin) {
                $guest->checkin = true;
                $this->command->info('(main entrance) Checkin THE guest');
            }

            $guest->save();

            // 2. Sessions

            $sessions = $guest->sessions()->get();

            foreach ($sessions as $session) {
                // 2.1 Places
                $places = $session->pivot->places;
                if ($places > $nbCompanions + 1) {
                    $places = $nbCompanions + 1;
                }

                if ($session->pivot->places > 1) {
                    SessionService::registerGuestAndCompanions($guest, $session, $places);

                    $session->guests()->updateExistingPivot($guest->id, [
                        'places' => 1
                    ]);

                    $this->command->info('(session id:' . $session->id . ') Added ' . $places . ' places (companions + guest)');
                }
                else if ($session->pivot->places === 1) {
                    // OK !
                }
                else {
                    $session->guests()->detach($guest->id); // remove relation if guest checkin = 1 but place = 0
                }

                // 2.2 Checkin
                if ($session->pivot->checkin > 1) {
                    $toCheckin = $session->pivot->checkin;
                    if ($toCheckin > $places) {
                        $toCheckin = $places;
                    }
                    $toCheckin =  $toCheckin - 1; // guest is manually checkin after companions

                    $companions = $guest->companions()->get();
                    for($i=0; $i<$toCheckin; $i++) {
                        if(!SessionService::checkinCompanion(App\Companion::find($companions[$i]->id), $session), true) {
                            $this->command->error('Can\'t checkin companion ' . $companions[$i]->id. ', probably missing place');
                            exit(1);
                        }

                        $this->command->info('(session id:' . $session->id . ') Checkin companion ' . $companions[$i]->id);
                    }

                    $session->guests()->updateExistingPivot($guest->id, [
                        'checkin' => true
                    ]);
                    $this->command->info('(session id:' . $session->id . ') Checkin THE guest');
                }
            }


            // Validation

            // Validate:
            // NB companion
            // NB checkin to sessions
            // NB checkin to event
            if (!$this->validateGuestMigration($guest->id, $originalEventCheckin, $originalNbCompanions, $originalSessionPlaces, $originalSessionCheckin)) {
                $this->command->error('ERROR in guest:' . $guest->id);
                exit(1);
            }
            else {
                $this->command->info('Validation passed');
            }
        }
    }


    private function validateGuestMigration(int $id, int $originalEventCheckin, 
        int $originalNbCompanions, array $originalSessionPlaces, array $originalSessionCheckin) : bool {
            
        $valid = true;

        $guest = App\Guest::with('companions', 'sessions')->find($id);

        if ($guest->companions()->count() !== $originalNbCompanions) {
            $this->command->error('NB companions doesn\'t match !');
            $valid = false;
        }

        foreach($originalSessionPlaces as $id => $nbPlaces) {
            $session = $guest->sessions()->where('sessions.id', $id)->first();

            if($nbPlaces > $originalNbCompanions + 1) {
                $nbPlaces = $originalNbCompanions + 1;
            }

            if ($session === null && $nbPlaces > 0) {
                $this->command->error('Relation to session ' . $id . ' doesn\'t exists ! (places validation)');
                $valid = false;
            }
            else if ($nbPlaces !== $guest->countSessionPlaces($session)) {
                $this->command->error('Missing places for session ' . $id . '. We want ' . $nbPlaces . ' places, but have ' . $guest->countSessionPlaces($session));
                $valid = false;
            }
        }

        foreach($originalSessionCheckin as $id => $nbCheckin) {
            $session = $guest->sessions()->where('sessions.id', $id)->first();

            if($nbCheckin > $originalSessionPlaces[$id]) {
                $nbCheckin = $originalSessionPlaces[$id];
            }

            if ($session === null && $nbCheckin > 0) {
                $this->command->error('Relation to session ' . $id . ' doesn\'t exists ! (checkin validation), we want: ' .$nbCheckin. ' checkins');
                $valid = false;
            }
            else if ($session === null && $nbCheckin === 0) {
                // OK
            }
            else if ($nbCheckin !== $guest->countSessionCheckin($session)) {
                $this->command->error('Missing checkin for session ' . $id . '. We want ' . $nbCheckin . ' checkin, but have ' . $guest->countSessionCheckin($session));
                $valid = false;
            }
        }

        return $valid;
    }
}
