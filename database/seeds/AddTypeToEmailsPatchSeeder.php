<?php

use Illuminate\Database\Seeder;

use App\Email;
use App\EmailType;

class AddTypeToEmailsPatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Email::all() as $email) {
            if ($email->getOriginal('isInvit')) {
                $email->update(['type' => EmailType::INVITATION]);
            }
            else if ($email->getOriginal('isConfirm')) {
                $email->update(['type' => EmailType::CONFIRMATION]);
            }
            else if ($email->getOriginal('isEventFullNotification')) {
                $email->update(['type' => EmailType::EVENT_FULL_NOTIFICATION]);
            }
        }
    }
}
