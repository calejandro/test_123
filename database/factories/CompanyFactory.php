<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'companyName' => $faker->name,
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'phone' => $faker->name,
        'email' => $faker->name,
    ];
});

$factory->state(App\Company::class, 'personal_smtp', function (Faker $faker) {
    return [
        'smtp_host' => $faker->name,
        'smtp_port' => 443,
        'smtp_from_mail_address' => $faker->unique()->safeEmail,
        'smtp_from_name' => $faker->name,
        'smtp_encryption' => 'ssl',
        'smtp_password' => bcrypt('secret')
    ];
});