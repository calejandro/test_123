<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10)
    ];
});

// a tenant has a company
$factory->state(App\User::class, 'tenant', function (Faker $faker) {
    return [
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        }
    ];
});

// a controller has an event and a company
$factory->state(App\User::class, 'controller', function (Faker $faker) {
    return [
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        },
        'event_id' => function () {
            return factory(App\Event::class)->create()->id;
        }
    ];
});