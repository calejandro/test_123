<?php

use Faker\Generator as Faker;

$factory->define(App\Guest::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'extendedFields' => [],
        'invited' => false,
        'subscribed' => false,
        'checkin' => false
    ];
});

$factory->state(App\Guest::class, 'invited', function (Faker $faker) {
    return [
        'invited' => true
    ];
});

$factory->state(App\Guest::class, 'subscribed', function (Faker $faker) {
    return [
        'invited' => true,
        'subscribed' => true
    ];
});

$factory->state(App\Guest::class, 'checkin', function (Faker $faker) {
    return [
        'invited' => true,
        'subscribed' => true,
        'checkin' => true
    ];
});