<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'name_fr' => $faker->name,
        'description_fr' => $faker->text,
        'start_date' => date("d/m/Y H:i"),
        'end_date' =>  date("d/m/Y H:i"),
        'nb_places' => $faker->randomDigitNotNull,
        'address' => $faker->address,
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        },
        'active' => true,
        // 'category_id' => str_random(10),
        'organisator_firstname' => $faker->firstNameMale,
        'organisator_lastname' => $faker->lastName,
        'organisator_email' => $faker->unique()->safeEmail,
        'advanced_minisite_mode' => true,
        'nominative_companions' => false
    ];
});
