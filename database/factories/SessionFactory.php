<?php

use Faker\Generator as Faker;

$factory->define(App\Session::class, function (Faker $faker) {
    $n = $faker->name;
    return [
        'name_fr' => $n,
        'name_de' => $n,
        'name_en' => $n,
        'places' => 10
    ];
});
