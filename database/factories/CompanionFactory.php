<?php

use Faker\Generator as Faker;

$factory->define(App\Companion::class, function (Faker $faker) {
    return [
        'checkin' => false
    ];
});
