<?php

namespace Tests\Browser;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;
use App\Companion;

use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Lang;
use Log;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class EventCheckinTest extends DuskTestCase
{
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Test checkin page access for companion
     */
    public function testEventCheckinCompanionAccessValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create()[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id,
                'subscribed' => true
            ])[0];

        $companions = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ]);

        $perms = Permission::whereIn('name', ['users-create'])->get();

        $this->browse(function (Browser $browser) use ($event, $companions, $guest) {
            $browser->loginAs($event->controller)
                    ->visit('/guests/checkin/' . $companions[0]->access_hash)
                    ->assertSee($guest->lastname . " " . $guest->firstname. " (" . trans_choice('models.guest.companion', 1) . ")")
                    ->assertSeeIn('#guest-reserved-places', 2)
                    ->assertSeeIn('#guest-checkin-places', 0);
        });
    }

    /**
     * Test checkin for companion
     */
    /*public function testEventCheckinCompanionCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create()[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companions = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ]);

        $this->browse(function (Browser $browser) use ($event, $companions) {
            $browser->loginAs($event->controller)
                    ->visit('/guests/checkin/' . $companions[0]->access_hash);
        });
    }*/
}
