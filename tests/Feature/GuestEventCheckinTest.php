<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;
use App\Companion;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Mobile checkin app API dedicated test
 * Test Guest checkin in Event
 */
class GuestEventCheckinTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        // $this->withoutExceptionHandling(); // For test debug purpose
    }

    private function setUpEvent(Company $company): Event
    {
        $event = factory(Event::class, 1)
            ->create([
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        return $event;
    }

    public function testCheckinAppAccessValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->get("/events/$event->id/checkin/");

        // Check
        $response->assertStatus(200);
    }

    /**
     * Test Case
     *  * Checkin the Mobile App is accessible for controller
     */
    public function testGlobalCheckinAppAccessValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->get("/checkin/");

        // Check
        $response->assertStatus(200);
    }
    
    /**
     * Test Case
     *  * Data: Guest not checkin to event with 1 companion
     *  * Action: Checkin the guest only
     *  * Assert: Guest checkin == 1
     *  * Assert: Companion checkin unchanged
     *  * Assert: { type: 'guest', data: { id: GOODID } } received
     */
    public function testGuestEventCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 1, "The guest has not been correclty updated");
        $companionUpdated = Companion::find($companion->id);
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorreclty updated");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "guest", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $guest->id, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest checkin to event with 1 companion
     *  * Action: Checkin the guest only
     *  * Assert: 409 received
     *  * Assert: { type: 'guest', data: { id: GOODID } } received
     *  * Assert: Guest checkin unchanged
     *  * Assert: Companion checkin unchanged
     */
    public function testGuestEventCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'checkin' => 1,
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(409);

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "guest", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $guest->id, "The API response is uncorrectly formated");
      
        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 1, "The guest has been uncorreclty updated");
        $companionUpdated = Companion::find($companion->id);
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorreclty updated");
    }

    /**
     * Test Case
     *   * Data: Guest not checkin to event
     *   * Action: Trying to checkin a guest with the wrong hash
     *   * Assert: 404 received
     *   * Assert: Guest checkin unchanged
     */
    public function testGuestEventCheckinWrongHash()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);
        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/0000-WRONGHASH-0000");

        // Check
        $response->assertStatus(404);
        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorreclty updated");
    }

    /**
     * Test Case
     *   * Data: Guest not checkin to Event1, Event2
     *   * Action: Trying to checkin Guest in Event2
     *   * Assert: 404 received
     *   * Assert: Guest checkin unchanged
     */
    public function testGuestEventCheckinWrongEvent()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);
        $event2 = $this->setUpEvent($company);
        $guest = factory(Guest::class, 1)
            ->create([
                'checkin' => 0,
                'event_id' => $event->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event2->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(404);
        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorreclty updated");
    }

    /**
     * Test Case
     *   * Data: Guest1 with Companion1 checkin to event, Guest2 checkin to event
     *   * Action: Uncheckin the Guest1
     *   * Assert: Guest1 checkin==0
     *   * Assert: Companion1 checkin unchanged
     *   * Assert: Guest2 checkin unchanged
     */
    public function testGuestEventUnCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guests = factory(Guest::class, 2)
            ->create([
                'event_id' => $event->id,
                'checkin' => 1
            ]);

        $guest = $guests[0];
        $guest1 = $guests[1];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/$guest->id/_uncheckin");

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has not been correclty updated");
        
        $guestNotUpdated = Guest::find($guest1->id);
        $this->assertTrue($guestNotUpdated->checkin == 1, "The guest 2 should not be updated");
        
        $companionUpdated = Companion::find($companion->id);
        $this->assertTrue($companionUpdated->checkin == 1, "The companion should not be updated");
    }

    /**
     * Test Case
     *   * Data: Guest1 not checkin to event with Companion1 checkin, Guest2 checkin to event
     *   * Action: Trying to uncheckin Guest1
     *   * Assert: 200 received
     *   * Assert: Guest1 not updated
     *   * Assert: Companion1 not updated
     *   * Assert: Guest2 not updated
     */
    public function testGuestEventUnCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 2)
            ->create([
                'event_id' => $event->id,
                'checkin' => 0
            ])[0];
        $guest1 = factory(Guest::class, 2)
            ->create([
                'event_id' => $event->id,
                'checkin' => 1
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/$guest->id/_uncheckin");

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorreclty updated");
        
        $guestNotUpdated = Guest::find($guest1->id);
        $this->assertTrue($guestNotUpdated->checkin == 1, "The guest has been uncorreclty updated");
        
        $companionUpdated = Companion::find($companion->id);
        $this->assertTrue($companionUpdated->checkin == 1, "The companion has been uncorreclty updated");
    }
}
