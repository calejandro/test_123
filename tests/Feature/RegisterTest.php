<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Lang;
use Log;

/**
 * Register tests: concern only subscribtion in public minisite, without private link
 */
class RegisterTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        $this->withoutExceptionHandling(); // For test debug purpose
    }

    /**
     * New guest, not in DB
     */
    public function testNewGuestRegisterValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 3
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "companions" => 2,
            "email" => "steve.visinand@he-arc.ch",
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id => 0,
                $sessions[1]->id => 2,
                $sessions[2]->id => 1
            ]
        ];
        $response = $this->post("/api/events/$event->id/guests/_register", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));
    }

    /**
     * Guest exists in DB but isn't subscribed
     */
    public function testExistingGuestSubscribeValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 3
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $guestOrgExtFields = [
            "tel" => "123456",
            "adresse" => "adresse 5"
        ];

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id,
                'subscribed' => false,
                'extendedFields' => $guestOrgExtFields
            ])[0];

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "companions" => 2,
            "email" => $guest->email,
            "extendedFields" => [
                "tel" => "6668AB"
            ],
            "firstname" => $guest->firstname."a",
            "lastname" => $guest->lastname."a",
            "sessions" => [
                $sessions[0]->id => 0,
                $sessions[1]->id => 2,
                $sessions[2]->id => 1
            ]
        ];
        $response = $this->post("/api/events/$event->id/guests/_register", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest'], $guestOrgExtFields);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_saved'));
    }

    /**
     * Guest exists in DB but isn't subscribed
     */
    public function testExistingGuestWithExtFieldsNullSubscribeValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 3
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id,
                'subscribed' => false,
                'extendedFields' => null
            ])[0];

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "companions" => 2,
            "email" => $guest->email,
            "extendedFields" => [
                "tel" => "6668AB"
            ],
            "firstname" => $guest->firstname."a",
            "lastname" => $guest->lastname."a",
            "sessions" => [
                $sessions[0]->id => 0,
                $sessions[1]->id => 2,
                $sessions[2]->id => 1
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_saved'));
    }

    /**
     * Guest exists in DB and is subscribed
     */
    public function testExistingGuestSubscribeAllreadySubscribedInvalid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $guestOrgExtFields = [
            "tel" => "123456",
            "adresse" => "adresse 5"
        ];

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id,
                'subscribed' => true,
                'extendedFields' => $guestOrgExtFields
            ])[0];

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "companions" => 2,
            "email" => $guest->email,
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => $guest->firstname."a",
            "lastname" => $guest->lastname."a",
            "sessions" => [
                $sessions[0]->id => 0,
                $sessions[1]->id => 2,
                $sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_allready_done'), 
            'Wrong message displayed, displayed: ' . $datas['message']);

        $this->assertRegisteredGuest([ // assert nothing has changed
            "companions" => 0,
            "email" => $guest->email,
            "extendedFields" => $guestOrgExtFields,
            "firstname" => $guest->firstname,
            "lastname" => $guest->lastname,
            "sessions" => []
        ], $datas['guest'], $guestOrgExtFields);
    }

    /**
     * Subscibe to multiple session.
     * Check:
     * * Guest is created
     * * Correct number of Companions are created
     * * Correct number of Companions are linked to sessions
     */
    public function testRegisterSessionValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $sessions = factory(Session::class, 4)
            ->create([
                'event_id' => $event->id,
                'places' => 10
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [
                "".$sessions[0]->id => 2,
                "".$sessions[1]->id => 3,
                "".$sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        $guest = $event->guests()->first();
        $this->assertTrue($guest->firstname === "TestBoyFirstName");
        $this->assertTrue($guest->lastname === "TestBoyLastName");
        $this->assertTrue($guest->email === "test.boy@he-arc.ch");

        $this->assertTrue($guest->companions()->count() === 3);

        $this->assertTrue($guest->countSessionPlaces($sessions[0]) === 2);
        $this->assertTrue($guest->countSessionPlaces($sessions[1]) === 3);
        $this->assertTrue($guest->countSessionPlaces($sessions[2]) === 1);
        $this->assertTrue($guest->countSessionPlaces($sessions[3]) === 0);

        $this->assertTrue($guest->countSessionCheckin($sessions[0]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[1]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[2]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[3]) === 0);
    }

    public function testRegisterSessionFull()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 2
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [
                "".$sessions[0]->id => 2,
                "".$sessions[1]->id => 2,
                "".$sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName2",
            "lastname" => "TestBoyLastName2",
            "email" => "test.boy2@he-arc.ch",
            "companions" => 1,
            "sessions" => [
                "".$sessions[0]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();
        Log::info($datas['message']);
        $this->assertTrue($datas['message'] === Lang::get('models.session.guest_no_more_place_in_sessions', [
            'sessions' => $sessions[0]->name
        ]));
    }

    /**
     * This case can be produced only if
     * 1. User register
     * 2. Admin unregister guest
     * 3. User try to unregister in sessions
     */
    public function testUnRegisterSessionFull()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 2
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [
                "".$sessions[0]->id => 2,
                "".$sessions[1]->id => 2,
                "".$sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));


        // Unregister guest (admin panel)
        $guest = $event->guests()->where('email', 'test.boy@he-arc.ch')->firstOrFail();
        $guest->subscribed = false;
        $guest->subscribed_at = null;
        $guest->save();


        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 0,
            "sessions" => [
                "".$sessions[0]->id => 0,
                "".$sessions[1]->id => 0,
                "".$sessions[2]->id => 0
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_saved'));

        $eventSessions = $event->sessions()->get();
        $this->assertTrue($eventSessions[0]->placesReserved === 0, 'Session not correctly updated, session: ' . $eventSessions[0]->id . ' , places reserved: ' . $eventSessions[0]->placesReserved);
        $this->assertTrue($eventSessions[1]->placesReserved === 0, 'Session not correctly updated, session: ' . $eventSessions[1]->id . ' , places reserved: ' . $eventSessions[1]->placesReserved);
        $this->assertTrue($eventSessions[2]->placesReserved === 0, 'Session not correctly updated, session: ' . $eventSessions[2]->id . ' , places reserved: ' . $eventSessions[2]->placesReserved);
    }

    public function testRegisterEventFull()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 4,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName2",
            "lastname" => "TestBoyLastName2",
            "email" => "test.boy2@he-arc.ch",
            "companions" => 1,
            "sessions" => [],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guests_limit_reached'));
    }


     /**
     * Assert guest datas
     * - normal fields
     * - ext fields
     * - sessions
     * - companions
     */
    private function assertRegisteredGuest(array $askedDatas, Guest $guest, array $guestOrgExtFields = [])
    {
        $this->assertTrue($guest->firstname === $askedDatas['firstname']);
        $this->assertTrue($guest->lastname === $askedDatas['lastname']);
        $this->assertTrue($guest->email === $askedDatas['email']);

        foreach($askedDatas['extendedFields'] as $key => $value) {
            $this->assertTrue($guest->extendedFields[$key] === $value);
        }

        // Assert existing values in ext. fields aren't deleted
        foreach($guestOrgExtFields as $key => $value) {
            if(!array_key_exists($key, $askedDatas['extendedFields'])) {
                $this->assertTrue($guest->extendedFields[$key] === $value, 
                    '[assertRegisteredGuest] Guest has lost an extended field : ' . $key);
            }
        }

        // Assert companions are created
        $this->assertTrue($guest->companions()->count() === $askedDatas['companions']);

        foreach($askedDatas['sessions'] as $key => $value) {
            $this->assertTrue($guest->countSessionPlaces(Session::findOrFail($key)) === $value);
        }
    }
}
