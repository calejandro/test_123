<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Ticket;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Log;

class CompanyEventControllerTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        $this->withoutExceptionHandling(); // For test debug purpose
    }

    /**
     * Test CompanyUserController@index
     *
     * @return void
     */
    public function testCreate()
    {
        // Datas
        $company = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company, ['events-create']);

        $t = Type::first();
        // Ask
        $rawEvt = [
            'name_fr' => 'The He-arc BIG Event',
            'nb_places' => 200,
            'companions_limit' => 0,
            'public_minisite' => 0,
            'start_date' => '10/02/2015 14:30',
            'end_date' => '10/02/2015 15:30',
            'address' => 'NE, Espace de l\'Europe 1',
            'type_id' => $t->id,
            'ctrl_password' => '123456',
            'ctrl_password_confirmation' => '123456',
            'advanced_minisite_mode' => false,
            'language_fr' => "1",
            'language_en' => "0",
            'language_de' => "0"
        ];
        $response = $this->post("/companies/$company->id/events", $rawEvt);

        // Check
        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/events");

        $events = Event::where('name_fr', $rawEvt['name_fr'])->get();
        $this->assertTrue(count($events) === 1);
        $event = $events[0];

        $this->assertEventsSame($rawEvt, $event);
        $this->assertEventValid($event);
    }

    /**
     * Test CompanyUserController@index
     *
     * @return void
     */
    public function testDuplicate()
    {
        // Datas
        $company = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company, ['events-create']);

        // Test datas
        $t = Type::first();
        $eventSrc = factory(Event::class, 1)
            ->create()
            ->each(function ($e) use ($t) {
                $t->events()->save($e);

                // Add ticket
                $ticket = new Ticket();
                $ticket->event_id = $e->id;
                $ticket->save();
            })[0];

        $eventSrc->setDefaultConfirmMail();
        $eventSrc->setDefaultFullEventMail();
        $eventSrc->setDefaultSubscribeForm();
        $eventSrc->setDefaultSurveyForm();
        $eventSrc->setDefaultController('111111');
        $eventSrc->setDefaultTicket();

        $this->assertEventValid($eventSrc);

        // Ask
        $response = $this->get("/companies/$company->id/events/$eventSrc->id/duplicate");

        // Check
        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/events");

        $events = Event::where('name_fr', "[Copie] " . $eventSrc->name_fr)->get();
        $this->assertTrue(count($events) === 1);
        $event = $events[0];

        $this->assertEventsCopy($eventSrc, $event);
        $this->assertEventValid($event);
    }
}
