<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Lang;
use Log;

/**
 * Subscribe tests: concern only subscribtion with personal link
 */
class SubscribeTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        $this->withoutExceptionHandling(); // For test debug purpose
    }

    public function testSubscribeAccessValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create()[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        // Ask
        $response = $this->get("/subscribe/$guest->accessHash");

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent()->getData();
        
        $this->assertTrue($datas['event']->id === $event->id);
    }

    public function testSubscribeAccessUnValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create()[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        // Ask
        $response = $this->get("/subscribe/aaaaaaaaaaaaaaaaaa");

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent()->getData();
        
        $response->assertSee(Lang::get('models.subscribeForm.badLink'));
    }

    /**
     * Subscibe to multiple session.
     * Check:
     * * Guest is created
     * * Correct number of Companions are created
     * * Correct number of Companions are linked to sessions
     */
    public function testSubscribeSessionValid()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $sessions = factory(Session::class, 4)
            ->create([
                'event_id' => $event->id,
                'places' => 10
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [
                "".$sessions[0]->id => 2,
                "".$sessions[1]->id => 3,
                "".$sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        $guest = $event->guests()->first();
        $this->assertTrue($guest->firstname === "TestBoyFirstName");
        $this->assertTrue($guest->lastname === "TestBoyLastName");
        $this->assertTrue($guest->email === "test.boy@he-arc.ch");

        $this->assertTrue($guest->companions()->count() === 3);

        $this->assertTrue($guest->countSessionPlaces($sessions[0]) === 2);
        $this->assertTrue($guest->countSessionPlaces($sessions[1]) === 3);
        $this->assertTrue($guest->countSessionPlaces($sessions[2]) === 1);
        $this->assertTrue($guest->countSessionPlaces($sessions[3]) === 0);

        $this->assertTrue($guest->countSessionCheckin($sessions[0]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[1]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[2]) === 0);
        $this->assertTrue($guest->countSessionCheckin($sessions[3]) === 0);
    }

    public function testSubscribeSessionFull()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 10,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 2
            ]);

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [
                "".$sessions[0]->id => 2,
                "".$sessions[1]->id => 2,
                "".$sessions[2]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName2",
            "lastname" => "TestBoyLastName2",
            "email" => "test.boy2@he-arc.ch",
            "companions" => 1,
            "sessions" => [
                "".$sessions[0]->id => 1
            ],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.session.guest_no_more_place_in_sessions', [
            'sessions' => $sessions[0]->name
        ]));
    }

    public function testSubscribeEventFull()
    {
        // Datas
        $company = factory(Company::class)->create();

        $event = factory(Event::class, 1)
            ->create([
                'nb_places' => 4,
                'companions_limit' => 4,
                'public_minisite' => true,
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName",
            "lastname" => "TestBoyLastName",
            "email" => "test.boy@he-arc.ch",
            "companions" => 3,
            "sessions" => [],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        // Ask
        $response = $this->post("/api/events/$event->id/guests/_register", [
            "extendedFields" => [],
            "firstname" => "TestBoyFirstName2",
            "lastname" => "TestBoyLastName2",
            "email" => "test.boy2@he-arc.ch",
            "companions" => 1,
            "sessions" => [],
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]);

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guests_limit_reached'));
    }
}
