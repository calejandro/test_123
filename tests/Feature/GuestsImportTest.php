<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Log;

class GuestsImportTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        $this->withoutExceptionHandling(); // For test debug purpose
    }

    private function buildDefaultEvent(Company $company)
    {
        // $company = factory(Company::class)->create();
        $t = Type::first();
        $event = factory(Event::class, 1)
            ->create()
            ->each(function ($e) use ($t, $company) {
                $t->events()->save($e);
                $company->events()->save($e);
            })[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();

        $this->assertEventValid($event);

        return $event;
    }

    private function doBasicImport(array $toImport, array $expectExtendedFields)
    {
        // Datas
        $company = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company, ['guests-create']);

        $event = $this->buildDefaultEvent($company);

        $response = $this->post("api/events/$event->id/guests/_import", $toImport);

        // Check
        $response->assertStatus(200);
        
        $e = Event::find($event->id);
        $this->assertTrue(count(array_diff($e->extendedFileds, $expectExtendedFields)) === 0);
    }

    public function testImportWithExtendedFieldsLinear()
    {
        $this->doBasicImport([
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com',
                    'adresse' => 'mhouse 42',
                    'tel' => '0001',
                    'type' => 'mouse',
                ],
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'adresse' => 'dstreet 43',
                    'tel' => '0002',
                    'type' => 'duck',
                ]
            ],
            'ignored' => []
        ], ['adresse', 'tel', 'type']);
    }

    public function testImportWithExtendedFieldsNotLinear()
    {
        $this->doBasicImport([
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com',
                    'adresse' => 'mhouse 42',
                    'tel' => '0001',
                ],
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'adresse' => 'dstreet 43',
                    'type' => 'duck',
                ]
            ],
            'ignored' => []
        ], ['adresse', 'tel', 'type']);
    }

    public function testImportWithoutExtendedFields()
    {
        $this->doBasicImport([
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com'
                ],
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com'
                ]
            ],
            'ignored' => []
        ], []);
    }

    public function testImportIgnoreExtendedFields()
    {
        $this->doBasicImport([
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com'
                ],
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'tel' => '12345'
                ]
            ],
            'ignored' => ['tel']
        ], []);
    }

    public function testImportNotAuthorized()
    {
        // Datas
        $company1 = factory(Company::class)->create();
        $company2 = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company1, ['guests-create']);

        $event = $this->buildDefaultEvent($company2);

        $response = $this->post("api/events/$event->id/guests/_import", [
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com'
                ],
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'tel' => '12345'
                ]
            ],
            'ignored' => []
        ]);

        // Check
        $response->assertStatus(302);
        
        $e = Event::find($event->id);
        $this->assertTrue($e->extendedFileds == null);
        $this->assertTrue(count($e->guests) === 0);
    }

    public function testDoubleImport()
    {
        // Datas
        $company = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company, ['guests-create']);

        $event = $this->buildDefaultEvent($company);

        $response = $this->post("api/events/$event->id/guests/_import", [
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'tel' => '12345'
                ]
            ],
            'ignored' => []
        ]);

        // Check 1
        $response->assertStatus(200);
        
        $e = Event::find($event->id);
        $this->assertTrue(count(array_diff($e->extendedFileds, ['tel'])) === 0);


        // rePost
        $response = $this->post("api/events/$event->id/guests/_import", [
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'mm@gmail.com',
                    'address' => 'my address'
                ]
            ],
            'ignored' => []
        ]);

        // Check 2
        $response->assertStatus(200);
        
        $e = Event::find($event->id);
        $this->assertTrue(count(array_diff($e->extendedFileds, ['tel', 'address'])) === 0);
    }

    public function testDoubleReplicateImport()
    {
        // Datas
        $company = factory(Company::class)->create();

        // Test boy
        $this->setTestBoy($company, ['guests-create']);

        $event = $this->buildDefaultEvent($company);

        $response = $this->post("api/events/$event->id/guests/_import", [
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'donald',
                    'nom' => 'duck',
                    'mail' => 'dd@gmail.com',
                    'tel' => '12345'
                ]
            ],
            'ignored' => []
        ]);

        // Check 1
        $response->assertStatus(200);
        
        $e = Event::find($event->id);
        $this->assertTrue(count(array_diff($e->extendedFileds, ['tel'])) === 0);


        // rePost
        $response = $this->post("api/events/$event->id/guests/_import", [
            'mapping' => [
                'prenom' => 'firstname',
                'nom' => 'lastname',
                'mail' => 'email'
            ],
            'guests' => [
                [
                    'prenom' => 'mickey',
                    'nom' => 'mouse',
                    'mail' => 'dd@gmail.com',
                    'address' => 'my address'
                ]
            ],
            'ignored' => []
        ]);

        // Check 2
        $response->assertStatus(201);
        
        $e = Event::find($event->id);
        $this->assertTrue(count(array_diff($e->extendedFileds, ['tel', 'address'])) === 0);
        $this->assertTrue(count($e->guests) === 2);
    }
}
