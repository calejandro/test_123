<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;
use App\Companion;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Mobile checkin app API dedicated test
 * Test Companion checkin in Session
 */
class CompanionSessionCheckinTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        // $this->withoutExceptionHandling(); // For test debug purpose
    }

    private function setUpEvent(Company $company): Event
    {
        $event = factory(Event::class, 1)
            ->create([
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        return $event;
    }
    
    /**
     * Test Case
     *  * Data: Guest with 2 companions (no checkin to event).
     *              Guest Subscribed to Session 1 and Session 2. no Checkin.
     *              Companion1 Subscribed to Session1 and Session2. Checkin to session 2 only.
     *              Companion2 Subscribed to Session1 and Session2. no Checkin.
     *  * Action: Checkin the companion only in Session1 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 1
     *  * Assert: Companion1<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 0 (unchanged)
     *  * Assert: { type: 'companion', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testCompanionSessionCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion2->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/$companion1->access_hash");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1));
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2));

        $this->assertTrue($companionUpdated1->isCheckinToSession($session1), "The companion should be checkin to session1 now");
        $this->assertTrue($companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session2));

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion1->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest with 2 companions (no checkin to event).
     *              Guest Subscribed to Session 1 and Session 2. no Checkin.
     *              Companion1 Subscribed to Session1 and Session2. no Checkin.
     *              Companion2 Subscribed to Session1 only. no Checkin.
     *  * Action: Checkin the companion 2 only in Session2 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion1<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 1
     *  * Assert: Guest<->Session1 subscribed == true (unchanged)
     *  * Assert: Guest<->Session2 subscribed == true (unchanged)
     *  * Assert: Companion1<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion1<->Session2 subscribed == false // Place stolen here
     *  * Assert: Companion2<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion2<->Session2 subscribed == true // Place stolen comes here
     *  * Assert: { type: 'companion', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testCompanionSessionCheckinStolePlaceToCompanionValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session2->id/_checkin/$companion2->access_hash");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        // Assert sessions checkin
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1));
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated1->isCheckinToSession($session1));
        $this->assertTrue(!$companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated2->isCheckinToSession($session2), "The companion2 should be checkin in session2");

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue($guestUpdated->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
        $this->assertTrue(!$companionUpdated1->isSubscribedToSession($session2), "The companion1 should not be subscribed to session2 anymore");

        $this->assertTrue($companionUpdated2->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated2->isSubscribedToSession($session2), "The companion2 should have stolen a place to companion1");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion2->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest with 2 companions (no checkin to event).
     *              Guest Subscribed to Session 1 and Session 2. no Checkin.
     *              Companion1 Subscribed to Session1 and Session2. all Checkin !
     *              Companion2 Subscribed to Session1 only. no Checkin.
     *  * Action: Checkin the companion 2 only in Session2 with hash
     *  * Assert: 200 received // place stolen
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion1<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 1
     *  * Assert: Guest<->Session1 subscribed == true (unchanged)
     *  * Assert: Guest<->Session2 subscribed == false
     *  * Assert: Companion1<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion1<->Session2 subscribed == true (unchanged)
     *  * Assert: Companion2<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion2<->Session2 subscribed == true
     *  * Assert: { type: 'companion', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testCompanionSessionCheckinStolePlaceToGuestValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session2->id/_checkin/$companion2->access_hash");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        // Assert sessions checkin
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1));
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2));

        $this->assertTrue($companionUpdated1->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated2->isCheckinToSession($session2), "The companion2 should have stolen a place");

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue(!$guestUpdated->isSubscribedToSession($session2), "The companion2 should have stolen this place");

        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated2->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated2->isSubscribedToSession($session2), "The companion2 should have stolen a place");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion2->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest with 2 companions (no checkin to event).
     *              Guest Subscribed to Session 1 and Session 2. all Checkin !
     *              Companion1 Subscribed to Session1 and Session2. all Checkin !
     *              Companion2 Subscribed to Session1 only. no Checkin.
     *  * Action: Checkin the companion 2 only in Session2 with hash
     *  * Assert: 404 received // No place to stole
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion1<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 subscribed == true (unchanged)
     *  * Assert: Guest<->Session2 subscribed == true (unchanged)
     *  * Assert: Companion1<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion1<->Session2 subscribed == true (unchanged)
     *  * Assert: Companion2<->Session1 subscribed == true (unchanged)
     *  * Assert: Companion2<->Session2 subscribed == false (unchanged)
     *  * Assert: { type: 'companion', data: { id: GOODID, sessions: [<1>] } } received
     */
    public function testCompanionSessionCheckinStolePlaceFailed()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session2->id/_checkin/$companion2->access_hash");

        // Check
        $response->assertStatus(404);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        // Assert sessions checkin
        $this->assertTrue($guestUpdated->isCheckinToSession($session1));
        $this->assertTrue($guestUpdated->isCheckinToSession($session2));

        $this->assertTrue($companionUpdated1->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue(!$companionUpdated2->isCheckinToSession($session2));

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue($guestUpdated->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated2->isSubscribedToSession($session1));
        $this->assertTrue(!$companionUpdated2->isSubscribedToSession($session2), "The companion2 should not be able to stole checkin place");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion2->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 1, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1
     *  * Action: Checkin the companion only in Session1 with hash
     *  * Assert: 404 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 0 (unchanged)
     */
    public function testCompanionSessionCheckinWrongHash()
    {
       // Datas
       $company = factory(Company::class)->create();
       $event = $this->setUpEvent($company);

       $guest = factory(Guest::class, 1)
           ->create([
               'event_id' => $event->id
           ])[0];

       $companion1 = factory(Companion::class, 1)
           ->create([
               'guest_id' => $guest->id
           ])[0];
       
       $session1 = factory(Session::class, 1)
           ->create([
               'event_id' => $event->id
           ])[0];

       $session1->guests()->attach($guest->id, [
           'checkin' => false
       ]);

       $session1->companions()->attach($companion1->id, [
           'checkin' => false
       ]);

       // Test boy
       $this->actingAs($event->controller);

       // Ask
       $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/0000-WRONG-HASH-0000");

       // Check
       $response->assertStatus(404);

       $guestUpdated = Guest::find($guest->id);
       $companionUpdated1 = Companion::find($companion1->id);

       $this->assertTrue(!$guestUpdated->checkin);
       $this->assertTrue(!$companionUpdated1->checkin);

       // Assert sessions checkin
       $this->assertTrue(!$guestUpdated->isCheckinToSession($session1));
       $this->assertTrue(!$companionUpdated1->isCheckinToSession($session1));

       // Assert sessions subscription
       $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
       $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Companion Subscribed to Session1 and Session2.
     *              Companion Checkin to Session1 only
     *  * Action: Checkin the companion in Session1 with hash
     *  * Assert: 409 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 subscription == 1 (unchanged)
     *  * Assert: Guest<->Session2 subscription == 1 (unchanged)
     *  * Assert: Companion<->Session1 subscription == 1 (unchanged)
     *  * Assert: Companion<->Session2 subscription == 1 (unchanged)
     *  * Assert: { type: 'companion', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testCompanionSessionCheckinAllreadyDone() // (no more place)
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];


        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);

        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);

        $session1->companions()->attach($companion1->id, [
            'checkin' => true
        ]);

        $session2->companions()->attach($companion1->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/$companion1->access_hash");

        // Check
        $response->assertStatus(409);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);

        // Assert sessions checkin
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated1->isCheckinToSession($session1));

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2));
        $this->assertTrue(!$companionUpdated1->isCheckinToSession($session2));

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));

        $this->assertTrue($guestUpdated->isSubscribedToSession($session2));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session2));
    
        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion1->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 2 companions.
     *              Subscribed to Session1 and Session2 (guest and companions).
     *              Checkin to Session1 and Session2 (guest and companions).
     *  * Action: UnCheckin the Companion1 only in Session1
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion1<->Event checkin == 0 (unchanged)
     *  * Assert: Companion2<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 0
     *  * Assert: Companion1<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session1 subscription == 1 (unchanged)
     *  * Assert: Guest<->Session2 subscription == 1 (unchanged)
     *  * Assert: Companion1<->Session1 subscription == 1 (unchanged)
     *  * Assert: Companion1<->Session2 subscription == 1 (unchanged)
     *  * Assert: Companion2<->Session1 subscription == 1 (unchanged)
     *  * Assert: Companion2<->Session2 subscription == 1 (unchanged)
     */
    public function testCompanionSessionUnCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion2->id, [
            'checkin' => true
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/companions/$companion1->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        // Assert sessions checkin
        $this->assertTrue($guestUpdated->isCheckinToSession($session1));
        $this->assertTrue($guestUpdated->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated1->isCheckinToSession($session1), "The companion sould not be checkin anymore in session1");
        $this->assertTrue($companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue($companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated2->isCheckinToSession($session2));

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue($guestUpdated->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated2->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated2->isSubscribedToSession($session2));
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 2 companions.
     *              Subscribed to Session1 and Session2 (guest and companions).
     *              Guest Checkin to Session1 and Session2.
     *              Companion1 Checkin to Session1.
     *              Companion2 Checkin to Session1 and Session2.
     *  * Action: Try to UnCheckin the Companion1 only in Session2 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion1<->Event checkin == 0 (unchanged)
     *  * Assert: Companion2<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion1<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion1<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion2<->Session2 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session1 subscription == 1 (unchanged)
     *  * Assert: Guest<->Session2 subscription == 1 (unchanged)
     *  * Assert: Companion1<->Session1 subscription == 1 (unchanged)
     *  * Assert: Companion1<->Session2 subscription == 1 (unchanged)
     *  * Assert: Companion2<->Session1 subscription == 1 (unchanged)
     *  * Assert: Companion2<->Session2 subscription == 1 (unchanged)
     */
    public function testCompanionSessionUnCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion1->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion1->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion2->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion2->id, [
            'checkin' => true
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/companions/$companion1->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated1 = Companion::find($companion1->id);
        $companionUpdated2 = Companion::find($companion2->id);

        $this->assertTrue(!$guestUpdated->checkin);
        $this->assertTrue(!$companionUpdated1->checkin);
        $this->assertTrue(!$companionUpdated2->checkin);

        // Assert sessions checkin
        $this->assertTrue($guestUpdated->isCheckinToSession($session1));
        $this->assertTrue($guestUpdated->isCheckinToSession($session2));

        $this->assertTrue(!$companionUpdated1->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated1->isCheckinToSession($session2));

        $this->assertTrue($companionUpdated2->isCheckinToSession($session1));
        $this->assertTrue($companionUpdated2->isCheckinToSession($session2));

        // Assert sessions subscription
        $this->assertTrue($guestUpdated->isSubscribedToSession($session1));
        $this->assertTrue($guestUpdated->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated1->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated1->isSubscribedToSession($session2));

        $this->assertTrue($companionUpdated2->isSubscribedToSession($session1));
        $this->assertTrue($companionUpdated2->isSubscribedToSession($session2));
    }
}
