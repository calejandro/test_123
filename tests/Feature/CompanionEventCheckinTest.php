<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;
use App\Companion;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Mobile checkin app API dedicated test
 * Test Companion checkin in Event
 */
class CompanionEventCheckinTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        // $this->withoutExceptionHandling(); // For test debug purpose
    }

    private function setUpEvent(Company $company): Event
    {
        $event = factory(Event::class, 1)
            ->create([
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        return $event;
    }

    /**
     * Test Case
     *  * Data: Companion not checkin to event, Guest and Companion2 not checkin
     *  * Action: Checkin the companion only
     *  * Assert: Companion checkin == 1
     *  * Assert: Guest checkin unchanged
     *  * Assert: { type: 'companion', data: { id: GOODID } } received
     */
    public function testCompanionEventCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/$companion1->access_hash");

        // Check
        $response->assertStatus(200);

        $companionUpdated = Companion::find($companion1->id);
        $this->assertTrue($companionUpdated->checkin == 1, "The companion1 has been uncorrectly updated");

        $companionUpdated2 = Companion::find($companion2->id);
        $this->assertTrue($companionUpdated2->checkin == 0, "The companion2 has been uncorrectly updated");

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorrectly updated");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion1->id, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Companion checkin to event, 1 Guest 2 Companions
     *  * Action: Checkin the companion only
     *  * Assert: 409 received
     *  * Assert: { type: 'companion', data: { id: GOODID } } received
     *  * Assert: Guest checkin unchanged
     *  * Assert: Companions checkin unchanged
     */
    public function testCompanionEventCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'checkin' => 0,
                'event_id' => $event->id
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'checkin' => 1,
                'guest_id' => $guest->id
            ])[0];

        $companion2 = factory(Companion::class, 1)
            ->create([
                'checkin' => 0,
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/$companion1->access_hash");

        // Check
        $response->assertStatus(409);

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "companion", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $companion1->id, "The API response is uncorrectly formated");
      
        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorrectly updated");
        $companionUpdated1 = Companion::find($companion1->id);
        $this->assertTrue($companionUpdated1->checkin == 1, "The companion has been uncorrectly updated");
        $companionUpdated2 = Companion::find($companion2->id);
        $this->assertTrue($companionUpdated2->checkin == 0, "The companion has been uncorrectly updated");
    }

    /**
     * Test Case
     *   * Data: Companion and Guest not checkin to event
     *   * Action: Trying to checkin a guest with the wrong hash
     *   * Assert: 404 received
     *   * Assert: Guest checkin unchanged
     */
    public function testCompanionEventCheckinWrongHash()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);
        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];
        
        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/_checkin/0000-WRONGHASH-0000");

        // Check
        $response->assertStatus(404);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorrectly updated");
        $this->assertTrue($companionUpdated->checkin == 0, "The companion has been uncorrectly updated");
    }

    /**
     * Test Case
     *   * Data: Companion and Guest not checkin to Event1, Event2
     *   * Action: Trying to checkin Companion in Event2
     *   * Assert: 404 received
     *   * Assert: Companion checkin unchanged
     */
    public function testCompanionEventCheckinWrongEvent()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);
        $event2 = $this->setUpEvent($company);
        $guest = factory(Guest::class, 1)
            ->create([
                'checkin' => 0,
                'event_id' => $event->id
            ])[0];
        $companion = factory(Companion::class, 1)
            ->create([
                'checkin' => 0,
                'guest_id' => $guest->id
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event2->id/_checkin/$companion->access_hash");

        // Check
        $response->assertStatus(404);
        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest has been uncorrectly updated");
        $companionUpdated = Companion::find($companion->id);
        $this->assertTrue($companionUpdated->checkin == 0, "The companion has been uncorrectly updated");
    }

    /**
     * Test Case
     *   * Data: Guest1 with Companion1, Companion2 all checkin to event
     *   * Action: Uncheckin the Companion1
     *   * Assert: Companion1 checkin==0
     *   * Assert: Companion2 checkin unchanged
     *   * Assert: Guest1 checkin unchanged
     */
    public function testCompanionEventUnCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 2)
            ->create([
                'event_id' => $event->id,
                'checkin' => 1
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];

        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/companions/$companion1->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 1, "The guest should not be updated");
        
        $companionUpdated1 = Companion::find($companion1->id);
        $this->assertTrue($companionUpdated1->checkin == 0, "The companion has not be uncheckin");

        $companionUpdated2 = Companion::find($companion2->id);
        $this->assertTrue($companionUpdated2->checkin == 1, "The companion has not be updated");
    }

    /**
     * Test Case
     *   * Data: Guest1 not checkin to event with Companion1 not checkin and Companion2 checkin
     *   * Action: Trying to uncheckin Guest1
     *   * Assert: 200 received
     *   * Assert: Guest1 not updated
     *   * Assert: Companion1 not updated
     *   * Assert: Guest2 not updated
     */
    public function testCompanionEventUnCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 2)
            ->create([
                'event_id' => $event->id,
                'checkin' => 0
            ])[0];

        $companion1 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 0
            ])[0];

        $companion2 = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/companions/$companion1->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $this->assertTrue($guestUpdated->checkin == 0, "The guest should not be updated");
        
        $companionUpdated1 = Companion::find($companion1->id);
        $this->assertTrue($companionUpdated1->checkin == 0, "The companion should not be updated");

        $companionUpdated2 = Companion::find($companion2->id);
        $this->assertTrue($companionUpdated2->checkin == 1, "The companion should not be updated");
    }
}
