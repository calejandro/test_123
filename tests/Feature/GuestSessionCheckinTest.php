<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;
use App\Companion;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Mobile checkin app API dedicated test
 * Test Guest checkin in Session
 */
class GuestSessionCheckinTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        // $this->withoutExceptionHandling(); // For test debug purpose
    }

    private function setUpEvent(Company $company): Event
    {
        $event = factory(Event::class, 1)
            ->create([
                'company_id' => $company->id
            ])[0];
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');

        return $event;
    }
    
    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1 and Session2.
     *  * Action: Checkin the guest only in Session1 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 1
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 0 (unchanged)
     *  * Assert: { type: 'guest', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testGuestSessionCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue(!$guestUpdated->checkin, "The guest has been uncorreclty updated");
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorreclty updated");

        $this->assertTrue($guestUpdated->isCheckinToSession($session1), "The guest is not checkin to session");
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2), "The guest is checkin to wrong session");

        $this->assertTrue(!$companionUpdated->isCheckinToSession($session1), "The companion is checkin to unwanted session");
        $this->assertTrue(!$companionUpdated->isCheckinToSession($session2), "The companion is checkin to unwanted session");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "guest", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $guest->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1
     *  * Action: Checkin the guest only in Session1 with hash
     *  * Assert: 404 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 0 (unchanged)
     */
    public function testGuestSessionCheckinWrongHash()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/0000-WRONG-HASH-0000");

        // Check
        $response->assertStatus(404);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue(!$guestUpdated->checkin, "The guest has been uncorrectly updated");
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorrectly updated");

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1), "The guest has been uncorrectly updated");
        $this->assertTrue(!$companionUpdated->isCheckinToSession($session1), "The companion has been uncorrectly updated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1 and Session2.
     *              Checkin to Session1 only
     *  * Action: Checkin the guest only in Session1 with hash
     *  * Assert: 409 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 1 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 0 (unchanged)
     *  * Assert: { type: 'guest', data: { id: GOODID, sessions: [<2>] } } received
     */
    public function testGuestSessionCheckinAllreadyDone() // (no more place)
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session1->companions()->attach($companion->id, [
            'checkin' => false
        ]);
        $session2->companions()->attach($companion->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(409);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue(!$guestUpdated->checkin, "The guest has been uncorrectly updated");
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorrectly updated");

        $this->assertTrue($guestUpdated->isCheckinToSession($session1), "The guest is not checkin to session anymore");
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2), "The guest has been uncorrectly updated");

        $this->assertTrue(!$companionUpdated->isCheckinToSession($session1), "The companion is checkin to unwanted session");
        $this->assertTrue(!$companionUpdated->isCheckinToSession($session2), "The companion is checkin to unwanted session");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "guest", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $guest->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 2, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1 only.
     *  * Action: Checkin the guest only in Session2 with hash
     *  * Assert: 404 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 0 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 0 (unchanged)
     *  * Assert: { type: 'guest', data: { id: GOODID, sessions: [<1>] } } received
     */
    public function testGuestSessionCheckinWrongSession()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);

        $session1->companions()->attach($companion->id, [
            'checkin' => false
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session2->id/_checkin/$guest->accessHash");

        // Check
        $response->assertStatus(404);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue(!$guestUpdated->checkin, "The guest has been uncorrectly updated");
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorrectly updated");

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1), "The guest has been uncorrectly updated");
        $this->assertTrue(!$guestUpdated->isSubscribedToSession($session2), "The guest has been uncorrectly updated");
        $this->assertTrue(!$guestUpdated->isCheckinToSession($session2), "The guest has been uncorrectly updated");

        $this->assertTrue(!$companionUpdated->isCheckinToSession($session1), "The companion is checkin to unwanted session");
        $this->assertTrue(!$companionUpdated->isSubscribedToSession($session2), "The companion is subscribed to unwanted session");
        $this->assertTrue(!$companionUpdated->isCheckinToSession($session2), "The companion is subscribed to unwanted session");

        $datas = (array)json_decode($response->content(), true);
        $this->assertTrue($datas['type'] === "guest", "The API response is uncorrectly formated");
        $this->assertTrue($datas['data']['id'] === $guest->id, "The API response is uncorrectly formated");
        $this->assertTrue(sizeof($datas['data']['sessions']) === 1, "The API response is uncorrectly formated");
    }

    /**
     * Test Case
     *  * Data: Guest not checkin to event and session with 1 companion.
     *              Subscribed to Session1 and Session2 (guest and companion).
     *              Checkin to Session1 and Session2 (guest and companion).
     *  * Action: UnCheckin the guest only in Session1 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0
     *  * Assert: Guest<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 1 (unchanged)
     */
    public function testGuestSessionUnCheckinValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion->id, [
            'checkin' => true
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/guests/$guest->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue(!$guestUpdated->checkin, "The guest has been uncorrectly updated");
        $this->assertTrue(!$companionUpdated->checkin, "The companion has been uncorrectly updated");

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1), "The guest has not been uncheckin");
        $this->assertTrue($guestUpdated->isCheckinToSession($session2), "The guest is not checkin to session anymore");

        $this->assertTrue($companionUpdated->isCheckinToSession($session1), "The companion is not checkin to session anymore");
        $this->assertTrue($companionUpdated->isCheckinToSession($session2), "The companion is not checkin to session anymore");
    }

    /**
     * Test Case
     *  * Data: Guest checkin to event with 1 companion. 
     *              Subscribed to Session1 and Session2 (guest and companion).
     *              Checkin to Session2 only.
     *              Companion checkin to Session1 and Session2.
     *  * Action: Try to UnCheckin the guest only in Session1 with hash
     *  * Assert: 200 received
     *  * Assert: Guest<->Event checkin == 0 (unchanged)
     *  * Assert: Companion<->Event checkin == 0 (unchanged)
     *  * Assert: Guest<->Session1 checkin == 0 (unchanged)
     *  * Assert: Guest<->Session2 checkin == 1 (unchanged)
     *  * Assert: Companion<->Session1 checkin == 1 (unchanged)
     *  * Assert: Companion<->Session2 checkin == 1 (unchanged)
     */
    public function testGuestSessionUnCheckinAllreadyDone()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = $this->setUpEvent($company);

        $guest = factory(Guest::class, 1)
            ->create([
                'event_id' => $event->id,
                'checkin' => 1
            ])[0];

        $companion = factory(Companion::class, 1)
            ->create([
                'guest_id' => $guest->id,
                'checkin' => 1
            ])[0];
        
        $session1 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session2 = factory(Session::class, 1)
            ->create([
                'event_id' => $event->id
            ])[0];

        $session1->guests()->attach($guest->id, [
            'checkin' => false
        ]);
        $session2->guests()->attach($guest->id, [
            'checkin' => true
        ]);
        $session1->companions()->attach($companion->id, [
            'checkin' => true
        ]);
        $session2->companions()->attach($companion->id, [
            'checkin' => true
        ]);

        // Test boy
        $this->actingAs($event->controller);

        // Ask
        $response = $this->post("/api/events/$event->id/sessions/$session1->id/guests/$guest->id/_uncheckin");

        // Check
        $response->assertStatus(200);

        $guestUpdated = Guest::find($guest->id);
        $companionUpdated = Companion::find($companion->id);

        $this->assertTrue($guestUpdated->checkin == 1, "The guest has been uncorrectly updated");
        $this->assertTrue($companionUpdated->checkin == 1, "The companion has been uncorrectly updated");

        $this->assertTrue(!$guestUpdated->isCheckinToSession($session1), "The guest has been unexpectedly updated");
        $this->assertTrue($guestUpdated->isCheckinToSession($session2), "The guest is not checkin to session anymore");

        $this->assertTrue($companionUpdated->isCheckinToSession($session1), "The companion is not checkin to session anymore");
        $this->assertTrue($companionUpdated->isCheckinToSession($session2), "The companion is not checkin to session anymore");
    }
}
