<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;
use App\Type;
use App\Event;
use App\Guest;
use App\Session;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Lang;
use Log;

/**
 * Nominative register tests: concern only subscribtion in public minisite, without private link and with nominative event
 */
class RegisterNominativeTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        //$this->withoutExceptionHandling(); // For test debug purpose
    }

    /**
     * New guest, not in DB
     * Subscribe to event success
     * Subscribe to multiple sessions success
     * Subscribe with companions success
     */
    public function testNewGuestRegisterValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 3 // nb places in event
            ])[0];

        $this->setDefaultEventValues($event);

        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 5
            ]);

        // Ask
        $params = [
            "email" => "steve.visinand@he-arc.ch",
            "extendedFields" => [
                "tel"=> "0000 0001"
            ],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id,
                $sessions[1]->id,
                $sessions[2]->id
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id,
                        $sessions[1]->id,
                        $sessions[2]->id
                    ]
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $json = $response->getOriginalContent(); // in: guest, message

        $this->assertRegisteredGuest($params, $json['guest']);
        $this->assertTrue($json['message'] === Lang::get('models.event.guest_registration_created'));
    }

    /**
     * New guest want to register to some session with companions but 1 session is full
     *
     * New guest, not in DB
     * Subscribe to event success
     *
     * Assert guest is created
     * Assert no session place is taken
     * Assert no companion is created
     */
    public function testNewGuestRegisterSessionFull()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 5
            ])[0];

        $this->setDefaultEventValues($event);
        
        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 3
            ]);

        // First Ask
        $params = [
            "email" => "steve.visinand@he-arc.ch",
            "extendedFields" => [],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id,
                $sessions[1]->id,
                $sessions[2]->id
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id,
                        $sessions[1]->id
                    ]
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        $this->assertTrue($sessions[0]->placesReserved === 3); // session full
        $this->assertTrue($sessions[1]->placesReserved === 2); // 1 place remains
        $this->assertTrue($sessions[2]->placesReserved === 1); // 2 places remains

        // Second Ask

        $params = [
            "email" => "steve.visinand.full@he-arc.ch",
            "extendedFields" => [],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id, // full, will throw an error
                $sessions[1]->id, // limit
                $sessions[2]->id // 1
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m.full@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[2]->id // 2 : limit
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();


        $fullSessions = collect([$sessions[0]]);
        $this->assertTrue($datas['message'] === Lang::get('models.session.guest_no_more_place_in_sessions', [
            'sessions' => $fullSessions->implode(Session::localAttributeField('name'), ', ')
        ]));

        // Same check as previous, no more registrations
        $this->assertTrue($sessions[0]->placesReserved === 3); // session full
        $this->assertTrue($sessions[1]->placesReserved === 2); // 1 place remains
        $this->assertTrue($sessions[2]->placesReserved === 1); // 2 places remains

        $this->assertTrue($event->guests()->where('guests.email', $params['email'])->count() === 0, 'Guest created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][0]['email'])->count() === 0, 'Companions created');
    }

    /**
     * New guest want to register with multiple companions with sessions but event is full
     *
     * New guest, not in DB
     * Subscribe to event FAILED
     * Subscribe to multiple sessions canceled
     * Subscribe companions canceled
     *
     * Assert no guest is created
     * Assert no session place is taken
     * Assert no companion is created
     */
    public function testNewGuestRegisterEventFull()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 2 // limitation
            ])[0];

        $this->setDefaultEventValues($event);
        
        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 3
            ]);

        // First Ask
        $params = [
            "email" => "steve.visinand.event.full@he-arc.ch",
            "extendedFields" => [],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id,
                $sessions[1]->id,
                $sessions[2]->id
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m.event.full@disney.com",
                    "company" => "Disney",
                    "sessions" => []
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d.event.full@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guests_limit_reached'));

        $this->assertTrue($sessions[0]->placesReserved === 0);
        $this->assertTrue($sessions[1]->placesReserved === 0);
        $this->assertTrue($sessions[2]->placesReserved === 0);
        
        // Assert no guest or companion was created
        $this->assertTrue($event->guests()->where('guests.email', $params['email'])->count() === 0, 'Guest created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][0]['email'])->count() === 0, 'Companions created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][1]['email'])->count() === 0, 'Companions created');
    }


     /**
     * Existing (not subscribed) guest want to register to event and sessions with companions
     *
     * Guest in DB
     * Subscribe to event success
     * Subscribe to multiple sessions success
     * Subscribe with companions success
     */
    public function testGuestRegisterValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 3
            ])[0];

        $this->setDefaultEventValues($event);
      
        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        $guest = factory(Guest::class)
            ->create([
                'checkin' => 0,
                'email' => 'existing.guest@mail.com',
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "email" => $guest->email,
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => $guest->firstname . '_a',
            "lastname" =>  $guest->lastname . '_a',
            "sessions" => [
                $sessions[0]->id,
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[2]->id
                    ]
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertRegisteredGuest($params, Guest::findOrFail($guest->id));
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_saved'));
    }

    /**
     * Existing guest want to register without companions and without sessions
     *
     * Existing guest, in DB
     * Subscribe to event success
     *
     * Assert guest is created
     * Assert no session place is taken
     * Assert no companion is created
     */
    public function testGuestRegisterWithoutCompanionsWithoutSessionsValid()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 3
            ])[0];

        $this->setDefaultEventValues($event);

        $sessions = factory(Session::class, 2)
            ->create([
                'event_id' => $event->id
            ]);

        $guest = factory(Guest::class)
            ->create([
                'checkin' => 0,
                'email' => 'existing.guest@mail.com',
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "email" => $guest->email,
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => $guest->firstname,
            "lastname" =>  $guest->lastname,
            "sessions" => [
            ],
            "companionsData" => []
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_saved'));

        $this->assertTrue($sessions[0]->placesReserved === 0);
        $this->assertTrue($sessions[1]->placesReserved === 0);
    }

    /**
     * Existing guest want to register with multiple companions with sessions but event is full
     *
     * Existing guest, in DB
     * Subscribe to event FAILED
     * Subscribe to multiple sessions canceled
     * Subscribe companions canceled
     * Assert no guest is subscribed
     * Assert no session place is taken
     * Assert no companion is subscribed
     */
    public function testGuestRegisterEventFull()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 1 // only 1 place
            ])[0];

        $this->setDefaultEventValues($event);
    
        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id
            ]);

        $guest = factory(Guest::class)
            ->create([
                'checkin' => 0,
                'email' => 'existing.guest@mail.com',
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "email" => $guest->email,
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => $guest->firstname . '_a',
            "lastname" =>  $guest->lastname . '_a',
            "sessions" => [
                $sessions[0]->id,
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[2]->id
                    ]
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guests_limit_reached'));

        $this->assertTrue($sessions[0]->placesReserved === 0);
        $this->assertTrue($sessions[1]->placesReserved === 0);
        $this->assertTrue($sessions[2]->placesReserved === 0);

        // Assert no guest or companion was created
        $this->assertTrue(!$event->guests()->where('guests.email', $params['email'])->firstOrFail()->subscribed, 'Guest created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][0]['email'])->count() === 0, 'Companions created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][1]['email'])->count() === 0, 'Companions created');
    }

    /**
     * Existing guest want to register with multiple companions with session but one session is full
     *
     * Existing guest, in DB
     * Subscribe to event canceled
     * Subscribe to multiple sessions FAILED
     * Subscribe companions canceled
     *
     * Assert no guest is created
     * Assert no session place is taken
     * Assert no companion is created
     */
    public function testGuestRegisterSessionFull()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 5
            ])[0];

        $this->setDefaultEventValues($event);
        
        $sessions = factory(Session::class, 3)
            ->create([
                'event_id' => $event->id,
                'places' => 2
            ]);

        // First Ask
        $params = [
            "email" => "steve.visinand@he-arc.ch",
            "extendedFields" => [],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[0]->id,
                $sessions[1]->id
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[0]->id,
                        $sessions[1]->id
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(200);
        $datas = $response->getOriginalContent();

        $this->assertRegisteredGuest($params, $datas['guest']);
        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_created'));

        $this->assertTrue($sessions[0]->placesReserved === 2); // session full
        $this->assertTrue($sessions[1]->placesReserved === 2); // session full
        $this->assertTrue($sessions[2]->placesReserved === 0); // 2 places remains

        // Second Ask

        $params = [
            "email" => "steve.visinand.full@he-arc.ch",
            "extendedFields" => [],
            "firstname" => "Steve",
            "lastname" => "Visinand",
            "sessions" => [
                $sessions[1]->id, // full
                $sessions[2]->id, // remains 1
            ],
            "companionsData" => [
                [
                    "firstname" => "Mickey",
                    "lastname" => "Mouse",
                    "email" => "m.m.full@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[2]->id // remains 0
                    ],
                ],
                [
                    "firstname" => "Donald",
                    "lastname" => "Duck",
                    "email" => "d.d.full@disney.com",
                    "company" => "Disney",
                    "sessions" => [
                        $sessions[2]->id // 2 : full
                    ]
                ]
            ]
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $fullSessions = collect([$sessions[1], $sessions[2]]);
        $this->assertTrue($datas['message'] === Lang::get('models.session.guest_no_more_place_in_sessions', [
            'sessions' => $fullSessions->implode(Session::localAttributeField('name'), ', ')
        ]));

        // Same check as previous, no more registrations
        $this->assertTrue($sessions[0]->placesReserved === 2); // session full
        $this->assertTrue($sessions[1]->placesReserved === 2); // session full
        $this->assertTrue($sessions[2]->placesReserved === 0); // 2 places remains

        $this->assertTrue($event->guests()->where('guests.email', $params['email'])->count() === 0, 'Guest created');
        $this->assertTrue($event->companions()->where('companions.email', $params['companionsData'][0]['email'])->count() === 0, 'Companions created');
    }


    /**
     * Existing SUBSCRIBED guest want to change his personal informations (based on email) (from public link) but he can't
     *
     * guest in DB
     * Try to change name and sessions
     *
     * Assert guest is not modified
     * Assert sessions registration not modified
     */
    public function testGuestChangePersonalInformationsFailed()
    {
        // Datas
        $company = factory(Company::class)->create();
        $event = factory(Event::class, 1)
            ->create([
                'nominative_companions' => true,
                'nb_places' => 3
            ])[0];

        $this->setDefaultEventValues($event);

        $guest = factory(Guest::class)
            ->create([
                'checkin' => 0,
                'subscribed' => 1,
                'email' => 'existing.guest@mail.com',
                'event_id' => $event->id
            ]);

        // Ask
        $params = [
            "email" => $guest->email,
            "extendedFields" => [
                "tel"=> "6668AB"
            ],
            "firstname" => $guest->firstname . '_a',
            "lastname" =>  $guest->lastname . '_a',
            "sessions" => [],
            "companionsData" => [],
        ];

        $response = $this->post("/api/events/$event->id/guests/_register_nominative", array_merge($params, [
            "recaptchaResponse" => "captcha-test" // work only with test RECAPTCHA_SECRETKEY
        ]));

        // Check
        $response->assertStatus(403);
        $datas = $response->getOriginalContent();

        $this->assertTrue($datas['message'] === Lang::get('models.event.guest_registration_allready_done'));

        // Assert no changes on guest
        $guestFresh = Guest::findOrFail($guest->id);
        $this->assertTrue($guestFresh->firstname === $guestFresh->firstname);
        $this->assertTrue($guestFresh->lastname === $guestFresh->lastname);
        $this->assertTrue($guestFresh->subscribed == 1); // always subscribed
    }

     /**
     * Assert guest datas
     * - normal fields
     * - ext fields
     * - sessions
     *
     * Assert Companions datas
     * - normal fields
     * - sessions
     */
    private function assertRegisteredGuest(array $askedDatas, Guest $guest)
    {
        $this->assertTrue($guest->firstname === $askedDatas['firstname']);
        $this->assertTrue($guest->lastname === $askedDatas['lastname']);
        $this->assertTrue($guest->email === $askedDatas['email']);
        $this->assertTrue($guest->subscribed > 0);

        foreach($askedDatas['extendedFields'] as $key => $value) {
            $this->assertTrue($guest->extendedFields[$key] === $value);
        }

        // Assert guest has all the sessions
        $this->assertTrue($guest->sessions()->count() === count($askedDatas['sessions']));

        foreach ($askedDatas['sessions'] as $sessionId) {
            $session = Session::findOrFail($sessionId);
            $this->assertTrue($guest->isSubscribedToSession($session),
               '[assertRegisteredGuest] Guest is not registerd to session ' . $sessionId);
        }

        // Assert companions are created
        $nbCompanion = array_key_exists('companionsData', $askedDatas) ? count($askedDatas['companionsData']) : 0;
        $this->assertTrue($guest->companions()->count() === $nbCompanion);

        // Assert companions has been correctly inserted
        foreach ($askedDatas['companionsData'] as $companionRaw) {

            $companion = $guest->companions()->where('email', $companionRaw['email'])->get();
            $this->assertTrue($companion->count() === 1, '[assertRegisteredGuest] Missing companion');
            $companion = $companion[0];

            $this->assertTrue($companion->firstname === $companionRaw['firstname']);
            $this->assertTrue($companion->lastname === $companionRaw['lastname']);
            $this->assertTrue($companion->company === $companionRaw['company']);

            $this->assertTrue($companion->sessions()->count() === count($companionRaw['sessions']));

            foreach ($companionRaw['sessions'] as $sessionsId) {
                $session = Session::findOrFail($sessionsId);
                $this->assertTrue($companion->isSubscribedToSession($session));
            }
        }
    }

    private function setDefaultEventValues(Event $event)
    {
        $event->setDefaultConfirmMail();
        $event->setDefaultFullEventMail();
        $event->setDefaultSubscribeForm();
        $event->setDefaultSurveyForm();
        $event->setDefaultController('111111');
        $event->setDefaultTicket();
    }
}
