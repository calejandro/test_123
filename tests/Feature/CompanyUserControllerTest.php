<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use App\Permission;

use Tests\TestCase;
use App\Utils\TestTrait;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyUserControllerTest extends TestCase
{
    use RefreshDatabase;
    use TestTrait;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan("db:seed");
        $this->withoutExceptionHandling(); // For test debug purpose
    }

    /**
     * Test CompanyUserController@index
     *
     * @return void
     */
    public function testIndexDisplay()
    {
        // Datas
        $company = factory(Company::class)->create();
        $users = factory(User::class, 10)
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            });

        $controllers = factory(User::class, 5)
            ->states('controller')
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            });

        // Test boy
        $u = $this->setTestBoy($company, ['users-view']);

        // Ask
        $response = $this->get("/companies/$company->id/users");

        // Check
        $response->assertStatus(200);

        $datas = $response->getOriginalContent()->getData();
        
        $this->assertTrue($datas['company']->id === $company->id);
        $this->assertModelsSame($users->push($u), $datas['users']);
    }

    public function testCreate()
    {
        $company = factory(Company::class)->create();
        $this->setTestBoy($company, ['users-create']);

        // Show form
        $response = $this->get("/companies/$company->id/users/create");
        $response->assertStatus(200);

        // Post form
        $new = [
            'name' => 'Sebastian Bergmann 42', 
            'email' => 'ab@aba.com', 
            'password' => 'secret12',
            'password_confirmation' => 'secret12'
        ];
        $response = $this->post("/companies/$company->id/users", $new);

        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/users");

        $this->assertUserExists($new);
    }

    public function testUpdate()
    {
        $company = factory(Company::class)->create();
        $u = factory(User::class, 1)
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            })[0];

        $this->setTestBoy($company, ['users-update']);
        
        // Show form
        $response = $this->get("/companies/$company->id/users/$u->id/edit");
        $response->assertStatus(200);

        // Post form
        $update = [
            'name' => 'Sebastian Bergmann 45', 
            'email' => 'ab@aba.com', 
            'password' => 'secret12',
            'password_confirmation' => 'secret12'
        ];
       
        $response = $this->put("/companies/$company->id/users/$u->id", $update);

        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/users");

        $this->assertUserExists($update, $u->id);
    }

    public function testDelete()
    {
        $company = factory(Company::class)->create();
        $u = factory(User::class, 1)
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            })[0];

        $this->setTestBoy($company, ['users-delete']);

        $response = $this->delete("/companies/$company->id/users/$u->id");
        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/users");

        $this->assertTrue(User::find($u->id) === null);
    }

    /**
     * Test CompanyUserController@index
     *
     * @return void
     */
    public function testHasNotPermission()
    {
        $this->withExceptionHandling(); // For test debug purpose

        // Datas
        $company = factory(Company::class)->create();

        // Testing

        // index
        $this->setTestBoy($company, $this->getAllPermsNamesExcept('users-view'));
        $response = $this->get("/companies/$company->id/users");
        $response->assertStatus(403);

        // create
        $this->setTestBoy($company, $this->getAllPermsNamesExcept('users-create'));
        $response = $this->post("/companies/$company->id/users", [
            'name' => 'Sebastian Bergmann 42', 
            'email' => 'ab@aba.com', 
            'password' => 'secret12',
            'password_confirmation' => 'secret12'
        ]);
        $response->assertStatus(403);

        // create show
        $response = $this->get("/companies/$company->id/users/create");
        $response->assertStatus(403);
        
        // update
        $this->setTestBoy($company, $this->getAllPermsNamesExcept('users-update'));
        $u = factory(User::class, 1)
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            })[0];

        $response = $this->put("/companies/$company->id/users/$u->id", [
            'name' => 'Sebastian Bergmann 43', 
            'email' => 'ab@aba.com', 
            'password' => 'secret12',
            'password_confirmation' => 'secret12'
        ]);
        $response->assertStatus(403);

        // update show
        $response = $this->get("/companies/$company->id/users/$u->id/edit");
        $response->assertStatus(403);

        // destroy
        $this->setTestBoy($company, $this->getAllPermsNamesExcept('users-delete'));

        $response = $this->delete("/companies/$company->id/users/$u->id");
        $response->assertStatus(403);

        // edit_permissions
        $this->setTestBoy($company, $this->getAllPermsNamesExcept(''));
        $u = factory(User::class, 1)
            ->create()
            ->each(function ($u) use ($company) {
                $company->users()->save($u);
            })[0];

        $response = $this->get("/admins/$u->id/edit_permissions");
        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/events");

        // update_permissions
        $response = $this->post("/admins/$u->id/edit_permissions", []);
        $response->assertStatus(302);
        $response->assertRedirect("/companies/$company->id/events");
    }
}
