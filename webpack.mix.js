let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/tinymce/plugins', 'public/js/plugins');
mix.copy('node_modules/tinymce/skins', 'public/js/skins');
mix.copy('resources/assets/js/plugins/', 'public/js/plugins/');
mix.copy('resources/assets/js/tools/i18n-tinymce', 'public/js/langs');

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/app-checkin/index.js', 'public/js/checkin')
    .js('resources/assets/js/app-subscribe/index.js', 'public/js/subscribe')
    .js('resources/assets/js/app-survey/index.js', 'public/js/survey')
    .js('resources/assets/js/app-minisite/index.js', 'public/js/minisite')
    .js('resources/assets/js/app-compatibility-message-box/index.js', 'public/js/compatibility/message-box.js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.version();