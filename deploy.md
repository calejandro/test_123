nvm install --lts
php artisan down

# récupérer la version voulue
git fetch
git checkout master
git pull

# installer les dépendences manquantes
php -d allow_url_fopen=on ../../composer.phar install

# lancer les migrations
php artisan migrate

# lancer les scripts de seed
# ex: php artisan db:seed --class=MinisiteSlugFixSeeder

# vérifier et paramètrer le .env

npm run prod

php artisan up

/opt/php7.1/bin/php /home/nfs-clients/f64506fad5265c9aa21d8bd91d77141a/web/eventwise-final/artisan queue:restart
