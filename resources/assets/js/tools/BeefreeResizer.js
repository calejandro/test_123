export default class BeefreeResizer {
    constructor (fullscreen) {
        this.refreshDomDependencies()
        this.fullscreen = fullscreen
        this.addBeefreeHeightListener()
    }

    checkDomDependencies () {
        return this.beefreeContainer.length > 0 && this.siteHeader > 0 && this.headBar > 0 &&
            this.headPannel.length > 0 && this.headCtrl.length > 0
    }

    refreshDomDependencies () {
        this.beefreeContainer = $('#mail-edition-container')
        this.siteHeader = $('#site-header')
        this.headBar = $('.head-bar-nav-info')
        this.headPannel = $('#head-panel')
        this.headCtrl = $('.email-tab-control')
        this.panelConfig = $('.pannel-mail-config')
    }

    /**
     * Compute best height for beefree
     */
    getBeefreeheight (siteHeader, headPannel, headCtrl, panelConfig, headBar) {
        let h = window.innerHeight - siteHeader.height()
        h = h - headPannel.outerHeight(true) - headCtrl.outerHeight(true) - panelConfig.outerHeight(true) - headBar.outerHeight(true)
        h = h  - 72 // Remove magin and paddings
        return h
    }

    getHeight() {
        if(!this.checkDomDependencies()) {
            this.refreshDomDependencies()
        }

        let h = 0
        if(this.fullscreen) {
            h = window.innerHeight - this.panelConfig.outerHeight(true)
        }
        else {
            h = this.getBeefreeheight(this.siteHeader, this.headPannel, this.headCtrl, this.panelConfig, this.headBar)
        }

        return h
    }

    /**
     * Force resize
     */
    resizeBeefree () {
        this.beefreeContainer.height(this.getHeight())
    }
    
    addBeefreeHeightListener () {

        if(!this.checkDomDependencies()) {
            this.refreshDomDependencies()
        }

        let resizeFct = () => {
            if(this.beefreeContainer.length === 0) {
                console.log('Return')
                setTimeout(() => {
                    this.refreshDomDependencies()
                    this.addBeefreeHeightListener()
                }, 200)
                return;
            }
            let h = 0
            if(this.fullscreen) {
                h = window.innerHeight - this.panelConfig.outerHeight(true)
            }
            else {
                h = this.getBeefreeheight(this.siteHeader, this.headPannel, this.headCtrl, this.panelConfig, this.headBar)
            }
            this.beefreeContainer.height(h)
        }

        resizeFct()
        $(window).resize(() => {
            resizeFct()
        })
    }
}