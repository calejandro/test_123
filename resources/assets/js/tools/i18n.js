import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

import Cookies from './cookies.js'

// French language
import { french } from './lang/fr.js'
import { deutsch } from './lang/de.js'
import { english } from './lang/en.js'


// Ready translated locale messages
let messages = {
  fr: french,
  de: deutsch,
  en: english
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: Cookies.lang(), // set locale
  fallbackLocale: 'fr',
  messages, // set locale messages
})

i18n.getLocale = function() {
  return this.locale
}

i18n.getIternationalLocale = function() {
  let corrTable = {
      'fr': 'fr-FR',
      'de': 'de-DE',
      'en': 'en-US'
  }
  return corrTable[this.locale]
}

i18n.getLocale = function() {
  return this.locale
}

i18n.getLocalTranslations = function() {
  return this.messages[this.locale];
}

Vue.prototype.$locale = {
  change(lang){
    i18n.locale = lang
  },
  current(){
    return i18n.locale
  }
}

export default i18n
