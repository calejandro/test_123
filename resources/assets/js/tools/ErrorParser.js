import i18n from './i18n'
import EventBus from './EventBus'

export default {
    is401: function(error) {
        return error.response.status === 401;
    },
    is422: function(error) {
        return error.response.status === 422;
    },
    is403: function(error) {
        return error.response.status === 403;
    },
    is409: function(error) {
        return error.response.status === 409;
    },
    is404: function(error) {
        return error.response.status === 404;
    },
    formatMessage422: function(error) {
        return this.formatErrorList(error.response.data);
    },
    formatMessage403: function(error) {
        return error.response.data.message;
    },
    formatMessage409: function(error) {
        return error.response.data;
    },
    formatMessage401: function(error) {
        return error.response.data.message;
    },
    /**
     * @input: errors: {
     *  key: message,
     *  key: message
     * }
     */
    formatErrorList: function (errors) {
        let errorHtml = '<ul class="list-error-centered">'
        for (let key in errors) {
            errorHtml += '<li>' + errors[key] + '</li>'
        }
        return errorHtml + '</ul>'
    },
    formatErrorMessage: function (error) {
        if (this.is422(error)) {
            Raven.captureException(error)
            return this.formatMessage422(error)
        }
        else if (this.is403(error)) {
            return this.formatMessage403(error)
        }
        else if (this.is401(error)) {
            return this.formatMessage401(error)
        }
        else {
            Raven.captureException(error)
            return i18n.getLocalTranslations().shared.UnknownErrorContactAdmin
        }
    },
    /**
     * Format error  and throw on 'SHOW_ERROR' channel
     */
    throwEventBusError:  function (error) {
        EventBus.$emit('SHOW_ERROR', this.formatErrorMessage(error))
    }
}