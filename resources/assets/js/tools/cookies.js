let getCookie = (a) => {
    let b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

export default {
    lang: function () {
        return getCookie('lang');
    }
}
