export const english = {
    "beefree": {
        "BigMap": "Big Map",
        "LittleMap": "Little Map",
        "MiddleMap": "Medium Map"
    },
    "checkin": {
        "EventSelect": {
            "NoChoosenEvent": "No events selected",
            "Title": "Event"
        },
        "EventSelectModal": {
            "Title": "Select an event"
        },
        "GuestCancelLastCheckin": {
            "CancelCheckin": "Cancel entry",
            "ConfirmModalMessage": "Do you confirm that you want to cancel this guest's entry?",
            "ConfirmModalTitle": "Do you confirm the cancelation of the participant data?"
        },
        "GuestCheckinToast": {
            "AllreadyChecked": "no booking",
            "NoPlaceReserved": "invalid ticket/booking",
            "TicketNotValid": "invalid ticket/booking",
            "UnknownError": "unknown error",
            "ValidatedEntry": "Entry approved"
        },
        "GuestLastCheckin": {
            "PleasePlaceQrCodeInScanningArea": "Please scan a QR Code ticket",
            "PleaseSelectEvent": "Please select an event"
        },
        "GuestQrCodeScanner": {
            "ScanActive": "Scanning device activated",
            "ScanActivePresentValid": "Present a valid ticket (QR-code)",
            "ScanFreezed": "Scanning pending"
        },
        "GuestQuickInfosTable": {
            "name": "Name",
            "of": "ge",
            "sessions": "Sessions",
            "type": "Type",
            "typeCompanion": "accompanying person",
            "typeGuest": "Guest",
            "warning": {
                "allreadyCheckin": "Invalid: This ticket has already been scanned",
                "declined": "The guest declined the invitation",
                "notInvited": "The guest has not been invited",
                "notSubscribed": "The guest did not register"
            }
        },
        "GuestUnFreezeScanning": {
            "RestartScanning": "Resume scanning"
        },
        "ItemChooseModal": {
            "NoItem": "No item"
        },
        "NoCameraLock": {
            "NoCameraFound": "The scanning application requires access to a camera to operate"
        },
        "SessionSelect": {
            "MainDoor": "Main event entrance",
            "Title": "Entrance (session)"
        },
        "SessionSelectModal": {
            "Title": "Select a session"
        }
    },
    "companion": {
        "Companion": "Begleiter",
        "formAccess": "Access the form",
        "formAccessNotAvailable": "Minisite non-existent or page non-existent."
    },
    "companionSelect": {
        "Choice": "Companions",
        "Limit": "Maximum:"
    },
    "email": {
        "ActiveFullScreen": "Enable fullscreen mode",
        "Cancel": "Cancel",
        "Configuration": "Settings",
        "ConfirmMailSend": "Confirm the email sending",
        "ContentRecovered": "Content automatically recovered from the {lang} email",
        "DesactiveFullScreen": "Disable fullscreen mode",
        "EditConfiguration": "Edit settings",
        "Email": "Email",
        "EmailSended": "Email successfully send",
        "HideConfiguration": "Reduce",
        "JoinICSInvit": "Join ICS invitation",
        "JoinPDFTicket": "Join the entry ticket",
        "JoinedFiles": "Attached files",
        "Object": "Object",
        "PleaseSaveEmailBefore": "Veuillez vérifier et enregistrer l'email avant l'envoi",
        "PleaseSelectModel": "Please select a template",
        "ProcessEnded": "End of the process.",
        "ProcessLaunched": "The sending process has been correctly launched.",
        "Save": "Save",
        "SMTPErrorProduced": "An error occurred. Please check the SMTP configuration.",
        "SelectColumn": "Select a column",
        "Send": "Send",
        "SendMailIs": "Sending adress : ",
        "SendWithSponsorizeEmail": "Send with email Eventwise",
        "SenderFromColumn": "Mail from name column",
        "SendingInProcess": "An email sending is already in progress",
        "Sessions": "Sessions",
        "SessionCategory": "Category",
        "ShowOverview": "Show the preview",
        "SessionsStyle": "Sessions",
        "TemplateNameToSave": "Name of the template to save :",
        "TemplateSuccessfullyDeleted": "Email template successfully deleted",
        "TemplateSuccessfullyLoaded": "Email template successfully loaded",
        "TemplateSuccessfullySaved": "Template successfully saved",
        "TitleSessions": "Title",
        "YouHaveSpecifiedSpecialConfig": "<strong>Information :</strong> you have chosen a personalized email adress. The email will be send based on this new configuration.",
        "YouHaventSpecifiedFromColumn": "No sender name column has been chosen, the default sender name will be used.",
        "YouHaventSpecifiedSMTP": "you did not enter any SMTP configuration. The emails will be send through the Sponsorize server.",
        "YouWillSendThisMailTo": "You are about to send this email to {nbPeople} people",
        "tltipInvalidMail": "The email address seems invalid",
        "tltipToCheck": "the email adressé has not been checked",
        "tltipUnCheckable": "Unverifiable",
        "tltipValidMail": "The email address is valid"
    },
    "event": {
        "Categories": "Categories",
        "GuestsStatistics": "Guests statistics",
        "NoCategories": "No categories",
        "SurveyResults": "Survey results"
    },
    "guest": {
        "AnonymiseFinished": "Anonymisation done",
        "AreYouSureDeleteGuest": "Do you really want to delete this guest and his companions ?",
        "CannotDeleteUsedColumn": "Impossible to delete an assigned column",
        "Catchall": "Unverifiable",
        "CheckCompanions": "Please check companions informations.",
        "Checkin": "Check-in",
        "Companion": "Companion",
        "CompanionCheckin": "Check-in companion",
        "CompanionTicket": "Companion ticket",
        "Companions": "Companions",
        "Company": "Company",
        "CompanyPlaceHolder": "Company name",
        "Declined": "Declined",
        "DeleteAllConfirmMessage": "All guests of this event will be deleted. <br/> Do you want to continue?",
        "DeleteAllDoneMessage": "All guests of this event have been properly deleted.",
        "DeleteColumnConfirmMessage": "The\"{field}\" column will be deleted on all guests.",
        "DeleteColumnDoneMessage": "The\"{field}\" column will be deleted on all guests.",
        "DoYouConfirmColumnDelete": "Do you confirm the removal of the column?",
        "Email": "Email",
        "EmailPlaceHolder": "Enter your email",
        "ExtendedFields": "Extended fields",
        "FirsnamePlaceHolder": "Enter your firstname",
        "Firstname": "Firstname",
        "Guest": "Guest",
        "GuestCheckin": "Check-in main guest",
        "GuestListWarnings": "Guest list related warnings",
        "Guests": "Guests",
        "GuestsAllEventsCheckedIn": "Total of attending guests",
        "GuestsAllEventsDeclined": "Total of declining guests",
        "GuestsAllEventsImported": "All guests on all events",
        "GuestsAllEventsSubscribed": "Total of subscribed guests",
        "GuestsCheckedIn": "Checked-in guests",
        "GuestsDeclined": "Guests who declined the invitation",
        "GuestsImported": "Imported guests",
        "GuestsSubscribed": "Subscribed guests",
        "Imported": "Imported",
        "InvitationsAllEventsSended": "Total of invited guests",
        "InvitationsSended": "Invitations send",
        "Invited": "Invited",
        "Lastname": "Lastname",
        "LastnamePlaceHolder": "Enter your lastname",
        "NbCompanions": "Companions",
        "NoDataFoundInXLSFile": "No data was found in this file, please be sure to use the first sheet of the file to import.",
        "NoGuestToView": "No guest to display",
        "ObligatoryFields": "Standard fields",
        "SelectAll": "Select all",
        "SessionsCheckin": "Participated",
        "SessionsReserved": "Reserved",
        "SetDeclinedWarningMessage": "The guest and his/her companions will be unsubscribed from the event and sessions. Unsubscribtion of the sessions and the companions is irreversible.",
        "SetDeclinedWarningTitle": "Do you confirm the unsubscription of the guest?",
        "ShowQrCode": "Show the QRCode",
        "Subscribed": "Subscribed",
        "SurveyFields": "Survey",
        "ToRetest": "To check",
        "Total": "Total",
        "Type": "Type",
        "UnSelectAll": "Unselect all",
        "UnValid": "Invalide",
        "Valid": "Valide",
        "ValideMailAdress": "Valid email adress",
        "emailValidity": {
            "title" : "Email address validity",
            "description": "Description",
            "icon": "Icon",
            "underAudit": {
                "description": "Email address in the process of validation.",
                "name": "Under audit"
            },
            "unvalid": {
                "description": "Email address does not exist.",
                "name": "Invalid"
            },
            "unverifiable": {
                "description": "Unverifiable email address, the email server does not allow an address validation. It is impossible to determine if the address is valid or not.",
                "name": "Unauditable"
            },
            "unverified": {
                "description": "Email address has not yet been verified.",
                "name": "Unaudited"
            },
            "valid": {
                "description": "Email address has been verified and is valid.",
                "name": "Valid"
            },
            "validity": "Validity"
        },
        "importCodes": {
            "badFormat": "Invalid language code format",
            "email": "Invalid email address|Invalid email addresses",
            "empty": "Missing Values|Missing Values",
            "language": "Invalid language code",
            "length": "Values too long|Values too long"
        },
        "importSnippets": {
            "toLine": "to line|to lines"
        },
        "otherLinesHidden": "other line not displayed|other lines not displayed",
        "subscribeFormAccess": "Access subscribe form",
        "subscribeFormAccessNotAvailable": "Unexisting minisite or no subscribe page."
    },
    "guestIndex": {
        "columnHeaders": {
            "ExtendedFields": "Extended fields",
            "Guest": "Guest",
            "GuestComp": "guest and accompanying person",
            "MainGuest": "Main guest",
            "SessionsCheckin": "Participants",
            "SessionsReserved": "Registered participants",
            "Status": "Status",
            "SurveyFields": "Survey"
        }
    },
    "jobs": {
        "CustomFromColumn": "Custom from column",
        "DefaultEmail": "Eventwise Email",
        "DefaultValue": "Default value",
        "ExecutedBy": "Started by",
        "ExportXLS": "Export to XLS",
        "Finished": "Finished",
        "FromAddress": "From mail",
        "FromName": "From name",
        "IgnoredMails": "ignored|ignored",
        "IncludedICS": "ICS invitation included",
        "Job": "Job",
        "JobVerificationAlreadyRunning": "An email list check is in progress",
        "JobVerificationAlreadyRunningMessage": "A verification of the email addresses of the last imported guests is in progress.",
        "Jobs": "Jobs",
        "NoPastJobs": "No past jobs for the moment.",
        "NoPendingJobs": "No running jobs at the moment.",
        "PastJobs": "Past jobs",
        "PendingJobs": "Pending jobs",
        "ProcessedMail": "of processed mails",
        "Recipients": "recipient|recipients",
        "ResendFailed": "Send to failed recipients",
        "Resended": "Resended in job #",
        "SendedMail": "Email envoyé|Emails envoyés",
        "SentMails": "sent mail|sent mails",
        "Server": "Server",
        "Started": "Started",
        "TestedMails": "tested addresses|tested addresses",
        "Updated": "Updated"
    },
    "simpleMinisite": {
        "templates": "Templates",
        "save": "Save",
        "cancel": "Cancel",
        "saveTemplate": "Save as new template",
        "updateExistingTemplate": "Update an existing template",
        "loadTemplate": "Load template",
        "deleteTemplate": "Delete template",
        "chooseTemplate": "Choose a template",
        "template":{
            "headline": "Please select the template you want to update.",
            "info": "Advise: Do not forget to save your last changes !",
            "cancel": "Cancel",
            "update": "Update"
        }
    },
    "sessionForm": {
        "ReservedPlaces": "Number of places",
        "mandatory_session_group_alert": "You must select at least one session for this category",
        "mandatory_subscription" : "You must select at least one session",
        "mandatory_category_subscription" : "You must select at least one session in any category",
        "allowed_sessions_reached" : "You reached the maximum number of sessions allowed"
    },
    "MandatoryGroupHeader": {
        "mandatory_session_group" : "Subscription required"
    },
    "sessionSelect": {
        "Full": "Full",
        "PlacesRemaining": "Remaining places"
    },
    "shared": {
        "Actions": "Actions",
        "AreYouSurExitNotSaved": "Your modifications have not been saved, are you sure you want to leave this page ?",
        "Cancel": "Cancel",
        "Close": "Close",
        "Confirm": "Confirm",
        "Delete": "Delete",
        "DoYouConfirmDelete": "Do you confirm deletion?",
        "DoYouConfirmThisAction": "Do you confirm this action ?",
        "DoYouReallyWantToQuit": "Do you really want to quit ?",
        "Edit": "Edit",
        "ErrorHasProduced": "An error has occured",
        "ErrorHasProducedOnEmailSave": "An error has occurred at the email registration",
        "Information": "Information:",
        "InsertError": "Insert error",
        "ModificationsSaved": "Modifications saved",
        "Next": "Next",
        "PleaseUseAllRequiredFields": "Please fill all required fields",
        "Prev": "Previous",
        "ResponseSaved": "Thank you, your answer was well recorded.",
        "Statistics": "Statistics",
        "UnknownErrorContactAdmin": "An unexpected mistake happened, contact the event organizer if it happens again.",
        "Validate": "Validate",
        "ValidateAndSubscribe": "Validate and subscribe",
        "VerifyYourPassword": "Verify your password !",
        "WaitLoadingEnd": "Please wait until the end of the loading time",
        "YourPassword": "Your password",
        "lang": {
            "de": "de",
            "en": "en",
            "fr": "fr"
        },
        "no": "no",
        "yes": "yes"
    },
    "subscribe": {
        "NominativeSessionsForm": {
            "GuestWithoutName": "Guest"
        }
    },
    "subscribeForm": {
        "AuthorizeStandardFieldsUpdate": "Allow modification of standard fields",
        "ColumnName": "Column name",
        "CompanionCompany": "Company",
        "CompanionFirstname": "Firstname",
        "CompanionLastname": "Lastname",
        "Configuration": "Form parameters",
        "Confirmed": "Subscription confirmed",
        "ContentType": "Text type",
        "Description": "Description",
        "DisplaySettings": "Display settings",
        "Error": "An unknown error happened",
        "ExtendedFields": "Extended fields",
        "LabelColor": "Color of the label text",
        "Layout": "Layout",
        "NoSpecialChars": "The \"Column name\" field must not contain any special characters. Only lowercase and uppercase letters (a-z and A-Z), hyphens (-) and underscore (_) are allowed. Please use the \"Title\" field to specify the title of the question.",
        "Paragraph": "Paragraph",
        "PleaseAccept": "Please accept the condition ",
        "Problem": "A problem has been encountered",
        "RequiredCompanionDatas": "Mandatory information for the companion:",
        "SelectType": "Select a type",
        "Sessions": "Sessions",
        "SubmitBackgroundColor": "Background color of the confirmation button",
        "SubmitText": "Confirmation button text",
        "SubmitTextColor": "Text color of the confirmation button text",
        "SubscribeForm": "Suscribe form",
        "SubscriptionSettings": "Subscription settings",
        "TextBloc": "Text bloc",
        "Title": "Title",
        "Title1": "Title level 1",
        "Title2": "Title level 2",
        "Title3": "Title level 3",
        "TwiceSameValueInChoices": "Please enter an unique value for each of the choices.",
        "UserInformation": "User information",
        "colors": {
            "black": "Black",
            "grey": "Grey",
            "white": "White"
        },
        "fieldsTypes": {
            "checkbox": "multichoices check box",
            "date": "Date",
            "number": "Number",
            "radio": "Unique choice check box",
            "select": "Drop down box",
            "text": "Text field",
            "textarea": "Multilines text field"
        }
    },
    "surveyForm": {
        "AlreadyFilled": "You already filled this survey.",
        "CompanionForm": "Companion form",
        "Form": "Form",
        "GuestForm": "Guest form",
        "Layout": "Layout",
        "PleaseAuthToAnswearSurvey": "You must be logged in to complete the survey. Please access the site from your personal access link.",
        "ShowOverview": "Preview",
        "SurveyForm": "Survey form",
        "ValidationButton": "Validation button"
    },
    "surveyFormResult": {
        "MissingForm": "No form has been created",
        "NoResponsesToShow": "No responses to display"
    },
    "tickets": {
        "DownloadPDF": "Download entry ticket",
        "remainingLines": " remaining lines"
    }
}