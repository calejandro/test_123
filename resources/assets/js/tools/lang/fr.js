export const french = {
    "beefree": {
        "BigMap": "Grande carte",
        "LittleMap": "Petite carte",
        "MiddleMap": "Carte moyenne"
    },
    "checkin": {
        "EventSelect": {
            "NoChoosenEvent": "Aucun évènement choisi",
            "Title": "Evènement"
        },
        "EventSelectModal": {
            "Title": "Choisissez un évènement"
        },
        "GuestCancelLastCheckin": {
            "CancelCheckin": "Annuler l'entrée",
            "ConfirmModalMessage": "Confirmez-vous vouloir annuler l'entrée de cet invité ?",
            "ConfirmModalTitle": "Attention annulation de l'entrée"
        },
        "GuestCheckinToast": {
            "AllreadyChecked": "Entrée invalide: Ce billet a déjà été scanné",
            "NoPlaceReserved": "Entrée invalide: Aucune place n'est réservée",
            "TicketNotValid": "Entrée invalide: Ticket non valide ou aucune place réservée",
            "UnknownError": "Erreur inconnue: Impossible de valider l'entrée",
            "ValidatedEntry": "Entrée validée"
        },
        "GuestLastCheckin": {
            "PleasePlaceQrCodeInScanningArea": "Veuillez scanner un QR Code d'entrée",
            "PleaseSelectEvent": "Veuillez sélectioner un évènement"
        },
        "GuestQrCodeScanner": {
            "ScanActive": "Scan actif",
            "ScanActivePresentValid": "Scan actif, présentez un billet valide",
            "ScanFreezed": "Scan stoppé, en attente"
        },
        "GuestQuickInfosTable": {
            "name": "nom",
            "of": "de",
            "sessions": "sessions",
            "type": "type",
            "typeCompanion": "accompagnant",
            "typeGuest": "invité",
            "warning": {
                "allreadyCheckin": "Entrée invalide, ce billet à déjà été scanné",
                "declined": "L'invité à décliné l'invitation",
                "notInvited": "L'invité n'a pas été invité",
                "notSubscribed": "L'invité ne s'est pas inscrit"
            }
        },
        "GuestUnFreezeScanning": {
            "RestartScanning": "Reprendre le scan"
        },
        "ItemChooseModal": {
            "NoItem": "Aucun élément"
        },
        "NoCameraLock": {
            "NoCameraFound": "L'application de validation des entrées nécessite l'accès à une caméra pour fonctionner"
        },
        "SessionSelect": {
            "MainDoor": "Entrée principale",
            "Title": "Entrée (session)"
        },
        "SessionSelectModal": {
            "Title": "Choisissez une session"
        }
    },
    "companion": {
        "Companion": "Accompagnant",
        "formAccess": "Accéder au formulaire",
        "formAccessNotAvailable": "Minisite inexistant ou page inexistante"
    },
    "companionSelect": {
        "Choice": "Nombre d'accompagnants",
        "Limit": "Maximum: "
    },
    "email": {
        "ActiveFullScreen": "Activer le mode plein écran",
        "Cancel": "Annuler",
        "Configuration": "Paramètres",
        "ConfirmMailSend": "Confirmer l'envoi de mail",
        "ContentRecovered": "Contenu récupéré automatiquement depuis le mail en {lang}",
        "DesactiveFullScreen": "Désactiver le mode plein écran",
        "EditConfiguration": "Modifier les paramètres",
        "Email": "Email",
        "EmailSended": "E-Mail envoyé avec success",
        "HideConfiguration": "Réduire",
        "JoinICSInvit": "Joindre invitation ICS",
        "JoinPDFTicket": "Joindre le billet d'entrée",
        "JoinedFiles": "Fichiers joints",
        "Object": "Objet",
        "PleaseSaveEmailBefore": "Veuillez vérifier et enregistrer l'email avant l'envoi",
        "PleaseSelectModel": "Veuillez sélectionner un modèle",
        "ProcessEnded": "Le processus est terminé.",
        "ProcessLaunched": "Le processus d'envoi des mails a été correctement déclenché. L'envoi est en cours.",
        "Save": "Sauvegarder",
        "SMTPErrorProduced": "Une erreur s'est produite. Veuillez vérifier la configuration SMTP.",
        "SelectColumn": "Sélectionnez une colonne",
        "Send": "Envoyer",
        "SendMailIs": "Adresse d'envoi : ",
        "SendWithSponsorizeEmail": "Envoi avec email Eventwise",
        "SenderFromColumn": "Colonne de nom d'expéditeur",
        "SendingInProcess": "Un envoi est déjà en cours",
        "Sessions": "Sessions",        
        "SessionCategory": "Catégorie",
        "ShowOverview": "Afficher l'aperçu",
        "SessionsStyle": "Sessions",
        "TemplateNameToSave": "Nom du template à sauvegarder :",
        "TemplateSuccessfullyDeleted": "Modèle de mail supprimé avec succès",
        "TemplateSuccessfullyLoaded": "Modèle de mail chargé avec succès",
        "TemplateSuccessfullySaved": "Template sauvegardée avec succès",
        "TitleSessions": "Titre",
        "YouHaveSpecifiedSpecialConfig": "<strong>Information :</strong> vous avez spécifié une configuration mail personnalisée. Les mails seront donc envoyés avec cette configuration.",
        "YouHaventSpecifiedFromColumn": "Aucune colonne de nom d'expéditeur n'a été choisie, le nom d'expéditeur par défaut sera utilisé.",
        "YouHaventSpecifiedSMTP": "Vous n'avez pas spécifié de configuration SMTP. Les mails seront donc envoyés avec le serveur de Sponsorize.",
        "YouWillSendThisMailTo": "Vous êtes sur le point d'envoyer ce mail à {nbPeople} personnes",
        "tltipInvalidMail": "L'adresse mail semble invalide",
        "tltipToCheck": "L'adresse mail n'a pas été vérifiée",
        "tltipUnCheckable": "Invérifiable",
        "tltipValidMail": "L'adresse mail semble valide"
    },
    "event": {
        "Categories": "Catégories",
        "GuestsStatistics": "Statistiques des invités",
        "NoCategories": "Aucune catégorie",
        "SurveyResults": "Résultats du sondage"
    },
    "guest": {
        "AnonymiseFinished": "Anonymisation terminée",
        "AreYouSureDeleteGuest": "Êtes-vous sur de vouloir supprimer cet invité et ses accompagnants ?",
        "CannotDeleteUsedColumn": "Impossible de supprimer une colonne assignée",
        "Catchall": "Invérifiable",
        "CheckCompanions": "Veuillez contrôler les informations relatives aux accompagnants.",
        "Checkin": "Participants",
        "Companion": "Accompagnant",
        "CompanionCheckin": "Check-in accompagnant",
        "CompanionTicket": "Ticket accompagnant",
        "Companions": "Accompagnants",
        "Company": "Entreprise",
        "CompanyPlaceHolder": "Nom de l'entreprise",
        "Declined": "Ayant décliné",
        "DeleteAllConfirmMessage": "Tout les invités de cet événement seront supprimés. <br/> Voulez-vous continuer ?",
        "DeleteAllDoneMessage": "Tout les invités de cet événement ont correctement été supprimés.",
        "DeleteColumnConfirmMessage": "La colonne \"{field}\" va être supprimée sur l'ensemble des invités.",
        "DeleteColumnDoneMessage": "La colonne \"{field}\" à été supprimée sur l'ensemble des invités.",
        "DoYouConfirmColumnDelete": "Confirmez-vous la suppression de la colonne ?",
        "Email": "Email",
        "EmailPlaceHolder": "Entrez votre email",
        "ExtendedFields": "Champs étendus",
        "FirsnamePlaceHolder": "Entrez votre prénom",
        "Firstname": "Prénom",
        "Guest": "Invité",
        "GuestCheckin": "Check-in invité principal",
        "GuestListWarnings": "Avertissements relatifs à la liste d'invités",
        "Guests": "Invités",
        "GuestsAllEventsCheckedIn": "Total des invités ayant participé à l'un des événements",
        "GuestsAllEventsDeclined": "Total des invités ayant décliné l'invitation à l'un des événements",
        "GuestsAllEventsImported": "Total des invités sur tous les événements",
        "GuestsAllEventsSubscribed": "Total des invités inscrits à l'un des événements",
        "GuestsCheckedIn": "Invités ayant participé à l'événement",
        "GuestsDeclined": "Invités ayant décliné l'invitation",
        "GuestsImported": "Total des invités de l'événement",
        "GuestsSubscribed": "Invités inscrits",
        "Imported": "Total invités",
        "InvitationsAllEventsSended": "Total des invités ayant reçu une inscription",
        "InvitationsSended": "Invités ayant reçu l'invitation",
        "Invited": "Invités (mail)",
        "Lastname": "Nom",
        "LastnamePlaceHolder": "Entrez votre nom",
        "NbCompanions": "Nombre d'accompagnants",
        "NoDataFoundInXLSFile": "Aucune donnée n'a été trouvée dans ce fichier, assurez-vous d'utiliser la première feuille du document.",
        "NoGuestToView": "Aucun invité à afficher",
        "ObligatoryFields": "Champs standards",
        "SelectAll": "Tout sélectionner",
        "SessionsCheckin": "Participants",
        "SessionsReserved": "Inscrits",
        "SetDeclinedWarningMessage": "L'invité ainsi que ses accompagnants seront désinscris de l'événement et des sessions.<br/> La désinscriptions aux sessions et des accompagnants est irréversible.",
        "SetDeclinedWarningTitle": "Confirmez-vous la désinscription de l'invité ?",
        "ShowQrCode": "Voir le QRCode",
        "Subscribed": "Inscrits",
        "SurveyFields": "Sondage",
        "ToRetest": "A vérifier",
        "Total": "Total",
        "Type": "Type",
        "UnSelectAll": "Tout désélectionner",
        "UnValid": "Invalide",
        "Valid": "Valide",
        "ValideMailAdress": "Adresse mail valide",
        "emailValidity": {
            "title" : "Validitée de l'adresse email",
            "description": "Description",
            "icon": "Icône",
            "underAudit": {
                "description": "Adresse email en cours de validation.",
                "name": "En cours de vérification"
            },
            "unvalid": {
                "description": "Adresse email non existante.",
                "name": "Invalide"
            },
            "unverifiable": {
                "description": "Adresse email invérifiable, le serveur email ne permet pas une validation de l'adresse. Impossible de déterminer si l'adresse est valide ou non.",
                "name": "Invérifiable"
            },
            "unverified": {
                "description": "Adresse email pas encore été vérifiée.",
                "name": "Non-vérifiée"
            },
            "valid": {
                "description": "Adresse email vérifiée et valide.",
                "name": "Valide"
            },
            "validity": "Validitée"
        },
        "importCodes": {
            "badFormat": "Mauvais format de code de langue",
            "email": "Addresse email invalide|Addresses email invalides",
            "empty": "Valeure manquante|Valeures manquantes",
            "language": "Un code de langue est invalide",
            "length": "Valeur trop longue|Valeurs trop longues"
        },
        "importSnippets": {
            "toLine": "à la ligne|aux lignes"
        },
        "otherLinesHidden": "autre ligne non affichée|autres lignes non affichées",
        "subscribeFormAccess": "Accéder au formulaire d'inscription",
        "subscribeFormAccessNotAvailable": "Minisite inexistant ou page inexistante."
    },
    "guestIndex": {
        "columnHeaders": {
            "ExtendedFields": "Champs étendus",
            "Guest": "Invité",
            "GuestComp": "Invité et accompagnant",
            "MainGuest": "Invité principal",
            "SessionsCheckin": "Participants",
            "SessionsReserved": "Inscrits",
            "Status": "Status",
            "SurveyFields": "Sondage"
        }
    },
    "jobs": {
        "CustomFromColumn": "Colonne expéditeur",
        "DefaultEmail": "Email Eventwise",
        "DefaultValue": "Valeur par défaut",
        "ExecutedBy": "Exécuté par",
        "ExportXLS": "Exporter vers XLS",
        "Finished": "Terminé",
        "FromAddress": "Mail expéditeur",
        "FromName": "Nom expéditeur",
        "IgnoredMails": "envoi ignoré|envois ignorés",
        "IncludedICS": "Invitation ICS incluse",
        "Job": "Envoi",
        "JobVerificationAlreadyRunning": "Une vérification de liste d'adresses mail est en cours",
        "JobVerificationAlreadyRunningMessage": "Une vérification des adresses mail des derniers invités importés est en cours.",
        "Jobs": "Envois",
        "NoPastJobs": "Pas d'envoi passé pour le moment.",
        "NoPendingJobs": "Pas d'envoi en cours pour le moment.",
        "PastJobs": "Envois terminés",
        "PendingJobs": "Envois en cours",
        "ProcessedMail": "des mails traités",
        "Recipients": "destinataire|destinataires",
        "ResendFailed": "Envoyer aux destinataires ayant échoué",
        "Resended": "Renvoyé dans l’envoi #",
        "SendedMail": "Email envoyé|Emails envoyés",
        "SentMails": "mail envoyé|mails envoyés",
        "Server": "Serveur",
        "Started": "Commencé",
        "TestedMails": "adresse testée|adresses testées",
        "Updated": "Mis à jour"
    },
    "simpleMinisite": {
        "templates": "Templates",
        "save": "Sauvegarder",
        "cancel": "Retour",
        "saveTemplate": "Sauvegarder en tant que nouveau template",
        "updateExistingTemplate": "Mettre à jour un template existant",
        "loadTemplate": "Charger template",
        "deleteTemplate": "Supprimer template",
        "chooseTemplate": "Sélectionnez un template",
        "template":{
            "headline": "Veuillez sélectionner le template à mettre à jour.",
            "info": "Conseil : N\'oubliez pas de sauvegarder vos derniers changements !",
            "cancel": "Retour",
            "update": "Mettre à jour"
        }
    },
    "sessionForm": {
        "ReservedPlaces": "Nombre de places",
        "mandatory_session_group_alert": "Vous devez sélectionner au moins une session pour cette catégorie",
        "mandatory_subscription" : "Vous devez sélectionner au moins une session",
        "mandatory_category_subscription" : "Vous devez sélectionner au moins une session dans une catégorie",
        "allowed_sessions_reached" : "Vous avez atteint le nombre maximum d'inscriptions autorisées"
    },
     "MandatoryGroupHeader": {
        "mandatory_session_group" : "Inscription obligatoire"
    },
    "sessionSelect": {
        "Full": "Complet",
        "PlacesRemaining": "Places restantes"
    },
    "shared": {
        "Actions": "Actions",
        "AreYouSurExitNotSaved": "Vos modifications ne sont pas enregistrées, êtes-vous sûr de vouloir quitter ?",
        "Cancel": "Annuler",
        "Close": "Fermer",
        "Confirm": "Confirmer",
        "Delete": "Supprimer",
        "DoYouConfirmDelete": "Confirmez-vous la suppression ?",
        "DoYouConfirmThisAction": "Confirmez-vous cette action ?",
        "DoYouReallyWantToQuit": "Êtes-vous sûr de vouloir quitter ?",
        "Edit": "Modifier",
        "ErrorHasProduced": "Une erreur s'est produite",
        "ErrorHasProducedOnEmailSave": "Une erreur s'est produite à l'enregistrement de l'email",
        "Information": "Information:",
        "InsertError": "Erreur lors de l'insertion",
        "ModificationsSaved": "Modifications enregistrées",
        "Next": "Suivant",
        "PleaseUseAllRequiredFields": "Veuillez utiliser tout les labels obligatoirs",
        "Prev": "Précédent",
        "ResponseSaved": "Merci, votre réponse à bien été enregistrée.",
        "Statistics": "Statistiques",
        "UnknownErrorContactAdmin": "Une erreur inattendue s'est produite, contactez l'organisateur de l'événement si cela se reproduit.",
        "Validate": "Valider",
        "ValidateAndSubscribe": "Valider et s'enregistrer",
        "VerifyYourPassword": "Veuillez vérifier votre mot de passe",
        "WaitLoadingEnd": "Attendez la fin du chargement s'il vous plaît",
        "YourPassword": "Votre mot de passe",
        "lang": {
            "de": "de",
            "en": "en",
            "fr": "fr"
        },
        "no": "non",
        "yes": "oui"
    },
    "subscribe": {
        "NominativeSessionsForm": {
            "GuestWithoutName": "Invité"
        }
    },
    "subscribeForm": {
        "AuthorizeStandardFieldsUpdate": "Autoriser la modification des champs standards",
        "ColumnName": "Nom de colonne",
        "CompanionCompany": "Entreprise",
        "CompanionFirstname": "Prénom",
        "CompanionLastname": "Nom",
        "Configuration": "Paramètres du formulaire",
        "Confirmed": "Inscription confirmée",
        "ContentType": "Type de texte",
        "Description": "Description",
        "DisplaySettings": "Paramètres d'affichage",
        "Error": "Une erreur inconnue s'est produite",
        "ExtendedFields": "Champs étendus",
        "LabelColor": "Couleur de texte des labels",
        "Layout": "Mise en page",
        "NoSpecialChars": "Le champ \"Nom de colonne\" ne doit pas contenir de caractères spéciaux. Seules les lettres minuscules et majuscules (a-z et A-Z), les traits d'union (-) et tiret bas (_) sont autorisés. Veuillez utiliser le champ \"Titre\" pour spécifier l'intitulé de la question.",
        "Paragraph": "Paragraphe",
        "PleaseAccept": "Veuillez accepter la condition ",
        "Problem": "Un problème a été rencontré",
        "RequiredCompanionDatas": "Informations obligatoires de l'accompagnant:",
        "SelectType": "Sélectionnez un type",
        "Sessions": "Sessions",
        "SubmitBackgroundColor": "Couleur de fond du bouton de confirmation",
        "SubmitText": "Texte du bouton de confirmation",
        "SubmitTextColor": "Couleur du texte du bouton de confirmation",
        "SubscribeForm": "Formulaire d'inscription",
        "SubscriptionSettings": "Informations d'inscription",
        "TextBloc": "Bloc de texte",
        "Title": "Titre",
        "Title1": "Titre niveau 1",
        "Title2": "Titre niveau 2",
        "Title3": "Titre niveau 3",
        "TwiceSameValueInChoices": "Veuillez entrer une valeur unique pour chacun des choix.",
        "UserInformation": "Informations utilisateur",
        "colors": {
            "black": "Noir",
            "grey": "Gris",
            "white": "Blanc"
        },
        "fieldsTypes": {
            "checkbox": "Cases à choix multiples",
            "date": "Date",
            "number": "Nombre",
            "radio": "Cases à choix unique",
            "select": "Liste à choix déroulante",
            "text": "Champs texte",
            "textarea": "Champs text multilignes"
        }
    },
    "surveyForm": {
        "AlreadyFilled": "Vous avez déjà répondu à ce sondage.",
        "CompanionForm": "Formulaire accompagnant",
        "Form": "Formulaire",
        "GuestForm": "Formulaire invité",
        "Layout": "Mise en page",
        "PleaseAuthToAnswearSurvey": "Vous devez être authentifié pour répondre au sondage. Veuillez accéder au site à partir de votre lien d'accès personnel.",
        "ShowOverview": "Aperçu",
        "SurveyForm": "Formulaire de sondage",
        "ValidationButton": "Bouton valider"
    },
    "surveyFormResult": {
        "MissingForm": "Aucun formulaire n'a été créé",
        "NoResponsesToShow": "Aucune réponse à afficher"
    },
    "tickets": {
        "DownloadPDF": "Télécharger le billet d'entrée",
        "remainingLines": " lignes restantes"
    }
}