export const deutsch = {
    "beefree": {
        "BigMap": "Große Karte",
        "LittleMap": "Kleine Karte",
        "MiddleMap": "Mittlere Karte"
    },
    "checkin": {
        "EventSelect": {
            "NoChoosenEvent": "Keinen Event ausgewählt",
            "Title": "Event "
        },
        "EventSelectModal": {
            "Title": "Wählen Sie eine Veranstaltung"
        },
        "GuestCancelLastCheckin": {
            "CancelCheckin": "Eingabe abbrechen",
            "ConfirmModalMessage": "Bestätigen Sie, dass Sie den Eintrag dieses Gastes stornieren möchten?",
            "ConfirmModalTitle": "Möchten Sie die Daten des Teilnehmers löschen?"
        },
        "GuestCheckinToast": {
            "AllreadyChecked": "Ungültige Eingabe: Dieses Ticket wurde bereits gescannt.",
            "NoPlaceReserved": "Ungültiger Eintrag: Es ist kein Platz reserviert.",
            "TicketNotValid": "Ungültiger Eintrag: Ungültiges Ticket oder kein reservierter Platz",
            "UnknownError": "Unbekannter Fehler",
            "ValidatedEntry": "Eingang validiert"
        },
        "GuestLastCheckin": {
            "PleasePlaceQrCodeInScanningArea": "Bitte scannen Sie ein QR-Code-Ticket",
            "PleaseSelectEvent": "Bitte wählen Sie eine Veranstaltung aus"
        },
        "GuestQrCodeScanner": {
            "ScanActive": "Scangerät ist aktiv",
            "ScanActivePresentValid": "Präsentieren Sie ein gültiges Ticket (QR-Code)",
            "ScanFreezed": "Scanning im Wartemodus"
        },
        "GuestQuickInfosTable": {
            "name": "Name",
            "of": "de",
            "sessions": "Sessions",
            "type": "Typ",
            "typeCompanion": "Begleitperson",
            "typeGuest": "Gast",
            "warning": {
                "allreadyCheckin": "Ungültige Eingabe, dieses Ticket wurde bereits gescannt.",
                "declined": "Der Gast hat die Einladung abgelehnt.",
                "notInvited": "Der Gast wurde nicht eingeladen.",
                "notSubscribed": "Der Gast hat sich nicht registriert."
            }
        },
        "GuestUnFreezeScanning": {
            "RestartScanning": "Fortsetzen des Scanvorganges"
        },
        "ItemChooseModal": {
            "NoItem": "Kein Element"
        },
        "NoCameraLock": {
            "NoCameraFound": "Die Scanning-Anwendung erfordert den Zugriff auf Ihre Kamera."
        },
        "SessionSelect": {
            "MainDoor": "Haupt-Eventeingang",
            "Title": "Eingang (Session)"
        },
        "SessionSelectModal": {
            "Title": "Wählen Sie eine Session "
        }
    },
    "companion": {
        "Companion": "Begleitperson",
        "formAccess": "Aufrufen des Formulars",
        "formAccessNotAvailable": "Minisite nicht vorhanden oder Seite nicht vorhanden"
    },
    "companionSelect": {
        "Choice": "Anzahl Begleiter",
        "Limit": "Maximum:"
    },
    "email": {
        "ActiveFullScreen": "Vollbildmodus aktivieren",
        "Cancel": "Abbrechen",
        "Configuration": "Parameter",
        "ConfirmMailSend": "E-Mail-Versand bestätigen",
        "ContentRecovered": "Inhalte, die automatisch aus der Mail in {lang} abgerufen werden.",
        "DesactiveFullScreen": "Vollbildmodus deaktivieren",
        "EditConfiguration": "Einstellungen ändern",
        "Email": "E-Mail",
        "EmailSended": "E-Mail mit Erfolg gesendet",
        "HideConfiguration": "Reduzieren",
        "JoinICSInvit": "ICS-Einladung anhängen",
        "JoinPDFTicket": "Ticket anhängen",
        "JoinedFiles": "Angehängte Dateien",
        "Object": "Objekt",
        "PleaseSaveEmailBefore": "Bitte überprüfen und registrieren Sie die E-Mail vor dem Versand",
        "PleaseSelectModel": "Bitte wählen Sie eine Vorlage aus",
        "ProcessEnded": "Der Prozess ist abgeschlossen.",
        "ProcessLaunched": "Der Prozess des Versendens von E-Mails wurde erfolgreich ausgelöst. Die Sendung ist im Gange. ",
        "Save": "Speichern",
        "SMTPErrorProduced": "Ein Fehler ist aufgetreten. Bitte überprüfen Sie die SMTP-Konfiguration. ",
        "SelectColumn": "Spalte wählen",
        "Send": "Senden",
        "SendMailIs": "Sende Adresse:",
        "SendWithSponsorizeEmail": "Senden mit Eventwise E-mail",
        "SenderFromColumn": "Spalte des Absendernamens",
        "SendingInProcess": "Ein E-Mail-Versand ist bereits in Bearbeitung",
        "Sessions": "Sitzungen",
        "SessionCategory": "Kategorie",
        "ShowOverview": "Vorschau anzeigen",
        "SessionsStyle": "Sitzungen",
        "TemplateNameToSave": "Name des neuen Templates:",
        "TemplateSuccessfullyDeleted": "Mail-Vorlage wurde erfolgreich entfernen",
        "TemplateSuccessfullyLoaded": "Mail-Vorlage wurde erfolgreich geladen",
        "TemplateSuccessfullySaved": "Vorlage wurde erfolgreich gespeichert",
        "TitleSessions": "Titel",
        "YouHaveSpecifiedSpecialConfig": "<strong> Informationen: </ strong> Sie eine benutzerdefinierte Mail-Setup festlegen. Die Mails werden mit dieser Konfiguration gesendet. ",
        "YouHaventSpecifiedFromColumn": "Es wurde keine Spalte mit dem Absendernamen ausgewählt, der Standard-Absendername wird verwendet.",
        "YouHaventSpecifiedSMTP": "Sie haben keine SMTP-Konfiguration eingegeben. Die E-Mails werden über den Sponsorize-Server gesendet.",
        "YouWillSendThisMailTo": "Sie sind dabei, diese E-Mail an {nbPeople} zu senden",
        "tltipInvalidMail": "Die E-Mail scheint gültig zu sein",
        "tltipToCheck": "Die Adresse wurde nicht verifiziert",
        "tltipUnCheckable": "Unverifizierbar",
        "tltipValidMail": "Die E-Mail scheint ungültig zu sein"
    },
    "event": {
        "Categories": "Kategorien",
        "GuestsStatistics": "Statistiken",
        "NoCategories": "Keine Kategorien",
        "SurveyResults": "Umfrageergebnis"
    },
    "guest": {
        "AnonymiseFinished": "Anonymisierung abgeschlossen!",
        "AreYouSureDeleteGuest": "Möchten Sie wirklich diesen Gast und seine Begleiter löschen?",
        "CannotDeleteUsedColumn": "Eine zugewiesene Spalte kann nicht gelöscht werden",
        "Catchall": "Unverifizierbar",
        "CheckCompanions": "Bitte beachten Sie die Angaben zu den Begleitpersonen.",
        "Checkin": "Eingecheckt",
        "Companion": "Begleiter",
        "CompanionCheckin": "Check-in Begleiter",
        "CompanionTicket": "Begleitticket",
        "Companions": "Begleiter",
        "Company": "Firma",
        "CompanyPlaceHolder": "Firma Name",
        "Declined": "Abgesagt",
        "DeleteAllConfirmMessage": "Alle Gäste dieser Veranstaltung werden gelöscht. <br/> Möchten Sie fortfahren?",
        "DeleteAllDoneMessage": "Alle Gäste dieser Veranstaltung wurden ordnungsgemäß gelöscht.",
        "DeleteColumnConfirmMessage": "Die Spalte \"{field}\" wird bei allen Gästen gelöscht.",
        "DeleteColumnDoneMessage": "Die Spalte \"{field}\" wurde bei allen Gästen gelöscht.",
        "DoYouConfirmColumnDelete": "Bestätigen Sie die Entfernung der Säule?",
        "Email": "Email",
        "EmailPlaceHolder": "Geben Sie Ihre E-Mail-Adresse ein",
        "ExtendedFields": "Erweiterte Felder",
        "FirsnamePlaceHolder": "Gib deinen Vornamen ein",
        "Firstname": "Vorname",
        "Guest": "Gast",
        "GuestCheckin": "Check-in Hauptgast",
        "GuestListWarnings": "Warnungen zur Gästeliste",
        "Guests": "Gäste",
        "GuestsAllEventsCheckedIn": "Total der Eingeladenen, die an einem Event teilgenommen haben",
        "GuestsAllEventsDeclined": "Total der Eingeladenen, die für einen Event abgesagt haben",
        "GuestsAllEventsImported": "Total Eingeladene für alle Events",
        "GuestsAllEventsSubscribed": "Total der Eingeladenen, die sich für einen Event angemeldet haben",
        "GuestsCheckedIn": "Registrierte Gäste",
        "GuestsDeclined": "Gäste, die die Einladung abgelehnt haben",
        "GuestsImported": "Importierte Gäste",
        "GuestsSubscribed": "Registrierte Gäste",
        "Imported": "Importiert",
        "InvitationsAllEventsSended": "Total Eingeladene, die eine Einladung erhalten haben",
        "InvitationsSended": "Einladung gesendet",
        "Invited": "Eingeladene Gäste",
        "Lastname": "Nachname",
        "LastnamePlaceHolder": "Geben Sie Ihren Namen ein",
        "NbCompanions": "Begleiter",
        "NoDataFoundInXLSFile": "In dieser Datei wurden keine Daten gefunden. Bitte verwenden Sie unbedingt das erste Blatt der zu importierenden Datei.",
        "NoGuestToView": "Keine Gäste zum Anzeigen",
        "ObligatoryFields": "Standardfelder",
        "SelectAll": "Alles auswählen",
        "SessionsCheckin": "Teilnehmende",
        "SessionsReserved": "Reservierte",
        "SetDeclinedWarningMessage": "Der Gast und seine Begleitpersonen werden von der Veranstaltung und den Sitzungen abgemeldet, die Abmeldung von Sitzungen und Begleitpersonen ist unwiderruflich.",
        "SetDeclinedWarningTitle": "Bestätigen Sie die Abmeldung des Gastes?",
        "ShowQrCode": "QRCode anzeigen",
        "Subscribed": "Angemeldet",
        "SurveyFields": "Umfrage",
        "ToRetest": "Zu überprüfen",
        "Total": "Gesamt",
        "Type": "Typ",
        "UnSelectAll": "Alle abwählen",
        "UnValid": "ungültig",
        "Valid": "gültig",
        "ValideMailAdress": "Gültige E-Mail-Adresse",
        "emailValidity": {
            "title" : "Gültigkeit der E-Mail-Adressen",
            "description": "Beschreibung",
            "icon": "Icon",
            "underAudit": {
                "description": "E-Mail-Adresse im Prozess der Validierung.",
                "name": "in Prüfung"
            },
            "unvalid": {
                "description": "Die E-Mail-Adresse existiert nicht.",
                "name": "Ungültig"
            },
            "unverifiable": {
                "description": "Nicht überprüfbare E-Mail-Adresse, der E-Mail-Server erlaubt keine Adressvalidierung. Es ist unmöglich festzustellen, ob die Adresse gültig ist oder nicht.",
                "name": "nicht überprüfbar"
            },
            "unverified": {
                "description": "Die E-Mail-Adresse wurde noch nicht verifiziert.",
                "name": "Ungeprüft"
            },
            "valid": {
                "description": "Die E-Mail-Adresse wurde überprüft und ist gültig.",
                "name": "Gültig"
            },
            "validity": "Gültigkeit"
        },
        "importCodes": {
            "badFormat": "Falsches Sprachcodeformat",
            "email": "Ungültige E-Mail-Adresse",
            "empty": "Fehlender Wert|Fehlende Werte",
            "language": "Ein Sprachcode ist ungültig",
            "length": "Wert zu lang|Werte zu lang"
        },
        "importSnippets": {
            "toLine": "zu Zeile|zu Zeilen"
        },
        "otherLinesHidden": "andere Zeile wird nicht angezeigt|andere Linien nicht gezeigt",
        "subscribeFormAccess": "Zugang zum Anmeldeformular",
        "subscribeFormAccessNotAvailable": "Kein Minisite oder Anmeldeseite."
    },
    "guestIndex": {
        "columnHeaders": {
            "ExtendedFields": "Erweiterte Felder",
            "Guest": "Gast",
            "GuestComp": "Gast und Begleitperson",
            "MainGuest": "Hauptgast",
            "SessionsCheckin": "Teilnehmende",
            "SessionsReserved": "Registrierte Teilnehmer",
            "Status": "Status",
            "SurveyFields": "Umfrage"
        }
    },
    "jobs": {
        "CustomFromColumn": "Absender-Spalte",
        "DefaultEmail": "Email Eventwise",
        "DefaultValue": "Standardwert",
        "ExecutedBy": "Angefangen von",
        "ExportXLS": "Export nach XLS",
        "Finished": "Fertig",
        "FromAddress": "von E-Mail",
        "FromName": "von Namen",
        "IgnoredMails": "getestete Adresse|ignoriert",
        "IncludedICS": "ICS Einladung inklusive",
        "Job": "Job",
        "JobVerificationAlreadyRunning": "Eine Überprüfung der E-Mail-Liste ist im Gange",
        "JobVerificationAlreadyRunningMessage": "Eine Überprüfung der E-Mail-Adressen der zuletzt importierten Gäste ist im Gange.",
        "Jobs": "Jobs",
        "NoPastJobs": "Zur Zeit keine früheren Jobs.",
        "NoPendingJobs": "Im Moment keine laufenden Jobs.",
        "PastJobs": "Früheren Jobs",
        "PendingJobs": "Aktuelle Jobs",
        "ProcessedMail": "bearbeitete E-Mails",
        "Recipients": "Empfänger|Empfänger",
        "ResendFailed": "An fehlgeschlagene Empfänger senden",
        "Resended": "Zurückgeschickt job #",
        "SendedMail": "Email envoyé|Emails envoyés",
        "SentMails": "E-Mail gesendet|E-Mails gesendet",
        "Server": "Server",
        "Started": "Gestartet",
        "TestedMails": "getestete Adresse|getestete Adressen",
        "Updated": "Aktualisiert"
    },
    "MandatoryGroupHeader": {
        "mandatory_session_group": "Anmeldung obligatorisch",
    },
    "simpleMinisite": {
        "templates": "Templates",
        "save": "Speichern",
        "cancel": "Lösen",
        "saveTemplate": "Als neue Vorlage speichern",
        "updateExistingTemplate": "Eine vorhandene Vorlage aktualisieren",
        "loadTemplate": "Vorlage laden",
        "deleteTemplate": "Vorlage löschen",
        "chooseTemplate": "Template wählen",
        "template":{
            "headline": "Bitte wählen Sie das Template zum aktualisieren.",
            "info": "Hinweis: Vergessen sie nicht, Ihre letzten Änderungen zu speichern.",
            "cancel": "Lösen",
            "update": "Aktualisieren"
        }
    },
    "sessionForm": {
        "ReservedPlaces": "Anzahl der Plätze",
        "mandatory_category_subscription" : "Sie müssen mindestens eine Sitzung in einer beliebigen Kategorie auswählen",
        "mandatory_session_group_alert" : "Sie müssen mindestens eine Sitzung für diese Kategorie auswählen.",
        "mandatory_subscription" : "Sie müssen mindestens eine Sitzung auswählen.",
        "allowed_sessions_reached" : "Sie haben die maximal zulässige Anzahl von Anmeldungen erreicht"
    },
    "sessionSelect": {
        "Full": "Voll",
        "PlacesRemaining": "Verbleibende Plätze"
    },
    "shared": {
        "Actions": "Aktionen",
        "AreYouSurExitNotSaved": "Ihre Veränderungen werden nicht gespeichert. Möchten Sie die Seite wirklich verlassen?",
        "Cancel": "Annulieren",
        "Close": "Schließen",
        "Confirm": "Bestätiger",
        "Delete": "Löschen",
        "DoYouConfirmDelete": "Bestätigen Sie die Löschung?",
        "DoYouConfirmThisAction": "Bestätigen Sie diese Aktion?",
        "DoYouReallyWantToQuit": "Möchten Sie die Seite wirklich verlassen?",
        "Edit": "Bearbeiten",
        "ErrorHasProduced": "Ein Fehler ist aufgetreten",
        "ErrorHasProducedOnEmailSave": "Bei der E-Mail-Registrierung ist ein Fehler aufgetreten.",
        "Information": "Informationen:",
        "InsertError": "Fehler beim Einfügen",
        "ModificationsSaved": "Gespeicherte Änderungen",
        "Next": "Weiter",
        "PleaseUseAllRequiredFields": "Bitte alle obligatorischen Felder ausfüllen",
        "Prev": "Zurück",
        "ResponseSaved": "Vielen Dank, Ihre Antwort wurde aufgezeichnet.",
        "Statistics": "Statistiken",
        "UnknownErrorContactAdmin": "Ein unerwarteter Fehler ist aufgetreten, kontaktieren Sie den Veranstalter, wenn er erneut auftritt.",
        "Validate": "Bestätigen",
        "ValidateAndSubscribe": "Validieren und Speichern",
        "VerifyYourPassword": "Überprüfen Sie Ihr Passwort!",
        "WaitLoadingEnd": "Warten Sie bitte auf das Ende des Ladevorgangs",
        "YourPassword": "Ihr Passwort",
        "lang": {
            "de": "de",
            "en": "en",
            "fr": "fr"
        },
        "no": "Nein",
        "yes": "Ja"
    },
    "subscribe": {
        "NominativeSessionsForm": {
            "GuestWithoutName": "Gast"
        }
    },
    "subscribeForm": {
        "AuthorizeStandardFieldsUpdate": "Änderung von Standardfeldern erlauben",
        "ColumnName": "Kolumnenname",
        "CompanionCompany": "Unternehmen",
        "CompanionFirstname": "Vorname",
        "CompanionLastname": "Nachname",
        "Configuration": "Formularparameter",
        "Confirmed": "Anmeldung bestätigt",
        "ContentType": "Text Typ",
        "Description": "Beschreibung",
        "DisplaySettings": "Anzeigeeinstellungen",
        "Error": "Ein unbekannter Fehler ist aufgetreten",
        "ExtendedFields": "Erweiterte Felder",
        "LabelColor": "Label Text Farbe",
        "Layout": "Layout",
        "NoSpecialChars": "Das Feld \"Spaltenname\" darf keine Sonderzeichen enthalten. Es sind nur Klein- und Großbuchstaben (a-z und A-Z), Bindestriche (-) und Unterstrich (_) erlaubt. Bitte verwenden Sie das Feld \"Titel\", um den Titel der Frage anzugeben.",
        "Paragraph": "Absatz",
        "PleaseAccept": "Bitte akzeptieren Sie die Bedingung ",
        "Problem": "Es wurde ein Problem festgestellt.",
        "RequiredCompanionDatas": "Obligatorische Informationen der Begleitperson:",
        "SelectType": "Wählen Sie einen Typ",
        "Sessions": "Sessions",
        "SubmitBackgroundColor": "Hintergrundfarbe der Bestätigungsschaltfläche",
        "SubmitText": "Text der Bestätigungstaste",
        "SubmitTextColor": "Textfarbe des Textes der Bestätigungstaste Text",
        "SubscribeForm": "Registrierungsformular",
        "SubscriptionSettings": "Registrierungsinformationen",
        "TextBloc": "Textblock",
        "Title": "Titel",
        "Title1": "Titelstufe 1",
        "Title2": "Titelstufe 2",
        "Title3": "Titelstufe 3",
        "TwiceSameValueInChoices": "Bitte geben Sie für jede Auswahl einen eindeutigen Wert ein.",
        "UserInformation": "Benutzerinformationen",
        "colors": {
            "black": "Schwarz",
            "grey": "Grau",
            "white": "Weiss"
        },
        "fieldsTypes": {
            "checkbox": "Multiple-Choice-Boxen",
            "date": "Datum",
            "number": "Nummer",
            "radio": "Single-Choice-Boxen",
            "select": "Dropdown-Liste",
            "text": "Textfelder",
            "textarea": "Mehrzeilige Textfelder"
        }
    },
    "surveyForm": {
        "AlreadyFilled": "Sie haben diese Umfrage bereits ausgefüllt.",
        "CompanionForm": "Begleitformular",
        "Form": "Formular",
        "GuestForm": "Gästeformular",
        "Layout": "Layout",
        "PleaseAuthToAnswearSurvey": "Sie müssen eingeloggt sein, um die Umfrage abzuschließen. Bitte besuchen Sie die Seite über Ihren persönlichen Zugangslink.",
        "ShowOverview": "Vorschau",
        "SurveyForm": "Umfrageformular",
        "ValidationButton": "Schaltfläche Validieren"
    },
    "surveyFormResult": {
        "MissingForm": "Kein Formular wurde erstellt",
        "NoResponsesToShow": "Keine Antwort angezeigt"
    },
    "tickets": {
        "DownloadPDF": "Download Ticket",
        "remainingLines": " verbleibende Zeilen"
    }
}