export default {
  /**
   * Retrieve guest (or demo) hash in minisite URL
   */
  retrieveMinisiteHash: function() {
    const url = window.location.href
    let values = url.split('?')[0].split('/')

    let hash = values.pop()
    const lang = ['fr', 'de', 'en', '']
    while(lang.indexOf(hash) > -1) {
      hash = values.pop()
    }

    return hash
  }
}