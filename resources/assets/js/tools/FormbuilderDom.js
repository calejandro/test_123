/**
 * Tools for generated formbuilder template (DOM)
 */
export default {

    checkExists: function () {
        return $('.rendered-form').length > 0
    },

    /**
     * Fill default values
     */
    setDefaultValues: function (guest, requiredFields) {
        $(document).ready(()=>{
            $('input[type=date], textarea, input[type=password], input[type=tel], input[type=color], input[type=email], input[type=text], input[type=number]').each(function( index ) {
                if(requiredFields.indexOf($(this).attr('name')) > -1){
                    $(this).val(guest[$(this).attr('name')])
                }
                else if(guest['extendedFields'] != null){
                    $(this).val(guest['extendedFields'][$(this).attr('name')])
                }
            })

            $("select option").each(function( index ) {
                if(requiredFields.indexOf($(this).parent().attr('name')) > -1) { //it's not extended
                    let defaultValue = guest[$(this).parent().attr('name')]
                    if($(this).val() === defaultValue) {
                        $(this).attr('selected', 'selected')
                    }
                }
                else if(guest['extendedFields'] != null) {
                    let defaultValue = guest['extendedFields'][$(this).parent().attr('name')]
                    if($(this).val() === defaultValue) {
                        $(this).attr('selected', 'selected')
                    }
                }
            })

            $("input[type=radio]").each(function( index ) {
                if(requiredFields.indexOf($(this).attr('name')) > -1) { //it's not extended
                    let defaultValue = guest[$(this).attr('name')]
                    if($(this).val() === defaultValue) {
                        $(this).attr('checked', 'checked')
                    }
                }
                else if(guest['extendedFields'] != null) {
                    let defaultValue = guest['extendedFields'][$(this).attr('name')]
                    if($(this).val() === defaultValue) {
                        $(this).attr('checked', 'checked')
                    }
                }
            })

            // autocomplete case
            $("input").each(function( index ) {
                if($(this).attr('autocomplete') != null) {
                    $(this).val(guest['extendedFields'][$(this).next().attr('name')])
                }
            })

            // Get all values for input checkboxes
            $("input[type=checkbox]").each(function( index ) {
                let name = $(this).attr('name').replace('[]', '')
                let value = $(this).val().replace('[]', '')

                if(requiredFields.indexOf(name) > -1) { //it's not extended
                    let defaultValues = guest[name]
                    if(defaultValues != null && defaultValues.indexOf(value) > -1) {
                        $(this).attr('checked', 'checked')
                    }
                }
                else if(guest['extendedFields'] != null) {
                    let defaultValues = guest['extendedFields'][name]
                    if(defaultValues != null && defaultValues.indexOf(value) > -1) {
                        $(this).attr('checked', 'checked')
                    }
                }
            })
        })
    },

    /**
     * Build data object with DOM displayed form
     */
    getInput: function (requiredFields) {
        let data = {
            extendedFields: {}
        }

        // Get all values for inputs
        $('input[type=date], textarea, input[type=password], input[type=tel], input[type=color], input[type=email], input[type=text], input[type=number]').each(function( index ) {
            if(requiredFields.indexOf($(this).attr('name')) > -1){
                data[$(this).attr('name')] = $(this).val()
            }
            else{
                data['extendedFields'][$(this).attr('name')] = $(this).val()
            }
        })

        $('input[type=hidden]').each(function( index ) {
            let name = $(this).attr('name')
            let value = $(this).prev('input').val()
            data['extendedFields'][name] = value
        })

        // Get all values for select elements
        $("select option:selected").each(function( index ) {
            if(requiredFields.indexOf($(this).parent().attr('name')) > -1){
                data[$(this).parent().attr('name')] = $(this).val()
            }
            else{
                data['extendedFields'][$(this).parent().attr('name')] = $(this).val()
            }
        })

        // Get all values for input radio
        $("input[type=radio]:checked").each(function( index ) {
            if(requiredFields.indexOf($(this).attr('name')) > -1){
                data[$(this).attr('name')] = $(this).val()
            }
            else{
                data['extendedFields'][$(this).attr('name')] = $(this).val()
            }
        })

        // Get all values for input checkboxes
        $("input[type=checkbox]:checked").each(function( index ) {
            let field = $(this).attr('name').replace('[]', '')

            if(requiredFields.indexOf(field) > -1) {
                if(data[field] == null){
                    data[field] = []
                }
                data[field].push($(this).val())
            }
            else {
                if(data['extendedFields'][field] == null){
                    data['extendedFields'][field] = []
                }
                data['extendedFields'][field].push($(this).val())
            }
        })

        // Avoid setting recaptcha value as extended field
        if(data['extendedFields'] != null && data['extendedFields']['g-recaptcha-response'] != null) {
            delete data['extendedFields']['g-recaptcha-response']
        }

        return data
    },

    /**
     * Check if data object contain all required fields
     * @return error object
     */
    checkDataRequiredFields: function (data) {
        let req = []
        $("input:required").each(function(index) {
            if($(this).attr('name') != null) {
                req.push($(this).attr('name').replace('[]', ''))
            }
            else { // autocomplete type, get hidden (next) field
                req.push($(this).next().attr('name').replace('[]', ''))
            }
        })

        let errors = {}
        for(let required of req) {
            if((data.extendedFields[required] == null && data[required] == null)
                || (data.extendedFields[required] == '' && data[required] == '')
                || (data.extendedFields[required] == null && data[required] == '')
                || (data.extendedFields[required] == '' && data[required] == null)){
                errors[required] = 'Le champs "' + required + '" est requis'
            }
        }
        return errors
    },

    setBaseFieldReadOnly: function () {
        $(document).ready(()=>{
            $('#firstname').attr('readonly', 'readonly')
            $('#lastname').attr('readonly', 'readonly')
            $('#email').attr('readonly', 'readonly')
        })
    },

    removeSubmitButton: function () {
        $(document).ready(()=>{
            $('.fb-button button[type=submit]').remove()
        })
    }
}