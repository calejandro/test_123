$( document ).ready(function() {
    // Start JS page resize
    var resizeFct = function() {
      if(window.innerWidth >= 992){
        var h = window.innerHeight - $('#site-header').height();
  
        $('.container-full').height(h-1); // - border
        if($('.head-bar-nav-info').length > 0){
          h = h-50; // - Head-bar-navinfo
        }
        else{
          h = h-2; // - border
        }
        $('.container-full>.row').height(h);
      }
    };
    resizeFct();
    $(window).resize( function(){
      resizeFct();
    });
});