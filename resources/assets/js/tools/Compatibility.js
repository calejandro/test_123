export default {
  /**
  * Return null if browser is compatible. Return browser full name if not
  */
  isCompatible: function (ieSupport = false) {

    const browser = this.getBrowser()
    if (browser.name === "Chrome") {
      if (parseInt(browser.version) < 43) {
        return false
      }
    }
    else if (browser.name === "Firefox") {
      if (parseInt(browser.version) < 44) {
        return false
      }
    }
    else if (browser.name === "Internet Explorer") {
      if (!ieSupport) {
        return false
      }
      else if (parseInt(browser.version) <= 7) { // Trident 7 = IE 11
        return false
      }
    }
    else if (browser.name === "AppleWebKit") {
      let browserVersion = parseFloat(browser.version.split('.')[0])
      let minVersion = 601

      if (browserVersion < minVersion) {
        return false
      }
    }

    return true
  },

  getBrowser: function () {
    let detect = navigator.userAgent
    let versionBrowser
    let browserName

    if (detect.indexOf("Chrome") != -1 && detect.indexOf("Chromium") == -1 && detect.indexOf("Edge") == -1) {
      browserName = 'Chrome'
      versionBrowser = detect.substring(detect.indexOf("Chrome"), detect.length).split(" ")[0].split("/")[1].split(".")[0]
    }
    else if (detect.indexOf("Firefox") != -1 && detect.indexOf("Seamonkey") == -1) {
      browserName = 'Firefox'
      versionBrowser = detect.substring(detect.indexOf("Firefox"), detect.length).split(" ")[0].split("/")[1].split(".")[0]
    }
    else if (detect.indexOf("Trident") != -1) {
      browserName = 'Internet Explorer'
      versionBrowser = detect.substring(detect.indexOf("Trident"), detect.length).split(" ")[0].split("/")[1].split(".")[0]
    }
    else if (detect.indexOf("Edge") != -1) {
      browserName = 'Edge'
      versionBrowser = detect.substring(detect.indexOf("Chrome"), detect.length).split(" ")[0].split("/")[1].split(".")[0]
    }
    else if (detect.indexOf("AppleWebKit") != -1) {
      browserName = 'AppleWebKit'
      versionBrowser = detect.substring(detect.indexOf("AppleWebKit"), detect.length).split(" ")[0].split("/")[1].split(".")[0]
    }

    return {
      'name': browserName,
      'version': versionBrowser
    }
  }
}
