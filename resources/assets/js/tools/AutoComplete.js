/**
 * Set of tools for autocompletion
 */
export default {
    /**
    * Chrome decided to ignore autocomplete="off", so we provide him a name he cannot identify 
    */
    randomNameAttribute: function() {
        let r = Math.random().toString(36).substring(7)
        return r + '-' + new Date().getTime()
    },
    /**
    * Chrome decided to ignore autocomplete="off", so we provide him a autocomplete field he cannot identify
    */
    randomAutocompleteAttribute: function(field) {
        return field + ' ' + this.randomNameAttribute()
    }
}