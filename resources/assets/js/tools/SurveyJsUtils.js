export default {
    /**
     * Format HTML blocs to get titles as wanted
     * @param survey: SurveyVue.Model
     * @param locale: 'fr', 'de' or 'en'
     * @param orgTemplate: raw JSON survey template in db
     */
    translateHtmlBlocks: function(survey, locale, orgTemplate) {
        const orgHtmlQuestions = orgTemplate.pages[0].elements.filter(e => {
            return e.type === "html"
        })

        
if (!Array.prototype.find) {
    Array.prototype.find = function(predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;
        
        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

        survey.getAllQuestions().forEach((q) => {
            if (q.getType() === "html") {
                const orgElement = orgHtmlQuestions.find(e => {
                    return e.name === q.name
                })
                const balise = orgElement.balise === undefined ? 'p' : orgElement.balise
    
                if(locale != null){
                    let txtVal = q.localizableStrings.html.values[locale]
                    if (txtVal) {
                        q.localizableStrings.html.values[locale] = "<" + balise + ">" + txtVal + "</" + balise + ">"
                    } else {
                        q.html = "<" + balise + ">" + q.html + "</" + balise + ">"
                    }
                }else{
                    q.html = "<" + balise + ">" + q.html + "</" + balise + ">"
                }      
            }
        })
    },
}