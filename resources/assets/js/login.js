
import CompatibilityTool from './tools/Compatibility.js'

document.addEventListener("DOMContentLoaded", function(event) {
  if (document.getElementById("login-page").length === 0){
      return
  }
  if(CompatibilityTool.isCompatible() === false) {

    let browserInfos = CompatibilityTool.getBrowser();
    if (CompatibilityTool.getBrowser().name === "Internet Explorer") {
      let content = document.getElementById('compatibility-warn-message').getAttribute('data-message-content').replace('{browser}', browserInfos.name)
      document.getElementById('compatibility-warn-message').innerHTML = '<div class="alert alert-danger"><p>'+content+'</p></div>'
    }
    else {
      let content = document.getElementById('compatibility-warn-message').getAttribute('data-message-content').replace('{browser}', browserInfos.name +" "+ browserInfos.version)
      let minVersion = document.getElementById('compatibility-warn-message').getAttribute('data-message-minVersion').replace('{minVersion}', browserInfos.minVersion)
      document.getElementById('compatibility-warn-message').innerHTML = '<div class="alert alert-danger"><p>'+content+' '+minVersion+'</p></div>'
    }

  }
})
