
import GuestsImportTable from './components/guest-import/GuestsImportTable'
import i18n from './tools/i18n'

if($('#app-guest-import').length > 0 && !global._babelPolyfill) {
    require('babel-polyfill') // Fix IE import error but break formBuilder so ... do not place in app.js
}

/**
 * Starter file
 */
const appGuestImport = new Vue({
    el: '#app-guest-import',
    components: {
        'guests-import-table': GuestsImportTable //add it on laravel template
    },
    i18n
});