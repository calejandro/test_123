import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import * as actions from './actions'
import guestIndex from './guestIndex'
import guestsSelection from './guestsSelection'

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    actions,
    modules: {
        guestIndex,
        guestsSelection
    },
    strict: false,
    plugins: debug ? [createLogger()] : []
})
