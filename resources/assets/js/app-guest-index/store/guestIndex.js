import GuestService from '../../services/GuestService'
import CompanionService from '../../services/CompanionService'
import EventService from '../../services/EventService'
import SessionService from '../../services/SessionService'

import ErrorParser from '../../tools/ErrorParser'

import i18n from '../../tools/i18n'

// initial state
const state = {
  event: null,
  guests: [], // current page guests
  surveyFields: [],
  loading: {
    guests: false,
    sessions: false,
    guestItemIds: [], // each guest with id in the list is loading
    companionItemIds: [], // each companion with id in the list is loading
    verificationJob: false,
  },
  sessions: [], // sessions to display in table
  error: null,
  successMessage: null,
  filters: {
    filters: null, // null/"guest"/"companion"
    firstname: null,
    lastname: null,
    email: null,
    company: null,
    invited: null, // true/false/null
    subscribed: null, // true/false/null
    declined: null, // true/false/null
    checkin: null, // true/false/null
    valid_email: null, // 0/1/2/"all"/null (null = ToRetest)
    language: null,
    pagination: 25,         // Number of results per page
    page: 1,                // Currently displayed results page
    extendedFields: {}, // { 'FIELD': 'VALUE' }
    surveyFields: {}, // { 'FIELD_NAME': 'VALUE' }
    mainGuest: { // related guest of companion
      firstname: null,
      lastname: null,
      email: null,
      company: null
    },
    sessions: {
      subscribed: {}, // { SESSION_ID: true/false/null }
      checkin: {}  // { SESSION_ID: true/false/null }
    }
  },
  paginationInfos: {
    total: null,
    lastPage: null,
    from: null,
    to: null
  },
  filtersTimeout: null,
  showEmailValidityInfoModal: false,
  verificationJob: null
}

// getters
const getters = {
  event: state => state.event,
  guests: state => state.guests,
  sessions: state => state.sessions,
  extendedFields: state => {
    if (state.event == null) {
      return []
    }
    return state.event.extendedFileds === null ? [] : Object.values(state.event.extendedFileds)
  },
  surveyFields: state => state.surveyFields === null ? [] : state.surveyFields,
  nominativeCompanions: state => {
    if (state.event == null) {
      return null
    }
    return state.event.nominative_companions == 1
  },
  guestsLoading: state => state.loading.guests,
  error: state => state.error,
  paginationInfos: state => state.paginationInfos,
  perPage: state => state.filters.pagination,
  currentPage: state => state.filters.page,
  filters: state => state.filters,
  loadingGuests: state => state.loading.guestItemIds,
  loadingCompanions: state => state.loading.companionItemIds,
  loadingSessions: state => state.loading.sessions,
  loading: state => { // global component loading
    return state.loading.sessions || state.event === null
  },
  showEmailValidityInfoModal: state => state.showEmailValidityInfoModal,
  verificationJobRunning: state => {
    return state.verificationJob !== null && !state.verificationJob.finished
  },
  verificationJob: state => state.verificationJob,
  successMessage: state => state.successMessage,
  errorMessage: state => {
    if (state.error === null) {
      return null
    }
    return ErrorParser.formatErrorMessage(state.error)
  }
}

// actions
const actions = {
  setEvent({ commit, dispatch }, event) {
    commit('setEvent', event)
    dispatch('getGuests') // refresh guests
    dispatch('getSessions') // refresh sessions
    dispatch('watchRunningVerificationJob') // check if job is running
  },
  showEmailValidityInfoModal({ commit }) {
    commit('setShowEmailValidityInfoModal', true)
  },
  hideEmailValidityInfoModal({ commit }) {
    commit('setShowEmailValidityInfoModal', false)
  },
  setSurveyFields({ commit }, surveyFields) {
    commit('setSurveyFields', surveyFields)
  },
  getGuestsFirstPage({ dispatch }) {
    dispatch('changeGuestsPage', 1)
  },
  getGuests({ commit, getters, state }) {
    commit('setGuestsLoading', true)
    commit('setError', null)

    GuestService.retrievePaginate(getters.event.id, state.filters).then(response => {
      console.log(response.data)
      commit('setGuestsPaginate', response.data)
      commit('setGuestsLoading', false)
    }).catch(error => {
      commit('setError', error)
      commit('setGuestsLoading', false)
    })
  },
  getSessions({ commit, getters }) {
    commit('setSessionsLoading', true)
    commit('setError', null)

    SessionService.retrieve(getters.event.id).then(response => {
      commit('setSessions', response)
      commit('setSessionsLoading', false)
    }).catch(error => {
      commit('setError', error)
      commit('setSessionsLoading', false)
    })
  },
  changeGuestsPage({ commit, dispatch }, page) {
    commit('setPage', page)
    dispatch('getGuests')
  },
  filterGuests({ commit, dispatch, state }, filters) {
    commit('setFilters', filters)
    if(state.filtersTimeout != null) {
      clearTimeout(state.filtersTimeout)
    }
    state.filtersTimeout = setTimeout(() => {
      dispatch('getGuestsFirstPage')
    }, 500)
  },
  updateGuest({ commit, dispatch, getters }, guest) {
    commit('setGuestItemLoading', guest)

    GuestService.update(guest, getters.event).then(response => {
      commit('removeGuestItemLoading', guest)
      dispatch('getGuests')
    }).catch(error => {
      commit('setError', error)
      commit('removeGuestItemLoading', guest)
    })
  },
  updateCompanion({ commit, dispatch, getters }, companion) {
    commit('setCompanionItemLoading', companion)

    CompanionService.update(companion, getters.event).then(response => {
      commit('removeCompanionItemLoading', companion)
      dispatch('getGuests')
    }).catch(error => {
      commit('setError', error)
      commit('removeCompanionItemLoading', companion)
    })
  },
  deleteGuest({ commit, dispatch, getters }, guest) {
    commit('setGuestItemLoading', guest)

    GuestService.destroy(guest, getters.event).then(response => {
      commit('removeGuestItemLoading', guest)
      dispatch('getGuests')
    }).catch(error => {
      commit('setError', error)
      commit('removeGuestItemLoading', guest)
    })
  },
  deleteCompanion({ commit, dispatch, state }, companion) {
    commit('setCompanionItemLoading', companion)

    CompanionService.destroy(companion, state.event).then(response => {
      commit('removeCompanionItemLoading', companion)
      dispatch('getGuests')
    }).catch(error => {
      commit('setError', error)
      commit('removeCompanionItemLoading', companion)
    })
  },
  watchRunningVerificationJob({ commit, dispatch, state, getters }) {
    commit('setVerificationJobLoading', true)
    // Check if an email verification job is complete
    GuestService.checkEmailVerificationJobs(state.event.id).then(response => {
      commit('setVerificationJobLoading', false)
      if (response.data != null && response.data.finished != null && !response.data.finished) {
        // A verfication job is running
        commit('setVerificationJob',  response.data)

        // Get in touch
        setTimeout(() => {
          dispatch('watchRunningVerificationJob')
        }, 5000)
      }
      else if (response.data != null && response.data.finished != null && response.data.finished) {
        // Update guests
        commit('setVerificationJob', null)
        dispatch('getGuests')
      }
      else {
        // No verfication job is running
        if (getters.verificationJobRunning) { // if a verification job was running but not now anymore
          // update guests
          commit('setVerificationJob', null)
          dispatch('getGuests')
        }
      }
    }).catch(error => {
      commit('setVerificationJobLoading', false)
      commit('setError', error)
    })
  },
  removeExtendedField({ commit, dispatch, getters }, field) {
    commit('setGuestsLoading', true)
    commit('setError', null)

    EventService.removeExtField(getters.event.id, field).then((res) => {
      commit('setGuestsLoading', false)
      commit('removeEventExtendedField', field)
      commit('showRemoveExtendedFieldSuccessMessage', i18n.t('guest.DeleteColumnDoneMessage', { field: field }))

      dispatch('getGuests')
    }).catch((error) => {
      commit('setError', error)
      commit('setGuestsLoading', false)
    })
  },
  deleteAllGuestsAndCompanions({ commit, dispatch, getters }) {
    commit('setGuestsLoading', true)
    commit('setError', null)

    GuestService.destroyAll(getters.event.id).then((res) => {
      commit('setGuestsLoading', false)
      commit('showSuccessMessage', i18n.t('guest.DeleteAllDoneMessage'))

      dispatch('getGuestsFirstPage')
    }).catch((error) => {
      commit('setError', error)
      commit('setGuestsLoading', false)
    })
  },
  anonymise({ commit, dispatch, getters }, password) {
    commit('setGuestsLoading', true)
    commit('setError', null)

    GuestService.anonymise(getters.event.id, password).then(response => {
        commit('setGuestsLoading', false)
        commit('showSuccessMessage', i18n.t('guest.AnonymiseFinished'))

        dispatch('getGuestsFirstPage')

        $("#anonymise-modal .close").click() // TODO: full driven modal, remove view interation here
    }).catch((error) => {
        if (ErrorParser.is401(error)) {
          error.response.data = { message: i18n.t('shared.VerifyYourPassword') }
        }
        commit('setError', error)
        commit('setGuestsLoading', false)
    })
  }
}

// mutations
const mutations = {
  showSuccessMessage(state, message) {
    state.successMessage = message
  },
  /**
   * Remove field on loaded event (just for display)
   */
  removeEventExtendedField(state, field) {
    let newEvent = Object.assign({}, state.event)
    let extFields = newEvent.extendedFileds == null ? [] : Object.values(newEvent.extendedFileds)
    newEvent.extendedFileds = extFields.filter(f => f !== field)
    state.event = newEvent
  },
  setGuestItemLoading(state, guest) {
    if (state.loading.guestItemIds.find(x => guest.id === x) == null) {
      state.loading.guestItemIds.push(guest.id)
    }
  },
  setCompanionItemLoading(state, companion) {
    if (state.loading.companionItemIds.find(x => companion.id === x) == null) {
      state.loading.companionItemIds.push(companion.id)
    }
  },
  removeGuestItemLoading(state, guest) {
    state.loading.guestItemIds = state.loading.guestItemIds.filter(e => e != guest.id)
  },
  removeCompanionItemLoading(state, companion) {
    state.loading.companionItemIds = state.loading.companionItemIds.filter(e => e != companion.id)
  },
  setPage(state, page) {
    state.filters.page = page
  },
  setFilters(state, filters) {
    state.filters = filters
  },
  setEvent(state, event) {
    state.event = event
  },
  setSurveyFields(state, surveyFields) {
    state.surveyFields = surveyFields
  },
  setSessions(state, sessions) {
    state.sessions = sessions
  },
  setGuestsLoading(state, loading) {
    state.loading.guests = loading
  },
  setSessionsLoading(state, loading) {
    state.loading.sessions = loading
  },
  setVerificationJobLoading(state, loading) {
    state.loading.verificationJob = loading
  },
  setError(state, error) {
    state.error = error
  },
  setGuestsPaginate(state, { 
        data,
        meta
      }) {

    state.guests = data
  
    state.filters.pagination = meta.per_page
    state.filters.page = meta.current_page

    state.paginationInfos.total = meta.total
    state.paginationInfos.from = meta.from
    state.paginationInfos.to = meta.to
    state.paginationInfos.lastPage = meta.last_page
  },
  setShowEmailValidityInfoModal(state, show) {
    state.showEmailValidityInfoModal = show
  },
  setVerificationJob(state, jobs) {
    state.verificationJob = jobs
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
