import GuestService from '../../services/GuestService'

import ErrorParser from '../../tools/ErrorParser'

// initial state
const state = {
    selectionActive: false,
    event: null,
    guestIds: [],
    companionIds: [],
    loading: {
        filterSelection: false,
    },
    error: null,
}

// getters
const getters = {
    selectionActive: state => state.selectionActive,
    event: state => state.event,
    guestIds: state => state.guestIds,
    companionIds: state => state.companionIds,
    filtersSelectionLoading: state => state.loading.filterSelection,
    errorMessage: state => {
        if (state.error === null) {
            return null
        }
        return ErrorParser.formatErrorMessage(state.error)
    }
}

// actions
const actions = {
    setEvent ({commit}, event) {
        commit('setEvent', event)
    },
    selectGuestOrCompanion ({commit}, guest) {
        if (guest.guest_id === null || guest.guest_id === undefined) {
            commit('addGuest', guest.id)
        }
        else {
            commit('addCompanion', guest.id)
        }
        
    },
    unSelectGuestOrCompanion ({commit}, guest) {
        if (guest.guest_id === null || guest.guest_id === undefined) {
            commit('removeGuest', guest.id)
        }
        else {
            commit('removeCompanion', guest.id)
        }
    },
    selectAll ({commit, getters}, filters) {
        commit('setFiltersSelectionLoading', true)
        GuestService.filterAll(getters.event, filters).then(response => {
            commit('selectGuests', response.data.guests)
            commit('selectCompanions', response.data.companions)
            commit('setFiltersSelectionLoading', false)
        }).catch(error => {
            commit('setError', error)
            commit('setFiltersSelectionLoading', false)
        })
    },
    unSelectAll ({commit, getters}, filters) {
        commit('setFiltersSelectionLoading', true)
        GuestService.filterAll(getters.event, filters).then(response => {
            commit('unSelectGuests', response.data.guests)
            commit('unSelectCompanions', response.data.companions)
            commit('setFiltersSelectionLoading', false)
        }).catch(error => {
            commit('setError', error)
            commit('setFiltersSelectionLoading', false)
        })
    },
}

// mutations
const mutations = {
    activateSelection (state) {
        state.selectionActive = true
    },
    setEvent (state, event) {
        state.event = event
    },
    addGuest (state, guestId) {
        if (state.guestIds.indexOf(guestId) < 0) {
            state.guestIds.push(guestId)
        }
    },
    removeGuest (state, guestId) {
        state.guestIds = state.guestIds.filter(id => id !== guestId)
    },
    selectGuests (state, guestIds) {
        state.guestIds = [...new Set(state.guestIds.concat(guestIds))]
    },
    unSelectGuests (state, guestIds) {
        state.guestIds = state.guestIds.filter(id => guestIds.indexOf(id) < 0)
    },
    addCompanion (state, companionId) {
        if (state.companionIds.indexOf(companionId) < 0) {
            state.companionIds.push(companionId)
        }
    },
    selectCompanions (state, companionIds) {
        state.companionIds = [...new Set(state.companionIds.concat(companionIds))]
    },
    unSelectCompanions (state, companionIds) {
        state.companionIds = state.companionIds.filter(id => companionIds.indexOf(id) < 0)
    },
    removeCompanion (state, companionId) {
        state.companionIds = state.companionIds.filter(id => id !== companionId)
    },
    setFiltersSelectionLoading (state, loading) {
        state.loading.filterSelection = loading
    },
    setError (state, error) {
        state.error = error
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
