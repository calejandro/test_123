import Vuex from 'vuex'

/**
 * Checkin app starter
 */
require('../bootstrap')

require('../tools/templateResizer')

// Init Vue framework
window.Vue = require('vue')
window.VueI18n = require('vue-i18n')
window.Vuex = require('vuex')

window.Vue.use(window.VueI18n)
window.Vue.use(Vuex)

// Start sentry listening
require('../sentry.js')

// The app
require('./checkin.js')