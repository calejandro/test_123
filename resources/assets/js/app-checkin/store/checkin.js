import EventService from '../../services/EventService'
import GuestService from '../../services/GuestService'
import CompanionService from '../../services/CompanionService'
import SessionService from '../../services/SessionService'

import ErrorParser from '../../tools/ErrorParser'

// initial state
const state = {
    user: null,
    event: null, // selected event
    events: [], // all events
    session: null, // selected session
    sessions: [], // all sessions

    lastCheckin: null,
    lastCheckinType: null, // companion or guest
    lastHashScanned: null,
    checkinStatus: {
        error: null,
        success: null
    },

    showSelectEvent: false,
    showSelectSession: false,

    loading: {
        events: false,
        sessions: false,
        checkinGuest: false
    },

    freezeScan: false,

    error: null,

    cameraError: null,

    scanInited: false,
    unValidQrCode: false
}

// getters
const getters = {
    event: state => state.event,
    session: state => state.session,
    adminMode: state => state.user != null && !state.user.controller,
    isEventSelected: state => state.event != null,
    ready: state => state.user != null,
    readyToScan: state => state.user != null && (state.event != null || state.event != null),
    showSelectEvent: state => state.showSelectEvent,
    showSelectSession: state => state.showSelectSession,

    events: state => state.events,
    isEventsLoading: state => state.loading.events,
    sessions: state => state.sessions,
    isSessionsLoading: state => state.loading.sessions,
    isCheckinGuest: state => state.loading.checkinGuest,

    checkinStatusSuccess: state => state.checkinStatus.success,
    checkinStatusError: state => state.checkinStatus.error,

    lastHashScanned: state => state.lastHashScanned,

    lastCheckin: state => state.lastCheckin,
    lastCheckinType: state => state.lastCheckinType,

    freezeScan: state => state.freezeScan,

    hasNoCamera: state => state.cameraError != null,

    scanInited: state => state.scanInited,

    unValidQrCode: state => state.unValidQrCode
}

// actions
const actions = {
    initScan ({ commit }) {
        commit('setScanInited', true)
    },

    detectUnValidQrCode ({ commit }) {
        commit('setUnValidQrCode', true)
    },

    cameraError ({ commit }, error) {
        commit('setCameraError', error)
    },

    // User
    selectUser ({ commit }, user) {
        commit('setUser', user)
    },

    unfreezeScan ({ commit }) {
        commit('setLastCheckin', { data: null, type: null })
        commit('setFreezeScan', false)
    },

    // Event Selection
    getEvents ({ commit }) {
        commit('setLoading', {
            feature: 'events',
            loading: true
        })

        EventService.retrieveAll().then(res => {
            commit('setEvents', res.data)
            commit('setLoading', {
                feature: 'events',
                loading: false
            })
        }).catch((error) => {
            commit('setError', error)
            commit('setLoading', {
                feature: 'events',
                loading: false
            })
        })
    },
    selectEvent ({ commit, dispatch }, event) {
        commit('setEvent', event)
        dispatch('hideSelectEvents')
        dispatch('getSessions') // update sessions
        commit('setSession', null) // reset to main door
    },
    showSelectEvents ({ commit }) {
        commit('setShowSelectEvent', true)
        commit('setFreezeScan', true)
    },
    hideSelectEvents ({ commit }) {
        commit('setShowSelectEvent', false)
        commit('setFreezeScan', false)
    },

    // Session selection
    getSessions ({ getters, commit }) {
        commit('setLoading', {
            feature: 'sessions',
            loading: true
        })
        // Event must be selected before
        SessionService.retrieve(getters.event.id).then(sessions => {
            commit('setSessions', sessions)
            commit('setLoading', {
                feature: 'sessions',
                loading: false
            })
        }).catch((error) => {
            commit('setError', error)
            commit('setLoading', {
                feature: 'sessions',
                loading: false
            })
        })
    },
    selectSession ({ commit, dispatch }, session) {
        commit('setSession', session)
        dispatch('hideSelectSessions')
    },
    showSelectSessions ({ commit }) {
        commit('setShowSelectSession', true)
        commit('setFreezeScan', true)
    },
    hideSelectSessions ({ commit }) {
        commit('setShowSelectSession', false)
        commit('setFreezeScan', false)
    },

    // Checkin
    checkinGuest ({ commit, dispatch, getters }, hash) {
        commit('setUnValidQrCode', false)
        commit('setLoading', {
            feature: 'checkinGuest',
            loading: true
        })

        commit('setLastHashScanned', hash)

        let successCallback = (res) => {
            commit('setCheckinSuccess', res.data)
            commit('setLastCheckin', res.data)
            commit('setLoading', {
                feature: 'checkinGuest',
                loading: false
            })
        }
        let errorCallback = (error) => {
            if (ErrorParser.is409(error)) {
                commit('setCheckinErrorWithGuest', { error: error, allreadyCheckin: true})
                commit('setFreezeScan', true)
            }
            else if (ErrorParser.is404(error) && error.response.data != null && error.response.data.data != null) {
                commit('setCheckinErrorWithGuest', { error: error, allreadyCheckin: false})
                commit('setFreezeScan', true)
            }
            else {
                commit('setCheckinError', error)
                commit('setFreezeScan', true)
            }

            commit('setLoading', {
                feature: 'checkinGuest',
                loading: false
            })
        }

        if (getters.session === null) {
            // Main door checkin
            EventService.checkin(getters.event.id, hash)
                .then(successCallback)
                .catch(errorCallback)
        }
        else {
            // Session checkin
            SessionService.checkin(getters.event.id, getters.session.id, hash)
                .then(successCallback)
                .catch(errorCallback)
        }
    },
    cancelCheckinGuest ({ commit, dispatch, getters }, guest) {
        commit('setLoading', {
            feature: 'checkinGuest',
            loading: true
        })

        commit('setFreezeScan', true)

        let successCallback = (res) => {
            commit('setLastCheckin', { data: null, type: null })
            commit('setLoading', {
                feature: 'checkinGuest',
                loading: false
            })
        }
        let errorCallback = (error) => {
            commit('setCheckinError', error)
            commit('setLoading', {
                feature: 'checkinGuest',
                loading: false
            })
        }

        if (getters.session === null) {
            // Main door checkin
            if(getters.lastCheckinType === 'guest') {
                GuestService.unCheckin(getters.event.id, getters.lastCheckin.id)
                    .then(successCallback)
                    .catch(errorCallback)
            }
            else if(getters.lastCheckinType === 'companion') {
                CompanionService.unCheckin(getters.event.id, getters.lastCheckin.id)
                    .then(successCallback)
                    .catch(errorCallback)
            }
        }
        else {
            // Session uncheckin
            if(getters.lastCheckinType === 'guest') {
                GuestService.unCheckinSession(getters.event.id, getters.session.id, getters.lastCheckin.id)
                    .then(successCallback)
                    .catch(errorCallback)
            }
            else if(getters.lastCheckinType === 'companion') {
                CompanionService.unCheckinSession(getters.event.id, getters.session.id, getters.lastCheckin.id)
                    .then(successCallback)
                    .catch(errorCallback)
            }
        }

    },
    
}

// mutations
const mutations = {
    setScanInited (state, status) {
        state.scanInited = status
    },
    setUnValidQrCode (state, status) {
        state.unValidQrCode = status
    },
    setCameraError (state, error) {
        state.cameraError = error
    },
    setUser (state, user) {
        state.user = user
    },
    setEvent (state, event) {
        state.event = event
    },
    setEvents (state, events) {
        state.events = events
    },
    setSession (state, session) {
        state.session = session
    },
    setSessions (state, sessions) {
        state.sessions = sessions
    },
    setShowSelectEvent (state, show) {
        state.showSelectEvent = show
    },
    setShowSelectSession (state, show) {
        state.showSelectSession = show
    },
    setLoading (state, { feature, loading }) {
        state.loading[feature] = loading
    },
    setError (state, error) {
        state.error = error
    },
    setFreezeScan (state, freeze) {
        state.freezeScan = freeze
    },
    setLastHashScanned (state, hash) {
        state.lastHashScanned = hash
    },
    setLastCheckin (state, { data, type }) {
        state.lastCheckin = data
        state.lastCheckinType = type
    },
    setCheckinSuccess (state, { data, type }) {
        state.checkinStatus.success = data
        state.checkinStatus.error = null
    },
    setCheckinError (state, error) {
        state.lastCheckin = null
        state.lastCheckinType = null
        state.checkinStatus.success = null
        state.checkinStatus.error = error
    },
    setCheckinErrorWithGuest (state, {error, allreadyCheckin}) {
        state.lastCheckin = error.response.data.data
        state.lastCheckin.doorAllreadyCheckin = allreadyCheckin
        state.lastCheckinType = error.response.data.type
        state.checkinStatus.error = error
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
