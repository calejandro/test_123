import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import * as actions from './actions'
import checkin from './checkin'

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    actions,
    modules: {
        checkin
    },
    strict: false,
    plugins: debug ? [createLogger()] : []
})
