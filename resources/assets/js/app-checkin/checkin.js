
import App from './App.vue'
import i18n from '../tools/i18n'

import store from './store'

/**
 * Starter file
 */
const appCheckin = new Vue({
    el: '#app-checkin',
    store: store,
    components: {
        'checkin-main': App
    },
    i18n
});