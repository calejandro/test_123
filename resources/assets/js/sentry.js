/**
 * Sentry initialisation
 */

 
/**
 * Raven JS version
 */

import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

import ServerConfig from './config/server'

window.Raven = Raven
    .config(ServerConfig.sentryDsn)
    .addPlugin(RavenVue, window.Vue)
    .install();


//Raven.captureMessage('Something happened', {
//    level: 'info' // one of 'info', 'warning', or 'error'
//});


/**
 *  @sentry/browser version (latest)
 */

/*
import * as Sentry from '@sentry/browser'

Sentry.init({
  dsn: 'https://be46ad6db4ef4247bf46ccda14b632ae@sentry.io/1353538',
  integrations: [new Sentry.Integrations.Vue()] // window.Vue needs to be setted
})
*/