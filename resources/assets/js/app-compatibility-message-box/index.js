window.$ = window.jQuery = require('jquery')

import CompatibilityTool from '../tools/Compatibility.js'

function removeCompatibilityBox() {
    if (CompatibilityTool.isCompatible(true)) {
        $('#compatibility-box').remove()
    }
}

removeCompatibilityBox()

$(document).ready(()=>{
    removeCompatibilityBox()
})
