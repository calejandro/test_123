import Vuex from 'vuex'
import Polyfills from '../tools/Polyfills'

/**
 * Subscribe app starter
 */
require('../bootstrap')

Polyfills.applyAll()

// Init Vue framework
window.Vue = require('vue')
window.VueI18n = require('vue-i18n')
window.Vuex = require('vuex')

window.Vue.use(window.VueI18n)
window.Vue.use(Vuex)

// Start sentry listening
require('../sentry.js')

// The app
require('./subscribe-form.js')
require('./companion-subscribe-form.js')

$( document ).ready(function() {
    // Init Formbuilder plugin
    require('../../../../node_modules/formBuilder/dist/form-render.min.js'); // workAround for formRender import
    require('../../../../node_modules/formBuilder/dist/form-builder.min.js');
});