const state = {
  mandatorySessions: false,
  maxSubscriptions: null,
  selectedSessionPerGroup: [],
  atLeastOneSelectedPerMandatoryGroup: false,
  mandatory_groups: [],
  groups: [],
  selectedSessionObjects: {},
  companions_sessions: {
    guest: {sessions: []}
  },
  sessions: [],
  subscribed_by_guest: {},
  maximumCategoriesSubscriptions: null
};

const getters = {
  mandatorySessions: state => state.mandatorySessions == 1,
  selectedCounter: state =>
    Object.keys(state.selectedSessionObjects).reduce((acc, key, index) => {
      return acc + state.selectedSessionObjects[key].counter;
    }, 0),
  maxSubscriptions: state => state.maxSubscriptions,
  maxSubscriptionsReached: (state, getter) =>
    getter.selectedCounter >= state.maxSubscriptions,
  selectedSessionPerGroup: state => {
    let result = Object.keys(state.selectedSessionObjects).reduce(
      (acc, key, index) => {
        if (!state.selectedSessionObjects[key].sessionGroup) {
          return acc;
        }
        let sessionGroup = state.selectedSessionObjects[key].sessionGroup;
        if (!acc[sessionGroup.id]) acc[sessionGroup.id] = 0;
        acc[sessionGroup.id] += state.selectedSessionObjects[key].counter;

        return acc;
      },
      []
    );

    return result;
  },
  atLeastOneSelectedPerMandatoryGroup: (state, getter) => {
    if (state.mandatory_groups.length === 0) return true;

    return state.mandatory_groups
      .filter(group => {
        return group != null;
      })
      .every(group => getter.selectedSessionPerGroup[group.id] > 0);
  },
  subscribedByGuest: state => state.subscribed_by_guest,
  selectedSessionObjects: state => state.selectedSessionObjects,
  groups: state => state.groups,
  eventSessions: state => state.sessions,
  sessionsIds: state => {
    let ids = {};

    state.sessions.forEach(session => {
      if(state.selectedSessionObjects[session.id])  {
        ids[session.id] = state.selectedSessionObjects[session.id].counter;
      }else{
        ids[session.id] = 0;
      }

    });

    return ids;
  },
  nomitativeSessionsSelected: state => {
    return Object.keys(state.companions_sessions).reduce((acc, key, index) => {
      if (state.companions_sessions[key].sessions)
        return acc + state.companions_sessions[key].sessions.length;

      return acc;
    }, 0);
  },
  companionsSessions: state => {
    return state.companions_sessions;
  },
  nominativeSessionsCounterByGroup: state => {
    let acc = {};

    Object.keys(state.companions_sessions).forEach(key => {
      let tmp = state.companions_sessions[key].sessions
        .filter(session => session.sessionGroup)
        .map(session => {
          return session.sessionGroup.id;
        });

      for (let index = 0; index < tmp.length; index++) {
        const groupId = tmp[index];

        if (!acc[groupId]) acc[groupId] = 0;
        acc[groupId] += 1;
      }
    });

    return acc;
  },
  nominativeMandatoryGroupsAreSelected: (state, getter) => {
    return state.mandatory_groups.every(group => {
      return getter.nominativeSessionsCounterByGroup[group.id] > 0;
    });
  },
  atLeastOneSessionInGroupSelected: (state, getter) => {
    return state.groups.some(group => {
      return getter.selectedSessionPerGroup[group.id] > 0;
    });
  },
  groupsSelected: (state, getter) => {
    let acc = 0;

    for(let i = 0; i < getter.groups.length; i++){
      const groupId = getter.groups[i].id;
      if (getter.selectedSessionPerGroup[groupId] > 0){
        acc += 1;
      }
    }

    return acc;
  },
  atLeastOneNominativeSessionInGroupSelected: (state, getter) => {
    return state.groups.some(group => {
      return getter.nominativeSessionsCounterByGroup[group.id] > 0;
    });
  },
  nominativeGroupsSelected: (state, getter) => {
    return state.groups.reduce((acc, group) => {
      if(getter.nominativeSessionsCounterByGroup[group.id] > 0) acc++;
      return acc;
    }, 0);
  },
  nominativeCounterBySessions: state => {
    return Object.keys(state.companions_sessions).reduce((acc, key) => {
      state.companions_sessions[key].sessions.forEach(session => {
        if (!acc[session.id]) acc[session.id] = 0;
        acc[session.id] += 1;
      });

      return acc;
    }, {});
  },
  nominativeSessionIds: state => { //sessions subscription for each companions and guest
    let sessions = {
      guest: (state.companions_sessions["guest"].sessions || []).map(
        session => session.id
      ),
      companions: 
        Object.keys(state.companions_sessions)
          .filter(key => key != "guest")
          .reduce((acc, key) => {
            acc[key] = state.companions_sessions[key].sessions.map(session => session.id);
            return acc;
          }, {}) || {}      
    };

    return sessions;
  },
  maximumCategoriesSubscriptions: (state) => {
    return state.maximumCategoriesSubscriptions;
  },
};

const actions = {};

const mutations = {
  addSelectedSessions: (state, payload) => {
    let counter;
    if (
      state.selectedSessionObjects[payload.id] &&
      state.selectedSessionObjects[payload.id].counter
    ) {
      counter = state.selectedSessionObjects[payload.id].counter;
    } else {
      counter = 0;
    }

    state.selectedSessionObjects = {
      ...state.selectedSessionObjects,
      ...{ [payload.id]: { ...payload, ...{ counter: counter + 1 } } }
    };
  },
  removeSelectedSessions: (state, payload) => {
    let counter;
    if (
      state.selectedSessionObjects[payload.id] &&
      state.selectedSessionObjects[payload.id].counter
    ) {
      counter = state.selectedSessionObjects[payload.id].counter;
      if (counter > 1) {
        state.selectedSessionObjects = {
          ...state.selectedSessionObjects,
          ...{ [payload.id]: { ...payload, ...{ counter: counter - 1 } } }
        };

        return;
      }
    }

    let key = payload.id.toString();
    const { [key]: value, ...newState } = state.selectedSessionObjects;

    state.selectedSessionObjects = newState;
  },
  setSelectedSessions: (state, payload) => {
    state.selectedSessionObjects = {
      ...state.selectedSessionObjects,
      ...{ [payload.id]: payload }
    };

    state.subscribed_by_guest = {
      ...state.subscribed_by_guest,
      ...{ [payload.id]: payload.counter }
    };
  },
  addGroup: (state, group) => {
    state.groups = [...state.groups, { id: group.id, counter: 0 }];
  },
  addSession: (state, session) => {
    state.sessions = [...state.sessions, session];
  },
  addEventSessions: (state, sessions) => {
    state.sessions = sessions;
  },
  resetSelected: (state, companions) => {
    state.selectedSessionObjects = {};
  },
  resetCompanionsSelected: (state, { companions }) => {
    state.companions_sessions = Object.assign({}, state.companions_sessions, {
      [companions]: {
        sessions: []
      }
    });
  },
  addSessionsWithNominativeCompanions: (state, payload) => {
    let { session, id } = payload;

    let companion = state.companions_sessions[id];
    let sessions = companion ? companion.sessions : [];

    state.companions_sessions = {
      ...state.companions_sessions,
      ...{
        [id]: {
          sessions: [...sessions, session]
        }
      }
    };
  },
  removeSessionsWithNominativeCompanions: (state, payload) => {
    let { session, id } = payload;

    let guestObj = state.companions_sessions[id];
    let sessions = guestObj ? guestObj.sessions : [];

    state.companions_sessions = Object.assign({}, state.companions_sessions, {
      [id]: {
        sessions: sessions.filter(s => {
          return s.id != session.id;
        })
      }
    });
  },
  addMandatoryGroup: (state, payload) => {
    state.mandatory_groups = [...state.mandatory_groups, payload];
  },
  setMandatorySessions(state, mandatory) {
    state.mandatorySessions = mandatory;
  },
  setMaxSubscriptions(state, amount) {
    state.maxSubscriptions = amount;
  },
  setSelectedSessionGroup(state, { groupId, value }) {
    state.selectedSessionPerGroup[groupId] = value;
  },
  setMaximumCategoriesSubscriptions(state, value) {
    console.log(value);
    state.maximumCategoriesSubscriptions = value;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
