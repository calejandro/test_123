import Vuex from "vuex";
import createLogger from "vuex/dist/logger";

// import * as actions from "./actions";
import sessions from "./sessions";

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
 // actions,
  modules: {
    sessions
  },
  strict: false,
  plugins: debug ? [createLogger()] : []
});
