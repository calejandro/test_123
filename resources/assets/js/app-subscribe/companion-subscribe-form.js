import i18n from '../tools/i18n'
import CompanionSubscribeForm from './AppCompanionSubscribeForm'
import store from "./store";

/**
 * Starter file
 */
const appCompanionSubscribeForm = new Vue({
  el: "#app-companion-subscribe-form-view",
  components: {
    "companion-subscribe-form": CompanionSubscribeForm // When using survey js (new advanced editor)
  },
  i18n,
  store: store
});
