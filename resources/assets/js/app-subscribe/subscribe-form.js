import i18n from '../tools/i18n'
import SubscribeForm from './AppSubscribeFormFormBuilder'
import SubscribeFormSurveyJs from './AppSubscribeFormSurveyJs'
import store from './store'

/**
 * Starter file
 */
const appSubscribeForm = new Vue({
    el: '#app-subscribe-form-view',
    components: {
        'subscribe-form': SubscribeForm,                 // When using BeeFree (old form editor)
        'advanced-subscribe-form': SubscribeFormSurveyJs // When using survey js (new advanced editor)
    },
    i18n,
    store: store
});
