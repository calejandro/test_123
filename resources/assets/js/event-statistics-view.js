
import EventStatistics from './components/event-statistics/EventStatistics'
import i18n from './tools/i18n'

/**
 * Starter file
 */
const appEventStats = new Vue({
    el: '#app-event-stats',
    components: {
        'event-statistics': EventStatistics // add it in the laravel template
    },
    i18n
});