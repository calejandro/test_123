import i18n from './tools/i18n'

function countDescriptionLines(value) {

  if (value === undefined) {
    return null;
  }

  // 1 line ~61 chars on ticket
  // max 9 lines on tickets
  let aLines = value.split("\n");

  let totalLine = 0;
  for(let line of aLines) {
    let nbLine = Math.ceil(line.length / 60);
    totalLine = totalLine + (nbLine > 0 ? nbLine : 1)
  }

  return totalLine;
}

function setDescriptionProgress(lines, lang) {
  const nbLinesMax = 11;
  let progress = lines / nbLinesMax * 100;
  $("#description-progress-bar-" + lang)
      .css("width", progress + "%")
      .attr("aria-valuenow", progress)
      .text(nbLinesMax-lines + i18n.getLocalTranslations().tickets.remainingLines);

  if (lines >= nbLinesMax-2 && lines <= nbLinesMax) {
    $("#description-progress-bar-" + lang).removeClass("progress-bar-danger");
    $("#description-progress-bar-" + lang).addClass("progress-bar-warning");
  }
  else if (lines > nbLinesMax) {
    $("#description-progress-bar-" + lang).removeClass("progress-bar-warning");
    $("#description-progress-bar-" + lang).addClass("progress-bar-danger");
  }
  else {
    $("#description-progress-bar-" + lang).removeClass("progress-bar-danger");
    $("#description-progress-bar-" + lang).removeClass("progress-bar-warning");
  }
}

$( document ).ready(function() {

    $("#unkeep_ticket_image_1").on("click", function(){
      $("#hidden_image_1").val(0);
      $("#existing_image_1_bloc").hide();
      $("#new_image_1_bloc").css('display', 'block');
    });

    $("#unkeep_ticket_image_2").on("click", function(){
      $("#hidden_image_2").val(0);
      $("#existing_image_2_bloc").hide();
      $("#new_image_2_bloc").css('display', 'block');
    });

    setDescriptionProgress(countDescriptionLines($("#ticket-description-fr").val()), 'fr');
    setDescriptionProgress(countDescriptionLines($("#ticket-description-en").val()), 'en');
    setDescriptionProgress(countDescriptionLines($("#ticket-description-de").val()), 'de');

    $("#ticket-description-fr").on("change", function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'fr');
    });
    $("#ticket-description-fr").keyup(function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'fr');
    });

    $("#ticket-description-en").on("change", function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'en');
    });
    $("#ticket-description-en").keyup(function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'en');
    });

    $("#ticket-description-de").on("change", function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'fde');
    });
    $("#ticket-description-de").keyup(function() {
      setDescriptionProgress(countDescriptionLines($(this).val()), 'de');
    });

    
});
