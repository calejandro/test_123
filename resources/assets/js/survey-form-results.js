import SurveyFormResults from './components/survey-form-results/SurveyFormResults'
import i18n from './tools/i18n'

/**
 * Starter file
 */
const appSurveyFormResults = new Vue({
    el: '#app-survey-form-results',
    components: {
        'survey-form-results': SurveyFormResults,
    },
    i18n
});