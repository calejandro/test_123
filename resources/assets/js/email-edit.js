
import EmailEdit from './components/email-edit/EmailEdit'
import i18n from './tools/i18n'

import store from './app-guest-index/store'

/**
 * Starter file for email edit app
 */
new Vue({
    el: '#app-mail-edit',
    store: store,
    data () {
        return {
            editMail: true
        }
    },
    components: {
        'email-edit': EmailEdit
    },
    i18n
})