/**
 * Commuon code for Vanilla JS pages
 */

export default {
    displayError: function (errorMsg) {
        $('#success-box').css('display', 'none');
        $('#error-box .adjusted-message').html(errorMsg);
        $('#error-box').css('display', 'block');

        setTimeout(()=>{
            $('#error-box').css('display', 'none');
        }, 8000);
    },

    displaySucess: function (successMsg) {
        $('#error-box').css('display', 'none');
        $('#success-box .adjusted-message').html(successMsg);
        $('#success-box').css('display', 'block');

        setTimeout(()=>{
            $('#success-box').css('display', 'none');
        }, 6000);
    },

    stringToSlug: function (str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
    
        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
    
        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes
    
        return str;
    }
}