import SurveyFormService from '../services/SurveyFormService'

import Tools from '../standalone-pages-tools'
import i18n from '../tools/i18n'

function generateInput(name, value, question, type) {
    return {
        name: name,
        value: value,
        label: question,
        type: type
    }
}

/**
 * Support for survey form display with formbuilder
 */
$( document ).ready(function() {
    if ($('#survey-form-page').parent().length === 0) {
        return;
    }

    // Lock survey form if user isn't auth
    if ($('#json-guest-infos').length <= 0) {
        $('#survey-form-page input').attr("disabled", "disabled");
        $('#survey-form-page button[type=submit]').attr("disabled", "disabled");
        $('#survey-form-page').before(`<div class="alert alert-info"><div class="pull-left alert-icon"><i class="fa fa-info-circle fa-fw fa-2x"></i></div><div class="adjusted-message">${i18n.getLocalTranslations().surveyForm.PleaseAuthToAnswearSurvey}</div></div>`);
        return;
    }

    let guest = JSON.parse($('#json-guest-infos').html())

    $('button[type=submit]').click(function(){
        let data = {
            surveyFields: {}
        }

        // Get all input type text/number values
        $('textarea, input[type=date], input[type=password], input[type=tel], input[type=color], input[type=email], input[type=text], input[type=number]').each(function( index ) {
            let name = $(this).attr('name')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, $(this).val(), question, $(this).attr('type'))
        })

        $('input[type=hidden]').each(function( index ) {
            let name = $(this).attr('name')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, $(this).prev('input').val(), question, 'autocomplete')
        })

        // Get all values for select elements
        $("select option:selected").each(function( index ) {
            let name = $(this).parent().attr('name')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, $(this).val(), question, 'select')
        })

        // default values
        $("input[type=checkbox]").each(function( index ) {
            let name = $(this).attr('name').replace('[]', '')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, '', question, $(this).attr('type'))
        })

        $("input[type=radio]").each(function( index ) {
            let name = $(this).attr('name')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, '', question, $(this).attr('type'))
        })

        // Get all values for input radio
        $("input[type=radio]:checked").each(function( index ) {
            let name = $(this).attr('name')
            let question = $('label[for=' + name + ']').html()
            data['surveyFields'][name] = generateInput(name, $(this).val(), question, $(this).attr('type'))
        })

        // Get all values for input checkboxes
        $("input[type=checkbox]:checked").each(function( index ) {
            let name = $(this).attr('name').replace('[]', '')
            let question = $('label[for=' + name + ']').html()
            if(data['surveyFields'][name]['value'] == '') {
                data['surveyFields'][name] = generateInput(name, [$(this).val()], question, $(this).attr('type'))
            }
            else{
                data['surveyFields'][name].value.push($(this).val())
            }
        })

        // validate required fields
        let requiredFields = []
        $("input:required").each(function( index ) {
            requiredFields.push($(this).attr('name').replace('[]', ''))
        })

        let errors = {}
        for(let required of requiredFields) {
            if(data.surveyFields[required] == null || data.surveyFields[required] == ''){
                errors[required] = 'Le champs "' + required + '" est requis'
            }
        }

        if(Object.keys(errors).length > 0){
            let errorsHtml = "<ul>"
            
            for(let err in errors) {
                errorsHtml += "<li>" + errors[err] + "</li>"
            }

            errorsHtml += "</ul>"
            Tools.displayError(errorsHtml)
        }
        else {
            if ($('#json-is-demo').html() != 1) {
                SurveyFormService.reply(guest.event_id, guest.id, data).then((response) => {
                    Tools.displaySucess(i18n.getLocalTranslations().shared.ResponseSaved);
                }).catch((error) => {
                    if(error.response.status === 422) {
                        let errors = "<ul>"
        
                        for(let err in error.response.data) {
                            errors += "<li>" + error.response.data[err] + "</li>"
                        }
        
                        errors += "</ul>"
                        Tools.displayError(errors)
                    }
                    else{
                        Tools.displayError(error.response.data.error)
                    }
                })
            }
        }
    })
});

import AppSurveyFormSurveyJs from './AppSurveyFormSurveyJs'

/**
 * Starter file
 */
const appSurveyForm = new Vue({
    el: '#app-survey-form-view',
    components: {
        'advanced-survey-form': AppSurveyFormSurveyJs
    },
    i18n
});
