import Polyfills from '../tools/Polyfills'

/**
 * Survey app starter
 */
require('../bootstrap')

Polyfills.applyAll()

// Init Vue framework
window.Vue = require('vue')
window.VueI18n = require('vue-i18n')

window.Vue.use(window.VueI18n)

// Start sentry listening
require('../sentry.js')

// The app
require('./survey-form-view.js')

$( document ).ready(function() {
    // Init Formbuilder plugin
    require('../../../../node_modules/formBuilder/dist/form-render.min.js'); // workAround for formRender import
    require('../../../../node_modules/formBuilder/dist/form-builder.min.js');
});