import i18n from './tools/i18n'

import AppGuestIndex from './app-guest-index/AppGuestIndex'
import store from './app-guest-index/store'

/**
 * Starter file for Guest index app
 */
new Vue({
    el: '#app-guest-index',
    store: store,
    components: {
        'app-guest-index': AppGuestIndex
    },
    i18n
})
