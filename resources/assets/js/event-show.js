
import EventStatsShow from './components/event-show/EventStatsShow'
import i18n from './tools/i18n'

/**
 * Starter file
 */
const appEventShow = new Vue({
    el: '#app-event-show',
    components: {
        'event-stats-show': EventStatsShow //add it on laravel template
    },
    i18n
});