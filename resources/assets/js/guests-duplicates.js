
import GuestDuplicate from './components/guest-import/GuestDuplicate'
import i18n from './tools/i18n'

/**
 * Starter file
 */
const appGuestDuplicates = new Vue({
    el: '#app-guest-duplicates',
    components: {
        'guest-duplicate': GuestDuplicate //add it on laravel template
    },
    i18n
});
