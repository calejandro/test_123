/**
 * Some interface resizing utils
 */

export default {
  getGuestIndexTableHeight: function(add, siteHeader, headPannel, headNavInfo, warnCompany, alertsH) {
    let warnH = warnCompany.height()
    if(warnCompany.length <= 0) {
      warnH = 0
    }
    let paginate = $('#guests-pagination').outerHeight() > 0 ? $('#guests-pagination').outerHeight() : 62 // default value
    var h = window.innerHeight - siteHeader.height()
    h = h - headPannel.outerHeight() - paginate - headNavInfo.outerHeight() - warnH
    h = h - alertsH
    h = h  - 30 - add // Remove magin and paddings, 30 = pannel-body paddings
    return h
  },

  resizeGuestIndexTable: function(add = 0) {
    let siteHeader = $('#site-header')
    let headPannel = $('#head-panel')
    let headNavInfo = $('.head-bar-nav-info')
    let warnCompany = $('.warning-on-company')

    let alertsH = 0
    $(".guests-list-remove").each(function(index) {
      alertsH = alertsH + $(this).outerHeight(true)
    })

    if(window.innerWidth >= 992) {
      var h = this.getGuestIndexTableHeight(add, siteHeader, headPannel, headNavInfo, warnCompany, alertsH)
      $('.guests-table').height(h)
    }
  },

  addGuestIndexTableHeightListener: function (add = 0) {
    let siteHeader = $('#site-header')
    let headPannel = $('#head-panel')
    let headNavInfo = $('.head-bar-nav-info')
    let warnCompany = $('.warning-on-company')

    var resizeFct = () => {
      let table = $('.guests-table')
      if(table.length === 0) {
        return false
      }

      let alertsH = 0
      $(".guests-list-remove").each(function(index) {
        alertsH = alertsH + $(this).outerHeight(true)
      })

      if(window.innerWidth >= 992) {
        var h = this.getGuestIndexTableHeight(add, siteHeader, headPannel, headNavInfo, warnCompany, alertsH)
        table.height(h)
      }
      return true
    }

    var tryResize = () => {
      if(!resizeFct()) {
        setTimeout(() => { 
          tryResize()
        }, 100);
      }
    }

    tryResize()
    $(window).resize(() => {
      resizeFct()
    })
  }
}