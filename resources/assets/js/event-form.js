
$( document ).ready(function() {
  /*$('input[type=radio][name=advanced_minisite_mode]').change(function() {
    if(this.value == 1){
      $('#minisite_visibility_section').fadeIn(320, function(){})
    }else{
      $('#minisite_visibility_section').fadeOut(320, function(){})
    }
  })*/

  $('input[type=number][name=companions_limit]').change(function() {
    if(this.value > 0){
      $('#nominative-companions-bloc').fadeIn(320, function(){})
    }else{
      $('input[type=checkbox][name=nominative_companions]').prop('checked', false)
      $('#nominative-companions-bloc').fadeOut(320, function(){})
    }
  })

  function toggleBaseFieldsBloc(lang){
    if($('#checkbox-support-' + lang).is(':checked')){
      $('#bloc-fields-' + lang).fadeIn()
    }else{
      $('#bloc-fields-' + lang).fadeOut()
    }
  }

  function toggleVisibleCaptha(){
    if($('#public_minisite').is(':checked')){
      $('#visible-captcha-bloc').fadeIn()
    }else{
      $('#visible-captcha-bloc').fadeOut()
    }
  }

  toggleBaseFieldsBloc('fr');
  toggleBaseFieldsBloc('en');
  toggleBaseFieldsBloc('de');
  toggleVisibleCaptha();

  // Toggling base fields blocs depending on languages
  $("#checkbox-support-fr").on("change", function() {
    toggleBaseFieldsBloc('fr');
  });

  $("#checkbox-support-en").on("change", function() {
    toggleBaseFieldsBloc('en');
  });

  $("#checkbox-support-de").on("change", function() {
    toggleBaseFieldsBloc('de');
  });
  
  $("#public_minisite").on("change", function() {
    toggleVisibleCaptha();
  });

});
