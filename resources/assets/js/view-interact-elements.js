/**
 * Listener to interactive HTML elements
 */
$( document ).ready(function() {

  $('.btn-collapse-panel, .btn-collapsible-close').click(function() {
    let elem = $('.panel-collapsible')
    let defaultH = elem.height()
    
    let autoH = 0
    if (defaultH == 0) {
      elem.css('height', 'auto')
      autoH = elem.height()
      elem.css('height', defaultH)
    }

    elem.animate({ height: autoH }, 400)
  })
})