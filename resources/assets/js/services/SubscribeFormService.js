import ServerConfig from '../config/server' // Necessary because the get is called from minisite under subdomain!

export default {
    show: function(eventId) {
        return axios.get(`/api/events/${eventId}/subscribeform/show`)
    },
    update: function(subscribeForm) {
        return axios.patch(`/api/events/${subscribeForm.event_id}/subscribeform`, subscribeForm)
    },
    /**
     * @param accessHash: required if call is made without authentification
     */
    get: function(eventId, accessHash=null, publicRoute=true) {
        let baseUrl = ServerConfig.url + '/'
        if (!publicRoute) {
            baseUrl = '/'
        }

        let params = Object.assign({}, axios.defaults)
        if (accessHash !== null) {
            params.headers.Authorization = accessHash
        }

        return axios.get(`${baseUrl}api/events/${eventId}/subscribeform`, params)
    },
     /**
     * @param accessHash: required if call is made without authentification
     */
    getCompanion: function(eventId, accessHash=null, publicRoute=true) {
        let baseUrl = ServerConfig.url + '/'
        if (!publicRoute) {
            baseUrl = '/'
        }

        let params = Object.assign({}, axios.defaults)
        if (accessHash !== null) {
            params.headers.Authorization = accessHash
        }
        
        return axios.get(`${baseUrl}api/events/${eventId}/subscribeform/_companion`, params)
    }
}
