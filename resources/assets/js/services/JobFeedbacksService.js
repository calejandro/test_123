export default {
    retrieveAll: function(eventId, emailId){
        return axios.get('/api/events/' + eventId + '/emails/' + emailId + '/jobfeedbacks')
    },
    exportToXls: function (emailId, jobId) {
        return axios({
            method: 'GET',
            responseType: 'blob',
            url: `/api/emails/${emailId}/jobfeedbacks/${jobId}/export`
        })
    }
}
