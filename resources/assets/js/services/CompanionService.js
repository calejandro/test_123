export default {
    retrieveAll: function (eventId, guestId, hash) {
        return axios.get(`/api/events/${eventId}/guests/${guestId}/companions`,{
            headers: {
              Authorization: hash
            }
        })
    },
    unCheckin: function (eventId, companionId) {
        return axios.post(`/api/events/${eventId}/companions/${companionId}/_uncheckin`)
    },
    unCheckinSession: function (eventId, sessionId, guestId) {
        return axios.post(`/api/events/${eventId}/sessions/${sessionId}/companions/${guestId}/_uncheckin`)
    },
    updateParticipation: function (eventId, companion) {
        return axios.patch(`/api/events/${eventId}/companions/${companion.id}/_update_participation`, {
            firstname: companion.firstname,
            lastname: companion.lastname,
            email: companion.email,
            company: companion.company,
            extended_fields: companion.extended_fields,
            access_hash: companion.access_hash
        },{
            headers: {
                Authorization: companion.access_hash
            }
        })
    },
    update: function (companion, event) {
        return axios.patch(`/api/events/${event.id}/companions/${companion.id}`, companion)
    },
    destroy: function (companion, event) {
        return axios.delete(`/api/events/${event.id}/companions/${companion.id}`)
    },
}
