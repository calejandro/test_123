import { Session } from '../models/Session'

export default {
    /**
     * Return parsed Session
     */
    retrieve: function(eventId, accessHash=null) {
        let params = Object.assign({}, axios.defaults)
        if (accessHash !== null) {
            params.headers.Authorization = accessHash
        }

        return new Promise((resolve, reject) => {
            axios.get(`/api/events/${eventId}/sessions`, params).then(response => {
                resolve(response.data.map(s => Session.createFromJson(s)))
            }).catch(error => reject(error))
        })
    },
    retrieveOfGuest: function(guestId, accessHash) {
        return axios.get(`/api/guests/${guestId}/sessions`, {
            headers: {
                Authorization: accessHash
            }
        })
    },
    checkin: function(eventId, sessionId, hash) {
        return axios.post(`/api/events/${eventId}/sessions/${sessionId}/_checkin/${hash}`)
    },
    /**
     * Regroup sessions by session group
     * @param session: array containing Session objects
     */
    regroupBySessionGroup: function(sessions) {
        let sessionsGroups = {}
        
        for (let session of sessions) {
            if (session.sessionGroup != null) {
                let tgp = session.totalGuestPlaces ? session.totalGuestPlaces : 0;
                let newSession = new Session(
                  session.id,
                  session.createdAt,
                  session.updatedAt,
                  session.name,
                  session.places,
                  session.eventId,
                  session.placesReserved,
                  session.placesCheckin, null,
                  tgp
                );
                  newSession.sessionGroup = session.sessionGroup;

                if(sessionsGroups[session.sessionGroup.id] == null) {
                  sessionsGroups[session.sessionGroup.id] = session.sessionGroup;
                  sessionsGroups[session.sessionGroup.id].sessions = [newSession];
                }
                else {
                  sessionsGroups[session.sessionGroup.id].sessions.push(newSession)
                }
            }
        }
        
        return Object.values(sessionsGroups)
    }
}
