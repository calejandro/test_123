
export default {
    /**
     * Respond filtered guest and companions ids (separate lists)
     */
    filterAll: function(event, filters) {
        return axios.post(`/api/events/${event.id}/guests/_filter_all`, filters)
    },
    retrievePaginate: function(eventId, filters) {
        return axios.post(`/api/events/${eventId}/guests/filter?page=${filters.page}`, filters)
    },
    anonymise: function(eventId, password){
        return axios.post(`/api/events/${eventId}/guests/anonymise`, {
            password: password,
            event: eventId
        })
    },
    retrieveAll: function(eventId) {
        return axios.get('/api/events/' + eventId + '/guests')
    },
    destroy: function(guest, event) {
        return axios.delete(`/api/events/${event.id}/guests/${guest.id}`)
    },
    destroyAll: function(eventId) {
        return axios.delete('/api/events/' + eventId + '/guests/_destroy_all')
    },
    update: function(guest, event) {
        return axios.patch(`/api/events/${event.id}/guests/${guest.id}`, guest)
    },
    retrieveAttributes: function(eventId) {
        return axios.get('/api/events/' + eventId + '/guests/_attributes')
    },
    retrieveMinimumAttributes: function(eventId) {
        return axios.get('/api/events/' + eventId + '/guests/_minattributes')
    },
    import: function(mappingLabelByName, guests, ignored, eventId) {
        return axios.post('/api/events/' + eventId + '/guests/_import', {
            mapping: mappingLabelByName,
            guests: guests,
            ignored: ignored
        })
    },
    solveConflict: function(event_id, guest_id){
      return axios.post('/api/events/' + event_id + '/guests/solve-duplicate/' + guest_id, {
          event_id: event_id,
          guest_id: guest_id,
      })
    },
    subscribe: function(guestSubscribeRequestData, sessionIds, guestId, eventId, hash) {
        guestSubscribeRequestData.sessions = sessionIds
        return axios.patch(`/api/events/${eventId}/guests/${guestId}/_subscribe`, guestSubscribeRequestData, {
            headers: {
                Authorization: hash
            }
        })
    },
    register: function(guestDatas, sessionIds, eventId, captchaTocken) {
        const datas = Object.assign({
            'recaptchaResponse': captchaTocken,
            'sessions': sessionIds,
            'eventId': eventId
        }, guestDatas)
        return axios.post('/api/events/' + eventId + '/guests/_register', datas)
    },
    subscribeNominative: function({firstname, lastname, company, email, extendedFields, companionsData, language}, 
            guestSessions, guestId, eventId, hash) {
        return axios.patch(`/api/events/${eventId}/guests/${guestId}/_subscribe_nominative`, { // TODO
            firstname: firstname,
            lastname: lastname,
            company: company,
            email: email,
            extendedFields: extendedFields,
            companionsData: companionsData,
            language: language,
            sessions: guestSessions
        },{
            headers: {
                Authorization: hash
            }
        })
    },
    registerNominative: function({firstname, lastname, company, email, extendedFields, companionsData, language}, 
            guestSessions, eventId, captchaTocken) {
        return axios.post(`/api/events/${eventId}/guests/_register_nominative`, {
            firstname: firstname,
            lastname: lastname,
            company: company,
            email: email,
            extendedFields: extendedFields,
            companionsData: companionsData,
            language: language,
            sessions: guestSessions,
            recaptchaResponse: captchaTocken,
            eventId: eventId
        })
    },
    checkEmailVerificationJobs: function(eventId) {
        return axios.get('/api/events/' + eventId + '/_check_email_verification_jobs')
    },
    unCheckin: function(eventId, guestId) {
        return axios.post(`/api/events/${eventId}/guests/${guestId}/_uncheckin`)
    },
    unCheckinSession: function(eventId, sessionId, guestId) {
        return axios.post(`/api/events/${eventId}/sessions/${sessionId}/guests/${guestId}/_uncheckin`)
    },
    /* decline: function ($eventId, $guestHashControl){
        return axios.get('/api/events/' + eventId + '/guests/' + $guestHashControl + '/decline')
    } */
}
