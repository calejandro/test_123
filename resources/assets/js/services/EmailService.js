export default {
    store: function(email, lang) {
        return axios.post('/api/events/' + email.event_id + '/emails' + '/' + lang, email)
    },
    update: function(email, lang) {
        return axios.patch('/api/events/' + email.event_id + '/emails/' + email.id + '/' + lang, email)
    },
    send: function(email, guests, companions, fromColumn, password, mailMode, joinICS) {
        return axios.post(`/api/events/${email.event_id}/emails/${email.id}/send`, {
            guests: guests,
            companions: companions,
            fromColumn: fromColumn,
            password: password,
            mailMode: mailMode,
            joinICS: joinICS
        })
    },
    resend: function(jobId, email, fromColumn, mailMode, joinICS){
        return axios.post(`/api/events/${email.event_id}/emails/${email.id}/resend`, {
            jobFeedbackId: jobId,
            fromColumn: fromColumn,
            mailMode: mailMode,
            joinICS: joinICS
        })
    }
}
