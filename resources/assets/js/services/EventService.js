export default {
    stats: function(id){
        return axios.get(`/api/events/${id}/_stats`)
    },
    retrieveAll: function() {
        return axios.get('/api/events')
    },
    /**
     * @param id : id of the event to retrieve
     * @param guestAccessHash : Guest authorization hash if event is private
     */
    retrieve: function(id, guestAccessHash=null) {
        return axios.get(`/api/events/${id}`, {
            headers: {
                Authorization: guestAccessHash
            }
        })
    },
    /**
     * @param id : id of the event that contains wanted extended fields
     * @param guestAccessHash : Guest authorization hash if event is private
     */
    extendedFields: function(id, guestAccessHash=null) {
        return axios.get(`/api/events/${id}/_extendedFields`, {
            headers: {
                Authorization: guestAccessHash
            }
        })
    },
    checkin: function(id, hash) {
        return axios.post(`/api/events/${id}/_checkin/${hash}`)
    },
    removeExtField: function(eventId, field) {
        return axios.post(`/api/events/${eventId}/_remove_extended_field`, {
            field: field
        })
    },
}
