export default {
    show: function(event_id) {
        return axios.get(`/api/events/${event_id}/surveyform/show`)
    },
    update: function(surveyForm) {
        return axios.patch(`/api/events/${surveyForm.event_id}/surveyform`, surveyForm)
    },
    reply: function(eventId, guestId, accessHash, data) {
        return axios.patch(`/api/events/${eventId}/guests/${guestId}/surveyform/_reply`, data, {
            headers: {
                Authorization: accessHash
            }
        })
    },
    results: function(eventId) {
        return axios.get(`/api/events/${eventId}/surveyform/_results`)
    },
    get: function(eventId, accessHash=null) {
        let params = Object.assign({}, axios.defaults)
        if (accessHash !== null) {
            params.headers.Authorization = accessHash
        }

        return axios.get(`/api/events/${eventId}/surveyform`, params)
    }
}
