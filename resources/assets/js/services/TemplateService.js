export default {
  get: function(template_id, company_id){
    return axios.get('/api/companies/' + company_id + '/templates/' + template_id)
  },
  store: function(template, company_id) {
    return axios.post('/api/companies/' + company_id + '/templates', template)
  },
  update: function(template, company_id, template_id) {
    return axios.patch('/api/companies/' + company_id + '/templates/' + template_id, template)
  },
  delete: function(template_id, company_id){
    return axios.delete('/api/companies/' + company_id + '/templates/' + template_id)
  }
}
