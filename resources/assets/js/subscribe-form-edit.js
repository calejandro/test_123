import SubscribeFormEdit from './components/subscribe-form-edit/SubscribeFormEdit'
import TemplateButton from './components/subscribe-form-edit/TemplateButton'
import TemplatesCollapsiblePanel from './components/subscribe-form-edit/TemplatesCollapsiblePanel'

import i18n from './tools/i18n'

/**
 * Starter file
 */
const appSubscribeForm = new Vue({
    el: '#app-subscribe-form-edit',
    components: {
        'subscribe-form-edit': SubscribeFormEdit
    },
    i18n
});

const appTemplateButton = new Vue({
    el: '#app-template-button',
    components: {
        'template-button': TemplateButton
    },
    i18n
});