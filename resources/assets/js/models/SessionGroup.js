export class SessionGroup {
  constructor(id, createdAt, updatedAt, name, mandatory, allowed_sessions) {
    this.id = id;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.name = name;
    this.mandatory = mandatory === 1;
    this.allowed_sessions = allowed_sessions;
  }

  static createFromJson(json) {
    return new SessionGroup(
      json.id,
      json.created_at,
      json.updated_at,
      json.name,
      json.mandatory,
      json.allowed_session_subscriptions
    );
  }
}
