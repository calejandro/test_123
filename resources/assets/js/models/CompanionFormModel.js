/**
 * Subscribe form companion representation
 */
export class CompanionFormModel  {
    constructor(id, index, firstname, lastname, email, company,
            requiredFirstname = true, requiredLastname = true, requiredCompany = true) {
        this.id = id
        this.index = index
        this.firstname = firstname
        this.lastname = lastname
        this.email = email
        this.company = company
        this.requiredFirstname = requiredFirstname
        this.requiredLastname = requiredLastname
        this.requiredCompany = requiredCompany
    }

    get valid() {
        let errors = 0
        if (this.requiredFirstname) {
            errors += this.firstname != null && this.firstname !== "" ? 0 : 1
        }
        if (this.requiredLastname) {
            errors += this.lastname != null && this.lastname !== "" ? 0 : 1
        }
        if (this.requiredCompany) {
            errors += this.company != null && this.company !== "" ? 0 : 1
        }
        errors += this.email && this.email !== "" && this.emailIsValid(this.email) ? 0 : 1
        return errors === 0
    }

    emailIsValid (email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return true
    }
}
