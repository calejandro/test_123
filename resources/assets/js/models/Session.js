import { SessionGroup } from "./SessionGroup";

export class Session  {
    constructor(id, createdAt, updatedAt, name, places, eventId,
            placesReserved, placesCheckin, sessionGroup, totalGuestPlaces) {
        this.id = id
        this.createdAt = createdAt
        this.updatedAt = updatedAt
        this.name = name
        this.places = places
        this.eventId = eventId
        this.placesReserved = placesReserved
        this.placesCheckin = placesCheckin
        this.sessionGroup = sessionGroup
        this.totalGuestPlaces = totalGuestPlaces // linked guest reserved places
    }

    get isFull() {
        return this.isFullWithWantedPlaces(0)
    }

    isFullWithWantedPlaces(wantedPlaces) {
        return this.places + this.totalGuestPlaces <= this.placesReserved + wantedPlaces
    }

    static createFromJson(json) {
        let sessionGroup = null
        if (json.session_group) {
            sessionGroup = SessionGroup.createFromJson(json.session_group)
        }

        return new Session(json.id, json.created_at, json.updated_at, json.name, json.places, json.event_id,
            json.placesReserved, json.placesCheckin, sessionGroup, json.totalGuestPlaces)
    }
}
