import SurveyFormEdit from './components/survey-form-edit/SurveyFormEdit'
import i18n from './tools/i18n'

/**
 * Starter file
 */
const appSurveyForm = new Vue({
    el: '#app-survey-form-edit',
    components: {
        'survey-form-edit': SurveyFormEdit,
    },
    i18n
});