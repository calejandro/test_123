import tools from './standalone-pages-tools'

$( document ).ready(function() {
  if($('#slug_string_page_name').length > 0){
    $('#page_name_field').keyup(function(){
      $("#slug_string_page_name").val(tools.stringToSlug($('#page_name_field').val()));
    })
  }
});