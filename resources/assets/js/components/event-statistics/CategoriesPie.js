
import { Pie } from 'vue-chartjs'

export default Pie.extend({
  props: ['categories'],
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: Object.keys(this.categories),
      datasets: [
        {
          data: Object.values(this.categories),
          backgroundColor: ["#062B59", "#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
        }
      ]
    }, null)
  }
})