
import { Bar } from 'vue-chartjs'

export default Bar.extend({
    props: [
        'imported',
        'invited',
        'subscribed',
        'checkIn',
        'declined',
        'declinedCompanions',
        'checkinCompanions',
        'subscribedCompanions',
        'totalCompanions'
    ],
    mounted () {
        // Overwriting base render method with actual data.
        this.renderChart({
            labels: [
                this.$t('guest.Imported'),
                this.$t('guest.Invited'),
                this.$t('guest.Subscribed'),
                this.$t('guest.Checkin'),
                this.$t('guest.Declined')
            ],
            datasets: [
                {
                    label: this.$t('guest.Guests'),
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9", "#ff0000"],
                    data: [this.imported, this.invited, this.subscribed, this.checkIn, this.declined]
                },
                {
                    label: this.$t('guest.Companions'),
                    backgroundColor: ["#62cfff","","#54ffda", "#ffe6e0", "#ff4444"],
                    data: [this.totalCompanions, 0, this.subscribedCompanions, this.checkinCompanions, this.declinedCompanions]
                }
            ],

        }, {
            legend: { display: false },
            height: 200,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                        callback: function (value) { if (Number.isInteger(value)) { return value } }
                    }
                }]
            }
        })
    }
})
