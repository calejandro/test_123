import tools from './standalone-pages-tools'

$( document ).ready(function() {
    // Minisite auto-slug on title update
    //TODO: Make it DRY!
    if($("#minisite_title_field_fr").length > 0){
      $('#minisite_title_field_fr').keyup(function(){
        $("#slug_string_minisite_fr").val(tools.stringToSlug($('#minisite_title_field_fr').val()))
      })
    }

    if($("#minisite_title_field_en").length > 0){
      $('#minisite_title_field_en').keyup(function(){
        $("#slug_string_minisite_en").val(tools.stringToSlug($('#minisite_title_field_en').val()))
      })
    }

    if($("#minisite_title_field_de").length > 0){
      $('#minisite_title_field_de').keyup(function(){
        $("#slug_string_minisite_de").val(tools.stringToSlug($('#minisite_title_field_de').val()))
      })
    }

    $("#unkeep_bg_img_file").on("click", function(){
      $("#hidden_bg_img_file").val(0)
      $("#existing_bg_img_bloc").hide()
      $("#new_bg_img_bloc").css('display', 'block')
    });

    $("#unkeep_bottom_img_file").on("click", function(){
      $("#hidden_bottom_img_file").val(0)
      $("#existing_bottom_img_bloc").hide()
      $("#new_bottom_img_bloc").css('display', 'block')
    });

    $("#unkeep_top_img_file").on("click", function(){
      $("#hidden_top_img_file").val(0)
      $("#existing_top_img_bloc").hide()
      $("#new_top_img_bloc").css('display', 'block')
    });
});
