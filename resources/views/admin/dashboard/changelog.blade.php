@extends('layouts.app')

@section('content')
    <div class="container-full">

        @include('admin.sidebar')

        <div class="col-md-9 page-content">
            <h1>Changelog</h1>

            <section>
                <h2><span class="label label-lg label-primary">v5.9.0</span> 22.01.2020</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <pre>
                    npm install
                </pre>

                <h3>Corrections et nouvelles fonctionnalités</h3>
                <ul>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/437">#437 Solution pour l'affichage en colonne dans TinyMCE</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/679">#679 Import Excel: Erreur à l'import, étiquettes</a></li>
                </ul>
            </section>
           
            <section>
                <h2><span class="label label-lg label-primary">v5.8.5</span> 02.10.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucun patch.</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>
                <ul>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/653">#653 Fonction anonymiser ne fonctionne plus</a></li>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/652">#652 Formulaire d'inscription: Ne s'affiche pas avec IE 11</a></li>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/651">#651 Gestion des doublons: lorsqu'un doublon est résolu, on revient à la liste des invités même s'il reste des doublons.</a></li>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/643">#643 Erreurs dans les traductions</a></li>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/641">#641 Liste des invités et billet: Caractère spéciaux mal affichés</a></li>
                    <li><a href="https://labinfo.ing.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/558">#558 Le formuliaire d'inscription ne parvient pas à se charger</a></li>
                </ul>
            </section>

            <section>
                    <h2><span class="label label-lg label-primary">v5.8.3</span> 20.08.2019</h2>
    
                    <h3>Patch à appliquer au déploiement:</h3>
    
                    <p>Migrations à effectuer.</p>

                    <p>Aucun patch.</p>
    
                    <h3>Corrections et nouvelles fonctionnalités</h3>
    
                    <ul>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/624">#624 Faille accès ressources</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/618">#618 Sessions suppression "douce"</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/559">#559 Mobile: Impossible de se déconnecter en temps qu'administrateur</a></li>
                    </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.8.2</span> 16.08.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>aucun</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/628">#628 Billet: Les majuscules accentuées sont inscrites en minuscules pour le nom de l'invité</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/623">#623 Formulaire d'inscription: Le nom de la colonne est affiché à la place du titre</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/622">#622 Mettre à jour les traductions</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/619">#619 Liste invités: Filtre validité email ne fonctionnent plus</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/582">#582 Envoi Email : catch `Throwable`</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/579">#579 Création page minisite - Validation slug</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/530">#530 Sondage : Titres affichés comme paragraphe</a></li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.8.1</span> 14.06.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Lancer les migrations avant d'effectuer les patches.</p>

                <pre>php artisan db:seed --class PageTypesCompanionPatchSeeder</pre>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/574">#574 Validation emails: Tâche jamais arrêtée si liste invités vide</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/562">#562 Duplication d'évènement: Ne plus copier les formulaires FormBuilder</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/560">#560 Export XLS: Ajouter les sessions inscrites dans la liste des accompagnants</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/557">#557 Normaliser les noms de colonne (surveyJS + import)</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/511">#511 Ajout des accompagnants dans la page de la liste des invités</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/510">#510 Formulaire de l'accompagnant</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/495">#495 Edition de page du mini-site: Lorsqu'on quitte, revenir à la page de gestion du mini-site avancé</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/399">#399 Paramètre "Colonne de nom d'expéditeur" non enregistré</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/378">#378 Accompagnants nominatifs</a></li>
                </ul>
            </section>

            <section>
                    <h2><span class="label label-lg label-primary">v5.8.0</span> 09.05.2019</h2>

                    <h3>Patch à appliquer au déploiement:</h3>

                    <p>Lancer les migrations avant d'effectuer les patches.</p>

                    <pre>php artisan db:seed --class AddCompanionConfirmEmailPatchSeeder</pre>
                    <pre>php artisan db:seed --class AddTypeToEmailsPatchSeeder</pre>

                    <h3>Corrections et nouvelles fonctionnalités</h3>

                    <ul>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/540">#540 Duplication d'évènement: Les liens spéciaux dans les e-mails ne sont pas modifiés</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/509">#509 E-mail de confirmation d’inscription pour accompagnant</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/508">#508 Tableau d'inscription aux sessions pour les accompagnants (doodle)</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/507">#507 Catégorie de sessions</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/460">#460 SurveyJS: Problème de sélection pour les question à choix unique si plusieurs réponses ont les mêmes valeures</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/451">#451 Détection version du navigateur</a></li>
                    </ul>
            </section>

            <section>

                <h2><span class="label label-lg label-primary">v5.7.1</span> 30.04.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>
    
                <p>aucun</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/541">#541 Sélecteurs manquants pour tests</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/539">#539 Dupliquer évènement: incomplet</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/524">#524 Liste invités: Design responsive pour les messages d'avertissement</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/523">#523 Ajouter la colonne check-in à l'export XLS pour les accompagnants</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/521">#521 Dashboard event: Répétition de la langue</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/520">#520 Dashboard Event: Affichage traduction langue manquants</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/519">#519 Erreur 500 insertion image background minisite</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/501">#501 Erreur à l'ouverture d'une liste d'invités</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/499">#499 Billet: La mention Accompagnant n'est pas inscrite sur les billets de type simple</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/498">#498 Formulaire pour mini-site simple: Le nom de colonne est affiché au lieu de l'intitulé</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/497">#497 Formulaire d'inscription: Adapter la couleur du texte du message de confirmation d'inscription</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/490">#490 Création/Mise à jour event: Valider l'email organisaeur</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/481">#481 ErrorException: Undefined index: n0_de_personne_physique &&  Undefined index: email</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/474">#474 SurveyJS: Les cases à cocher unique ne tiennent pas compte du paramètre est obligatoire</a></li>
                    </ul>

            </section>

            <section>
                    <h2><span class="label label-lg label-primary">v5.7.0</span> 04.04.2019</h2>
    
                    <h3>Patch à appliquer au déploiement:</h3>
    
                    <p>aucun</p>
    
                    <h3>Corrections et nouvelles fonctionnalités</h3>
            
                    <ul>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/488">#488 Remplacer le message d'erreur verbeux de l'erreur 500 sur le mini-site</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/483">#483 Correction faute d'orthographe</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/471">#471 Meilleur traitement des invités qui déclinent</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/468">#468 Redimensionner et valider les images du minisite</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/393">#393 Fonction de scanner QR-Code pour les contrôleurs</a></li>
                    </ul>
            </section>

            <section>
                    <h2><span class="label label-lg label-primary">v5.6.0</span> 25.03.2019</h2>
    
                    <h3>Patch à appliquer au déploiement:</h3>
    
                    <p><b>Dump de la base de données avant de lancer le seeder.</b></p>

                    <p>Migrations à effectuer. Ensuite patchs.</p>

                    <pre>php artisan db:seed --class=ConfirmationEmailDefaultTitlesPatchSeeder</pre>
                    <pre>php artisan db:seed --class=EventTitlesAndDescriptionsMultilingualDefaultsPatchSeeder</pre>
    
                    <h3>Corrections et nouvelles fonctionnalités</h3>
            
                    <ul>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/489">#489 Liste des invités: Impossible de cocher ou décocher un invité pour les colonnes inscrit, refusé...</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/465">#465 Ajouter une marge sous le logo de bas de page du mini-site avancé</a></li>
                        <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/395">#395 Billet et e-mail de confirmation multi-lingue</a></li>
                    </ul>
            </section>

            <section>
                    <h2><span class="label label-lg label-primary">v5.5.1</span> 07.03.2019</h2>
    
                    <h3>Patch à appliquer au déploiement:</h3>
    
                    <p>Migrations</p>
    
                    <h3>Corrections et nouvelles fonctionnalités</h3>
                    <ul>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/478">#478 Le nb d'accompagnants n'est pas conservé</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/462">#462 Editeur mini-site avancé: Faire que le bouton Mettre à jour ne change pas de page</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/461">#461 Demander confirmation avant de bloquer une compagnie</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/456">#456 Config SMTP: Message d'avertissement si config avec no-reply@eventwise.ch</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/452">#452 Formbuilder encore utilisé pour le formulaire d'inscription des mini-sites simples</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/448">#448 Erreur 500 au scan d'un QR-Code</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/447">#447 Erreur lors du formatage d'un invité</a></li>                   
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/446">#446 Config SMTP: préremplissage des champs Nom d'utilisateur et Pwd compte SMTP</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/442">#442 Erreur à la vérification d'e-mail par NeverBounce: This file appears to be corrupt...</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/441">#441 Export XLS: entêtes des champs étendus intevertis et colonnes décalées</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/440">#440 Liste d'invités: filtres sur les colonnes des sessions</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/436">#436 Formulaire d'inscription: Problème à l'affichage l'édition des champs des select</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/433">#433 Le formulaire d'inscription ne traite plus les 422</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/429">#429 Les invités disparaissent à la modification</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/424">#424 Edition e-mail: Fusionner les boutons Réduire et Activer mode plein écran</a></li>
                            <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/412">#412 Plus de 90000 colonnes de champs étendus pour un évènement</a></li>
                        </ul>
                </section>




            <section>
                <h2><span class="label label-lg label-primary">v5.5.1</span> 25.02.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Migrations</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>
                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/462">#462 Editeur mini-site avancé: Faire que le bouton Mettre à jour ne change pas de page</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/461">#461 Demander confirmation avant de bloquer une compagnie</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/459">#459 Mini-site avancé: Angles arrondis sur les logos</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/456">#456 Config SMTP: Message d'avertissement si config avec no-reply@eventwise.ch</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/452">#452 Formbuilder encore utilisé pour le formulaire d'inscription des mini-sites simples</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/448">#448 Erreur 500 au scan d'un QR-Code</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/447">#447 Erreur lors du formatage d'un invité</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/446">#446 Config SMTP: préremplissage des champs Nom d'utilisateur et Pwd compte SMTP</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/442">#442 Erreur à la vérification d'e-mail par NeverBounce: This file appears to be corrupt...</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/441">#441 Export XLS: entêtes des champs étendus intevertis et colonnes décalées</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/440">#440 Liste d'invités: filtres sur les colonnes des sessions</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/436">#436 Formulaire d'inscription: Problème à l'affichage l'édition des champs des select</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/433">#433 Le formulaire d'inscription ne traite plus les 422</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/429">#429 Les invités disparaissent à la modification</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/424">#424 Edition e-mail: Fusionner les boutons Réduire et Activer mode plein écran</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/412">#412 Plus de 90000 colonnes de champs étendus pour un évènement</a></li>
                </ul>

            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.5.0</span> 15.02.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Migrations</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>
                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/435">#435 Formulaire d'inscription: Pré-remplissage des sessions ne se fait plus</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/430">#430 Duplication d'e-mail: les paramètres de l'e-mail ne sont pas copiés</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/419">#419 Accompagnants nominatifs simples</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/443">#443 Liste des invités: ajouter un bouton pour accéder au formulaire de l'invité</a></li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.4.2</span> 29.01.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucun</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/431">#431 Formulaire d'inscription: Erreur "validation.captcha"</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/428">#428 Invité inséré sans email</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/415">#415 Lien vers le formulaire d'inscription privé depuis un aperçu d'e-mail ne fonctionne pas</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/414">#414 Log: "Integrity constraint violation"</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/413">#413 Log: "Undefined index: template"</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/410">#410 Mini-site seulement en allemand ne s'affiche pas</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/409">#409 Ajout de paramètres de mise en page du mini-site</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/407">#407 i18n gestion des duplicatas</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/404">#404 Correction d'un message d'erreur</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/337">#337 Date et heures par défaut d'un évènement</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/332">#332 Nettoyeur de code pour TinyMCE</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/318">#318 Avertir l'utilisateur si colonne  "nom d'expéditeur" vide</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/295">#295 Rework de la page de gestion des permissions côté administrateurs</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/293">#293 Contenu des mails de notification dans des fichiers templates</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/282">#282 A la fin de la gestion des doublons, retourner à la liste des invités</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/281">#281 Améliorations de la validation du formulaire de création d'évènement</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/265">#265 Ajouter la police "open-sans" au mini-site</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/244">#244 Ajouter traduction de TinyMCE</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/216">#216 Problème d'affichage dans TinyMCE si la couleur du texte est blanc</a></li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.4.1</span> 16.01.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucun</p>

                <p>Configuration de Sentry nécessaire</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/422">#422 Erreur 500 à l'inscription</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/412">#412 Plus de 90000 colonnes de champs étendus pour un évènement</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/420">#420 Désactiver manuellement la validation des emails</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/398">#398 Créer un index des données pouvant être loguées</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/411">#411 Corriger les erreurs des tests en dev</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/349">#349 Liste d'invités: Compléter les infobulles de la colonne "Adresse e-mail valide"</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/405">#405 Intégration Sentry</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/344">#344 Invités: Mettre à jour la pagination lorsqu'on modifie les filtres</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/277">#277 Amélioration utilisabilité</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/268">#268 Ajouter un bouton dupliquer un e-mail</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/325">#325 Edition e-mail: Déplacer le bouton Aperçu</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/391">#391 Adresses avec espaces en début/fin considérées comme invalides à l'import</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/213">#213 Trouver une solution pour la colonne "0" à l'importation</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/201">#201 Fichier ICS plus complet</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/361">#361 Import XLS: changer le message d'erreur pour champs standard obligatoire</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/372">#372 Minisite : Heure de dernières modifications affichée en GMT</a></li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.4.0</span> 14.01.2019</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p><b>Dump de la base de données avant de lancer le seeder.</b></p>

                <pre>php artisan db:seed --class=CompanionModelsPatchSeeder</pre>

                <p>Migrations à effectuer.</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/368">#368 Permettre d’identifier les check-in des invités et des accompagnants</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/362">#362 Changement de l'éditeur de formulaire</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/382">#382 Formulaire: Permettre de modifier l'aspect du bouton Valider</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/385">#385 Problème dans le formulaire d'inscription public pour invité déjà dans la liste</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/390">#390 Erreur 500 à l'enregistrement d'une page du mini-site après un copier-coller depuis Word</a></li>
                    <li>Session multilingues</li>
                    <li>Impose au minimum une langue pour le mini-site</li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.14</span> 19.12.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalitées</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/408">#408 Mini-site simple: Le bouton valider et champs accompagnants n'est pas en dessous du formulaire mais en bas de page</a></li>
                <ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.13</span> 13.12.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <pre>php artisan db:seed --class=TypesI18nPatchSeeder</pre>

                <h3>Corrections et nouvelles fonctionnalitées</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/400">#400 Liste des invités: Le filtre par validité d'adresse e-mail ne fonctionne plus</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/403">#403 Traduire les "types"</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/396">#396 Trier les évènements par date de début et pas par date de création</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/387">#387 Modification de la validation du formulaire d'inscription pour mini-site public</a></li>
                <ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.12</span> 05.12.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalitées</h3>
                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/401">#401 Corrections de traductions allemand</a></li>
                <ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.11</span> 29.11.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalitées</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/392">#392 Le choix ICS pour les e-mails n'est pas conservé</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/384">#384 Problèmes de mise en page pour billet type Badge</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/389">#389 Message de confirmation de suppression pour les pages du mini-site</a></li>
                <ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.10</span> 19.11.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalitées</h3>
                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/390">#390 Erreur 500 à l'enregistrement d'une page du mini-site après un copier-coller depuis Word</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/384">#384 Problèmes de mise en page pour billet type Badge</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/385">#385 Problème dans le formulaire d'inscription public pour invité déjà dans la liste</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/373">#373 Envoi: Bouton "envoyer" bloqué après filtres liste invités</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/376">#376 FormBuilder: Renommer checkbox et radio par réponses</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/388">#388 Une balise personalisée ne s'affiche pas dans l'aperçu d'un e-mail</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/386">#386 Message "Une erreur s'est produite" à l'envoi d'un e-mail</a></li>
                </ul>

            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.9</span> 30.10.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/369">#369 Vérification adresse e-mail pas démarrée</a></li>
                    <li>Envoi aux adresses emails invalides corrigé</li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.8</span> 16.10.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <p>Aucuns, quickfix</p>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li>Dépassements et tailles des billets corrigés</li>
                </ul>
            </section>

            <section>
                <h2><span class="label label-lg label-primary">v5.3.7</span> 12.10.2018</h2>

                <h3>Patch à appliquer au déploiement:</h3>

                <pre>php artisan db:seed --class=ChangeDefaultMinisiteTitleFontSizePatchSeeder</pre>

                <h3>Corrections et nouvelles fonctionnalités</h3>

                <ul>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/187">#187 Possibiliter de changer la police du titre principal et du menu sur le mini-site à onglet</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/364">#364 Billet: Agrandir l'image du bas</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/365">#365 Billet: Centrer le nom de l'invité et de l'entreprise</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/363">#363 Sessions: Ajouter une notification pour session complète</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/367">#367 Liste Invités: remplacer le filtre numérique de la colonne Participants par un booleen</a></li>
                    <li><a href="https://projets-labinfo.he-arc.ch/gitlab/ticb/17ticb18/web-app/issues/366">#366 Email non reçu</a></li>
                </ul>
            </section>
        </div>
    </div>
@endsection
