@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.admin.create_new', 1)) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('admins.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admins', 'class' => 'form-horizontal', 'files' => true]) !!}

                            @include ('users.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
