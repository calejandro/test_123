@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.user.user', 1)) }} #{{ $user->id }}</div>
                    <div class="panel-body">
                        <a href="{{ route('admins.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($user, [
                            'method' => 'PATCH',
                            'url' => '/admins/' . $user->id,
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('users.form', ['submitButtonText' => ucfirst(trans_choice('interface.update', 1))])

                        {!! Form::close() !!}


                        <div class="col-md-5 col-sm-12">
                          <h2>Permissions attribuées</h2>
                          <p>
                            Les permissions suivantes sont associées à l'utilisateur ayant pour adresse mail <strong>{{ $user->email }}</strong>.
                          </p>
                          @forelse($user->permissions as $permission)
                          <span class="label label-default">{{ $permission->name }}</span>
                          @empty
                          <p>
                            Aucune permission définie pour cet utilisateur !
                          </p>
                          @endforelse

                          <hr/>
                          <a href="{{ route('admin_edit_permissions', [$user->id])}}" class="btn btn-primary btn-sm">Éditer les permissions</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
