@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ ucfirst(trans_choice('models.admin.admin', 2)) }}
                    
                        <div class="pull-right">
                            <a href="{{ url('/admins/create') }}" class="btn btn-success btn-sm" title="Add New user">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.add')) }}
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>{{ ucfirst(__('validation.attributes.name')) }}</th><th>{{ ucfirst(__('validation.attributes.email')) }}</th><th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->email }}</td>
                                        <td class="text-right">
                                            <a href="{{ url('/admins/' . $item->id . '/edit') }}" title="Edit user"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admins', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete user',
                                                        'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->name])) . "')"
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
