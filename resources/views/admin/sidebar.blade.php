<div class="col-md-3 left-sidebar" id="app-left-sidebar">
    <div class="panel panel-default panel-flush">
        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('admin.dashboard.changelog') }}" class="{{ Active::checkRoute(['admin.dashboard.changelog']) }}">
                        <i class="fa fa-code-fork" aria-hidden="true"></i> Changelog
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ route('companies.index') }}" class="{{ Active::checkRoute(['companies.index', 'companies.create', 'companies.update']) }}">
                        <i class="fa fa-building-o" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.company.company', 2)) }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ route('admins.index') }}" class="{{ Active::checkRoute(['admins.*']) }}">
                        <i class="fa fa-user-secret" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.admin.admin', 2)) }}
                    </a>
                </li>
                
                <li role="presentation">
                    <a href="{{ route('checkin.index') }}" target="_blank" class="{{ Active::checkRoute(['checkin.*']) }}" data-test="side-bar-title-checkin">
                        <i class="fa fa-ticket" aria-hidden="true"></i> {{ __('interface.menu.Checkin') }}
                    </a>
                </li>
            </ul>

            @include('common._mobileUserNavGroup')
        </div>
    </div>
</div>