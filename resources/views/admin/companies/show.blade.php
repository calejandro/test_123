@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.company.company', 1)) }} #{{ $company->id }}</div>
                    <div class="panel-body">

                        @if ($company->lock)
                            <a href="{{ route('companies._unlock', [$company]) }}" title="Unlock Company"><button class="btn btn-success btn-xs"><i class="fa fa-lock" aria-hidden="true"></i> {{ ucfirst(__('interface.unlock')) }}</button></a>
                        @else
                            <a href="{{ route('companies._lock', [$company]) }}" title="Lock Company"><button class="btn btn-danger btn-xs"><i class="fa fa-unlock" aria-hidden="true"></i> {{ ucfirst(__('interface.lock')) }}</button></a>
                        @endif
                        <a href="{{ route('companies.edit', [$company]) }}" title="Edit Company"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}</button></a>
                        
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/companies', $company->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Company',
                                    'onclick'=>'return confirm("' . ucfirst(__('interface.confirm_delete')) . '")'
                            )) !!}
                        {!! Form::close() !!}

                        <a href="{{ route('companies.users.index', [$company]) }}" title="Show users"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.open')) }}</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>#</th><td>{{ $company->id }}</td>
                                    </tr>
                                    <tr><th> {{ ucfirst(__('validation.attributes.companyName')) }} </th><td> {{ $company->companyName }} </td></tr>
                                    <tr><th> {{ ucfirst(__('validation.attributes.last_name')) }} </th><td> {{ $company->lastname }} </td></tr>
                                    <tr><th> {{ ucfirst(__('validation.attributes.first_name')) }} </th><td> {{ $company->firstname }} </td></tr>
                                    <tr><th> {{ ucfirst(__('validation.attributes.phone')) }} </th><td> {{ $company->phone }} </td></tr>
                                    <tr><th> {{ ucfirst(__('validation.attributes.email')) }} </th><td> {{ $company->email }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
