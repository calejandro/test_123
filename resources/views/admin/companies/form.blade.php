<div class="form-group {{ $errors->has('companyName') ? 'has-error' : ''}}">
    {!! Form::label('companyName', ucfirst(__('validation.attributes.companyName')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('companyName', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('companyName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    {!! Form::label('firstname', ucfirst(__('validation.attributes.first_name')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
    {!! Form::label('lastname', ucfirst(__('validation.attributes.last_name')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', ucfirst(__('validation.attributes.phone')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', ucfirst(__('validation.attributes.email')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('interface.create')), ['class' => 'btn btn-primary']) !!}
    </div>
</div>
