@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.company.company', 1)) }} #{{ $company->id }}</div>
                    <div class="panel-body">
                        <a href="{{ route('companies.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }} </button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($company, [
                            'method' => 'PATCH',
                            'url' => ['/companies', $company->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.companies.form', ['submitButtonText' => ucfirst(__('interface.update'))])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
