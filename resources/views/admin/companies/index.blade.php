@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ ucfirst(trans_choice('models.company.company', 2)) }}
                        <div class="pull-right">
                            <a href="{{ route('companies.create') }}" class="btn btn-success btn-sm" title="{{ ucfirst(__('interface.add')) }}">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.add')) }}
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <!--<div>
                            {!! Form::open(['method' => 'GET', 'url' => '/companies', 'class' => '', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="{{ ucfirst(__('interface.search')) }}...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>

                        <br/>-->

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>{{ ucfirst(__('validation.attributes.companyName')) }}</th><th> {{ ucfirst(__('validation.attributes.first_name')) }} </th><th>{{ ucfirst(__('validation.attributes.last_name')) }}</th><th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->companyName }}</td>
                                        <td>{{ $item->firstname }}</td>
                                        <td>{{ $item->lastname }}</td>
                                        <td class="text-right">
                                            <a href="{{ route('companies.show', [$item]) }}" class="btn btn-info btn-xs" title="View Company">
                                                <i class="fa fa-eye" aria-hidden="true"></i> {{ ucfirst(__('interface.show')) }}
                                            </a>
                                            <a href="{{ route('companies.edit', [$item]) }}" class="btn btn-primary btn-xs" title="Edit Company">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}
                                            </a>

                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/companies', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Company',

                                                        'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->companyName])) . "')"
                                                )) !!}
                                            {!! Form::close() !!}

                                            <div class="btn-ctrl-div">
                                                @if ($item->lock)
                                                    <a href="{{ route('companies._unlock', [$item]) }}" 
                                                        class="btn btn-success btn-xs"
                                                        title="Unlock Company"
                                                        onclick='return confirm("{{ ucfirst(__('interface.companies_page.confirm_unlock', ['name' => $item->companyName])) }}")'>
                                                        <i class="fa fa-lock" aria-hidden="true"></i> {{ ucfirst(__('interface.unlock')) }}
                                                    </a>
                                                @else
                                                    <a href="{{ route('companies._lock', [$item]) }}"
                                                        class="btn btn-danger btn-xs"
                                                        title="Lock Company"
                                                        onclick='return confirm("{{ ucfirst(__('interface.companies_page.confirm_lock', ['name' => $item->companyName])) }}")'
                                                        >
                                                        <i class="fa fa-unlock" aria-hidden="true"></i> {{ ucfirst(__('interface.lock')) }}
                                                    </a>
                                                @endif

                                                <a href="{{ route('companies.users.index', [$item]) }}" class="btn btn-success btn-xs" title="Show users">
                                                   <i class="fa fa-folder-open-o" aria-hidden="true"></i> {{ ucfirst(__('interface.open')) }}
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $companies->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
