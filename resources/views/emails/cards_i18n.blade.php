{{-- -------------------------------------
-------- Email i18n cards display --------
------------------------------------------
--
-- A list of cards representing the emails
-- @param $emails
-- @param $event
--}}

@foreach($emails as $item)
    <div class="email-card">
        <div class="email-icon-section bg-success">
        <p class="email-icon"><i class="fa fa-check-square" aria-hidden="true"></i></p>
        </div>
        <h1>{{ $item->title }}</h1>
        <div class="email-content-section">
        @can('edit', App\Email::class)
            <div class="btn-group btn-group-justified" role="group" aria-label="Edit buttons">
                @foreach ($event->getLangsAttribute() as $lang)

                <a class="btn btn-primary" href="{{ route('events.emails.edit.lang', [$event, $item, $lang]) }}" data-test="email-confirm-button-edit-{{$lang}}">
                    @if(!$item->hasContentInLang($lang))
                    <i class="text-warning fa fa-warning"></i>
                    @endif
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }} <span class="badge">{{ $lang }}</span>
                </a>
            @endforeach
            </div>
        @endcan

        @can('index', App\Email::class)
            <div class="btn-group btn-group-justified" role="group" aria-label="Show buttons">
                @foreach ($event->getLangsAttribute() as $lang)
                    <a class="btn btn-info" href="{{ route('events.emails.render.lang', [$event, $item, $lang]) }}" target="_blank" data-test="email-confirm-button-preview-{{$lang}}" title="Preview">
                        <i class="fa fa-eye" aria-hidden="true"></i> {{ ucfirst(__('interface.show')) }} <span class="badge">{{ $lang }}</span>
                    </a>
                @endforeach
            </div>
        @endcan
        </div>
    </div>
@endforeach
