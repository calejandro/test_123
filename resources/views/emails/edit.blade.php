@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading" id="head-panel">
          @if ($email->isInvit)
            {{ ucfirst(trans_choice('models.email.invitEmail', 1)) }}
          @elseif ($email->isConfirm)
            {{ ucfirst(trans_choice('models.email.confirmEmail', 1)) }}
          @else
            {{ ucfirst(trans_choice('models.email.otherEmail', 1)) }}
          @endif

          <div class="pull-right">
            <button class="btn btn-warning btn-sm btn-right-divid btn-collapse-panel" data-test="template-button">
              <i class="fa fa-clone" aria-hidden="true"></i> {{ ucfirst(__('interface.templates')) }}
            </button>

            <button class="btn btn-danger btn-sm" id="btn-cancel">
              <i class="fa fa-ban" aria-hidden="true"></i> {{ ucfirst(__('interface.cancel')) }}
            </button>

            <button class="btn btn-primary btn-sm" id="btn-save">
              <i class="fa fa-floppy-o" aria-hidden="true"></i> {{ ucfirst(__('interface.save')) }}
            </button>

            @if ($mailJobNotAlreadyRunning AND $verificationJobNotAlreadyRunning)
            <button class="btn btn-success btn-sm" disabled="true" id="btn-send">
              <i class="fa fa-paper-plane" aria-hidden="true"></i> {{ ucfirst(__('interface.send')) }}
            </button>
            @endif
          </div>


        </div>

        <div class="panel-body">

          <!-- JSON informations -->
          <div class="hidden-values" id="json-mail-infos">{{ $email->toJson() }}</div>
          <script>window.seedTemplateMailInfos = {!! $template !!}</script>
          <script>window.seedTemplateMailInfos_fr = {!! $email->template_fr !!}</script>
          <script>window.seedTemplateMailInfos_en = {!! $email->template_en !!}</script>
          <script>window.seedTemplateMailInfos_de = {!! $email->template_de !!}</script>
   
          <div class="hidden-values" id="json-event-infos">{{ $event->toJson() }}</div>

          @if($extendedFields != "null")
          <div class="hidden-values" id="json-extendedfields-infos">{{ $extendedFields }}</div>
          @endif

          @if(isset($lang))
          <div class="hidden-values" id="json-lang">{{ $lang }}</div>
          @endif

          @if ($event->advanced_minisite_mode == 1)
          @if(!empty($minisite))
          <div class="hidden-values" id="json-minisite-infos">{{ $minisite->toJson() }}</div>
          @else
          <div class="alert alert-danger guests-list-remove">
            <div class="pull-left">
              <i class="fa fa-warning fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
              <strong>{{ ucfirst(__('interface.messages.no_minisite_created.headline')) }}</strong><br/>
              {{ ucfirst(__('interface.messages.no_minisite_created.subline')) }}
              <a href="{{ route('events.minisites.index', [$event]) }}">{{ ucfirst(__('interface.messages.no_minisite_created.action')) }}</a>
            </div>
          </div>
          @endif
          @endif


          @if (!$mailJobNotAlreadyRunning)
          <div class="alert alert-info guests-list-remove">
            <div class="pull-left">
              <i class="fa fa-info-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
              <strong>{{ ucfirst(__('interface.messages.job_already_running.headline')) }}</strong><br/>
              {{ ucfirst(__('interface.messages.job_already_running.subline')) }}<br/>
            </div>
          </div>
          @endif

          @if (!$verificationJobNotAlreadyRunning)
          <div class="alert alert-info guests-list-remove">
            <div class="pull-left">
              <i class="fa fa-info-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
              <strong>{{ ucfirst(__('interface.messages.verif_job_already_running.headline')) }}</strong><br/>
              {{ ucfirst(__('interface.messages.verif_job_already_running.subline')) }}<br/>
            </div>
          </div>
          @endif

          <div class="hidden-values" id="json-company-infos">{{ $company->toJson() }}</div>


          <div class="panel-collapsible">
            <div class="panel-config-collapsible">
              <h1><i class="fa fa-clone" aria-hidden="true"></i> {{ ucfirst(__('interface.templates')) }}</h1>

              <button class="btn btn-outline pull-right btn-collapsible-close btn-sm"><i class="fa fa-fw fa-times"></i></button>

              <button class="btn btn-success btn-sm" id="btn-save-template">
                <i class="fa fa-fw fa-plus"></i>&nbsp;{{ ucfirst(__('interface.save_template')) }}
              </button>

              @if (count($templates) > 0)
                <button class="btn btn-primary btn-sm" id="btn-update-template" data-toggle="modal" data-target="#updateTemplateModal">
                  <i class="fa fa-fw fa-pencil"></i>&nbsp;{{ ucfirst(__('interface.update_existing_template')) }}
                </button>

                <div class="form-inline edit-inline-template-form">
                  <select id="template-load-select" class="form-horizontal form-validate form-control">
                    <option value="0">{{ ucfirst(__('interface.choose_template')) }}</option>
                    @foreach ($templates as $template)
                      <option value="{{$template->id}}">{{ $template->name }}</option>
                    @endforeach
                  </select>

                  <button class="btn btn-warning btn-sm" id="btn-load-template">
                    {{ ucfirst(__('interface.load_template')) }}
                  </button>
                  <button class="btn btn-warning btn-sm" id="btn-delete-template">
                    {{ ucfirst(__('interface.delete_template')) }}
                  </button>
                </div>
              @endif
            </div>
          </div>

          <!-- Template update modal -->
          <div class="modal fade" tabindex="-1" role="dialog" id="updateTemplateModal" aria-labelledby="updateTemplateModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">

                <div class="modal-body">
                  <p><strong>{{ ucfirst(__('interface.template.headline')) }}</strong></p>

                  <div class="alert alert-info">
                    <p>{{ ucfirst(__('interface.template.info')) }}</p>
                  </div>

                  <select id="template-update-select" class="form-control">
                    <option value="0">{{ ucfirst(__('interface.choose_template')) }}</option>
                    @foreach ($templates as $template)
                      <option value="{{$template->id}}">{{ $template->name }}</option>
                    @endforeach
                  </select>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">{{ ucfirst(__('interface.template.cancel')) }}</button>
                  <button type="button" class="btn btn-primary" id="btn-update-existing-template">{{ ucfirst(__('interface.template.update')) }}</button>
                </div>
              </div>
            </div>
          </div>

          

          <div id="app-mail-edit">
            <email-edit
              :advanced-mode={{ $event->advanced_minisite_mode }}
              :email='@json($email)'>
            </email-edit>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
