<div class="table-responsive">
    <table class="table table-borderless">
        <thead>
            <tr>
                <th>{{ ucfirst(__('validation.attributes.sended')) }}</th><th>{{ ucfirst(__('validation.attributes.title')) }}</th><th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($emails as $item)
            <tr>
                <td>{{ $item->sended }}</td><td>{{ $item->title }}</td><td>{{ $item->email }}</td>
                <td class="text-right">
                    <a href="{{ url('/events/' . $event->id . '/emails/' . $item->id . '/edit') }}" title="Edit email"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}</button></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/events/' . $event->id . '/emails/' . $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete email',
                                'onclick'=>'return confirm("' . ucfirst(__('interface.confirm_delete')) . '")'
                        )) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
