@extends('layouts.app')

@section('content')

<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">
          {{ ucfirst(trans_choice('models.email.email', 2)) }}
          <div class="pull-right">
            @can('create', App\Email::class)
            <a href="{{ route('events.emails.createInvit', [$event]) }}" class="btn btn-default btn-sm" title="Add New email">
              <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.emails_page.add_invit')) }}
            </a>
            <a href="{{ route('events.emails.createOther', [$event]) }}" class="btn btn-success btn-sm" title="Add New email">
              <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.emails_page.add_other_email')) }}
            </a>
            @endcan
          </div>
        </div>

        <div class="panel-body">

          <!-- Invitations -->
          <div class="panel panel-default">
            <div class="panel-heading"><h4>{{ ucfirst(trans_choice('models.email.invitEmail', 2)) }}</h4></div>
            <div class="panel-body">
              @include ('emails.cards', ['emails' => $emailsInvit])
            </div>
            <div class="panel-footer">
              @can('create', App\Email::class)
              <a href="{{ route('events.emails.createInvit', [$event]) }}" class="btn btn-default btn-sm" title="Add New email">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.emails_page.add_invit')) }}
              </a>
              @endcan
            </div>
          </div>

          <!-- System -->
          <div class="panel panel-default">
            <div class="panel-heading"><h4>{{ ucfirst(trans_choice('models.email.systemEmail', 2)) }}</h4></div>
            <div class="panel-body">

              @include ('emails.cards_i18n', [
                'emails' => $emailsConfirm,
                'event' => $event
              ])

            </div>
          </div>


          <!-- Others -->
          <div class="panel panel-default">
            <div class="panel-heading"><h4>{{ ucfirst(trans_choice('models.email.otherEmail', 2)) }}</h4></div>
            <div class="panel-body">

              @if($emailEventFull)
                <div class="email-card">
                  <div class="email-icon-section bg-danger">
                    <p class="email-icon"><i class="fa fa-square" aria-hidden="true"></i></p>
                  </div>
                  <h1>{{ $emailEventFull->title }}</h1>
                  <div class="email-content-section">
                    @can('edit', App\Email::class)
                    <a href="{{ route('events.emails.edit', [$event, $emailEventFull]) }}" class="btn btn-primary btn-block" title="Edit email" data-test="email-event-full-edit-button">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}
                    </a>
                    @endcan
                    @can('index', App\Email::class)
                    <a href="{{ route('events.emails.render', [$event, $emailEventFull]) }}" target="_blank" class="btn btn-info btn-block btn-sm" title="Preview">
                      <i class="fa fa-eye" aria-hidden="true"></i> {{ ucfirst(__('interface.show')) }}
                    </a>
                    @endcan
                  </div>
                </div>
              @endif

              @include ('emails.cards', ['emails' => $emailsOther])
            </div>
            <div class="panel-footer">
              @can('create', App\Email::class)
              <a href="{{ route('events.emails.createOther', [$event]) }}" class="btn btn-success btn-sm" title="Add New email">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.emails_page.add_other_email')) }}
              </a>
              @endcan
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  @endsection
