@extends('layouts.guest')

@section('content')

    <br/>
    <div class="container">
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
           
            <a class="navbar-brand" href="#">Eventwise</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if(!empty($lang))
                    <li class="@if (!$showMobile) active @endif">
                        <a href="{{ route('events.emails.render.lang', [$event, $email, $lang, 'desktop']) }}" data-test="menu-render-desktop-button-lang">Desktop</a>
                    </li>
                    <li class="@if ($showMobile) active @endif">
                        <a href="{{ route('events.emails.render.lang', [$event, $email, $lang, 'mobile']) }}" data-test="menu-render-mobile-button-lang">Mobile</a>
                    </li>
                    @else 
                    <li class="@if (!$showMobile) active @endif">
                            <a href="{{ route('events.emails.render', [$event, $email, 'desktop']) }}" data-test="menu-render-desktop-button">Desktop</a>
                        </li>
                        <li class="@if ($showMobile) active @endif">
                            <a href="{{ route('events.emails.render', [$event, $email, 'mobile']) }}" data-test="menu-render-mobile-button">Mobile</a>
                        </li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        </nav>
    </div>

    @if ($showMobile)
        <div class="mobile-template-view">
                @if(!empty($lang))
                <iframe data-test="email-mobile-render-lang" src="{{ route('events.emails.render_template.lang', [$event, $email, $lang]) }}" width="100%" height="627px">
                </iframe>
                @else 
                <iframe data-test="email-mobile-render" src="{{ route('events.emails.render_template', [$event, $email]) }}" width="100%" height="627px">
                </iframe>
                @endif
        </div>
    @else
        <div>
            {!! $renderMail !!}
        </div>
    @endif

@endsection
