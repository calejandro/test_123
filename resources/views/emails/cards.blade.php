@foreach($emails as $item)
    <div class="email-card">
      @if ($item->isInvit)
        <div class="email-icon-section bg-info">
            <p class="email-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></p>
        </div>
      @else
      <div class="email-icon-section bg-primary">
          <p class="email-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></p>
      </div>
      @endif
        <h1>{{ $item->title }}</h1>
        <div class="email-content-section">
             <span class="badge badge-primary">{{ $item->sended == NULL ? 0 : $item->sended }} {{ ucfirst(__('interface.emails_page.sent')) }}</span>
             <p></p>
            @can('edit', App\Email::class)
            <a href="{{ url('/events/' . $event->id . '/emails/' . $item->id . '/edit') }}" title="Edit email">
                <button class="btn btn-primary btn-block btn-sm" data-test="mail-edit-button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}</button>
            </a>
            @endcan
            @can('create', App\Email::class)
                @if(!$item->isConfirm)
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/events/' . $event->id . '/emails/' . $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-block btn-sm',
                                'title' => 'Delete email',
                                'onclick'=>'return confirm("' . ucfirst(__('interface.confirm_delete')) . '")'
                        )) !!}
                    {!! Form::close() !!}


                    {!! Form::open([
                        'method'=>'POST',
                        'url' => ['/events/' . $event->id . '/emails/' . $item->id . '/duplicate'],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::button('<i class="fa fa-files-o" aria-hidden="true"></i> ' . ucfirst(__('interface.duplicate')), array(
                                'type' => 'submit',
                                'class' => 'btn btn-warning btn-block btn-sm',
                                'title' => 'Duplicate email'
                        )) !!}
                    {!! Form::close() !!}
                @endif
            @endcan
            @can('index', App\Email::class)
            <a href="{{ route('events.emails.render', [$event, $item]) }}" target="_blank" class="btn btn-info btn-block btn-sm" title="Preview">
              <i class="fa fa-eye" aria-hidden="true"></i> {{ ucfirst(__('interface.show')) }}
            </a>
            @endcan
        </div>
    </div>
@endforeach
