@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="head-bar-nav-info">
            <h1>{{ $company->companyName }}</h1>
        </div>
        <div class="row">
          @include('config.sidebar')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      {{ ucfirst(trans_choice('models.category.category', 2)) }}

                      <div class="pull-right">
                        <a href="{{ url('/companies/' . $company->id . '/categories/create') }}" class="btn btn-success btn-sm" title="{{ ucfirst(__('models.category.create_new')) }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('models.category.create_new')) }}
                        </a>
                      </div>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>{{ ucfirst(__('models.category.category_name')) }}</th><th>{{ ucfirst(__('models.category.category_description')) }}</th><th class="text-right">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td class="text-right">
                                            <a href="{{ url('/companies/' . $company->id . '/categories/' . $item->id . '/edit') }}" title="{{ ucfirst(__('interface.edit')) }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span style="text-transform:capitalize">{{ ucfirst(__('interface.edit')) }}</span></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/companies/' . $company->id . '/categories', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;' . ucfirst(__('interface.delete')), array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Category',
                                                        'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->name])) . "')"

                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
