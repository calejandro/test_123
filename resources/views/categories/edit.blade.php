@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="head-bar-nav-info">
            <h1>{{ $company->companyName }}</h1>
        </div>
        <div class="row">
            @include('config.sidebar')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(__('models.category.category_edit_following')) }} : <strong>{{ $category->name }}</strong></div>
                    <div class="panel-body">
                        <a href="{{ url('/companies/' . $company->id . '/categories') }}" title="{{ ucfirst(__('interface.back')) }}"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($category, [
                            'method' => 'PATCH',
                            'url' => ['/companies/' . $company->id . '/categories', $category->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('categories.form', ['submitButtonText' => ucfirst(__('interface.update'))])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
