@extends('emails_templates.notifications.layout')

@section('content')

<p>Un invité de votre événement <strong>{{ $eventName }}</strong> a modifié ses informations de base lors de l'inscription.</p>
<h3>Résumé des modifications</h3>

<strong>Données avant modification :</strong> {{ $guestBefore }} <br/>
<strong>Données après modification :</strong> {{ $guestAfter }}

@endsection