@extends('emails_templates.notifications.layout')

@section('content')

<p>Le nombre d'inscrits maximum a été atteint pour la session <strong>{{ $sessionName }}</strong> de votre évènement {{ $eventName }}</p>

@endsection