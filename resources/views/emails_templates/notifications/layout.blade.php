<h2>Notification système Eventwise</h2>

@yield('content')

<br/>
<hr/>

<p>
    <i>Vous recevez cet email car votre adresse mail est associée à l'entreprise {{ $companyName }} sur la plateforme Eventwise.</i>
</p>
<p>
    <i>Ce mail a été envoyé automatiquement. Si vous pensez que c'est une erreur, merci de prendre contact avec Sponsorize : info@sponsorize.ch</i>
</p>