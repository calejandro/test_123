@extends('emails_templates.notifications.layout')

@section('content')

<p>Le nombre d'inscrits maximum a été atteint pour l'événement <strong>{{ $eventName }}</strong>.</p>

@endsection