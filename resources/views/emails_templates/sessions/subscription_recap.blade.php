<style>
  .col-sm-6{
    width: 49%;
    display: inline-block;
    position: relative;
    vertical-align: top;
  }

  .email-p, .email-ul {
    margin-top: 5px;
    margin-bottom: 5px;
    color: black;
  }

  .recap-size{
    font-size: 1.5em;
  }
  .no-group-item {
    list-style-type: circle;
  }

  .mb-5px{
    margin-bottom: 5px;
  }
</style>

<div class="row">
  <div class="col-sm-12">
      <strong>
      <p class="badge-recap only-one-line recap-size email-p" style="{!! $title_style !!}">
        {{ mb_strtoupper(ucfirst(trans_choice('models.email.session_recap', 2))) }}
      </p>
      </strong>
  </div>
</div>
<div class="row mb-5px">
        @foreach ($sessions->split(2) as $column)            
          <div class="col-sm-6">
            <ul class="email-ul">
              @foreach ($column as $key => $group)
              @if ($key != '')
                <li class="sessions-category" style="{!! $category_style !!}"><b>{{ mb_strtoupper($key) }}</b></li>
                <ul class="email-ul">
                  @foreach ($group as $session)
                      <li class="row mb-5px" style="{!! $sessions_style !!}">{{ $session->getEventAwareTranslatedAttribute('name', $event) }}</li>
                  @endforeach
                </ul>
                @else
                    @foreach ($group as $session)
                      <li class="no-group-item" style="{!! $sessions_style !!}"
                      >{{ $session->getEventAwareTranslatedAttribute('name', $event) }}</li>                  
                    @endforeach
                @endif
              @endforeach
            </ul>
          </div>
        @endforeach
</div>