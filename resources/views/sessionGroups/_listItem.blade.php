{{--
    Session group item for lists
    @param: $sessionGroup: item to display
--}}

<div class="inline-badge-card" data-test="session-group-card" data-id="{{ $sessionGroup->id }}">
    <h1 data-test="session-group-name">{{ $sessionGroup->name }}</h1>
    @if($sessionGroup->mandatory || $sessionGroup->allowed_session_subscriptions > 0)
        <div class="inline-badge-card-actions">
            @if ($sessionGroup->mandatory)
                <span class="label label-success">{{ ucfirst(__('models.session_groups.mandatory_icon')) }}</span>
            @endif
            @isset($sessionGroup->allowed_session_subscriptions)
                <span class="label label-info" data-test="session-category-badge">{{ $sessionGroup->allowed_session_subscriptions }}</span>
            @endisset
        </div>
    @endif
    <div class="inline-badge-card-actions">
        @can ('edit', App\Session::class)
            <a href="{{ route('events.session_groups.edit', [$event, $sessionGroup]) }}" class="btn btn-primary btn-xs btn-title-head" title="Edit Session Group">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.edit')) }}</span>
            </a>
            {!! Form::open([
                'method'=>'DELETE',
                'route' => ['events.session_groups.destroy', $event, $sessionGroup],
                'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;' . ucfirst(__('interface.delete')), [
                'type' => 'submit',
                'class' => 'btn btn-danger btn-xs',
                'title' => ucfirst(__('interface.delete')),
                'data-test' => 'session-category-delete-btn',
                'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $sessionGroup->name])) . "')"
            ]) !!}
            {!! Form::close() !!}
        @endcan
    </div>
</div>