{{--
    List of session groups
    @param: $sessionGroups: Colection of available session groups
    @param: $event: related event of displayed session groups
--}}

<div class="panel panel-default">
    <div class="panel-heading">
        {{ ucfirst(trans_choice('models.session_groups.session_group', 2)) }}

        @can ('create', App\Session::class)
            <a href="{{ route('events.session_groups.create', [$event]) }}" 
                class="btn btn-success btn-sm btn-title-head pull-right" 
                data-test="add-session-group"
                title="Add Session Group">
                <i class="fa fa-plus" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.add')) }}</span>
            </a>
        @endcan
    </div>
    <div class="panel-body">

        @forelse($sessionGroups as $sessionGroup)
            @include('sessionGroups._listItem', ['sessionGroup' => $sessionGroup])
        @empty
            <p class="text-center">
                {{ __('models.session_groups.no_item') }}
            </p>
        @endforelse

    </div>
</div>