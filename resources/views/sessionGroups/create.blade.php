@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">

            @include('sidebarEvent')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(__('models.session_groups.create_new')) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('events.sessions.index', [$event]) }}" title="{{ ucfirst(__('interface.back')) }}">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                {{ ucfirst(__('interface.back')) }}
                            </button>
                        </a>

                        @include('common._formErrorSummary', [
                            'errors' => $errors
                        ])

                        {!! Form::open([
                            'route' => ['events.session_groups.store', $event],
                            'class' => 'form-horizontal'
                        ]) !!}

                            @include ('sessionGroups._form', ['sessionGroup' => null])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
