@include('common._i18nFormText', [
    'name' => 'name',
    'label' => ucfirst(__('models.session_groups.name')),
    'langs' => $event->availableLanguages(),
    'errors' => $errors
])

@if ($sessionGroup)
<div class="form-group {{ $errors->has('mandatory') ? 'has-error' : ''}}">
      {!! Form::label('mandatory', ucfirst(trans_choice('models.sessions.mandatory', 2)) , ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::checkbox('mandatory', 1, old('mandatory', $sessionGroup->mandatory)) !!}
          {!! $errors->first('mandatory', '<p class="help-block">:message</p>') !!}
      </div>
  </div>

  <div class="form-group {{ $errors->has('allowed_session_subscriptions') ? 'has-error' : ''}}">
      {!! Form::label('allowed_session_subscriptions', ucfirst(trans_choice('models.sessions.allowed_sessions', 2)) , ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::number('allowed_session_subscriptions', old('allowed_session_subscriptions', $sessionGroup->allowed_session_subscriptions), ['class' => 'form-control', 'min' => 1]) !!}
          {!! $errors->first('allowed_session_subscriptions', '<p class="help-block">:message</p>') !!}
      </div>
  </div>    
@else
    <div class="form-group {{ $errors->has('mandatory') ? 'has-error' : ''}}">
      {!! Form::label('mandatory', ucfirst(trans_choice('models.sessions.mandatory', 2)) , ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::checkbox('mandatory', 1, old('mandatory', 0)) !!}
          {!! $errors->first('mandatory', '<p class="help-block">:message</p>') !!}
      </div>
  </div>

  <div class="form-group {{ $errors->has('allowed_session_subscriptions') ? 'has-error' : ''}}">
      {!! Form::label('allowed_session_subscriptions', ucfirst(trans_choice('models.sessions.allowed_sessions', 2)) , ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::number('allowed_session_subscriptions', old('allowed_session_subscriptions', null), ['class' => 'form-control', 'min' => 1]) !!}
          {!! $errors->first('allowed_session_subscriptions', '<p class="help-block">:message</p>') !!}
      </div>
  </div>    
@endif

<div class="form-group">
  <div class="col-md-offset-4 col-md-6">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('models.session_groups.create_new')), [
      'class' => 'btn btn-primary btn-block',
      'data-test' => 'submit-session-group'
    ]) !!}
  </div>
</div>
