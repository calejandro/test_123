{{--
    Simple error summary in alert-danger for forms
    @param: $errors : ErrorBag with validation errors
--}}
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif