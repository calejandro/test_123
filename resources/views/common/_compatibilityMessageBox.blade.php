<div class="fixed-message-box" id="compatibility-box">
    <div class="alert alert-danger">
        <div class="pull-left alert-icon">
            <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
        </div>
        <div class="adjusted-message">
            {{ __('interface.minisites_page.browser_not_compatible') }}
        </div>
    </div>  
</div>

<script src="{{ mix('js/compatibility/message-box.js') }}"></script>