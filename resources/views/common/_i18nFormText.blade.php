{{--
    i18n text input
    @param: $name : name of the input
    @param: $label : printed (i18n) name
    @param: $langs : array of wanted langs. ex: ['fr','en', 'de']
    @param: $errors: ErrorBag with validation errors
--}}
@foreach ($langs as $lang)
    <div class="form-group {{ $errors->has($name . '_' . $lang) ? 'has-error' : ''}}">
        {!! Form::label($name . '_' . $lang, $label . ' ' .$lang, ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text($name . '_' . $lang, null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first($name . '_' . $lang, '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endforeach