{{--
    Nav group that contains user related links.
    Visible only on mobile.
--}}
<ul class="nav nav-group only-mobile" role="tablist">
    <li role="presentation">
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" data-test="logout-button">
            <i class="fa fa-sign-out" aria-hidden="true"></i> {{ ucfirst(__('interface.logout')) }}
        </a>
    </li>
</ul>