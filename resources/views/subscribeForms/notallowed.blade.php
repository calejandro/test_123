@extends('layouts.guest')

@section('content')
    <p class="text-center">{{ HTML::image('images/logo_eventwise.png', 'Eventwise Logo', ['height' => '80', 'class' => 'logo-full-top']) }}</p>
    <p class="text-center">{{ __('models.subscribeForm.badLink') }}</p>
    <p class="text-center">{{ __('models.subscribeForm.contactOrg') }}</p>
@endsection
