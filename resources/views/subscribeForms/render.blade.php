@extends('layouts.guest')

@section('content')

    <br/>
    <div class="container">
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

            <a class="navbar-brand" href="#">Eventwise</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="@if (!$showMobile) active @endif"><a href="{{ route('events.subscribeForms.render', [$event, 'desktop']) }}">Desktop</a></li>
                    <li class="@if ($showMobile) active @endif"><a href="{{ route('events.subscribeForms.render', [$event, 'mobile']) }}">Mobile</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        </nav>
    </div>

    @if ($showMobile)
        <div class="mobile-template-view">
            <iframe src="{{ route('events.subscribeForms.render_template', [$event]) }}" width="100%" height="627px">
            </iframe>
        </div>
    @else
        <div class="col-md-6">
            {!! $renderForm !!}
        </div>
    @endif


@endsection
