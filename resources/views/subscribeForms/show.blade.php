@extends('layouts.guest')

@section('content')

    @isset($guest)
      <div class="hidden-values" id="json-guest-infos">@json($guest)</div>
    @endisset

    <div class="hidden-values" id="json-required-fields">{{ json_encode($minAttributes) }}</div>
    <div class="hidden-values" id="json-allow-basefield-modification">{{ $subscribeForm->allow_basefields_modification }}</div>
    <div class="hidden-values" id="count-empty-places">{{ $nbEmptyPlaces }}</div>
    <div class="hidden-values" id="json-event-id">{{ $event->id }}</div>
    <div class="hidden-values" id="json-subscribe-form-submit-text">{{ $subscribeForm->submit_text }}</div>

    <div id="subscribe-form-page">

      @include('subscribeForms._subscribeFormStyles', [
          'subscribeForm' => $subscribeForm
      ])

      @if ($nbEmptyPlaces <= 0 && (!isset($guest) || !$guest->subscribed))
        <div class="alert alert-danger alert-margin alert-event-full">{{ __('models.event.guests_limit_reached') }}</div>
      @endif

      {!! $viewBody !!}

    </div>

    @if ($event->public_minisite)    
      <!-- Recaptcha -->
      <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit&hl={{ app()->getLocale() }}" async defer></script>
    @endif
@endsection
