@extends('layouts.guest')

@section('content')
  <div class="hidden-values" id="json-companion-infos">{{ $companion->toJson() }}</div>
  <div class="hidden-values" id="json-language">{{ $companion->guest->language }}</div>
  <div class="hidden-values" id="json-event-id">{{ $event->id }}</div>

  <div id="subscribe-form-page" class="container companion-subscribe-form-page">

    @include('subscribeForms._subscribeFormStyles', [
        'subscribeForm' => $subscribeForm
    ])

    <div id="app-companion-subscribe-form-view">
      <companion-subscribe-form></companion-subscribe-form>
    </div>

  </div>
@endsection
