@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">

          @if ($event->advanced_minisite_mode)
          {{ ucfirst(trans_choice('models.subscribeForm.subscribeForm_advanced', 1)) }}
          @else
          {{ ucfirst(trans_choice('models.subscribeForm.subscribeForm', 1)) }}
          @endif

          <div id="app-template-button" class="pull-right">
            <template-button></template-button>
          </div>
        </div>

        <div class="panel-body">

          <!-- JSON of email informations -->
          <div class="hidden-values" id="json-event-infos">
            {{ $event->toJson() }}
          </div>

          <div class="hidden-values" id="accesshash-infos">{{ $accessHash }}</div>

          <div id="app-subscribe-form-edit">
              <subscribe-form-edit :advanced_mode={{ $event->advanced_minisite_mode }}
                :templates="{{ $templates }}"></subscribe-form-edit>
          </div>       

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
