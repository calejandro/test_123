{{--
    Styles to apply for the subscribe form
    @param: $subscribeForm: SubscribeForm
--}}

<style>
    @if($subscribeForm->label_color == 0)
        label, h5 span, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p {color: #777777; font-weight: bold;}
    @elseif($subscribeForm->label_color == 1)
        label, h5 span, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p{color: #FFFFFF; font-weight: bold;}
    @else
        label, h5 span, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p {color: #000000; font-weight: bold;}
    @endif
    #submit {
        color: #fff;
    }

    .rendered-form{ padding: 10px; }

    /* Form btns styles */
    .subscribe-form-btn, .subscribe-form-btn:visited{
        background-color: {{ $subscribeForm->submit_color_bg }} !important;
        color: {{ $subscribeForm->submit_color_text }} !important;
        border: none;
    }
    .subscribe-form-btn:hover {
        background-color: {{ $subscribeForm->submit_color_bg }};
        color: {{ $subscribeForm->submit_color_text }};
        opacity: 0.8;
    }
    .subscribe-form-btn:disabled, .subscribe-form-btn[disabled] {
        background-color: {{ $subscribeForm->submit_color_bg }} !important;
        color: {{ $subscribeForm->submit_color_text }} !important;
        opacity: 0.8;
    }
    .subscribe-form-btn:active {
        background-color: {{ $subscribeForm->submit_color_bg }} !important;
        color: {{ $subscribeForm->submit_color_text }} !important;
        opacity: 0.9;
    }
</style>