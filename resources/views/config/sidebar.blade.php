<div class="col-md-2 left-sidebar collapse navbar-collapse" id="app-left-sidebar">
    <div class="panel panel-default panel-flush">
        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                  @can('index', App\User::class)
                  <a href="{{ route('companies.users.index', [$company]) }}" class="{{ Active::checkRoute(['companies.users.*']) }}">
                    <i class="fa fa-user" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.user.user', 2)) }}
                  </a>
                  @endcan
                </li>
                <li role="presentation">
                  <a href="{{ route('companies.categories.index', [$company]) }}" class="{{ Active::checkRoute(['companies.categories.*']) }}">
                    <i class="fa fa-filter" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.category.category', 2)) }}
                  </a>
                </li>
                <li role="presentation">
                  @if(in_array('configurations-update', Auth::user()->permissions()->get()->pluck('name')->toArray()))
                  <a href="{{ route('companies.config.update', [$company]) }}" class="{{ Active::checkRoute(['companies.config']) }}">
                    <i class="fa fa-cogs" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.company.configuration_smtp', 2)) }}
                  </a>
                  @endif
                </li>
            </ul>

            @include('common._mobileUserNavGroup')
        </div>
    </div>
</div>
