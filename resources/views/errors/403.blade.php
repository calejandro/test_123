@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">

            <h1 style="font-size: 5em; font-weight: 800; position: relative; margin-top: 20%;">403</h1>
            <p class="lead">Cette action n'est pas autorisée.</p>

            </div>
        </div>
    </div>
@endsection
