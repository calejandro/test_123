@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">

          <h1 style="font-size: 5em; font-weight: 800; position: relative; margin-top: 20%;">500</h1>
          <p class="lead">Une erreur est survenue, veuillez contacter un administrateur.</p>

          @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
            <div class="subtitle">Code d'erreur: {{ Sentry::getLastEventID() }} </div>

            <!-- Sentry JS SDK 2.1.+ required -->
            <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

            <script>
                Raven.showReportDialog({
                    eventId: '{{ Sentry::getLastEventID() }}',
                    // use the public DSN (dont include your secret!)
                    dsn: '{{ config('sentry.public_dsn') }}',
                    user: {
                        'name': '',
                        'email': '',
                    }
                });
            </script>
        @endif

        </div>
    </div>
</div>
@endsection
