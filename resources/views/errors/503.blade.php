@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">

        <h1 class="maintenance-message-title">{{ __('interface.maintenance.title') }}</h1>
          <p class="lead">{{ __('interface.maintenance.message') }}</p>

        </div>
    </div>
</div>
@endsection
