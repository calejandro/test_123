
@extends('layouts.minisite')

@section('title')
  {{ $minisite->{'title_' . $lang} }} - {{ $page->{'name_' . $lang} }}
@endsection

@section('style')

/* Page general elements */
@if ($minisite->site_background_image == null)
  body{ background-color: {{ $minisite->site_background_color }}; }
@else
  body{ background-image: url('{{ asset($minisite->site_background_image) }}'); background-size:cover; background-attachment: fixed;}
@endif

/* Site title */
.site-title{
  color: {{ $minisite->title_font_color }};
  font-size: {{ $minisite->title_font_size }}px;
  font-family: {!! $minisite->title_font !!};
  margin-bottom: {!! $minisite->title_menu_spacing !!}px;
  text-align: {!! $minisite->title_alignment !!};
}

/* Menu elements */
.navbar-default{
  background-color: {{ $minisite->menu_background_color }};
  border:none;
}

/* Responsive menu toggler colors adaptations */
.navbar-default .navbar-toggle{
  border-color: {{ $minisite->menu_font_color }};
}

.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus{
  background-color: {{ $minisite->menu_item_hover_background_color }};
  color: {{ $minisite->menu_item_hover_font_color }};
}

.navbar-default .navbar-toggle .icon-bar{
  background-color: {{ $minisite->menu_font_color }};
}

.navbar-default .navbar-nav > li > a {
  font-size: {{ $minisite->menu_font_size }}px;
  color: {{ $minisite->menu_font_color }};
  font-family: {!! $minisite->menu_font !!};
}

/* Navbar active */
.navbar-default .navbar-nav > .active > a {
  font-weight: bold;
  background-color: {{ $minisite->menu_item_active_background_color }};
  color: {{ $minisite->menu_item_active_font_color }};
}

/* Navbar hover */
.navbar-default .navbar-nav > li > a:hover {
  background-color: {{ $minisite->menu_item_hover_background_color }};
  color: {{ $minisite->menu_item_hover_font_color }};
}

/* Content elements */
.content{
  background-color: {{ $minisite->content_background_color }};
  border-radius: 3px;
  padding-top: 10px;
  padding-bottom: 10px;
}

/* Responsive adjustments */
iframe{position:relative; max-width: 100%;}
img {max-width: 100%; height: auto;}
@endsection

@section('content')

<div class="container minisite">
  <div class="col-md-12">

    @if ($minisite->site_top_image)
    <div class="row">
      <div class="col-md-12 text-{!! $minisite->top_image_alignment !!}">
        <p></p>
        <span style="display: inline-block;">
          <img src="{{ asset($minisite->site_top_image) }}"
            @if($minisite->top_image_size == 0)
            style="height: 100px;"
            @else
            width="{!! $minisite->top_image_size !!}%"
            @endif
            >
        </span>
        <p></p>
      </div>
    </div>
    @endif

    <h1 class="site-title" data-test="site-title">{{ $minisite->{'title_' . $lang} }}</h1>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            @forelse ($pages->sortBy('order_id') as $p)
              @if($p->{'name_' . $lang} != null)

                @if ($demo || !($p->pageType->isTypeSubscribeForm() && isset($companion)) && !($p->pageType->isTypeCompanionSubscribeForm() && !isset($companion)))
                  <li {!! $p->id == $page->id ? 'class="active"' : "" !!} >
                    @if ($hash)
                      <a href="{{ route("minisite_page_subdomain_show", [$minisite->slug, $p->{'slug_' . $lang}, $p, $hash, $lang]) }}" class="m-item" data-test="minisite-tab-{{ $p->{'name_' . $lang} }}">
                        {{ $p->{'name_' . $lang} }}
                      </a>
                    @else
                      <a href="{{ route("minisite_page_subdomain_show", [$minisite->slug, $p->{'slug_' . $lang}, $p, $lang]) }}" class="m-item" data-test="minisite-tab-{{ $p->{'name_'  . $lang} }}">
                        {{ $p->{'name_' . $lang} }}
                      </a>
                    @endif
                  </li>
                @endif
              @endif
            @empty

            @endforelse
          </ul>

          @if (count($event->availableLanguages()) > 1)
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-test="minisite-language-dropdown-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                {{ ucfirst(trans_choice('interface.minisite.languages', 1)) }} <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">

                @forelse ($event->availableLanguages() as $e_lang)
                  <li><a href="{{ route("minisite_subdomain_show", [$minisite->slug, $hash, $e_lang]) }}" data-test="minisite-lang-option" class="m-item">{{ $e_lang }}</a></li>
                @empty

                @endforelse

              </ul>
            </li>
          </ul>
          @endif

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="col-md-12 content" data-test="minisite-page-content">
      {!! $page->{'content_' . $lang} !!}

      <!-- Every data needed when using forms -->
      @isset($guest)
        <div class="hidden-values" id="json-guest-infos">@json($guest)</div>
      @endisset

      @isset($companion)
        <div class="hidden-values" id="json-companion-infos">@json($companion)</div>
      @endisset

      <div class="hidden-values" id="json-event-id">{{ $event->id }}</div>
      <div class="hidden-values" id="json-required-fields">{{ json_encode($minAttributes) }}</div>
      <div class="hidden-values" id="json-allow-basefield-modification">{{ $subscribeForm->allow_basefields_modification }}</div>
      <div class="hidden-values" id="json-subscribe-form-submit-text">{{ $subscribeForm->submit_text }}</div>
      <div class="hidden-values" id="json-survey-form-submit-text">{{ $surveyForm->submit_text }}</div>
      <div class="hidden-values" id="json-language">{{ $lang }}</div>


      @if ($page->pageType->isTypeContent())
        <script src="{{ mix('js/minisite/index.js') }}"></script>
      @endif

      @if ($page->pageType->isTypeSubscribeForm() || $page->pageType->isTypeCompanionSubscribeForm())

        <hr/>
          <style>
            @if($subscribeForm->label_color == 0)
              label,
              .rendered-form .btn,
              .rendered-form h5, .sv_main h5,
              .rendered-form h4, .sv_main h4,
              .rendered-form h3, .sv_main h3,
              .rendered-form h2, .sv_main h2,
              .rendered-form h1, .sv_main h1, .sv_main .small,
              .rendered-form p {color: #777777; font-weight: bold;}
            @elseif($subscribeForm->label_color == 1)
              label,
              .rendered-form .btn,
              .rendered-form h5, .sv_main h5,
              .rendered-form h4, .sv_main h4,
              .rendered-form h3, .sv_main h3,
              .rendered-form h2, .sv_main h2,
              .rendered-form h1, .sv_main h1, .sv_main .small,
              .rendered-form p {color: #FFFFFF; font-weight: bold;}
            @else
              label,
              .rendered-form .btn,
              .rendered-form h5, .sv_main h5,
              .rendered-form h4, .sv_main h4,
              .rendered-form h3, .sv_main h3,
              .rendered-form h2, .sv_main h2,
              .rendered-form h1, .sv_main h1, .sv_main .small,
              .rendered-form p {color: #000000; font-weight: bold;}
            @endif

            /* Form btns styles */
            .subscribe-form-btn, .subscribe-form-btn:visited {
              background-color: {{ $subscribeForm->submit_color_bg }} !important;
              color: {{ $subscribeForm->submit_color_text }} !important;
              border: none;
            }
            .subscribe-form-btn:disabled, .subscribe-form-btn[disabled] {
              background-color: {{ $subscribeForm->submit_color_bg }} !important;
              color: {{ $subscribeForm->submit_color_text }} !important;
              opacity: 0.8;
            }
            .subscribe-form-btn:hover {
              background-color: {{ $subscribeForm->submit_color_bg }};
              color: {{ $subscribeForm->submit_color_text }};
              opacity: 0.8;
            }
            .subscribe-form-btn:active {
              background-color: {{ $subscribeForm->submit_color_bg }} !important;
              color: {{ $subscribeForm->submit_color_text }} !important;
              opacity: 0.9;
            }

            .fb-button button[type=submit] {
              display: none;
            }

          </style>

          @if ($event->empty_places <= 0 && (!isset($guest) || !$guest->subscribed))
            <div class="alert alert-danger alert-margin alert-event-full">{{ __('models.event.guests_limit_reached') }}</div>
          @endif

      @endif


      @if ($page->pageType->isTypeSubscribeForm())
        <!-- Subscribe form is displayed here -->
        <div id="app-subscribe-form-view">

          @if ($event->advanced_subscription_form_mode)

            <div id="app-advanced-subscribe-form-view">
              <advanced-subscribe-form
                :event-id="{{ $event->id }}"
                @isset($guest)
                  :guest='@json($guest)'
                @endisset
                :allow-base-field-modification="{{ $subscribeForm->allow_basefields_modification }}"
              ></advanced-subscribe-form>
            </div>

          @else

            <div id="subscribe-form-page">
              {!! $subscribeForm->formHtml !!}
            </div>

            <subscribe-form></subscribe-form>

          @endif

        </div>

        <!-- Scripts -->
        <script src="{{ mix('js/subscribe/index.js') }}"></script>

      @elseif ($page->pageType->isTypeCompanionSubscribeForm())

        <div id="app-companion-subscribe-form-view">
          <companion-subscribe-form></companion-subscribe-form>
        </div>

        <!-- Scripts -->
        <script src="{{ mix('js/subscribe/index.js') }}"></script>

      @elseif ($page->pageType->isTypeSurveyForm())
        <hr/>

        <style>
          @if($surveyForm->label_color == 0)
            label,
            .rendered-form h5, .sv_main h5,
            .rendered-form h4, .sv_main h4,
            .rendered-form h3, .sv_main h3,
            .rendered-form h2, .sv_main h2,
            .rendered-form h1, .sv_main h1, .sv_main .small,
            .rendered-form .btn{color: #777777; font-weight: bold;}
          @elseif($surveyForm->label_color == 1)
            label,
            .rendered-form h5, .sv_main h5,
            .rendered-form h4, .sv_main h4,
            .rendered-form h3, .sv_main h3,
            .rendered-form h2, .sv_main h2,
            .rendered-form h1, .sv_main h1, .sv_main .small,
            .rendered-form .btn{color: #FFFFFF; font-weight: bold;}
          @else
            label,
            .rendered-form h5, .sv_main h5,
            .rendered-form h4, .sv_main h4,
            .rendered-form h3, .sv_main h3,
            .rendered-form h2, .sv_main h2,
            .rendered-form h1, .sv_main h1, .sv_main .small,
            .rendered-form .btn{color: #000000; font-weight: bold;}
          @endif

          /* Form btns styles */
          .survey-form-btn, .survey-form-btn:visited{
            background-color: {{ $surveyForm->submit_color_bg }} !important;
            color: {{ $surveyForm->submit_color_text }} !important;
            border: none;
          }
          .survey-form-btn:hover {
            background-color: {{ $surveyForm->submit_color_bg }};
            color: {{ $surveyForm->submit_color_text }};
            opacity: 0.8;
          }
          .survey-form-btn:active {
            background-color: {{ $surveyForm->submit_color_bg }} !important;
            color: {{ $surveyForm->submit_color_text }} !important;
            opacity: 0.9;
          }
        </style>

        @if ($event->advanced_subscription_form_mode)
          <div id="app-survey-form-view">
            <advanced-survey-form></advanced-survey-form>
          </div>
        @else
          <div id="survey-form-page">
            {!! $surveyForm->formHtml !!}
          </div>
        @endif  
        
        <!-- Scripts -->
        <script src="{{ mix('js/survey/index.js') }}"></script>

      @endif

      </div>


      @if ($minisite->bottom_image_path)

        <div class="row">
        <div class="col-md-12 text-{!! $minisite->bottom_image_alignment !!}">
        <p></p>
        <span style="display: inline-block;">
          <img src="{{ asset($minisite->bottom_image_path) }}" class="minisite-footer-img"
            @if($minisite->bottom_image_size == 0)
              style="height: 100px;"
            @else
              width="{!! $minisite->bottom_image_size !!}%"
            @endif
          >
        </span>
        </div>
        </div>

      @endif

      </div>
      </div>

      @endsection
