{{-- -------------------------------------
------- Minisite page list section -------
------------------------------------------
--
-- A list of pages available for this minisite
--}}

<h4>{{ ucfirst(trans_choice('models.pages.site_pages', 1)) }}</h4>
<p><small>{{ ucfirst(trans_choice('models.pages.under_list', 1)) }}</small></p>
@can('edit', App\Minisite::class)

    <div class="btn-group">
    <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-test="minisite-add-page-button">
        <i class="fa fa-plus fa-fw"></i> {{ ucfirst(trans_choice('interface.add', 1)) }} page <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @forelse ($event->availableLanguages() as $e_lang)
            <li><a href="{{ route('page_create', ['event' =>$event, 'minisite' => $minisite, 'lang' => $e_lang]) }}" data-test="minisite-page-lang-choice-{{ $e_lang }}">{{ $e_lang }}</a></li>
            @empty
            {{ __('interface.minisites_page.no_language') }}
        @endforelse
    </ul>
    </div>

@endcan

{{-- Table of pages --}}

<table class="table table-hover table-bordered pages-table">
    @forelse ($minisite->pages->sortBy('order_id') as $page)
    <tr>
        <td>
        <div>
            <strong><span class="text-muted" style="color:#CCC;">#{{ $page->order_id }}</span>
            @forelse ($page->availableLanguages() as $p_lang)
                <a href="{{ route("minisite_page_subdomain_show", [$minisite->slug, $page->{'slug_' . $p_lang}, $page, $accessHash, $p_lang]) }}" target="_blank">
                {{ str_limit($page->{'name_' . $p_lang}, 15, '...') }}
                </a>
                <small class="text-muted">({{ $p_lang }})</small>&nbsp;
            @empty
                {{ __('interface.minisites_page.no_language') }}
            @endforelse
            </strong>


            {{-- Actions on pages --}}

            <div class="pull-right">
            <div class="row">
                <div class="col-md-12">
                @can('edit', App\Minisite::class)
                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-test="page-edit-properties-button-{{ $page->order_id }}">
                        {{ ucfirst(trans_choice('interface.edit_properties', 1)) }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">

                        @forelse ($event->availableLanguages() as $e_lang)
                        <li>
                        <a href="{{ route('page_edit', [$event, $minisite, $page, $e_lang]) }}" data-test="page-edit-button-{{ $e_lang }}-{{ $page->order_id }}">
                            @if ($page->{'content_' . $e_lang} == "" || is_null($page->{'content_' . $e_lang}))
                            <i class="text-warning fa fa-warning"></i>
                            @else
                            <i class="text-success fa fa-check"></i>
                            @endif

                        {{ $e_lang }}
                        </a>
                    </li>
                    @empty
                        {{ __('interface.minisites_page.no_language') }}
                    @endforelse
                    </ul>
                </div>
                @endcan
                @can('edit', App\Minisite::class)
                <form method="POST" style="display:inline;" action="{{ route('events.minisites.pages.destroy', ['event' => $event, 'minisite' => $minisite, 'page' => $page]) }}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit"
                        class="btn btn-xs btn-danger"
                        onclick="return confirm('{{ ucfirst(__('interface.confirm_delete_object', ['name' => $page->name])) }}')">
                        {{ ucfirst(trans_choice('interface.delete', 1)) }}
                    </button>
                </form>
                @endcan
            </div>
            </div>


            @can('edit', App\Minisite::class)
            <div class="row">
            <div class="col-md-12" style="padding-top: 5px;">
                <div class="pull-right">
                @if ($minisite->pages->count() > 1)
                    @if ($loop->first)
                        <form method="POST" style="display:inline;" action="{{ route('minisite.pages.page_down', ['minisite' => $minisite, 'page' => $page]) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-down"></i></button>
                        </form>
                    @elseif ($loop->last)
                        <form method="POST" style="display:inline;" action="{{ route('minisite.pages.page_up', ['minisite' => $minisite, 'page' => $page]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-up"></i></button>
                        </form>
                    @else

                    <form method="POST" style="display:inline;" action="{{ route('minisite.pages.page_up', ['minisite' => $minisite, 'page' => $page]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-up"></i></button>
                    </form>

                    <form method="POST" style="display:inline;" action="{{ route('minisite.pages.page_down', ['minisite' => $minisite, 'page' => $page]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-xs"><i class="glyphicon glyphicon-chevron-down"></i></button>
                    </form>

                    @endif
                @endif
                </div>
            </div>
            </div>
            @endcan

        </div>
        </div>
        <div>
        <small class="text-muted">
            {{ $page->pageType->name}}
            (
            @forelse ($page->availableLanguages() as $p_lang)
            {{ $p_lang }}&nbsp;
            @empty
            {{ __('interface.minisites_page.no_language') }}
            @endforelse
            )
        </small>
        </div>
    </td>

    </tr>

    @empty

    <p>{{ __('interface.minisites_page.No_minisite_page_create_one_message') }}</p>

@endforelse

</table>
