@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">
          {{ ucfirst(trans_choice('models.minisite.manage_minisite', 1)) }}

          @if($minisite)
            <div class="pull-right">
              <a href="{{ $event->public_minisite ? $minisite->accessLink() : $minisite->accessLink($accessHash) }}"
                target="_blank" class="btn btn-success btn-sm btn-title-head" title="Voir le site" data-test="minisite-view">
                <i class="fa fa-eye" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(trans_choice('interface.show', 1)) }}</span>
              </a>
              @can('edit', App\Minisite::class)
              <a href="{{ route('events.minisites.edit', ['event' => $event, 'minisite' => $minisite]) }}" class="btn btn-primary btn-sm btn-title-head" title="Modifier le site" data-test="minisite-edit-properties-button" >
                <i class="fa fa-pencil" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(trans_choice('interface.edit_properties', 1)) }}</span>
              </a>
              @endcan
            </div>
          @endif
        </div>

        <div class="panel-body">
          @include('common._alert')

          @if($minisite)
            <div class="row">
              <div class="col-md-6">
                @include('minisite._panel_infos', ['event' => $event, 'minisite' => $minisite])
              </div>

              <div class="col-md-6">
                @include('minisite._table_pages', ['event' => $event, 'minisite' => $minisite])
              </div>
            </div>
          @else
            <p>{{ __('interface.minisites_page.No_minisite_create_one_message') }}</p>
            <a href="{{ route('events.minisites.create', ['event' => $event]) }}" class="btn btn-primary btn-sm" data-test="button-create-minisite">{{ ucfirst(__('interface.minisites_page.create_minisite')) }}</a>
          @endif
        </div>
    </div>
  </div>
</div>
</div>

@endsection
