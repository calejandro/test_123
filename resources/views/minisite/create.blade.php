@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="head-bar-nav-info">
        <h1>{{ $event->name }}</h1>
    </div>

    <div class="row">
        @include('sidebarEvent')

        <div class="col-md-10 page-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ ucfirst(__('interface.minisites_create_page.create_title')) }}
                </div>

                <div class="panel-body">

                  @if ($errors->any())
                      <ul class="alert alert-danger">
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  @endif

                    {!! Form::open([
                        'url' => 'events/' . $event->id . '/minisites',
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('minisite.form')

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
