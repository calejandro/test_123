
<h2>{{ ucfirst(__('models.minisite.main_properties')) }}</h2>

@foreach ($event->availableLanguages() as $e_lang)

  <div class="form-group {{ $errors->has('title_' . $e_lang) ? 'has-error' : ''}}">
    {!! Form::label('title_' . $e_lang, ucfirst(__('models.minisite.title')) . ' ' . $e_lang, ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          {!! Form::text('title_' . $e_lang, null, ['class' => 'form-control', 'required' => 'required', 'id' => 'minisite_title_field_' . $e_lang]) !!}
          {!! $errors->first('title_' . $e_lang, '<p class="help-block">:message</p>') !!}
        </div>
      </div>
    </div>
  </div>

@endforeach

<div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
  {!! Form::label('slug', ucfirst(__('models.minisite.slug')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    <div class="row">
      <div class="col-md-8">
        {!! Form::text('slug', null, [
          'class' => 'form-control',
          'required' => 'required',
          'id' => 'slug',
          'maxlength' => 63
        ]) !!}
        {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}

        @if(!$errors->has('slug'))
          <p class="text-muted">{{ ucfirst(__('models.minisite.slug_validation_rules')) }}</p>
        @endif
      </div>
      <div class="col-md-4">.{{ config('app.url') }}</div>
    </div>
  </div>
</div>


<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('site_background_color', ucfirst(__('models.minisite.site_background_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('site_background_color', (isset($minisite)) ? null : 'rgb(225, 225, 225)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('site_background_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>

@if($edit_mode)

  {!! Form::hidden('keep_bg_img_file', '1', ["id" => 'hidden_bg_img_file']) !!}

  @if($minisite->site_background_image != null)
    <div class="row">
      <div class="col-md-4 text-right">
        {!! Form::label('site_background_image', ucfirst(__('models.minisite.site_background_image')), ['class' => 'control-label']) !!}
        <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
      </div>
      <div class="col-md-6">
        <div id="existing_bg_img_bloc">
          <img src="{{ asset($minisite->site_background_image) }}" width="100">
          <button type="button" class="btn btn-danger btn-xs" id="unkeep_bg_img_file">X</button>
          {!! $errors->first('site_background_image', '<p class="help-block">:message</p>') !!}
        </div>

        <div id="new_bg_img_bloc" style="display:none;">
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <div class="col-md-6">
              {!! Form::file('site_background_image', null, ['class' => 'form-control']) !!}
              {!! $errors->first('site_background_image', '<p class="help-block">:message</p>') !!}
            </div>
          </div>
        </div>
      </div>
    </div>

  @else

    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
      <div class="col-md-4 text-right">
          {!! Form::label('site_background_image', ucfirst(__('models.minisite.site_background_image')), ['class' => 'control-label']) !!}
          <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
      </div>
      <div class="col-md-6">
        {!! Form::file('site_background_image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('site_background_image', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

  @endif

@else

  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="col-md-4 text-right">
      {!! Form::label('site_background_image', ucfirst(__('models.minisite.site_background_image')), ['class' => 'control-label']) !!}
      <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
    </div>
    <div class="col-md-6">
      {!! Form::file('site_background_image', null, ['class' => 'form-control']) !!}
      {!! $errors->first('site_background_image', '<p class="help-block">:message</p>') !!}
    </div>
  </div>

@endif


<hr/>


<h2>{{ ucfirst(__('models.minisite.title_properties')) }}</h2>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('title_font_size', ucfirst(__('models.minisite.title_font_size')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::number('title_font_size', (isset($minisite)) ? null : 36, ['class' => 'form-control', 'required' => 'required']) !!} <span>px</span>
      </div>
      {!! $errors->first('title_font_size', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('title_font', ucfirst(__('models.minisite.title_font')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('title_font', App\Minisite::$availableFonts, (isset($minisite)) ? null : 'Raleway', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('title_font', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('title_font_color', ucfirst(__('models.minisite.site_title_font_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('title_font_color', (isset($minisite))  ? null : 'rgb(0, 0, 0)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('title_font_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('title_alignment', ucfirst(__('models.minisite.title_alignment')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('title_alignment', App\Minisite::$availableAlignments, (isset($minisite)) ? null : 'left', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('title_alignment', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('title_menu_spacing', ucfirst(__('models.minisite.title_menu_spacing')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('title_menu_spacing', App\Minisite::$availableSpaces, (isset($minisite)) ? null : 'left', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('title_menu_spacing', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<hr/>

<h2>{{ ucfirst(__('models.minisite.menu_properties')) }}</h2>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_font_size', ucfirst(__('models.minisite.menu_font_size')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::number('menu_font_size', (isset($minisite)) ? null : 12, ['class' => 'form-control', 'required' => 'required']) !!} <span>px</span>
      </div>
      {!! $errors->first('menu_font_size', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_font', ucfirst(__('models.minisite.menu_font')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('menu_font', App\Minisite::$availableFonts, (isset($minisite)) ? null : 'Raleway', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('menu_font', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_font_color', ucfirst(__('models.minisite.menu_font_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_font_color', (isset($minisite)) ? null : 'rgb(0, 0, 0)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_font_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_background_color', ucfirst(__('models.minisite.menu_background_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_background_color', (isset($minisite))  ? null : 'rgb(245, 245, 245)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_background_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<h4>{{ ucfirst(__('models.minisite.menu_hover_properties')) }}</h4>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_item_hover_font_color', ucfirst(__('models.minisite.menu_hover_font_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_item_hover_font_color', (isset($minisite))  ? null : 'rgb(0, 0, 0)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_item_hover_font_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_item_hover_background_color', ucfirst(__('models.minisite.menu_hover_bg_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_item_hover_background_color', (isset($minisite))  ? null : 'rgb(220, 220, 220)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_item_hover_background_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<h4>{{ ucfirst(__('models.minisite.menu_active_properties')) }}</h4>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_item_active_font_color', ucfirst(__('models.minisite.menu_active_font_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_item_active_font_color', (isset($minisite))  ? null : 'rgb(10, 10, 50)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_item_active_font_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('menu_item_active_background_color', ucfirst(__('models.minisite.menu_active_bg_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('menu_item_active_background_color', (isset($minisite))  ? null : 'rgb(220, 220, 220)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('menu_item_active_background_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<hr/>

<h2>{{ ucfirst(__('models.minisite.content_properties')) }}</h2>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('content_background_color', ucfirst(__('models.minisite.content_bg_color')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('content_background_color', (isset($minisite))  ? null : 'rgb(245, 245, 245)', ['class' => 'colorpicker form-control']) !!}
    {!! $errors->first('content_background_color', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<h4>{{ ucfirst(__('models.minisite.property_top_image')) }}</h4>

@if($edit_mode)

  {!! Form::hidden('keep_top_img_file', '1', ["id" => 'hidden_top_img_file']) !!}

  @if($minisite->site_top_image != null)

    <div class="row">
      <div class="col-md-4 text-right">
      <strong>{{ ucfirst(__('models.minisite.top_image')) }}</strong>
      <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
      </div>
      <div class="col-md-6">
        <div id="existing_top_img_bloc">
          <img src="{{ asset($minisite->site_top_image) }}" width="100">
          <button type="button" class="btn btn-danger btn-xs" id="unkeep_top_img_file">X</button>
          {!! $errors->first('site_top_image', '<p class="help-block">:message</p>') !!}
        </div>
      </div>

      <div id="new_top_img_bloc" style="display:none;">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
          <div class="col-md-6">
            {!! Form::file('site_top_image', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('site_top_image', '<p class="help-block">:message</p>') !!}
          </div>
        </div>
      </div>
    </div>

  @else
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
      <div class="col-md-4 text-right">
          {!! Form::label('site_top_image', ucfirst(__('models.minisite.top_image')), ['class' => 'control-label']) !!}
          <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
      </div>
      <div class="col-md-6">
        {!! Form::file('site_top_image', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('site_top_image', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
  @endif

@else

  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="col-md-4 text-right">
        {!! Form::label('site_top_image', ucfirst(__('models.minisite.top_image')), ['class' => 'control-label']) !!}
        <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
    </div>
    
    <div class="col-md-6">
      {!! Form::file('site_top_image', null, ['class' => 'form-control', 'required' => 'required']) !!}
      {!! $errors->first('site_top_image', '<p class="help-block">:message</p>') !!}
    </div>
  </div>

@endif



<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('top_image_alignment', ucfirst(__('models.minisite.top_image_alignment')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('top_image_alignment', App\Minisite::$availableAlignments, (isset($minisite)) ? null : 'left', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('top_image_alignment', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('top_image_size', ucfirst(__('models.minisite.top_image_size')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::number('top_image_size', (isset($minisite)) ? $minisite->top_image_size : 100, ['min' => 1, 'max' => 1000, 'class' => 'form-control', 'required' => 'required']) !!} %
          <span id="helpBlock" class="help-block"><i class="fa fa-info-circle"></i> {{ ucfirst(__('models.minisite.size_percentage')) }}</span>
        </div>
      {!! $errors->first('top_image_size', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<h4>{{ ucfirst(__('models.minisite.property_bottom_image')) }}</h4>

@if($edit_mode)

  {!! Form::hidden('keep_bottom_img_file', '1', ["id" => 'hidden_bottom_img_file']) !!}

  @if($minisite->bottom_image_path != null)

  <div class="row">
    <div class="col-md-4 text-right">
      <strong>{{ ucfirst(__('models.minisite.bottom_image')) }}</strong>
      <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
    </div>
    <div class="col-md-6">
      <div id="existing_bottom_img_bloc">
        <img src="{{ asset($minisite->bottom_image_path) }}" width="100">
        <button type="button" class="btn btn-danger btn-xs" id="unkeep_bottom_img_file">X</button>
        {!! $errors->first('bottom_image_path', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div id="new_bottom_img_bloc" style="display:none;">
      <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        <div class="col-md-6">
          {!! Form::file('bottom_image_path', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('bottom_image_path', '<p class="help-block">:message</p>') !!}
        </div>
      </div>
    </div>
  </div>

  @else

  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="col-md-4 text-right">
        {!! Form::label('bottom_image_path', ucfirst(__('models.minisite.bottom_image')), ['class' => 'control-label']) !!}
        <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
    </div>
    <div class="col-md-6">
      {!! Form::file('bottom_image_path', null, ['class' => 'form-control', 'required' => 'required']) !!}
      {!! $errors->first('bottom_image_path', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  @endif


@else

  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
      <div class="col-md-4 text-right">
          {!! Form::label('bottom_image_path', ucfirst(__('models.minisite.bottom_image')), ['class' => 'control-label']) !!}
          <div><small>{{ ucfirst(__('interface.minisites_update_page.maximum_size', ['size' => '5'])) }}</small></div>
      </div>
      <div class="col-md-6">
        {!! Form::file('bottom_image_path', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('bottom_image_path', '<p class="help-block">:message</p>') !!}
      </div>
  </div>

@endif


<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('bottom_image_alignment', ucfirst(__('models.minisite.bottom_image_alignment')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
          {!! Form::select('bottom_image_alignment', App\Minisite::$availableAlignments, (isset($minisite)) ? null : 'left', ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      {!! $errors->first('bottom_image_alignment', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('bottom_image_size', ucfirst(__('models.minisite.bottom_image_size')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="input-group-inline">
        {!! Form::number('bottom_image_size', (isset($minisite)) ? $minisite->bottom_image_size : 100, ['min' => 1, 'max' => 1000, 'class' => 'form-control', 'required' => 'required']) !!} %
        <span id="helpBlock" class="help-block"><i class="fa fa-info-circle"></i> {{ ucfirst(__('models.minisite.size_percentage')) }}</span>
      </div>
      {!! $errors->first('bottom_image_size', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<hr/>

<div class="form-group">
  <div class="col-md-offset-4 col-md-4">
    @if($edit_mode)
    {!! Form::submit(ucfirst(__('interface.update')), ['class' => 'btn btn-primary', 'data-test' => 'minisite-submit-button']) !!}
    @else
    {!! Form::submit(ucfirst(__('interface.create')), ['class' => 'btn btn-primary', 'data-test' => 'minisite-submit-button']) !!}
    @endif
  </div>
</div>
