

{{-- -------------------------------------
------- Minisite informations panel ------
------------------------------------------
--
-- A list of basic informations about the minisite
--}}
<div class="panel panel-primary">
    <div class="panel-heading">
        {{ ucfirst(__('models.minisite.minisite_infos')) }}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-borderless">
            <tbody>

                <tr>
                <th>{{ ucfirst(__('models.minisite.access')) }}</th>
                <td>
                    @if ($event->public_minisite)
                    <span class="label label-success" date-test="minisite-badge-public"><i class="fa fa-globe" aria-hidden="true"></i> {{ ucfirst(__('interface.public')) }}</span>
                    @else
                    <span class="label label-danger" date-test="minisite-badge-private"><i class="fa fa-user-secret" aria-hidden="true"></i> {{ ucfirst(__('interface.private')) }}</span>
                    @endif
                </td>
                </tr>

                <tr>
                <th>{{ ucfirst(trans_choice('models.minisite.minisite_address', 1)) }}</th>
                <td style="white-space: nowrap;">
                    @forelse ($event->availableLanguages() as $m_lang)
                    <i class="text-success fa fa-check"></i>
                    @if($event->public_minisite)
                    <span class="label label-default">{{ $m_lang }}</span> <a target="_blank" href="{{ $minisite->accessLink(null, $m_lang) }}">{{ $minisite->accessLink(null, $m_lang) }}</a><br/>
                    @else
                    <span class="label label-default">{{ $m_lang }}</span> <a target="_blank" href="{{ $minisite->accessLink($accessHash, $m_lang) }}">{{ $minisite->accessLink($accessHash, $m_lang) }}</a><br/>
                    @endif

                    @empty
                    {{ __('interface.minisites_page.no_configurated_language') }}
                    @endforelse
                </td>
                </tr>

                <tr>
                <th>{{ ucfirst(trans_choice('models.pages.page_number', 1)) }}</th>
                <td>
                    {{ $minisite->pages()->count() }}
                </td>
                </tr>

                <tr>
                <th>{{ ucfirst(trans_choice('interface.last_modification', 1)) }}</th>
                <td>
                    {{ $minisite->updated_at->setTimezone('Europe/Zurich')->format('d.m.Y H:i') }}
                </td>
                </tr>

                <tr>
                    <th>{{ ucfirst(__('interface.events_form.event_available_languages')) }}</th>
                <td>
                    @forelse ($event->availableLanguages() as $m_lang)
                        <span class="label label-default" data-test="available-lang-event-config-{{ $m_lang }}">{{ $m_lang }}</span>&nbsp;
                    @empty
                        {{ __('interface.minisites_page.no_configurated_language') }}
                    @endforelse
                </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>
