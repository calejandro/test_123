@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="head-bar-nav-info">
        <h1>{{ $event->name }}</h1>
    </div>

    <div class="row">
        @include('sidebarEvent')

        <div class="col-md-10 page-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                   {{ ucfirst(__('interface.minisites_update_page.update_title')) }}
                </div>

                <div class="panel-body">

                  {!! Form::model($minisite, [
                      'method' => 'PATCH',
                      'url' => 'events/' . $event->id . '/minisites/' . $minisite->id,
                      'class' => 'form-horizontal',
                      'files' => true
                  ]) !!}

                  @include ('minisite.form', ['submitButtonText' => ucfirst(__('interface.update'))])

                  {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
