@extends('layouts.app')

@section('content')

<div class="container-full">
  @include('events.sidebar')
  <div class="row">
    <div class="col-md-10 page-content">

      <div class="panel panel-default">
        <div class="panel-heading" style="text-transform: capitalize;">{{ ucfirst(trans_choice('models.event.event', 2)) }}

          <div class="pull-right">
            @can('create', App\Event::class)
              <a href="{{ url('/companies/' . $company->id . '/events/create') }}" class="btn btn-success btn-sm" title="Add Event" data-test="event-add-button">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.add')) }}
              </a>
            @endcan
          </div>
        </div>

        <div class="panel-body">
          <!-- Current / Past events switch buttons -->
          <div class="btn-group btn-group-justified" role="group">
            <a href="#" class="btn btn-default active">{{ ucfirst(__('interface.current_future')) }}</a>
            <a href="{{ route('past_events', [$company]) }}" class="btn btn-default">{{ ucfirst(__('interface.finished')) }}</a>
          </div>
            @include('events.filter')
            @include('events.table')
          </div>
        </div>
      </div>
    </div>

    <script>


    </script>

    @endsection
