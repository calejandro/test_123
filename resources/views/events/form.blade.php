<!-- MINISITE SUPPORTED LANGUAGES FORM MODE -->
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <h4>{{ __('interface.events_form.event_languages_title') }}</h4>

    <p class="text-muted">{{ __('interface.events_form.event_languages_infos') }}</p>

    @if($edit_mode)
      <p class="text-muted">
        {{ __('interface.events_form.event_languages_warning_title_change') }}
      </p>
    @endif
    <br/>
  </div>
</div>

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="form-group {{ $errors->has('language_fr') ? 'has-error' : ''}}">
      {!! Form::label('language_fr', __('interface.events_form.event_available_languages'), ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        {!! Form::checkbox('language_fr', '1', $edit_mode ? null : true, ['id' => 'checkbox-support-fr']) !!} <span>Français (FR)</span><br/>
        {!! Form::checkbox('language_de', '1', null, ['id' => 'checkbox-support-de']) !!} <span>Deutsch (DE)</span><br/>
        {!! Form::checkbox('language_en', '1', null, ['id' => 'checkbox-support-en']) !!} <span>English (EN)</span><br/>
      </div>
    </div>
  </div>
</div>


<div class="row">
    <div class="col-md-8 col-md-offset-2">


      <h4>{{ ucfirst(__('interface.events_form.general_informations')) }}</h4>

      @foreach (config('app.locales') as $lang)
      <div id="bloc-fields-{{$lang}}" style="display:none;">
          <span class="badge">{{$lang}}</span>
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name_' . $lang, ucfirst(__('models.event.event_name')), ['class' => 'col-md-4 control-label']) !!} 
            <div class="col-md-6">
              {!! Form::text('name_' . $lang, null, ['class' => 'form-control']) !!}
              {!! $errors->first('name_' . $lang, '<p class="help-block">:message</p>') !!}
            </div>
          </div>
          <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            {!! Form::label('description_' . $lang, ucfirst(__('models.event.event_description')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
              {!! Form::textarea('description_' . $lang, null, ['class' => 'form-control', 'rows' => '3']) !!}
              {!! $errors->first('description_' . $lang, '<p class="help-block">:message</p>') !!}
            </div>
          </div>
          <hr/>
      </div>

      

      @endforeach


      <div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
        {!! Form::label('start_date', ucfirst(__('models.event.event_start_date')), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
          {!! Form::input('text', 'start_date', isset($event) ? $event->start_date->format('d.m.Y H:i') : null, [
            'class' => 'form-control date-picker', 
            'required' => 'required'
          ]) !!}
          {!! $errors->first('start_date', '<p class="help-block">:message</p>') !!}
        </div>
      </div>
      <div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
        {!! Form::label('end_date', ucfirst(__('models.event.event_end_date')), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
          {!! Form::input('text', 'end_date', isset($event) ? $event->end_date->format('d.m.Y H:i') : null, [
            'class' => 'form-control date-picker',
            'required' => 'required'
          ]) !!}
          {!! $errors->first('end_date', '<p class="help-block">:message</p>') !!}
        </div>
      </div>


<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
  {!! Form::label('address', ucfirst(__('models.event.event_address')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::text('address', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('public_minisite') ? 'has-error' : ''}}">
  {!! Form::label('public_minisite', __('models.event.public_minisite'), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::checkbox('public_minisite', 1, isset($event->public_minisite) ? $event->public_minisite : false) !!}
    <br/> ({{ __('interface.events_form.public_minisite_infos') }})
  </div>
</div>

<div id="visible-captcha-bloc"
  style="display:none;"
  class="form-group {{ $errors->has('visible_captcha') ? 'has-error' : ''}}">
  {!! Form::label('visible_captcha', __('models.event.visible_captcha'), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::checkbox('visible_captcha', 1, isset($event->visible_captcha) ? $event->visible_captcha : true) !!}
    <br/> ({{ __('interface.events_form.visible_captcha_infos') }})
  </div>
</div>

<div class="form-group {{ $errors->has('event_id') ? 'has-error' : ''}}">
  {!! Form::label('type_id', ucfirst(trans_choice('models.event.event_type', 2)), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::select('type_id',
    $types,
    null,
    ['class' => 'form-control']) !!}
  </div>
</div>


<div class="form-group {{ $errors->has('company_id') ? 'has-error' : ''}}">
  {!! Form::label('categories[]', ucfirst(trans_choice('models.event.event_category', 2)), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::select('categories[]',
    $categories,
    null,
    ['class' => 'form-control', 'multiple']) !!}
  </div>
</div>

</div>
</div>

<hr/>

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <h4>{{ __('interface.events_form.places_and_companions') }}</h4>

    <p class="text-muted">
      {{ __('interface.events_form.places_and_companions_infos') }}
    </p>
    <br/>
  </div>
</div>

<div class="row">
  <div class="col-md-8 col-md-offset-2">
<div class="form-group {{ $errors->has('nb_places') ? 'has-error' : ''}}">
  {!! Form::label('nb_places', ucfirst(__('models.event.event_places')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::number('nb_places', null, ['class' => 'form-control']) !!}
    {!! $errors->first('nb_places', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('companions_limit') ? 'has-error' : ''}}">
  {!! Form::label('companions_limit', ucfirst(trans_choice('models.event.companions_limit', 2)), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::number('companions_limit', null, ['id' => 'companions-limit', 'class' => 'form-control', 'min' => '0', 'max' => '50']) !!}
    <span id="helpBlock" class="help-block"><i class="fa fa-info-circle"></i> {{ ucfirst(__('models.event.companion_field')) }}</span>
    {!! $errors->first('companions_limit', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div id="nominative-companions-bloc"
  @if(isset($event) && $event->companions_limit == 0)
    style="display:none;"
  @endif
  class="form-group {{ $errors->has('nominative_companions') ? 'has-error' : ''}}">
  {!! Form::label('nominative_companions', __('models.event.nominative_companions'), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::checkbox('nominative_companions', 1, isset($event) ? $event->nominative_companions : false, [
      "disabled" => isset($event) && $event->guests()->active()->count() > 0
    ]) !!}
    <br/><span class="help-block"><i class="fa fa-info-circle"></i> {{ __('interface.events_form.nominative_companions_infos') }}</span>
    <span class="help-block"><i class="fa fa-warning"></i> <strong>{{ __('interface.events_form.nominative_companions_warning') }}</strong></span>
  </div>
</div>

<div class="form-group {{ $errors->has('allow_subscription_update') ? 'has-error' : ''}}">
  {!! Form::label('allow_subscription_update', ucfirst(trans_choice('models.event.allow_subscription_update', 2)), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::checkbox('allow_subscription_update', 1, isset($event) ? $event->allow_subscription_update : true) !!}
    {!! $errors->first('allow_subscription_update', '<p class="help-block">:message</p>') !!}
  </div>
</div>

</div>
</div>


<hr/>

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <h4>{{ __('interface.events_form.coords_organisator') }}</h4>

    <p class="text-muted">
      {{ __('interface.events_form.coords_organisator_infos') }}
    </p>
    <br/>
  </div>
</div>


<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="form-group {{ $errors->has('advanced_minisite_mode') ? 'has-error' : ''}}">
      {!! Form::label('organisator_firstname', ucfirst(__('models.event.organisator_firstname')), ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        {!! Form::text('organisator_firstname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('organisator_firstname', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('organisator_lastname') ? 'has-error' : ''}}">
      {!! Form::label('organisator_lastname', ucfirst(__('models.event.organisator_lastname')), ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        {!! Form::text('organisator_lastname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('organisator_lastname', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('organisator_email') ? 'has-error' : ''}}">
      {!! Form::label('organisator_email', ucfirst(__('models.event.organisator_email')), ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        {!! Form::text('organisator_email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('organisator_email', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    @if(!$edit_mode)
      <div class="alert alert-info">
        <div class="pull-left">
          <i class="fa fa-info-circle fa-fw fa-2x"></i>
        </div>
        <div class="adjusted-message">
          {{ __('interface.events_form.default_controler_will_be_created') }}
        </div>
      </div>
    @endif
  </div>
</div>



<hr/>


<!-- MINISITE ADVANCED/SIMPLE MODE -->
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <h4>{{ __('interface.events_form.mini_site_title') }}</h4>

    @if($edit_mode)

    <p class="text-muted">
      {{ __('interface.events_form.mini_site_infos') }}
    </p>

    @endif

    <br/>
  </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
<div class="form-group {{ $errors->has('advanced_minisite_mode') ? 'has-error' : ''}}">
  {!! Form::label('advanced_minisite_mode', __('models.event.advanced_minisite_mode'), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::radio('advanced_minisite_mode', '0', true, ['data-test' => 'minisite-simple-mode-radio']) !!} <span>{{ ucfirst(__('models.event.mini_site_simple_mode')) }}</span><br/>
    {!! Form::radio('advanced_minisite_mode', '1', null, ['data-test' => 'minisite-advanced-mode-radio']) !!} <span>{{ ucfirst(__('models.event.mini_site_advanced_mode')) }}</span>
  </div>
</div>

<input type="hidden" name="edit_mode" value="{{ $edit_mode }}" />

<hr/>

<div class="form-group">
  <div class="col-md-offset-4 col-md-4">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('models.event.create_new')), ['class' => 'btn btn-primary btn-block', 'data-test' => 'event-create-edit-button']) !!}
  </div>
</div>