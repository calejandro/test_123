@extends('layouts.app')

@section('content')

<div class="container-full">
  @include('events.sidebar')
  <div class="row">
    <div class="col-md-10 page-content">

        <div class="panel panel-default">
            <div class="panel-heading">
                {{ ucfirst(trans_choice('models.event.statistic.statistic_full', 2)) }}

                <div class="pull-right">
                    <a href="{{ route('companies.events.statistics.export', [$company]) }}" class="btn btn-info btn-sm btn-title-head" title="Export">
                        <i class="fa fa-download" aria-hidden="true"></i> {{ ucfirst(__('models.event.statistic.download')) }}
                    </a>
                </div>

                
                <div class="hidden-values" id="json-categories-statistics-infos">{{ json_encode($events_categories) }}</div>
                <div class="hidden-values" id="json-guests-statistics-infos">{{ json_encode($guests_statistics) }}</div>
            </div>

            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>{{ ucfirst(__('models.event.statistic.events_count')) }}</th>
                            <td class="text-right">{{ $events_count }}</td>
                        </tr>
                        <tr>
                            <th>{{ ucfirst(__('models.event.statistic.events_past_count')) }}</th>
                            <td class="text-right">{{ $events_past_count }}</td>
                        </tr>
                        <tr>
                            <th>{{ ucfirst(__('models.event.statistic.events_actual_count')) }}</th>
                            <td class="text-right">{{ $events_actual_count }}</td>
                        </tr>
                        <tr>
                            <th>{{ ucfirst(__('models.event.statistic.events_futur_count')) }}</th>
                            <td class="text-right">{{ $events_futur_count }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="app-event-stats">
                <event-statistics></event-statistics>
            </div>

        </div>

    </div>

    <script>


    </script>

    @endsection
