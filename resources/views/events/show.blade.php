@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    @if(is_null($event->name))
    <span class="badge"> {{$event->getLangsAttribute()[0]}} </span> <strong class="mr-4">{{ $event->getTranslatedAttribute('name', $event->getLangsAttribute()[0]) }}</strong>
    @else
      <h1>{{$event->name}}</h1>
    @endif

  </div>
  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-body">

          <div class="hidden-values">
            <span id="json-event-infos">{{ $event->toJson() }}</span>
          </div>

          @if (count($failedGuests) >= 1)
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-danger">Cet événement semble contenir des invités ayant une adresse mail invalide. Veuillez effectuer un contrôle de la liste d'invités.</div>
            </div>
          </div>
          @endif

          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  @if ($event->public_minisite)
                    <span class="label label-success" data-test="event-badge-public"><i class="fa fa-globe" aria-hidden="true"></i> {{ ucfirst(__('interface.public')) }}</span>
                  @else
                    <span class="label label-danger" data-test="event-badge-private"><i class="fa fa-user-secret" aria-hidden="true"></i> {{ ucfirst(__('interface.private')) }}</span>
                  @endif
                  
                  {{ $event->getEventAwareTranslatedAttribute('name', $event) }}

                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-borderless">
                      <tbody>
                        <tr>
                          <th>{{ ucfirst(__('models.event.event_name')) }}</th>
                          <td> 
                            <strong data-test="event-title">{{ $event->getEventAwareTranslatedAttribute('name', $event) }}</strong>
                          </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.event_description')) }}</th>
                          <td> 
                            <strong data-test="event-description">{{ str_limit($event->getEventAwareTranslatedAttribute('description', $event), 50, '...') }}</strong>
                          </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.dates')) }}</th><td data-test="event-interval-dates"> {{ formatIntervalDates($event->start_date, $event->end_date) }}</td>
                        </tr>
                        <tr>
                            <th>{{ ucfirst(__('interface.events_form.event_available_languages')) }}</th>
                            <td> 
                              @foreach ($event->getLangsAttribute() as $lang)
                            <span class="label label-info" data-test="event-supported-lang-{{$lang}}">{{$lang}}</span>
                              @endforeach
                            </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.event_places')) }}</th><td data-test="event-nb-places">{{ $event->nb_places }}</td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.companions_limit')) }}</th><td>
                            @if($event->companions_limit > 0)
                              <span class="label label-info" data-test="event-cell-companions-yes">{{ ucfirst(__('interface.yes')) }}</span>
                              <span class="label label-info" data-test="event-cell-companions-number">{{ ucfirst(__('interface.limit')) }}: {{ $event->companions_limit }}</span>
                            @else
                              <span class="label label-info" data-test="event-cell-companions-no">{{ ucfirst(__('interface.no')) }}</span> 
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.nominative_companions')) }}</th><td>
                            @if($event->nominative_companions)
                              <span class="label label-info" data-test="event-cell-nominative-companions-yes">{{ ucfirst(__('interface.yes')) }}</span>
                            @else
                              <span class="label label-info" data-test="event-cell-nominative-companions-no">{{ ucfirst(__('interface.no')) }}</span>
                            @endif</td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.allow_subscription_update')) }}</th>
                          <td>
                            @if($event->allow_subscription_update) 
                              <span class="label label-info" data-test="event-cell-nominative-companions-yes">{{ ucfirst(__('interface.yes')) }}</span>
                            @else
                              <span class="label label-info" data-test="event-cell-nominative-companions-no">{{ ucfirst(__('interface.no')) }}</span> 
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('models.event.event_type')) }}</th><td data-test="event-type"><span class="label label-warning">{{ $event->type->name }}</span></td>
                        </tr>
                        <tr>
                          <th data-test="event-category">{{ ucfirst(trans_choice('models.event.event_category', 2)) }}</th>
                          <td>
                            @foreach ($event->categories as $category)
                              <span class="label label-primary">{{ $category->name }}</span>
                            @endforeach
                          </td>
                        </tr>
                        <tr>
                          <th data-test="event-address">{{ ucfirst(__('models.event.event_address')) }}</th><td> {{ $event->address }} </td>
                        </tr>
                        <tr>
                          <th>{{ ucfirst(__('interface.controller_account')) }}</th>
                          <td>
                            {{ $event->getDefaultController()->email }}
                            <span class="pull-right">
                              @can('edit', App\User::class)
                              <a href="{{ url('/companies/' . $company->id . '/users/' . $event->getDefaultController()->id . '/edit') }}" class="btn btn-primary btn-xs" data-test="event-edit-controller-button">{{ ucfirst(__('interface.edit')) }}</a>
                              @endcan
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <th colspan="2">
                            <iframe
                            src="https://www.google.com/maps/embed/v1/place?key={{ config('app.google_map_access_token') }}&q={{ $event->address }}"
                            width="100%"
                            height="200px"
                            frameborder="0"
                            style="border:0"
                            allowfullscreen>
                          </iframe>
                        </th>
                      </tr>

                    </tbody>
                  </table>
                </div>
                @can('edit', App\Event::class)
                <a href="{{ route('companies.events.edit', [$company, $event]) }}" class="btn btn-primary btn-xs" title="{{ ucfirst(__('interface.edit')) }}">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span style="text-transform:capitalize" data-test="event-edit-button">{{ ucfirst(__('interface.edit')) }}</span>
                </a>
                @endcan
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div id="app-event-show">
              @if (in_array('statistics-view', Auth::user()->permissions()->get()->pluck('name')->toArray()))
              <event-stats-show></event-stats-show>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
