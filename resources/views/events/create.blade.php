@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @include('events.sidebar')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(__('models.event.create_new')) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('companies.events.index', [$company]) }}" title="{{ ucfirst(__('interface.back')) }}"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => 'companies/' . $company->id . '/events', 'class' => 'form-horizontal', 'files' => true]) !!}

                            @include ('events.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
