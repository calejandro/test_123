<div class="table-responsive">
  <table class="table table-borderless table-hover table-clickable">
    <thead>
      <tr>
        <th>{{ ucfirst(__('models.event.event_name')) }}</th>
        <th>{{ ucfirst(__('models.event.event_dates')) }}</th>
        <th>&nbsp;</th>
        <th>{{ ucfirst(__('models.event.event_type')) }}</th>
        <th>{{ ucfirst(trans_choice('models.event.event_category', 2)) }}</th>
        <th>{{ ucfirst(__('models.event.event_places')) }}</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      @foreach($events as $index => $item)
      <tr onclick="window.document.location='{{ route('companies.events.show', [$company, $item]) }}';" data-test="event-item" data-id="{{ $item->id }}">
        <td>
            @foreach ($item->getLangsAttribute() as $lang)
              <span class="label label-info" data-test="event-supported-lang-{{$lang}}">{{$lang}}</span>
            @endforeach
            <strong data-test="event-title" data-id="{{ $item->id }}">{{ $item->getEventAwareTranslatedAttribute('name', $item) }}</strong>
        </td>
        <td>
            {{ formatIntervalDates($item->start_date, $item->end_date) }}
        </td>
        <td>
          @if ($item->public_minisite)
            <span class="label label-success"><i class="fa fa-globe" aria-hidden="true"></i> {{ ucfirst(__('interface.public')) }}</span>
          @else
            <span class="label label-danger"><i class="fa fa-user-secret" aria-hidden="true"></i> {{ ucfirst(__('interface.private')) }}</span>
          @endif
        </td>
        <td><span class="label label-warning">{{ $item->type->name }}</span></td>
        <td>
          @foreach($item->categories as $cat)
            <span class="label label-primary">{{$cat->name}}</span>
          @endforeach
        </td>
        <td>{{ $item->nb_places }}</td>
        <td class="text-right">

          @can('edit', App\Event::class)
            <a href="{{ route('companies.events.edit', [$company, $item]) }}" title="{{ ucfirst(__('interface.edit')) }}">
              <button class="btn btn-primary btn-xs" data-test="button-event-{{$item->id}}-edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span style="text-transform:capitalize">{{ ucfirst(__('interface.edit')) }}</span>
              </button>
            </a>
          @endif

          @can('create', App\Event::class)
          {!! Form::open([
            'method'=>'DELETE',
            'route' => ['companies.events.destroy', $company, $item],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;' . ucfirst(__('interface.delete')), array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'data-test' => 'button-event-' . $item->id . '-delete',
            'title' => ucfirst(__('interface.delete')),
            'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->name])) . "')"
            )) !!}
            {!! Form::close() !!}
            @endcan

            <div class="btn-ctrl-div">
              @can('show', App\Event::class)
              <a href="{{ route('companies.events.show', [$company, $item]) }}" title="{{ ucfirst(__('interface.open')) }}"><button class="btn btn-success btn-xs" data-test="button-event-{{$item->id}}-open"><i class="fa fa-folder-open-o" aria-hidden="true"></i> {{ ucfirst(__('interface.open')) }}</button></a>
              @endcan
              @can('create', App\Event::class)
              <a href="{{ route('companies.events.duplicate', [$company, $item]) }}" title="{{ ucfirst(__('interface.duplicate')) }}"><button class="btn btn-info btn-xs" data-test="button-event-{{$item->id}}-duplicate"><i class="fa fa-files-o" aria-hidden="true"></i> {{ ucfirst(__('interface.duplicate')) }}</button></a>
              @endcan
            </div>

          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>


@if (count($events) == 0 AND !isset($filter_categories) AND !Request::is('*/events/past'))

  <div class="col-md-4 col-md-offset-4">
    <div class="alert alert-info alert-dismissible" role="alert">

    <div class="alert-icon-title">
      <div class="icon-left">
        <i class="fa fa-plus-square fa-4x" aria-hidden="true"></i>
      </div>
      <div class="content-title">
        <p data-test="no-event-message">{{ ucfirst(__('interface.not_yet_created_event')) }}</p>
      </div>
    </div>

      <p class="text-justify">{{ ucfirst(__('interface.not_yet_created_info_event')) }}</p>
      <p class="text-right"><a class="btn btn-xs btn-default" href="{{ route('companies.events.create', [$company]) }}"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;{{ ucfirst(__('models.event.create_new')) }}</a></p>
    </div>
  </div>

@endif
