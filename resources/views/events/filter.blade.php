<br/>
<div class="panel panel-default">
  <div class="panel-heading">
    {{ ucfirst(__('interface.filter')) }}

    @if (count($company->categories) > 0)
        <p class="pull-right">
          <button class="btn btn-primary btn-sm" name="filter_btn" onclick="document.getElementById('filter-form').submit();">{{ ucfirst(__('interface.apply')) }}</button>
        </p>
        @endif

  </div>
  <div class="panel-body">

    <div class="row">
      <form name="filter" id="filter-form" method="get">
        {{ csrf_field() }}
        @forelse($company->categories as $cat)
        <div class="col-md-2">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="categories[]" value="{{$cat->id}}"
                @if(isset($filter_categories) && in_array($cat->id, $filter_categories))
                checked
                @endif>
              <small style="margin-top: -32px;">{{$cat->name}}</small></label>
            </div>
          </div>

          @empty

          <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p>
              <i class="fa fa-plus-square fa-2x" aria-hidden="true"></i>&nbsp;
              <strong>{{ ucfirst(__('interface.not_yet_created_cat')) }}</strong>
            </p>
            <p>{{ ucfirst(__('interface.not_yet_created_info_cat')) }}</p>
            <p class="text-right">
              <a class="btn btn-xs btn-default" href="{{ url('/companies/' . $company->id . '/categories/create') }}"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;{{ ucfirst(__('models.category.create_new')) }}</a>
            </p>
          </div>

          @endforelse
        </div>

      </form>
    </div>
  </div>
