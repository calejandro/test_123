@extends('layouts.app')

@section('content')

<div class="container-full">
  @include('events.sidebar')
  <div class="row">
    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading" style="text-transform: capitalize;">{{ ucfirst(trans_choice('models.event.event', 2)) }}</div>
        <div class="panel-body">
          <!-- Current / Past events switch buttons -->
          <div class="btn-group btn-group-justified" role="group">
            <a href="{{ route('companies.events.index', [$company]) }}" class="btn btn-default">{{ ucfirst(__('interface.current_future')) }}</a>
            <a href="#" class="btn btn-default active">{{ ucfirst(__('interface.finished')) }}</a>
          </div>

          @include('events.filter')
          @include('events.table')

        </div>
      </div>
    </div>
  </div>
  @endsection
