<div class="col-md-2 left-sidebar collapse navbar-collapse" id="app-left-sidebar">
    <div class="panel panel-default panel-flush">
        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('companies.events.index', [$company]) }}" class="{{ Active::checkRoute(['companies.events.index']) }}" style="text-transform: capitalize;">
                        <i class="fa fa-calendar" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.event.event', 2)) }}
                    </a>
                </li>
                <li role="presentation">
                  @if (in_array('statistics-view', Auth::user()->permissions()->get()->pluck('name')->toArray()))
                    <a href="{{ route('companies.events.statistics.index', [$company]) }}" class="{{ Active::checkRoute(['companies.events.statistics.*']) }}" style="text-transform: capitalize;">
                        <i class="fa fa-line-chart" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.event.statistic.statistic', 2)) }}
                    </a>
                  @endif
                </li>
            </ul>

            @include('common._mobileUserNavGroup')
        </div>
    </div>
</div>
