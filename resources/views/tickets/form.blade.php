
{!! Form::hidden('keep_image_1', '1', ["id" => 'hidden_image_1']) !!}

@if($ticket->image_1 != null)

<div id="existing_image_1_bloc">
  <div class="form-group {{ $errors->has('image_1') ? 'has-error' : ''}}">
    {!! Form::label('image_1', ucfirst(__('models.ticket.image_1')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <img src="{{ asset($ticket->image_1) }}" width="100">
      <button type="button" class="btn btn-danger btn-xs" id="unkeep_ticket_image_1">X</button>
    </div>
  </div>
</div>

<div id="new_image_1_bloc" style="display:none;">
  <div class="form-group {{ $errors->has('image_1') ? 'has-error' : ''}}">
    {!! Form::label('image_1', ucfirst(__('models.ticket.image_1')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {!! Form::file('image_1', null, ['class' => 'form-control', 'required' => 'required']) !!}
      {!! $errors->first('image_1', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>

@else

<div class="form-group {{ $errors->has('image_1') ? 'has-error' : ''}}">
  {!! Form::label('image_1', ucfirst(__('models.ticket.image_1')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::file('image_1', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('image_1', '<p class="help-block">:message</p>') !!}
  </div>
</div>

@endif

{!! Form::hidden('keep_image_2', '1', ["id" => 'hidden_image_2']) !!}

@if($ticket->image_2 != null)

<div id="existing_image_2_bloc">
  <div class="form-group {{ $errors->has('image_2') ? 'has-error' : ''}}">
    {!! Form::label('image_2', ucfirst(__('models.ticket.image_2')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <img src="{{ asset($ticket->image_2) }}" width="100">
      <button type="button" class="btn btn-danger btn-xs" id="unkeep_ticket_image_2">X</button>
    </div>
  </div>
</div>

<div id="new_image_2_bloc" style="display:none;">
  <div class="form-group {{ $errors->has('image_2') ? 'has-error' : ''}}">
    {!! Form::label('image_2', ucfirst(__('models.ticket.image_2')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {!! Form::file('image_2', null, ['class' => 'form-control', 'required' => 'required']) !!}
      {!! $errors->first('image_2', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>

@else

<div class="form-group {{ $errors->has('image_2') ? 'has-error' : ''}}">
  {!! Form::label('image_2', ucfirst(__('models.ticket.image_2')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::file('image_2', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('image_2', '<p class="help-block">:message</p>') !!}
  </div>
</div>

@endif

<div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
  {!! Form::label('type_id', ucfirst(__('models.ticket.ticket_type')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::select('type_id',
    $ticket_types,
    null,
    ['class' => 'form-control']) !!}
  </div>
</div>


<span id="helpBlock" class="help-block"><i class="fa fa-info-circle"></i> {{ ucfirst(__('models.ticket.empty_field')) }}</span>


{{-- Generaing descrpiption fields in available languages for this event --}}
@foreach ($event->getLangsAttribute() as $lang)
    
  <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description_' . $lang, ucfirst(__('models.ticket.text')) . ' ' . $lang, ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {!! Form::textarea('description_' . $lang, null, ['class' => 'form-control', 'id'=>"ticket-description-" . $lang]) !!}
      
      <div class="progress ticket-description">
        <div id="description-progress-bar-{{$lang}}"
          class="progress-bar progress-bar-success active"
          role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
          <span id="current-progress"></span>
        </div>
      </div>

    {!! $errors->first('description_' . $lang, '<p class="help-block">:message</p>') !!}
    </div>
  </div>

@endforeach

@if( isset($event->extendedFileds) && count($event->extendedFileds) > 0 && isset($ticket->sup_info_field))
  <div class="form-group {{ $errors->has('sup_info_field') ? 'has-error' : ''}}">
    {!! Form::label('sup_info_field', ucfirst(__('validation.attributes.sup_info_field')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {!! Form::select('sup_info_field', ['' => __('models.ticket.no_sup_info_field')] + array_reduce($event->extendedFileds, function($res, $item) { $res[$item] = $item; return $res; }, []), null, ['class' => 'form-control']) !!}
      {!! $errors->first('sup_info_field', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
@endif


<div class="form-group">
  <div class="col-md-offset-4 col-md-4">
    {!! Form::submit(ucfirst(__('models.ticket.update')), ['class' => 'btn btn-primary']) !!}
  </div>
</div>
