@extends('layouts.print')

@section('content')

<style type="text/css" media="all">
    @page{
      margin: 0px;
      padding: 0px;
    }
    body{
      padding: 0px !important;
      margin: 0px !important;
    }
</style>

<div id="four-quarter-badge">

  <!-- Top left case -->
  <div class="topleft-case">

    <div class="quarter-content">


      <!-- Header with image and event name -->
      <div class="header-bloc @if($event->ticket->image_1 === null) header-bloc-full-width @endif">
        @if($event->ticket->image_1 !== null)
          <img src="{{ public_path($event->ticket->image_1) }}" style="header-img">
        @endif
          <div class="header-text">
            <p class="title">{{ str_limit($event->getTranslatedAttribute('name', $lang), 60, '...')  }}</p>
            <p class="badge-date">{{ formatIntervalDates($event->start_date, $event->end_date) }}</p>
            <p class="only-one-line badge-address">{{ $event->address }}</p>
          </div>
      </div>

      <div style="width: 100%;">
        <div>
          <p style="text-align: center; text-transform: uppercase;">{{ Lang::choice('models.ticket.ticket_badge_title', 1, [], $lang) }}</p>
        </div>

        @if($event->ticket->image_2 !== null)
        <div style="text-align: center;">
          <img style="max-width: 80%; margin: 0 auto; max-height: 200px;" src="{{ public_path($event->ticket->image_2) }}">
        </div>
        @endif

        <div>
          <ol class="indications">
            <li>{{ Lang::choice('models.ticket.ticket_badge_print', 1, [], $lang) }}</li>
            <li>{{ Lang::choice('models.ticket.ticket_badge_fold', 1, [], $lang) }}</li>
            <li>{{ Lang::choice('models.ticket.ticket_badge_pocket', 1, [], $lang) }}</li>
          </ol>
        </div>
      </div>

      @if ($sessions->count() > 0)
        @include('emails_templates.sessions.subscription_recap',  ["title_style" => "", "category_style" => "", "sessions_style" => ""])       
      @endif
      
    </div>

  </div>

  <span class="pli-y">{{ Lang::choice('models.ticket.ticket_badge_fold_here', 1, [], $lang) }} (2)</span>

  <div class="topright-case">

    <div class="quarter-content">

      {{--  SPECIAL DISPLAY FOR COMPANY Sustainable Finance Geneva - SFG --}}
      @if($event->company_id !== 78)
        <!-- Header with image and event name -->
        <div class="header-bloc @if($event->ticket->image_1 === null) header-bloc-full-width @endif">
          @if($event->ticket->image_1 !== null)
          <img src="{{ public_path($event->ticket->image_1) }}" style="header-img">
          @endif
          <div class="header-text">
            <p class="title">{{ str_limit($event->getTranslatedAttribute('name', $lang), 60, '...')  }}</p>
            <p class="badge-date">{{ formatIntervalDates($event->start_date, $event->end_date) }}</p>
            <p class="only-one-line badge-address">{{ $event->address }}</p>
          </div>
        </div>
      @endif

      <!-- Name and firstname guest -->
      <div style="width: 100%; text-align:center;">
        @isset($companionId)
          {{ Lang::choice('models.guest.companion', 1, [], $lang) }}
        @endisset
        @if($demo)
          <p class="text-center badge-name"><strong>FIRSTNAME<br/>LASTNAME</strong></p>
        @else
          <p class="text-center badge-name">
            <strong>{{ mb_strtoupper($firstname) }}<br/>{{ mb_strtoupper($lastname) }}</strong>
          </p>
        @endif

        <p class="badge-enterprise only-one-line">
          {{--  SPECIAL DISPLAY FOR COMPANY Sustainable Finance Geneva - SFG --}}
          @if(($event->nominative_companions && isset($company)) || $event->company_id === 78) 
            <strong>{{ mb_strtoupper($company) }}<strong>
          @endif
        </p>

      </div>

      {{--  SPECIAL DISPLAY FOR COMPANY Sustainable Finance Geneva - SFG --}}
      @if($event->ticket->image_2 !== null && $event->company_id === 78)
        <div style="text-align: center;">
          <img style="max-width: 80%; margin: 0 auto; max-height: 200px;" src="{{ public_path($event->ticket->image_2) }}">
        </div>
      @endif

    </div>

  </div>


  <div class="bottomleft-case">

    <div class="pli-x">{{ Lang::choice('models.ticket.ticket_badge_fold_here', 1, [], $lang) }}  (1)</div>

    <div class="quarter-content">

      <!-- Header with image and event name -->
      <div class="header-bloc bottom @if($event->ticket->image_1 === null) header-bloc-full-width @endif">
        @if($event->ticket->image_1 !== null)
        <img src="{{ public_path($event->ticket->image_1) }}" style="header-img">
        @endif
        <div class="header-text">
          <p class="title">{{ str_limit($event->getTranslatedAttribute('name', $lang), 60, '...')  }}</p>
          <p class="badge-date">{{ formatIntervalDates($event->start_date, $event->end_date) }}</p>
          <p class="only-one-line badge-address">{{ $event->address }}</p>
        </div>
      </div>

      <!-- Event description -->

      <p class="qr-code"><span class="qr-code-img">{!! $qrcode !!}</span></p>

      <p class="badge-event-description">
        @if($event->ticket->hasTranslatedAttribute('description', $lang))
          {!! $event->ticket->getTranslatedAttribute('description', $lang) !!}
        @else
          {!! $event->getTranslatedAttribute('description', $lang) !!}
        @endif
      </p>

    </div>

  </div>

  <div class="bottomright-case">

    <div class="quarter-content">

      {{--  SPECIAL DISPLAY FOR COMPANY Sustainable Finance Geneva - SFG --}}
      @if($event->company_id !== 78)
        <!-- Header with image and event name -->
        <div class="header-bloc bottom @if($event->ticket->image_1 === null) header-bloc-full-width @endif">
          @if($event->ticket->image_1 !== null)
            <img src="{{ public_path($event->ticket->image_1) }}" style="header-img">
          @endif
          <div class="header-text">
            <p class="title">{{ str_limit($event->getTranslatedAttribute('name', $lang), 60, '...')  }}</p>
            <p class="badge-date">{{ formatIntervalDates($event->start_date, $event->end_date) }}</p>
            <p class="only-one-line badge-address">{{ $event->address }}</p>
          </div>
        </div>
      @endif

      <!-- Name and firstname guest -->
      <div style="width: 100%; text-align:center;">
        @isset($companionId)
          {{ Lang::choice('models.guest.companion', 1, [], $lang) }}
        @endisset
        @if($demo)
          <p class="text-center badge-name"><strong>FIRSTNAME<br/>LASTNAME</strong></p>
        @else
          <p class="text-center badge-name">
            @if(!empty($firstname) || !empty($lastname))
              <strong>{{ mb_strtoupper($firstname) }}<br/>{{ mb_strtoupper($lastname) }}</strong>
            @else
              <strong>{{ $email }}</strong>
            @endif
          </p>
        @endif

        <p class="badge-enterprise only-one-line">
          @isset($company)
            <strong>{{ mb_strtoupper($company) }}</strong>
          @endisset
        </p>
      </div>

      {{--  SPECIAL DISPLAY FOR COMPANY Sustainable Finance Geneva - SFG --}}
      @if($event->ticket->image_2 !== null && $event->company_id === 78)
        <div style=" text-align: center;">
          <img style="max-width: 80%; margin: 0 auto; max-height: 200px;" src="{{ public_path($event->ticket->image_2) }}">
        </div>
      @endif

    </div>

  </div>

</div>

@endsection
