@extends('layouts.print')

@section('content')

<div class="ticket">
  <div class="container">
    <div class="row">

      <div class="col-sm-4">
        @if($event->ticket->image_1 !== null)
        <img src="{{ public_path($event->ticket->image_1) }}" alt="Logo" class="img img-fluid logo-top" >
        @endif
      </div>

      <div class="col-sm-8">

        <div class="row text-center">
          
          <h1 class="ticket-header">
            {{ $event->getTranslatedAttribute('name', $lang) }}
          </h1>

          <p class="lead">{{ formatIntervalDates($event->start_date, $event->end_date) }}</p>
          <p class="only-one-line">{{ $event->address }}</p>
        </div>

        <p class="text-justify text-description">
          @if($event->ticket->hasTranslatedAttribute('description', $lang))
            {!! $event->ticket->getTranslatedAttribute('description', $lang) !!}
          @else
            {!! $event->getTranslatedAttribute('description', $lang) !!}
          @endif
        </p>

        <hr/>

        <div class="row">

          <div class="col-sm-4">
            <div class="adjusted-qrcode">
              {!! $qrcode !!}
            </div>
          </div>
          <div class="col-sm-8 vertical-center">
            <p>
              <br/><br/>
              <small>{{ Lang::choice('models.ticket.please_present', 1, [], $lang) }}</small><br/>
            </p>
          </div>
        </div>

        <hr/>

        @if ($sessions->count() > 0)
          @include('emails_templates.sessions.subscription_recap', ["title_style" => "", "category_style" => "", "sessions_style" => ""])
        @else
          <br/>    
        @endif

          </div>
        <hr/>
      </div>
    </div>

    @if(isset($companionId))
      <p class="text-center">{{ Lang::choice('models.guest.companion', 1, [], $lang) }}</p>
    @endif
    
    @if($demo)
      <p class="text-center badge-name only-one-line"><strong>FIRSTNAME LASTNAME</strong></p>
    @else

      <p class="text-center badge-name only-one-line">
        @if(!empty($firstname) || !empty($lastname))
          <strong>{{ mb_strtoupper($firstname) }} {{ mb_strtoupper($lastname) }}</strong>
        @else
          <strong>{{ $email }}</strong>
        @endif
      </p>
    
    @endif

    @if(!empty($company))
        <p class="text-center only-one-line">{{ mb_strtoupper($company) }}</p>
    @endif

    <div class="image-bottom-bloc">
      @if($event->ticket->image_2 !== null)
        <img class="img-bottom" src="{{ public_path($event->ticket->image_2) }}" alt="Image 2">
      @endif
    </div>
  </div>
</div>

@endsection
