@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="head-bar-nav-info">
            <h1>{{ ucfirst(__('models.ticket.update'))}}</h1>
        </div>
        <div class="row">
            @include('sidebarEvent')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        {{ ucfirst(trans_choice('models.ticket.ticket_to', 1)) }} <strong>{{ $event->name }}</strong>
                        <div class="pull-right">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">{{ __('interface.ticket_page.Download_demo') }}
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    @foreach ($event->getLangsAttribute() as $lang)
                                            <li>
                                                <a href="{{ route('events.ticket.download_demo', ['event' => $event, 'lang' => $lang]) }}" target="_blank" title="Demo Ticket">
                                                    <i class="fa fa-download" aria-hidden="true"></i><span class="text-link"> </span> {{ $lang }}
                                                </a>
                                            </li>
                                    @endforeach
                                </ul>
                            </div> 
                        </div>
                    </div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($ticket, [
                          'method' => 'PATCH',
                          'url' => route('events.ticket.update', ['event' => $event, 'ticket' => $event->ticket]),
                          'class' => 'form-horizontal',
                          'files' => true]) !!}

                            @include ('tickets.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
