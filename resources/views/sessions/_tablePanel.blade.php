{{--
    Table of sessions in panel
    @param: $sessions: Colection of available sessions
    @param: $event: related event of displayed sessions
--}}

<div class="panel panel-default">
    <div class="panel-heading">
        {{ ucfirst(trans_choice('models.session.session', 2)) }}

        @can ('create', App\Session::class)
            <a href="{{ route('events.sessions.create', [$event]) }}"
                class="btn btn-success btn-sm btn-title-head pull-right"
                data-test="add-session"
                title="Add Session">
                <i class="fa fa-plus" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.add')) }}</span>
            </a>
        @endcan
    </div>
    <div class="panel-body">

        @if($sessions->count() <= 0)
            <p class="text-center">
                {{ ucfirst(__('models.session.no_item')) }}
            </p>
        @else
            @include ('sessions._table')
        @endif

    </div>
</div>