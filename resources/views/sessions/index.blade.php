@extends('layouts.app')

@section('content')
<div class="container-full">

  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">

    @if (!Auth::user()->isController())
      @include('sidebarEvent')
    @endif

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading" id="head-panel">
          {{ ucfirst(trans_choice('models.session.session', 2)) }}
        </div>
          <div class="panel-body ">

            @if ($event->sessionGroups->count() == 0)                
              <form action="{{ route('events.update_sessions_controls', [$event]) }}" method="POST">
                @method('POST')
                @csrf

                <div class="panel panel-default">
                  <div class="panel-heading">
                      {{ ucfirst(trans_choice('models.session_groups.setup', 2)) }}
    
                      @can ('create', App\Session::class)
                          <button type="submit" 
                              class="btn btn-primary btn-sm btn-title-head pull-right" 
                              data-test="update-sesssions-controls"
                              title="Save sessions setup">
                              <i class="fa fa-save" aria-hidden="true"></i>
                              <span class="text-link">{{ ucfirst(trans_choice('models.sessions.save', 2)) }}</span>
                          </button>
                      @endcan
                  </div>
                  <div class="panel-body">
                      <div class="form-group">
                        <label for="mandatory">{{ ucfirst(trans_choice('models.sessions.mandatory', 2)) }}</label>
                        <input id="mandatory" name="mandatory_sessions" type="checkbox" {{ old('mandatory_sessions', $event->mandatory_sessions) ? 'checked' : '' }} >
                      </div>
                      <div class="form-group {{ $errors->has('allowed_session_subscription') ? 'has-error' : '' }}">
                        <label for="maxAllowed">{{ ucfirst(trans_choice('models.sessions.allowed_sessions', 2)) }}</label>
                        <input id="maxAllowed" min="1" name="allowed_session_subscription" type="number" 
                        value="{{ old('allowed_session_subscription', $event->allowed_session_subscription) }}">
                        @if ($errors->has('allowed_session_subscription'))
                                  <div class="help-block">{{ $errors->first('allowed_session_subscription') }}</div>
                              @endif  
                      </div>
                  </div>
                </div>
              </form>
            @else
              <form action="{{ route('events.update_mandatory_category_session_control', [$event]) }}" method="POST">
                @method('POST')
                @csrf

                <div class="panel panel-default">
                  <div class="panel-heading">
                      {{ ucfirst(trans_choice('models.session_groups.mandatory_category_setup', 2)) }}
    
                      @can ('create', App\Session::class)
                          <button type="submit" 
                              class="btn btn-primary btn-sm btn-title-head pull-right" 
                              data-test="update-mandatory-category-session-control"
                              title="Save mandatory category session setup">
                              <i class="fa fa-save" aria-hidden="true"></i>
                              <span class="text-link">{{ ucfirst(trans_choice('models.sessions.save', 2)) }}</span>
                          </button>
                      @endcan
                  </div>
                  <div class="panel-body">
                      <div class="form-group">
                        <label for="mandatory_category">{{ ucfirst(trans_choice('models.session_groups.mandatory_category', 2)) }}</label>
                        <input id="mandatory_category" name="one_mandatory_category_session" type="checkbox" {{ old('one_mandatory_category_session', $event->one_mandatory_category_session) ? 'checked' : '' }} >
                      </div>
                      <div class="form-group {{ $errors->has('allowed_session_subscription') ? 'has-error' : '' }}">
                        <label for="max_categories_allowed">{{ ucfirst(trans_choice('models.session_groups.max_categories_allowed', 2)) }}</label>
                        <input id="max_categories_allowed" name="maximum_categories_subscriptions" type="number"
                        value="{{ old('maximum_categories_subscriptions', $event->maximum_categories_subscriptions) }}">
                        @if ($errors->has('maximum_categories_subscriptions'))
                                  <div class="help-block">{{ $errors->first('maximum_categories_subscriptions') }}</div>
                        @endif  
                      </div>
                  </div>
                </div>
              </form>
            @endif

            @include('sessionGroups._listPanel', [
              'event' => $event,
              'sessionGroups' => $event->sessionGroups
            ])

            @include('sessions._tablePanel', [
              'event' => $event,
              'sessions' => $sessions
            ])

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
