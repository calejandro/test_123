@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            @if (!Auth::user()->isController())
                @include('sidebarEvent')
            @endif

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(__('models.sessions.create_new')) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('events.sessions.index', [$event]) }}" title="{{ ucfirst(__('interface.back')) }}">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                {{ ucfirst(__('interface.back')) }}
                            </button>
                        </a>

                        @include('common._formErrorSummary', [
                            'errors' => $errors
                        ])

                        {!! Form::open([
                            'route' => ['events.sessions.store', $event],
                            'class' => 'form-horizontal'
                        ]) !!}

                            @include ('sessions._form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
