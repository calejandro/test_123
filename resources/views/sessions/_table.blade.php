<div class="table table-responsive">
    <table class="table table-borderless">
        <thead>
            <tr>
                <th>{{ ucfirst(__('validation.attributes.name')) }}</th>
                <th>{{ ucfirst(trans_choice('models.session_groups.session_group', 1)) }}</th>
                <th>{{ ucfirst(__('validation.attributes.places')) }}</th>
                <th>{{ ucfirst(__('validation.attributes.subscribed')) }}</th>
                <th>{{ ucfirst(__('validation.attributes.checkin')) }}</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sessions as $item)
            <tr>
                <td>
                    @foreach ($event->availableLanguages() as $e_lang)
                        <span class="label label-default">{{ $e_lang }}</span>
                    @endforeach
                    <span data-test="session-title" data-id="{{ $item->id }}">{{ $item->getEventAwareTranslatedAttribute('name', $event) }}</span>
                </td>
                <td data-test="session-{{ $item->id }}-category-cell">
                    @isset ($item->sessionGroup)
                        <span class="label label-primary" data-test="session-category-badge">{{ $item->sessionGroup->name }}</span>
                    @endisset
                </td>
                <td data-test="session-{{ $item->id }}-places">{{ $item->places }}</td>
                <td data-test="session-{{ $item->id }}-places-booked">{{ $item->placesReserved }}</td>
                <td data-test="session-{{ $item->id }}-places-checkin">{{ $item->placesCheckin }}</td>
                <td class="text-right">

                    @can('edit', App\Session::class)
                    <a href="{{ route('events.sessions.edit', [$event, $item]) }}" title="Edit session">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        {{ ucfirst(__('interface.edit')) }}
                        </button>
                    </a>
                    @endcan
                    @can('destroy', App\Event::class)
                    {!! Form::open([
                        'method'=>'DELETE',
                        'route' => ['events.sessions.destroy', $event, $item],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;' . ucfirst(__('interface.delete')), [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => ucfirst(__('interface.delete')),
                        'data-test' => 'session-delete-button',
                        'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->name])) . "')"
                    ]) !!}
                    {!! Form::close() !!}
                    @endcan
                </td>
                </tr>
        @endforeach
        </tbody>
    </table>
</div>
