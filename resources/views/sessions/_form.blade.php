@include('common._i18nFormText', [
    'name' => 'name',
    'label' => ucfirst(__('models.session.name')),
    'langs' => $event->availableLanguages(),
    'errors' => $errors
])

<div class="form-group {{ $errors->has('places') ? 'has-error' : ''}}">
  {!! Form::label('places', ucfirst(__('models.session.places')), ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {!! Form::number('places', null, ['class' => 'form-control', 'required' => 'required', 'min' => 0]) !!}
    {!! $errors->first('places', '<p class="help-block">:message</p>') !!}
  </div>
</div>

@if(count($sessionGroups) > 0)
  <div class="form-group {{ $errors->has('session_group_id') ? 'has-error' : ''}}">
    {!! Form::label('session_group_id', ucfirst(trans_choice('models.session_groups.session_group', 2)), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {!! Form::select('session_group_id',
        [NULL => __('models.session.no_group')] + $sessionGroups->reduce(function($carry, $item) {
          $carry[$item->id] = $item->name;
          return $carry;
        }),
        null,
        ['class' => 'form-control'])
      !!}
    </div>
  </div>
@endif

<div class="form-group">
  <div class="col-md-offset-4 col-md-6" data-test="create-session-btn">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('models.session.create_new')), ['class' => 'btn btn-primary btn-block']) !!}
  </div>
</div>
