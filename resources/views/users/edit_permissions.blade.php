@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>Edit permissions</h1>
  </div>

  <div class="row">
    @include('config.sidebar')

    <div class="col-md-10 page-content">

      <div class="panel panel-default">
        <div class="panel-heading">{{ __('interface.permissions_page.Edit_permissions_for', ['email' => $user->email]) }}</div>
        <div class="panel-body">

          <p>{{ __('interface.permissions_page.Edit_permissions_explain') }}</p>

          <form method="post" action="{{ route('update_permissions', [$company->id, $user->id])}}">
            {{ csrf_field() }}

            <h3>{{ __('interface.permissions_page.Users_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $users_permissions])

            <h3>{{ __('interface.permissions_page.Events_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $events_permissions])

            <h3>{{ __('interface.permissions_page.Guests_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $guests_permissions])

            <h3>{{ __('interface.permissions_page.Emails_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $emails_permissions])

            <h3>{{ __('interface.permissions_page.Minisites_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $minisite_permissions])

            <h3>{{ __('interface.permissions_page.Sessions_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $session_permissions])

            <h3>{{ __('interface.permissions_page.Others_permissions') }}</h3>
            @include('users._permissions_list', ['permissions' => $other_permissions])

            <button class="btn btn-primary">{{ __('interface.confirm') }}</button>
          </form>

        </div>
      </div>

    </div>
  </div>
</div>

@endsection
