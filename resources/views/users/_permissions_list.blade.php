<div class="list-group">
    @forelse ($permissions as $permission)

        <div class="list-group-item" @if ($loop->index > 0) style="padding-left: 50px;" @endif>
            <strong><input type="checkbox" data-cid="{{ $permission->id }}" id="perm-{{ $permission->id }}" name="permissions[]" value="{{ $permission->id }}"
            @if ($user->permissions->contains($permission))
            checked
            @endif
            > {{ ucfirst(trans_choice('models.permissions.' . $permission->name, 2)) }}</strong>
            <p class="list-group-item-text">{{ ucfirst(trans_choice('models.permissions.descriptions.' . $permission->name, 2)) }}</p>
        </div>

    @empty

    @endforelse
</div>