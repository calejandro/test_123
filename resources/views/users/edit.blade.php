@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $company->companyName }}</h1>
  </div>

  <div class="row">
    @include('config.sidebar')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">{{ $user->name }}</div>
        <div class="panel-body">
          <a href="{{ route('companies.users.index', [$company]) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
          <br />
          <br />

          @if ($errors->any())
          <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
          @endif

          {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => '/companies/' . $company->id . '/users/' . $user->id,
            'class' => 'form-horizontal',
            'files' => true
            ]) !!}

            @include ('users.form', ['submitButtonText' => ucfirst(trans_choice('interface.update', 1))])

            {!! Form::close() !!}


            @if (!$user->isController())
            <div class="col-md-5 col-sm-12">
              <h2>{{ __('interface.permissions_page.Attributed_permissions') }}</h2>
              <p>
                {{ __('interface.permissions_page.Attributed_permissions_association', ['email' => $user->email]) }}
              </p>
              @forelse($user->permissions as $permission)
                <span class="label label-default">{{ ucfirst(trans_choice('models.permissions.' . $permission->name, 2)) }}</span>
              @empty
              <p>
                {{ __('interface.permissions_page.No_permissions_associated') }}
              </p>
              @endforelse

              <hr/>
              <a href="{{ route('edit_permissions', [$company->id, $user->id])}}" class="btn btn-primary btn-sm">
                {{ __('interface.permissions_page.Edit_permissions') }}
              </a>

            </div>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
