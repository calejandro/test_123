@extends('layouts.app')

@section('content')

    <div class="container-full">
        <div class="head-bar-nav-info">
            <h1>{{ $company->companyName }}</h1>
        </div>

        <div class="row">
            @include('config.sidebar')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ ucfirst(trans_choice('models.user.user', 2)) }}
                        <div class="pull-right">
                          @can('create', App\User::class)
                            <a href="{{ url('/companies/' . $company->id . '/users/create') }}" class="btn btn-success btn-sm" title="Add New user">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ ucfirst(__('interface.add')) }}
                            </a>
                          @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>{{ ucfirst(__('validation.attributes.name')) }}</th><th>{{ ucfirst(__('validation.attributes.email')) }}</th><th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $index => $item)
                                    <tr>
                                        <td>{{ $item->name }}</td><td data-test="user-{{$index}}-mail">{{ $item->email }}</td>
                                        <td class="text-right">

                                          @can('edit', App\User::class)
                                            <a href="{{ url('/companies/' . $company->id . '/users/' . $item->id . '/edit') }}" title="Edit user"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ ucfirst(__('interface.edit')) }}</button></a>
                                          @endcan
                                          @can('create', App\User::class)
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/companies', $company->id, 'users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . ucfirst(__('interface.delete')), array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete user',
                                                        'onclick'=>"return confirm('" . ucfirst(__('interface.confirm_delete_object', ['name' => $item->name])) . "')"
                                                )) !!}
                                            {!! Form::close() !!}
                                          @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
