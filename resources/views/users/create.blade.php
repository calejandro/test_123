@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="head-bar-nav-info">
            <h1>{{ $company->companyName }}</h1>
        </div>

        <div class="row">
            @include('config.sidebar')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.user.create_new', 1)) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('companies.users.index', [$company]) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/companies/' . $company->id .'/users', 'class' => 'form-horizontal', 'files' => true]) !!}

                            @include ('users.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
