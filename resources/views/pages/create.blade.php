@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="head-bar-nav-info">
        <h1>{{ $event->name }}</h1>
    </div>

    <div class="row">
        @include('sidebarEvent')

        <div class="col-md-10 page-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ __('models.pages.create_new') }}
                    <div class="pull-right">
                        <button onclick="document.pagecreate.submit();" class="btn btn-primary btn-sm btn-title-head" data-test="page-save-btn">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(trans_choice('interface.save', 1)) }}</span>
                        </button>
                    </div>
                </div>

                <div class="panel-body">
                    <a href="{{ route('events.minisites.index', [$event]) }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button>
                    </a>
                    <br/><br/>
                    {!! Form::open([
                        'route' => ['events.minisites.pages.store', $event, $minisite],
                        'class' => 'form-horizontal',
                        'files' => true,
                        'name' => 'pagecreate'
                    ]) !!}
                        @include ('pages.form', ['submitButtonText' => ucfirst(trans_choice('interface.save', 1))])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
