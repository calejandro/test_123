<!-- Event infos for map insertion -->
<div id ="json-event-infos" style="display:none;">
  {{ $event->toJson() }}
</div>

{!! Form::hidden('currently_editing_lang', $lang) !!}

<div class="form-group {{ $errors->has('name_' . $lang) ? 'has-error' : ''}}">
  {!! Form::label('name_' . $lang, ucfirst(__('models.pages.name')), ['class' => 'control-label']) !!}
  {!! Form::text('name_' . $lang, null, ['class' => 'form-control', 'id' => 'page_name_field']) !!}
  {!! $errors->first('name_' . $lang, '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('slug_' . $lang) ? 'has-error' : ''}}">
  {!! Form::label('slug_' . $lang, ucfirst(__('models.pages.page_url')), ['class' => 'control-label']) !!}
  {!! Form::text('slug_' . $lang, null, ['class' => 'form-control input-sm', 'id' => 'slug_string_page_name', 'readonly' => 'readonly', 'required' => 'required']) !!}
  {!! $errors->first('slug_' . $lang, '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('page_type_id') ? 'has-error' : ''}}">
  {!! Form::label('page_type_id', ucfirst(__('models.pages.content_type')), ['class' => 'control-label']) !!}
  {!! Form::select('page_type_id',
  $pageTypes,
  null,
  ['class' => 'form-control']) !!}
</div>

<div id="tiny-mce-content" class="form-group {{ $errors->has('content_' . $lang) ? 'has-error' : ''}}">
  {!! Form::label('content_' . $lang, ucfirst(__('models.pages.content')), ['class' => 'control-label']) !!}
  {!! Form::textarea('content_' . $lang, isset($get_lang) ? $page->{'content_' . $get_lang} : null, ['class' => 'tinymce form-control', 'id' => 'p_content']) !!}
  {!! $errors->first('content_' . $lang, '<p class="help-block">:message</p>') !!}
</div>


{{ Form::hidden('lang', $lang) }}

<div class="form-group">
  <div>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary btn-block', 'data-test' => 'page-create-edit-button']) !!}
  </div>
</div>
