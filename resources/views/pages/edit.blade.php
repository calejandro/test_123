@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="head-bar-nav-info">
        <h1>{{ $event->name }}</h1>
    </div>
    
    <div class="row">
        @include('sidebarEvent')

        <div class="col-md-10 page-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ __('models.pages.edit_lang', ['lang' => $lang]) }}
                    <div class="pull-right">
                        <button onclick="document.pageedit.submit();" class="btn btn-primary btn-sm btn-title-head" data-test="top-page-create-edit-button">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(trans_choice('interface.save', 1)) }}</span>
                        </button>
                    </div>
                </div>

                <div class="panel-body">
                    @include('common._alert')

                    <a href="{{ route('events.minisites.index', [$event]) }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button>
                    </a>
                    <br/>

                    @if (count($page->availableLanguages()) > 0 && ($page->{'content_' . $lang} == "" || is_null($page->{'content_' . $lang}) ))
                    <br/>
                    <div class="alert alert-info" data-test="get-content-block">
                        <p><strong>{{ __('models.pages.question_transfer_content_lang') }}</strong></p>
                        @forelse ($page->availableLanguages() as $language)
                            <a href="{{ route('page_edit', [$event, $minisite, $page, $lang, $language]) }}" data-test="page-get-content-link-{{ $language }}">{{ __('models.pages.transfer_content_lang', ['lang' => $language]) }}</a><br/>
                        @empty
                        @endforelse
                    </div>

                    @endif
                  <hr/>

                  {!! Form::model($page, [
                    'method' => 'PATCH',
                    'route' => ['page_update', $event, $minisite, $page, $lang],
                    'files' => true,
                    'name' => 'pageedit'
                  ]) !!}

                    @include ('pages.form', ['submitButtonText' => ucfirst(trans_choice('interface.save', 1))])

                  {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
