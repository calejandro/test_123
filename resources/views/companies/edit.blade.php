@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="head-bar-nav-info">
        <h1>Configuration</h1>
    </div>
    <div class="row">
        @include('config.sidebar')

        <div class="col-md-10 page-content">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ucfirst(trans_choice('models.company.configuration', 1)) }}</div>
                <div class="panel-body">
                    <a href="{{ route('companies.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }} </button></a>
                    <br/><br/>
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @endif
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ ucfirst(trans_choice('models.company.configuration_smtp', 1)) }}</div>
                        <div class="panel-body">
                                
                            {!! Form::model($company, [
                            'method' => 'PATCH',
                            'class' => 'form-horizontal',
                            'files' => true,
                            'autocomplete'=>"off"
                            ]) !!}

                            <!-- fix autocomplete issue -->
                            <input class="hidden-field-fishing-hook" type="text" name="username-autocomplete" />
                            <input class="hidden-field-fishing-hook" type="password" name="password-autocomplete" />

                            @include ('companies.form', ['submitButtonText' => ucfirst(__('interface.update'))])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
