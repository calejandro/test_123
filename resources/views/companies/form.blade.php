<div class="form-group {{ $errors->has('smtp_from_mail_address') ? 'has-error' : ''}}">
    {!! Form::label('smtp_from_mail_address', ucfirst(__('validation.attributes.smtp_from_mail_address')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('smtp_from_mail_address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('smtp_from_mail_address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('smtp_from_name') ? 'has-error' : ''}}">
    {!! Form::label('smtp_from_name', ucfirst(__('validation.attributes.smtp_from_name')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('smtp_from_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('smtp_from_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<hr/>

<div class="form-group {{ $errors->has('smtp_host') ? 'has-error' : ''}}">
    {!! Form::label('smtp_host', ucfirst(__('validation.attributes.smtp_host')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('smtp_host', null, ['class' => 'form-control']) !!}
        {!! $errors->first('smtp_host', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('smtp_port') ? 'has-error' : ''}}">
    {!! Form::label('smtp_port', ucfirst(__('validation.attributes.smtp_port')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('smtp_port', null, ['class' => 'form-control']) !!}
        {!! $errors->first('smtp_port', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('smtp_username') ? 'has-error' : ''}}">
    {!! Form::label('smtp_username', ucfirst(__('validation.attributes.smtp_username')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('smtp_username', null, ['class' => 'form-control', 'autocomplete' => 'off_test tsere']) !!}
        {!! $errors->first('smtp_username', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('smtp_password') ? 'has-error' : ''}}">
    {!! Form::label('smtp_password', ucfirst(__('validation.attributes.smtp_password')), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('smtp_password', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
        {!! $errors->first('smtp_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('interface.create')), ['class' => 'btn btn-primary']) !!}
    </div>
</div>
