<div class="panel panel-default panel-sessions-small">
    <div class="panel-heading">{{ __('interface.checkin_page.Subscribed_sessions') }}</div>
    <div class="panel-body">

        <table class="table">
            <thead>
                <tr>
                <th scope="col"></th>
                <th scope="col">@choice('interface.checkin_page.Reserved_place', 2)</th>
                <th scope="col">@choice('interface.checkin_page.Checkin_place', 2)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($guest->sessions as $s)
                    <tr>
                        <th scope="row">{{ $s->name }}</th>
                        <td class="text-left">{{ $s->pivot->places }}</td>
                        <td class="text-left">{{ $s->pivot->checkin }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>