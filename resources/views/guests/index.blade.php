@extends('layouts.app')

@section('content')
<div class="container-full">

  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
    <div class="hidden-values">
      <span id="json-event-infos">{{ $event->toJson() }}</span>
      <span id="json-survey-fields-infos">{{ $surveyFields->toJson() }}</span>
    </div>

  </div>

  <div class="row">

    @include('sidebarEvent')

    <div class="col-md-10 page-content page-content-wrap">
    
        <div class="panel panel-default">
          <div class="panel-heading" id="head-panel">
            {{ ucfirst(trans_choice('models.guest.guest', 2)) }}

            <!-- Showing warning error icons if needed -->
            @if (count($failedGuests) >= 1 || $duplicates_detected)
              <span class="label label-danger" id="btn-trigger-warning-modal" data-toggle="tooltip" data-placement="right" title="Avertissements relatifs à la liste">
                <div id="warning-modal-content" style="display:none">
                    @if (count($failedGuests) >= 1)
                    <div class="alert alert-warning guests-list-remove">
                      <div class="pull-left alert-icon">
                        <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
                      </div>
                      <div class="adjusted-message">
                        <strong>{{ __('interface.guests_page.warning_invalid_emails_title') }}</strong>
                        <p>{{ __('interface.guests_page.warning_invalid_emails') }}</p>
                      </div>
                    </div>
                  @endif
          
                  @if($duplicates_detected)
                    <div class="alert alert-warning guests-list-remove">
                        <div class="pull-left alert-icon">
                          <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
                        </div>
                        <div class="adjusted-message">
                          <strong>{{ __('interface.guests_page.warning_duplicates_emails_title') }}</strong>
                          <p>{!! __('interface.guests_page.warning_duplicates_emails') !!}</p>
                          @can ('edit', App\Guest::class)
                          <p><a href="{{ route('events.guests.duplicates', $event) }}" class="btn btn-warning btn-sm">{{ __('interface.guests_page.fix_duplicates') }}</a></p>
                          @endcan
                        </div>
                    </div>
                  @endif
                </div>
                <i class="fa fa-exclamation-circle fa-fw"></i>
              </span>
            @endif


            <div class="pull-right">
              @can ('create', App\Guest::class)
              &nbsp;<a href="{{ route('events.guests.create', [$event]) }}" class="btn btn-success btn-sm btn-title-head" title="Add New Guest">
                <i class="fa fa-plus" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.add')) }}</span>
              </a>
              @endcan
            </div>
            <div class="pull-right">
              @can ('destroy', App\Guest::class)
                <button class="btn btn-sm btn-danger btn-title-head" id="btn-delete-all-guests">
                  <i class="fa fa-trash fa-fw" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('models.guest.delete_all')) }}</span>
                </button>
              @endcan
            </div>

            @if (!Auth::user()->isController())
              <div class="pull-right">
                @can ('create', App\Guest::class)
                  <button class="btn btn-sm btn-danger btn-title-head" data-toggle="modal" data-target="#anonymise-modal">
                    <i class="fa fa-user-secret fa-fw" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('models.guest.anonymise')) }}</span>
                  </button>
                @endcan
              </div>

              <div class="pull-right">
                @can ('edit', App\Guest::class)
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-download" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('models.guest.download')) }}</span> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="{{ route('events.guests.export', [$event]) }}">{{ ucfirst(__('interface.guests_export.export_complete_list')) }}</a>
                    </li>
                      @if($event->nominative_companions && $hasRegisteredCompanions)
                      <li>
                      <a href="{{ route('events.guests.companions.export', [$event]) }}">{{ ucfirst(__('interface.guests_export.export_companions_list')) }}</a>
                      </li>
                      @else
                      <li  class="disabled">
                      <a href="#">{{ ucfirst(__('interface.guests_export.export_companions_list')) }}</a>
                      </li>
                      @endif
                    </li>
                  </ul>
                </div>
                @endcan
              </div>

              <div class="pull-right">
                @if(!$emailValidationRunning)
                  @can ('create', App\Guest::class)
                    {!! Form::open([
                      'files' => true,
                      'method'=>'POST',
                      'url' => ['events/' . $event->id . '/guests/import'],
                      'style' => 'display:inline',
                      'onchange' => 'document.getElementById("upload-xls-form").submit();',
                      'id' => 'upload-xls-form'
                      ]) !!}
                      <span class="btn btn-default btn-file">
                        <i class="fa fa-upload" aria-hidden="true"></i> <span class="text-link"> {{ ucfirst(__('models.guest.upload')) }}</span> {{ Form::file('import_guests') }}
                      </span>
                      {{ Form::close() }}
                    @endcan
                  @endif
              </div> 
            @endif
            </div>
            <div class="panel-body">

              @if(count($failedEmailValidation) > 0 && !$emailValidationRunning)
                <div class="alert alert-danger guests-list-remove">
                    <div class="pull-left alert-icon">
                      <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
                    </div>
                    <div class="adjusted-message">
                      <p>{!! __('interface.guests_page.warning_validation_failed') !!}</p>
                      <p>
                        {!! Form::open([
                          'method'=>'POST',
                          'url' => [route('events.guests.validate_emails', $event)],
                          'style' => 'display:inline'
                          ]) !!}
                          <input type="submit" class="btn btn-danger btn-sm" value="{{ __('interface.guests_page.fix_validation') }}" />
                        {{ Form::close() }}
                      </p>
                    </div>
                </div>
              @endif

              <div id="app-guest-index">
                <!-- Vue.js -->
                <app-guest-index 
                  :event='@json($event)'
                  :survey-fields='@json($surveyFields)'
                  :selection-mode='false'>
                </app-guest-index>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="anonymise-modal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close"
            data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"
            id="favoritesModalLabel">{{ ucfirst(__('interface.anonymise_modal.title')) }}</h4>
          </div>
          <div class="modal-body" id="anonymise-modal-body">
            <div id="modal-anonymise-error"></div>
            <p>
              <strong>{{ ucfirst(__('interface.anonymise_modal.headline')) }} <i>{{$event->name}}</i>.</strong>
            </p>
            <div class="alert alert-danger">
              <p>{{ ucfirst(__('interface.anonymise_modal.warning')) }}</p>
            </div>
            <div class="alert alert-info">
              <p>{{ ucfirst(__('interface.anonymise_modal.info')) }}</p>
            </div>
          </div>
          <div class="modal-footer">


            <input type="password" name="password" id="anonymise-password" class="form-control" placeholder="{{ ucfirst(__('validation.attributes.password')) }}" required />
            <br/>

            <button type="button"
            class="btn btn-default"
            data-dismiss="modal">Close</button>&nbsp;
            <span class="pull-right">

              <button type="submit" class="btn btn-danger" id="btn-anonymise" >
                {{ ucfirst(__('interface.confirm')) }}
              </button>

            </span>

          </div>
        </div>
      </div>
    </div>

    @endsection
