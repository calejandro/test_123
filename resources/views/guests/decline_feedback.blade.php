@extends('layouts.guest')

@section('content')

<div class="container-full">
  <div class="content text-center">


    @if(isset($errorMessage))

      <div class="alert alert-danger">
        <i class="fa fa-times fa-fw fa-5x"></i>
      </div>
      <p><strong>{{ $errorMessage }}</strong></p>

    @else

      <div class="alert alert-success">
        @isset($guest->language)
          <p>{{ 
              trans_choice('interface.decline_page.Declined_message', 1, [
                'event' => $event->getTranslatedAttributeWithFallback('name', $guest->language, ['fr', 'en', 'de'])
              ], $guest->language) 
            }}</p>
        @else 
          <p>{{ trans_choice('interface.decline_page.Declined_message', 1, ['event' => $event->name]) }}</p>
        @endisset
      </div>

    @endif
  </div>
</div>

@endsection
