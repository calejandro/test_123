<nav class="navbar navbar-default">
    <div class="container-fluid">

      <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> @if(!isset($session)) {{ __('interface.checkin_page.Main_entry') }} @else {{ $session->name }} @endif</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
              <li @if(!isset($session)) class="active" @endif>
                  <a href="{{ route('guests.checkin', [$hash]) }}">{{ __('interface.checkin_page.Main_entry') }}</a>
              </li>

              @foreach ($sessions as $s)
                  <li @if(isset($session) && $session->id == $s->id)  class="active" @endif>
                      <a href="{{ route('sessions.guests.checkin', [$s, $hash]) }}">{{ $s->name }}</a>
                  </li>
              @endforeach
            </ul>
          </div><!-- /.navbar-collapse -->
    </div>
</nav>