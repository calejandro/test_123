@extends('layouts.app')

@section('content')
<div class="container-full">

    <div class="head-bar-nav-info">
        <h1>{{ $event->name }}</h1>
    </div>

    <div class="row">

        @if (!Auth::user()->isController())
            @include('sidebarEvent')
        <div class="col-md-10 page-content">
        @else
            <div class="col-md-12 page-content">
        @endif


                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans_choice('models.guest.create_new', 1)) }}</div>
                    <div class="panel-body">
                        <a href="{{ route('events.guests.index', [$event]) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif

                        {!! Form::open(['url' => 'events/' . $event->id . '/guests', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('guests.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
