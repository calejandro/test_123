@extends('layouts.app')

@section('content')
    <div class="container-full">

        <div class="head-bar-nav-info">
            <h1>{{ $event->name }}</h1>
        </div>

        <div class="row">
            @include('sidebarEvent')

            <div class="col-md-10 page-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ ucfirst(trans_choice('models.guest.import', 1)) }}

                        <div class="pull-right">
                            {!! Form::open([
                                'files' => true,
                                'method'=>'POST',
                                'url' => ['events/' . $event->id . '/guests/import'],
                                'style' => 'display:inline',
                                'onchange' => 'document.getElementById("upload-xls-form").submit();',
                                'id' => 'upload-xls-form'
                                ]) !!}
                                <span class="btn btn-default btn-file">
                                    <i class="fa fa-upload" aria-hidden="true"></i> <span class="text-link"> {{ ucfirst(__('models.guest.upload_change')) }}</span> {{ Form::file('import_guests') }}
                                </span>
                            {{ Form::close() }}
                  
                            @if (!(count($errors) > 0))
                                <button class="btn btn-primary btn-sm" id="btn-valid">
                                    <i class="fa fa-check" aria-hidden="true"></i> {{ ucfirst(__('interface.valid_import')) }}
                                </button>
                            @endif
                        </div>

                    </div>
                    <div class="panel-body">
                        
                        @if($event->guests()->active()->count() == 0)
                        <div class="alert alert-danger">
                                {{ ucfirst(trans_choice('interface.import_nominative_companions_warning', 1)) }} 
                        </div>
                        @endif
                        

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <div class="pull-left alert-icon">
                                    <i class="fa fa-exclamation-triangle fa-fw fa-2x"></i>
                                </div>
                                <div class="adjusted-message">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <div id="app-guest-import">
                                <!-- Vue.js -->
                                <guests-import-table
                                    :event='@json($event)'
                                    :guests='@json($importRows)'
                                    :rows='@json($importColumns)'>
                                </guests-import-table>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
