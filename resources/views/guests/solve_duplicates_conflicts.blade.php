@extends('layouts.app')

@section('content')
<div class="container-full">

  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">
          {{ trans_choice('interface.solve_duplicates_conflicts_page.Solve_conflict', 2) }}
          <div class="pull-right">
            <a href="{{ route('events.guests.index', [$event]) }}" class="btn btn-danger btn-sm" id="btn-cancel">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}
            </a>
          </div>
        </div>
        <div class="panel-body">

        <!-- Modal -->
        <div class="modal fade" id="modalConfirmDuplicate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ __('interface.solve_duplicates_conflicts_page.confirm_modal.Please_confirm') }}</h4>
              </div>
              <div class="modal-body">
                <div class="alert alert-danger">
                    {{ __('interface.solve_duplicates_conflicts_page.confirm_modal.Warning_message') }}
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ ucfirst(__('interface.cancel')) }}</button>
                <button type="button" id="btn-solve-conflict" class="btn btn-danger" data-dismiss="modal">{{ ucfirst(__('interface.confirm')) }}</button>
              </div>
            </div>
          </div>
        </div>

          <div class="alert alert-warning">
              {{ __('interface.solve_duplicates_conflicts_page.Duplicates_detected_message') }}
          </div>

          @foreach($duplicate_blocs as $duplicates)
          <div class="well conflict-bloc">
            <h3>{{ __('interface.solve_duplicates_conflicts_page.Conflict') }} {{ $loop->iteration }} - <strong>{{ $duplicates[0]->email }}</strong></h3>

            <div class="row">

              @foreach($duplicates as $elem)
              <div class="table-responsive col-md-{{ floor(12 / count($duplicates)) < 2 ? 2 : floor(12 / count($duplicates)) }}" id="conflict-table-{{ $event->id }}-{{ $elem->id }}">
                <table class="table">
                  <tr><td> <button class="btn btn-warning btn-block" id="btn-trigger-modal-solve-{{ $loop->iteration }}-{{ $event->id }}-{{ $elem->id }}"><i class="fa fa-check" aria-hidden="true"></i> {{ __('interface.solve_duplicates_conflicts_page.Choose') }}</button></td></tr>
                  <!-- <tr><td><i class="glyphicon glyphicon-time"></i> <strong>{{$elem->updated_at}}</strong> </td></tr> -->
                  <tr><td>{{ $elem->firstname }} {{ $elem->lastname }}</td></tr>
                  <tr><td>{{ __('interface.solve_duplicates_conflicts_page.Change_at') }}: {{ $elem->updated_at }}</td></tr>
                </table>

                <div class="collapse collapse-{{ $duplicates[0]->accessHash }}">
                  <div class="table-responsive">
                    <table class="table">
                      @if($elem->extendedFields)
                        @foreach($elem->extendedFields as $key => $value)
                          @if(!is_array($value))
                            <tr><td><strong>{{ $key }}</strong> : {{ $value }}</td></tr>
                          @else
                            <tr><td><strong>{{ $key }}</strong> : {{ implode(", ", $value) }}</td></tr>
                          @endif
                        @endforeach
                      @endif
                    </table>
                  </div>
                </div>
              </div>
              @endforeach

            </div>

            <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target=".collapse-{{ $duplicates[0]->accessHash }}" aria-expanded="false" aria-controls="collapseExample">
                {{ __('interface.solve_duplicates_conflicts_page.Compare_in_details') }}
            </button>
          </div>

          <p></p>

          @endforeach

        </div>
      </div>
    </div>
  </div>
</div>


<div id="app-guest-duplicates">
  <guest-duplicate></guest-duplicate>
</div>

@endsection
