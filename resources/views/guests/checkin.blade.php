@extends('layouts.app')

@section('content')

<div class="container-full">
    <div class="text-center">

        @if(isset($confirmedCheckin))
            <!-- Success mode -->

            @if ($confirmedCheckin)
                <div class="alert alert-success" data-test="checkin-done-alert">
                    <p>{{ __('models.guest.checkin.CheckinDone') }}</p>
                </div>
                <p>
                    <a href="{{ route('events.guests.index', [$event]) }}" class="btn btn-primary btn-lg">
                        {{ __('models.guest.SeeList') }}
                    </a>
                </p>
            @else
                <div class="alert alert-danger" data-test="checkin-error-alert">
                    <p>{{ __('models.guest.checkin.TicketAllreadyScanned') }}</p>
                </div>
            @endif

        @else
            <!-- Ask confirm -->

            @include('guests._checkin_nav', [
                'event' => $event,
                'session' => $session ?? null,
                'sessions' => $sessions,
                'hash' => $hash
            ])

            @if(isset($error_message))
                <div class="alert alert-danger">
                    <i class="fa fa-times fa-fw fa-5x"></i>
                </div>
                <p><strong>{{ $error_message }}</strong></p>
            @else
                <hr/>

                <h2 data-test="guest-checkin-name"><strong><i>{{ $guest->lastname }} {{ $guest->firstname }}</i> @isset($companion) ({{ trans_choice('models.guest.companion', 1) }}) @endisset </strong></h2>
                <p data-test="guest-checkin-email">{{ $guest->email }}</p>

                @if(isset($session))
                    @include('guests._guest_reservation_summary', [
                        'reserved' => $sessionPlacesCount,
                        'checkin' => $sessionCheckinCount
                    ])
                @else
                    @include('guests._guest_reservation_summary', [
                        'reserved' => $guest->subscribed == null ? $guest->nbCompanions : $guest->nbCompanions + 1,
                        'checkin' => $guest->countCheckin()
                    ])
                @endif

                @if(isset($warning_message))
                    <div class="alert alert-danger alert-stack">
                        <p>{{ $warning_message }}</p>
                    </div>
                @endif

                <hr/>

                @if(!isset($session))
                    {!! Form::open(['url' => route('guests.confirm_checkin', [$hash]), 'class' => 'form-horizontal']) !!}
                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                        <input type="submit" name="confirm-checkin" value="{{ __('interface.confirm_checkin') }}" class="btn btn-success btn-lg" data-test="confirm-checkin">
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['url' => route('sessions.guests.confirm_checkin', [$session]), 'class' => 'form-horizontal']) !!}

                        @if($companion)
                          <input type="hidden" name="companion_id" value="{{ $companion->id }}">
                        @else
                          <input type="hidden" name="guest_id" value="{{ $guest->id }}">
                        @endif

                        <input type="submit" name="confirm-checkin" value="{{ __('interface.confirm_session_checkin') }}" class="btn btn-success btn-lg" data-test="confirm-session-checkin">

                    {!! Form::close() !!}
                @endif

                <br/>
                <a href="{{ route('events.guests.edit', [$event, $guest]) }}" class="btn btn-sm btn-warning">
                    {{ __('models.guest.edit') }}
                </a>
                <br/><br/><br/>
                <a href="{{ route('events.guests.index', [$event]) }}" class="btn btn-sm btn-warning">
                    {{ __('interface.checkin_page.Show_guest_list') }}
                </a>
            @endif

        @endif
    </div>
</div>

<div class="mb-2"></div>

@endsection
