<div class="guest-places-reservations row">
    <div class="col-xs-6">
        <div class="guest-places" id="guest-reserved-places">
            <p>@choice('interface.checkin_page.Reserved_place', 2)<p>
            <h1>{{ $reserved }}</h1>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="guest-places" id="guest-checkin-places">
            <p>@choice('interface.checkin_page.Checkin_place', 2)<p>
            <h1>{{ $checkin }}</h1>
        </div>
    </div>
</div>