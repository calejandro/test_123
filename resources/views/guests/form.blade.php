
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
            {!! Form::label('firstname', ucfirst(__('validation.attributes.first_name')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
            {!! Form::label('lastname', ucfirst(__('validation.attributes.last_name')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
            {!! Form::label('lastname', ucfirst(__('validation.attributes.company')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('company', null, ['class' => 'form-control']) !!}
                {!! $errors->first('company', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            {!! Form::label('email', ucfirst(__('validation.attributes.email')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>


        <div class="form-group {{ $errors->has('event_id') ? 'has-error' : ''}}">
            {!! Form::label('language', ucfirst(__('validation.attributes.lang')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
              {!! Form::select('language',
              $locales,
              null,
              ['class' => 'form-control']) !!}
            </div>
          </div>


    </div>
</div>

<hr/>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('invited') ? 'has-error' : ''}}">
            {!! Form::label('invited', ucfirst(__('validation.attributes.invited')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                <div class="checkbox">
            <label>{!! Form::radio('invited', '1') !!} {{ ucfirst(__('interface.yes')) }}</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('invited', '0', true) !!} {{ ucfirst(__('interface.no')) }}</label>
        </div>
                {!! $errors->first('invited', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('checkin') ? 'has-error' : ''}}">
            {!! Form::label('checkin', ucfirst(__('validation.attributes.checkin')), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                <div class="checkbox">
            <label>{!! Form::radio('checkin', '1') !!} {{ ucfirst(__('interface.yes')) }}</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('checkin', '0', true) !!} {{ ucfirst(__('interface.no')) }}</label>
        </div>
                {!! $errors->first('checkin', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<hr/>

@include ('guests._extended_fields_form_group', [
    'event' => $event,
    'extendedFields' => isset($guest) ? $guest->extendedFields : null
])

<hr/>

<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : ucfirst(__('interface.create')), ['class' => 'btn btn-primary btn-block']) !!}
</div>
