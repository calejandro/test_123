{{--
    Extended fields form groups
    @param: $event : Event
    @param: $extendedFields : guest / companion array of extended fields
--}}

@if ($event->extendedFileds)
    @foreach ($event->extendedFileds as $additionalField)
        <div class="form-group">
            {!! Form::label($additionalField, ucfirst($additionalField), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
              @if (isset($extendedFields[$additionalField]))
                @if (!is_array($extendedFields[$additionalField]))
                  {!! Form::text('extended_fields[' . $additionalField . ']', 
                        is_array($extendedFields) && array_key_exists($additionalField, $extendedFields) ? $extendedFields[$additionalField] : null, 
                        ['class' => 'form-control']) !!}
                @else
                  {!! Form::text('extended_fields[' . $additionalField . ']', 
                        implode(", ", $extendedFields[$additionalField]), 
                        ['class' => 'form-control']) !!}
                @endif
              @else
                {!! Form::text('extended_fields[' . $additionalField . ']', 
                    is_array($extendedFields) && array_key_exists($additionalField, $extendedFields) ? $extendedFields[$additionalField] : null, 
                    ['class' => 'form-control']) !!}
              @endif
            </div>
        </div>
    @endforeach
@endif