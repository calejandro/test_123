@extends('layouts.app')

@section('content')

@isset($event)
    <div class="container-full mobile-app-container">
        <div class="row">
        
                @include('sidebarEvent')
        

            <div class="col-md-10 page-content page-content-full-width">

@endisset
                <div id="app-checkin">
                    <checkin-main
                        :default-event='@json($event)'
                        :user='@json(Auth::user())'>
                    </checkin-main>
                </div>
@isset($event)

        </div>
    </div>
 @endisset
@endsection


@section('scripts')
    <script src="{{ mix('js/checkin/index.js') }}"></script>
@overwrite