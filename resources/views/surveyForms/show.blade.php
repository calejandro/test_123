@extends('layouts.guest')

@section('content')

    @if(isset($guest))
    <div class="hidden-values" id="json-guest-infos">@json($guest)</div>
    <div class="hidden-values" id="json-event-id">{{ $event->id }}</div>
    <div class="hidden-values" id="json-survey-form-submit-text">{{ $surveyForm->submit_text }}</div>
    <div class="hidden-values" id="json-is-demo">{{ $demo }}</div>

    @endif

    <div id="{{ $event->advanced_subscription_form_mode ? '' : 'survey-form-page' }}">
      <style>
        @if($surveyForm->label_color == 0)
          label, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p{color: #777777; font-weight: bold;}
        @elseif($surveyForm->label_color == 1)
          label, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p{color: #FFFFFF; font-weight: bold;}
        @else
          label, .rendered-form .btn, .rendered-form h5, .rendered-form h4, .rendered-form h3, .rendered-form h2, .rendered-form h1, .rendered-form p{color: #000000; font-weight: bold;}
        @endif

        .rendered-form{ padding: 10px; }
        
      </style>

      {!! $viewBody !!}
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/survey/index.js') }}"></script>

@endsection
