@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">
          {{ ucfirst(trans_choice('models.surveyForm.result', 2)) }}

          <div class="pull-right">
            @can ('edit', App\SurveyForm::class)
              <a href="{{ route('events.surveyForm.edit', [$event]) }}" class="btn btn-primary btn-sm">{{ ucfirst(__('models.surveyForm.edit')) }}</a>
            @endcan
          </div>

          <!-- JSON of email informations -->
          <div class="hidden-values" id="json-event-infos">
            {{ $event->toJson() }}
          </div>
        </div>

        <div class="panel-body">

        <div id="app-survey-form-results">
          @if (in_array('statistics-view', Auth::user()->permissions()->get()->pluck('name')->toArray()))
            <survey-form-results></survey-form-results>
          @else
            <p>Vous n'êtes pas autorisé à visualiser les résultats de sondage.</p>
          @endif
        </div>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
