@extends('layouts.app')

@section('content')
<div class="container-full">
  <div class="head-bar-nav-info">
    <h1>{{ $event->name }}</h1>
  </div>

  <div class="row">
    @include('sidebarEvent')

    <div class="col-md-10 page-content">
      <div class="panel panel-default">
        <div class="panel-heading">
          {{ ucfirst(trans_choice('models.surveyForm.surveyForm', 1)) }}

          <div class="pull-right">
            <button class="btn btn-danger btn-sm" id="btn-cancel">
              {{ ucfirst(__('interface.cancel')) }}
            </button>

            <button class="btn btn-primary btn-sm" id="btn-save">
              {{ ucfirst(__('interface.save')) }}
            </button>
          </div>
        </div>

        <div class="panel-body">

        <div class="hidden-values" id="json-event-infos">{{ $event->toJson() }}</div>
        <div class="hidden-values" id="json-accesshash-infos">{{ $accessHash }}</div>

        <div id="app-survey-form-edit">
            <survey-form-edit :advanced_mode={{ $event->advanced_minisite_mode }}></survey-form-edit>
        </div>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
