@extends('layouts.login')

@section('content')
<br/>
{{ HTML::image('images/logo_eventwise.png', 'Eventwise Logo', array('height' => '130', 'class' => 'center-block')) }}
<div class="container-full" id="login-page">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ucfirst(__('interface.login')) }}</div>

                <div class="panel-body">

                    <div id="compatibility-warn-message"                      
                      data-message-content="{{ __('interface.login_page.uncompatible_browser_text') }}"
                      data-message-minVersion="{{ __('interface.login_page.uncompatible_browser_min_version_compatible') }}">
                    </div>


                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ ucfirst(__('validation.attributes.email')) }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block" data-test="login-error-message-mail">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ ucfirst(__('validation.attributes.password')) }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block" id="login-error-message-password">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ ucfirst(__('interface.login_page.remember_me')) }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" data-test="btn-login">
                                    {{ ucfirst(__('interface.login')) }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <a href="https://www.event-wise.ch" class="btn btn-default"><i class="glyphicon glyphicon-arrow-right"></i> Visitez notre site web</a>

        </div>
    </div>
</div>



@endsection
