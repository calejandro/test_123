
<div class="col-md-2 left-sidebar collapse navbar-collapse collapse" expanded="false" id="app-left-sidebar">
  <div class="panel panel-default panel-flush">
    <div class="panel-body">
      <ul class="nav" role="tablist">
        @if (!Auth::user()->isController())
          <li role="presentation">
            <a href="{{ route('companies.events.show', [$event->company()->first(), $event]) }}" class="{{ Active::checkRoute(['companies.events.show']) }}"data-test="side-bar-title-dashboard">
              <i class="fa fa-dashboard" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.event.dashboard', 1)) }}
            </a>
          </li>
        @endif
        <li role="presentation">
          @can('index', App\Guest::class)
            <a href="{{ route('events.guests.index', [$event]) }}" class="{{ Active::checkRoute(['events.guests.*']) }}" data-test="side-bar-title-guests">
              <i class="fa fa-users" aria-hidden="true"></i> 
              @if($event->companions_limit > 0)
                {{ ucfirst(trans_choice('models.guest.guest_comp', 2)) }}
              @else 
                {{ ucfirst(trans_choice('models.guest.guest', 2)) }}
              @endif
            </a>
          @endcan
        </li>
        <li role="presentation">
          @can('index', App\Email::class)
            <a href="{{ route('events.emails.index', [$event]) }}" class="{{ Active::checkRoute(['events.emails.*']) }}" data-test="side-bar-title-email">
              <i class="fa fa-envelope-o" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.email.email', 2)) }}
            </a>
          @endcan
        </li>
        @if (!Auth::user()->isController())
          <li role="presentation">
            <a href="{{ route('events.ticket.edit', [$event]) }}" class="{{ Active::checkRoute(['events.ticket.*']) }}" data-test="side-bar-title-entry-ticket">
              <i class="fa fa-file-pdf-o" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.ticket.ticket', 2)) }}
            </a>
          </li>
        @endif
        @can('index', App\Minisite::class)
          @if ($event->advanced_minisite_mode)
            <li>
              <a href="{{ route('events.minisites.index', [$event]) }}" class="{{ Active::checkRoute(['events.minisites.*']) }}" data-test="side-bar-title-advanced-minisite">
                <i class="fa fa-th-large" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.minisite.advanced_minisite', 1)) }}
              </a>
            </li>
            <li role="presentation">
              @can('edit', App\Minisite::class)
                <a href="{{ route('events.subscribeForm.edit', [$event]) }}" class="{{ Active::checkRoute(['events.subscribeForm.*']) }}" data-test="side-bar-title-advanced-subscribe-form">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.subscribeForm.subscribeForm_advanced', 1)) }}
                </a>
              @endcan
            </li>
          @else
            <li role="presentation">
              @can('edit', App\Minisite::class)
                <a href="{{ route('events.subscribeForm.edit', [$event]) }}" class="{{ Active::checkRoute(['events.subscribeForm.*']) }}" data-test="side-bar-title-subscribe-form">
                  <i class="fa fa-file-text-o" aria-hidden="true" ></i> {{ ucfirst(trans_choice('models.subscribeForm.subscribeForm', 1)) }}
                </a>
              @endcan
            </li>
          @endif
        @endcan
        <li role="presentation">
          @can('index', App\Session::class)
            <a href="{{ route('events.sessions.index', [$event]) }}" class="{{ Active::checkRoute(['events.sessions.*']) }}" data-test="side-bar-title-sessions">
              <i class="fa fa-stack-exchange" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.session.session', 2)) }}
            </a>
          @endcan
        </li>
        <li role="presentation">
          @if (in_array('statistics-view', Auth::user()->permissions()->get()->pluck('name')->toArray()))
            <a href="{{ route('events.surveyForm.results', [$event]) }}" class="{{ Active::checkRoute(['events.surveyForm.*']) }}">
              <i class="fa fa-pie-chart" aria-hidden="true"></i> {{ ucfirst(trans_choice('models.surveyForm.survey', 1)) }}
            </a>
          @endif
        </li>

        <li role="presentation">
          <a href="{{ route('events.checkin.index', [$event]) }}" class="{{ Active::checkRoute(['events.checkin.*']) }}" data-test="side-bar-title-checkin">
            <i class="fa fa-ticket" aria-hidden="true"></i> {{ __('interface.menu.Checkin') }}
          </a>
        </li>
      </ul>

      @include('common._mobileUserNavGroup')
    </div>
  </div>
</div>
