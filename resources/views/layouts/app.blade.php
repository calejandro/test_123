<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Eventwise</title>

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>

<body id="body-template-app">
  <div id="app">

    @if (isset($currentCompany) && !Auth::user()->company)
    <p class="warning-on-company">{{ ucfirst(__('interface.warn_admin_company_on')) }} <b>{{ $currentCompany->companyName }}</b> - <a href="{{ route('companies.index') }}" class="link-default">{{ ucfirst(__('interface.quit')) }}</a></p>
    @endif

    <nav class="navbar navbar-default navbar-static-top" id="site-header">
      <div class="navbar-header">

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-left-sidebar">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <!-- Branding Image -->
      <a class="navbar-left brand-logo" href="{{ url('/') }}">
        {{ HTML::image('images/logo_eventwise.png', 'Eventwise Logo', array('height' => '40', 'style' => 'margin-top: 7px; margin-left: 7px;')) }}
      </a>
    </div>

    <div class="icon-navbar" id="app-navbar-collapse">

      <!-- Right Side Of Navbar -->
      <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @if (Auth::guest())
        <li><a href="{{ route('login') }}">{{ ucfirst(__('interface.login')) }}</a></li>
        @else
        <li class="dropdown only-desktop">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-test="dropdown-button">
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" data-test="logout-button">
              {{ ucfirst(__('interface.logout')) }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </li>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </li>
    @endif
  </ul>

  <!-- Left Side Of Navbar -->
  <ul class="nav navbar-nav navbar-right">
    @if (Auth::user() && !Auth::user()->isController())
      <li><a href="https://eventwise.zendesk.com" target="_blank" class="no"><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i><span class="text-link"> FAQ</span></a></li>
    
      @if ($currentCompany)
        <li><a href="{{ route('companies.events.index', [$currentCompany]) }}" class="{{ Active::checkRoute(['companies.events.*', 'events.*']) }}"><i class="fa fa-calendar" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.event')) }}</span></a></li>
        <li><a href="{{ route('companies.users.index', [$currentCompany]) }}" class="{{ Active::checkRoute(['companies.users.*', 'companies', 'admins.*', 'companies.categories.*']) }}"><i class="fa fa-cog" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.configuration')) }}</span></a></li>
      @else
        <li><a href="{{ route('companies.index') }}" class="{{ Active::checkRoute(['companies.users.*', 'companies', 'admins.*', 'companies.categories.*']) }}"><i class="fa fa-cog" aria-hidden="true"></i><span class="text-link"> {{ ucfirst(__('interface.configuration')) }}</span></a></li>
      @endif

    @endif

    <li class="dropdown dropdown-lang">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspop="true" aria-expanded="false">
        {{ App::getLocale() }}&nbsp;<span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        @foreach(Config::get('app.locales') as $language)
        @if($language != App::getLocale())
        <li><a href="{{ route('langroute', $language )}}">{{ $language }}</a></li>
        @endif
        @endforeach
      </ul>

    </li>
  </ul>
</div>
</nav>

@yield('content')
</div>

<!-- Scripts -->
@section('scripts')
  @include('layouts._scripts')
@show

</body>
</html>
