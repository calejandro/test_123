<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Eventwise</title>

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>

  <nav class="navbar navbar-default navbar-static-top" id="site-header">
    <div class="navbar-header">

      <!-- Collapsed Hamburger -->
      <!--<button type="button" class="navbar-toggle collapsed fa-iconed" data-toggle="collapse" data-target="#app-navbar-collapse">
      <span class="sr-only">Toggle Navigation</span>
      <i class="fa fa-cog" aria-hidden="true"></i>
    </button>-->

    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-left-sidebar">
      <span class="sr-only">Toggle Navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>


    <!-- Branding Image -->
    <a class="navbar-left brand-logo" href="{{ url('/') }}">
      {{ HTML::image('images/logo_eventwise.png', 'Eventwise Logo', array('height' => '40', 'style' => 'margin-top: 7px; margin-left: 7px;')) }}
    </a>
  </div>

  <!-- Left Side Of Navbar -->
  <ul class="nav navbar-nav navbar-right">



    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspop="true" aria-expanded="false">
        {{ App::getLocale() }}&nbsp;<span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        @foreach(Config::get('app.locales') as $language)
        @if($language != App::getLocale())
        <li><a href="{{ route('langroute', $language )}}">{{ $language }}</a></li>
        @endif
        @endforeach
      </ul>

    </li>

  </ul>

</div>
</nav>


<div id="app">
  @yield('content')
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
