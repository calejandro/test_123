<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Eventwise</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Recaptcha -->
    {{-- <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit&hl={{ app()->getLocale() }}" async defer></script> --}}
    
</head>
<body>
    <div class="fixed-message-box">
        <div id="error-box" class="alert alert-danger alert-box">
            <div class="pull-left alert-icon">
                <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
            </div>
        </div>
    </div>

    <div class="fixed-message-box">
        <div id="success-box" class="alert alert-success alert-box">
            <div class="pull-left alert-icon">
                <i class="fa fa-check-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
            </div>
        </div>
    </div>

    <div id="app app-guest">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/subscribe/index.js') }}"></script>
</body>
</html>
