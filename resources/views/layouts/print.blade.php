<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Eventwise</title>

  <style type="text/css" media="all">

  .logo-top{
    width: 60%;
    height: auto;
    max-width: 200px;
    max-height: 200px;
  }

  img{
    max-width: 100%;
    height: auto;
    position: relative;
    display: block;
    margin-bottom: 25px;
    margin: none !important;
  }

  .image-bottom-bloc{
    position: relative;
    margin-top: 10px;
    text-align: center;
    margin-bottom: 0;
    padding-bottom: 0;
    height: 255px;
    overflow: hidden;
  }

  .img-bottom{
    width: 100%;
    height: auto;
    position: absolute;
    top: 0;
    left: 0;
    margin: 0;
    padding: 0;
  }

  /* Base */
  body{
    padding: 40px 0px 20px 0px;
    color: #555;
    font-family: Helvetica;
    font-size: .8em;
  }
  .ticket{
    max-width: 100%;
    max-height: 100%;
    overflow-y: hidden;
  }

  /* Fake grid system */
  .col-sm-4{
    width: 33.333%;
    display: inline-block;
    position:relative;
    vertical-align: top;
  }
  .col-sm-6{
    width: 49%;
    display: inline-block;
    position: relative;
    vertical-align: top;
  }
  .col-sm-8{
    width: 65%;
    display: inline-block;
    position: relative;
    vertical-align: top;
  }
  .container{
    width: 100%;
    position: relative;
  }

  /* Typo styles */
  .lead{
    font-size: 1.4em;
  }

  .text-center{
    text-align: center;
  }

  .text-justify{
    text-align: justify;
  }

  .text-description{
    margin-top: 40px;
    margin-bottom: 20px;
    padding-right: 20px;

    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 16px;
    max-height: 220px;
    -webkit-line-clamp: 100; /* number of lines to show */
    -webkit-box-orient: vertical;
  }

  h1 {
    font-size: 32px !important;
  }

  .ticket-header {
    padding-top: 0 !important;
    margin-top: 0 !important;
    font-size: 27px !important;
    max-height: 65px;

    text-transform: uppercase;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .only-one-line {
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  hr{
    border: 0;
    height: 1px;
    background: black;
    margin: 0 40px !important;
  }
  p{
    font-size: 16px;
  }
  h1, p, small {
    color: black !important;
    font-family: Arial, Helvetica, sans-serif;
  }

  /* Images */
  .img{
    top: 0;
    position: relative;
  }

  /* Layout adjustments */
  .adjusted-qrcode{
    margin-left: -25px;
  }
  .vertical-center {
    padding-top: 20px;
  }



  /* TEMPLATE BADGE */
  
  #four-quarter-badge {
    position: fixed;
    top:0;
    left:0;
    width: 100%;
    height: 1122px; /* A4 height, to avoid any overflow issues */
    overflow: hidden;
  }

  #four-quarter-badge .quarter-content{
    padding: 20px;
    overflow: hidden;
  }
  #four-quarter-badge .topleft-case{
    width: 50%;
    height: 50%;
    display:inline-block;
    position: absolute;
    top: 0;
    left: 0;
  }
  #four-quarter-badge .topright-case{
    width: 50%;
    height: 50%;
    display:inline-block;
    position: absolute;
    top: 0;
    right: 0;
    border-left: 1px dotted #ccc;
  }
  #four-quarter-badge .bottomleft-case{
    border-top: 1px dotted #ccc;
    width: 50%;
    height: 50%;
    display:inline-block;
    position: absolute;
    bottom:0;
    left: 0;
  }
  #four-quarter-badge .bottomright-case{
    width: 50%;
    height: 50%;
    display:inline-block;
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 1px dotted #ccc;
    border-top: 1px dotted #ccc;
  }

  #four-quarter-badge .header-bloc{
    position: relative;
    display: inline-block;
    width: 100%;
    height: auto;
    padding: 20px 0px 0px 30px;
  }

  #four-quarter-badge .header-bloc img{
    position: relative;
    width: 21%;
    max-width: 21%;
    display: inline-block;
    vertical-align: top;
    padding: 2px;
  }

  #four-quarter-badge .header-bloc .title{
    text-transform: uppercase;
    font-size: 17px;
    max-height: 60px;
    overflow-y: hidden;
  }
  
  #four-quarter-badge .header-bloc p{ font-size: 10px;}
  #four-quarter-badge .header-bloc.bottom{ padding-top: 35px;}
  #four-quarter-badge .header-text{ 
    display:inline-block;
    max-width: 80%;
    text-align: center;
    padding: 2px 20px 0px 0px;
  }


  #four-quarter-badge .header-bloc-full-width{
    padding: 20px 0px 0px 0px !important;
  }

  #four-quarter-badge .header-bloc-full-width .header-text{
    max-width: 100% !important;
    padding: 0px !important;
    width: 100%;
  }

  #four-quarter-badge .header-text .title, .header-text p{ font-family: Helvetica; margin-bottom: 5px;}

  #four-quarter-badge .indications{font-size: 8px; margin-left: 10px;}

  #four-quarter-badge .qr-code{
    text-align: center;
  } 

  #four-quarter-badge .qr-code .qr-code-img{
    width: 150px;
    position: relative;
  }

  #four-quarter-badge .badge-event-description{
    padding: 0px 25px 0px 25px;
    margin: 0;
    font-size: 12px;
    text-align: justify;
    max-height: 170px;
    overflow: hidden;
  }
  #four-quarter-badge .badge-recap{
    margin-top: 60px;
    font-size: 20px;
    text-align: center;
    max-height: 180px;
    /*overflow: hidden;*/
    /*width: 90%;*/
    white-space: pre-wrap;
  }
  #four-quarter-badge .badge-name{
    margin-top: 60px;
    font-size: 36px;
    text-align: center;
    max-height: 180px;
    overflow: hidden;
  }
  #four-quarter-badge .badge-enterprise{
    text-align: center;
    text-transform: uppercase;
    font-size: 22px;
    margin-top: 10px;
  }
  #four-quarter-badge .badge-date{ margin-top: 0; margin-bottom: 0; padding: 0;}
  #four-quarter-badge .badge-address{ margin-top: 5px; margin-bottom: 0; padding: 0;}

  .pli-y{
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    background-color: #FFFFFF;
    font-size: 8px;

    width: 50px;
    position: fixed;
    top: 25%;
    left: 47%;
    z-index:1900;
  }

  #four-quarter-badge .pli-x{
    text-align:center;
    font-size: 8px;
    margin-top: -6px;
    z-index:1000;
  }

  </style>

</head>
<body>
  <div id="app app-guest">
    @yield('content')
  </div>
</body>
</html>
