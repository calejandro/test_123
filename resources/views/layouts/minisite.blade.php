<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    {{-- TODO: remove, specific check. Keep it for 5.8.* versions --}}
    @isset($event)
        @if ($event->id === 391 || $event->id === 779)
            <meta name="robots" content="noindex">
        @endif
    @endisset

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Recaptcha -->
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit&hl={{ app()->getLocale() }}" async defer></script>

    <style>
        @yield('style')
    </style>

</head>
<body>

    @include('common._compatibilityMessageBox')

    <div class="fixed-message-box">
        <div id="error-box" class="alert alert-danger alert-box">
            <div class="pull-left alert-icon">
                <i class="fa fa-exclamation-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
            </div>
        </div>
    </div>

    <div class="fixed-message-box">
        <div id="success-box" class="alert alert-success alert-box">
            <div class="pull-left alert-icon">
                <i class="fa fa-check-circle fa-fw fa-2x"></i>
            </div>
            <div class="adjusted-message">
            </div>
        </div>
    </div>

    <div id="app app-guest">
        @yield('content')
    </div>

</body>
</html>
