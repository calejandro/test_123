@extends('layouts.app')

@section('content')
    <div class="container-full">

        <div class="head-bar-nav-info">
            <h1>{{ $event->name }}</h1>
        </div>

        <div class="row">

            @if (!Auth::user()->isController())
                @include('sidebarEvent')
            <div class="col-md-10 page-content">
            @else
                <div class="col-md-12 page-content">
            @endif

                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('models.companion.edit') }}</div>
                    <div class="panel-body">
                        <a href="{{ route('events.guests.index', [$event]) }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ ucfirst(__('interface.back')) }}</button>
                        </a>
                        <br/><br/>

                        @include ('common._formErrorSummary', [
                            'errors' => $errors
                        ])

                        {!! Form::model($companion, [
                            'method' => 'PATCH',
                            'route' => ['events.companions.update', $event, $companion],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                            @include ('companions._form', [
                                'submitButtonText' => ucfirst(trans_choice('interface.update', 1))
                            ])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
