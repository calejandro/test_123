<?php

return [
    'attributes' => [
        'valid_email' => [
            'valid_email' => 'Gültigkeit der E-Mail',
            'valid_email_' => 'Ungeprüft',
            'valid_email_0' => 'Invalide',
            'valid_email_1' => 'Valide',
            'valid_email_2' => 'Nicht nachweisbar',
            'valid_email_3' => 'Um zu überprüfen',
        ],
    ],
    'to_define' => 'zu definieren',
    'public' => 'allgemein',
    'private' => 'Privat',
    'confirm_checkin' => 'Eingabe bestätigen',
    'save' => 'speichern',
    'save_template' => 'Als neue Vorlage speichern',
    'update_existing_template' => 'eine vorhandene Vorlage aktualisieren',
    'load_template' => 'Vorlage laden',
    'delete_template' => 'Vorlage löschen',
    'edit_template' => 'eine Vorlage bearbeiten',
    'templates' => 'templates',
    'cancel' => 'Abbrechen',
    'send' => 'senden',
    'add' => 'hinzufügen',
    'edit' => 'bearbeiten',
    'edit_properties' => 'Eigenschaften ändern',
    'last_modification' => 'jüngste Veränderungen',
    'show' => 'Ansehen',
    'open' => 'Öffnen',
    'search' => 'suchen',
    'delete' => 'löschen',
    'back' => 'zurück',
    'update' => 'aktualisieren',
    'create' => 'erstellen',
    'event' => 'Events',
    'configuration' => 'Konfiguration',
    'confirm' => 'Bestätigen',
    'confirm_delete' => 'Bestätigen Sie das Löschen?',
    'confirm_delete_object' => 'Bestätigen Sie das Löschen von ": name"?',
    'logout' => 'Verbindung trennen',
    'login' => 'Anmelden',
    'lock' => 'Sperren',
    'unlock' => 'Entsperren',
    'yes' => 'ja',
    'no' => 'nein',
    'limit' => 'limit',
    'duplicate' => 'duplizieren',
    'filter' => 'Filter',
    'current_future' => 'In Bearbeitung / Zukünftige',
    'finished' => 'Vergangene',
    'warn_admin_company_on' => 'Sie arbeiten an:',
    'quit' => 'beenden',
    'apply' => 'anwenden',
    'valid_import' => 'validiere den Import',
    'import_nominative_companions_warning' => 'Achtung: Nach dem Import Ihrer Gäste können Sie die Supporteinstellung für Namensschilder nicht mehr ändern.',
    'controller_account' => 'Kontroller Konto',
    'choose_template' => 'Template wählen',
    'anonymise_modal' => [
        'title' => 'Bestätigung Sie die Anonymisierung',
        'headline' => 'Sie sind dabei, die Gästeliste zu anonymisieren.',
        'warning' => 'Achtung! Diese Aktion ist irreversibel une macht die Gästeliste dieses Ereignisses unbrauchbar. E-mails können nicht mehr an diese Benutzer gesendet werden.',
        'info' => 'Sie werden alle statistischen Informationen in Bezug auf das Ereignis behalten.',
    ],
    'template' => [
        'headline' => 'Bitte wählen Sie das Template zum aktualisieren.',
        'info' => 'Hinweis: Vergessen sie nicht, Ihre letzten Änderungen zu speichern.',
        'update' => 'Aktualisieren',
        'cancel' => 'Lösen',
    ],
    'login_page' => [
        'remember_me' => 'Erinnere dich an mich',
        'forgot_your_password' => 'Passwort vergessen?',
        'uncompatible_browser_text' => 'Seien Sie vorsichtig, dass Ihr Browser ({browser}) nicht vollständig mit Eventwise kompatibel ist, einige Funktionen funktionieren möglicherweise nicht so gut, wie sie sollten..',
        'uncompatible_browser_min_version_compatible' => 'Bitte aktualisieren Sie es auf Version {minVersion} oder höher.',
    ],
    'emails_page' => [
        'add_invit' => 'eine Einladung hinzufügen',
        'add_other_email' => 'weitere Email hinzufügen',
        'sent' => 'gesendet'
    ],
    'guests_page' => [
        'warning_invalid_emails_title' => 'Ungültigen E-Mail-Adressen',
        'warning_invalid_emails' => 'Diese Veranstaltung enthält Gäste mit ungültigen E-Mail-Adressen. Bitte überprüfen Sie die Gästeliste.',
        'warning_duplicates_emails_title' => 'Duplikate erkannt',
        'warning_duplicates_emails' => 'Die Gästeliste für diese Veranstaltung scheint Duplikate zu enthalten (E-Mail-Adresse). Bitte bearbeiten Sie diese, damit diese Meldung verschwindet.',
        'fix_duplicates' => 'Prozess-Duplikate',
        'warning_validation_failed' => '<strong>Fehler bei der Validierung von E-Mails. </strong> E-Mails wurden nicht validiert, bitte starten Sie den Validierungsprozess neu.',
        'fix_validation' => 'Überprüfung des Neustarts'
    ],
    'messages' => [
        'job_already_running' => [
            'headline' => 'Für diese E-Mail ist bereits eine Sendung unterwegs.',
            'subline' => 'Die Sendefunktion dieser E-Mail wird wieder aktiviert, sobald der aktuelle Vorgang abgeschlossen ist.',
        ],
        'verif_job_already_running' => [
            'headline' => 'Für dieses Ereignis wird eine Adressverifizierung durchgeführt',
            'subline' => 'Die Mailversandfunktion wird nach Abschluss der Prüfung wieder aktiviert.',
        ],
        'no_minisite_created' => [
            'headline' => 'Minisite ist noch nicht erstellt.',
            'subline' => 'Dieses Ereignis befindet sich im erweiterten Minisite-Modus, ist jedoch noch nicht erstellt.',
            'action' => 'Erstelle Minisite',
        ],
    ],
    'events_form' => [
        'general_informations' => 'Allgemeine Informationen',
        'coords_organisator' => 'Kontaktdaten des Veranstalters',
        'coords_organisator_infos' => 'Benachrichtigungsmails werden an die unten angegebene E-Mail-Adresse gesendet. Diese Felder sind nicht erforderlich. Wenn keine Host-E-Mail-Adresse angegeben ist, werden Benachrichtigungen automatisch an die firmenbezogene Adresse gesendet.',
        'mini_site_title' => 'Mini-site',
        'mini_site_infos' => 'Wenn Sie die Art der Mini-Site ändern, vergessen Sie nicht, die Links in Ihren E-Mails zu aktualisieren! Alle zuvor erstellten Formulare und Seiten bleiben jedoch zugänglich.',
        'event_languages_title' => 'Veranstaltungssprachen',
        'event_languages_infos' => 'Wählen Sie aus, in welchen Sprachen der Inhalt dieser Veranstaltung verfügbar sein soll.',
        'event_languages_warning_title_change' => 'Wenn Sie die verfügbaren Sprachen ändern, vergessen Sie nicht, den Minisite-Titel in der entsprechenden Sprache (Minisite-Eigenschaftsseite) hinzuzufügen und Übersetzungen für Ihre vorhandenen Seiten hinzuzufügen!',
        'event_available_languages' => 'Verfügbare Sprachen',
        'ctrl_account_title' => 'Regler',
        'public_minisite_infos' => 'Wenn öffentlich, wird der Zugang zur Website und die Registrierung geöffnet sein.',
        'advanced_forms_mode' => 'Anmeldeformular',
        'advanced_forms_infos' => 'Der erweiterte Modus ermöglicht es Ihnen, das Anmeldeformular in mehreren Sprachen zu erstellen und bedingte Fragen in das Formular zu integrieren.',
        'places_and_companions' => 'Anzahl der Plätze und Begleitpersonen',
        'places_and_companions_infos' => 'Verwalten Sie die Gesamtzahl der Plätze für Ihre Veranstaltung sowie die Anzahl der Begleitpersonen pro Gast. Sie können auch die Unterstützung für registrierte Begleitpersonen aktivieren.',
        'nominative_companions_infos' => 'Bei der Anmeldung müssen die Gäste die Daten der Begleitperson angeben.',
        'nominative_companions_warning' => 'Diese Einstellung kann nur geändert werden, wenn die Gästeliste leer ist.',
        'default_controler_will_be_created' => 'Ein Standard-Controller wird mit dem Ereignis erstellt, Sie können es in den Ereigniseinstellungen ändern.',
        'visible_captcha_infos' => 'Wenn dieses Kästchen angekreuzt ist, wird das Google Recaptcha im Registrierungsformular verwendet',
    ],
    'minisites_create_page' => [
        'create_title' => 'Erstellen der Minisite',
        'success_alert' => 'Seite erfolgreich erstellt.',
    ],
    'minisites_update_page' => [
        'update_title' => 'Update der Minisite',
        'maximum_size' => 'Max :size Mb',
        'success_alert' => 'Seite erfolgreich erstellt.',
    ],
    'minisites_page' => [
        'No_minisite_create_one_message' => 'Es gibt keine Minisite, bitte erstellen Sie eine.',
        'No_minisite_page_create_one_message' => 'Keine Seite vorhanden, bitte erstellen Sie eine.',
        'create_minisite' => 'Minisite erstellen',
        'no_language' => 'keine Sprache',
        'no_configurated_language' => 'keine Sprache konfiguriert',
        'browser_not_compatible' => 'Ihr Internetbrowser ist nicht auf dem neusten Stand. Diese Seite funktioniert möglicherweise nicht richtig. Wir bitten Sie, Ihren Browser zu aktualisieren oder einen anderen Browser zu verwenden: Chrome, Firefox, Safari ...',
    ],
    'checkin_page' => [
        'Subscribed_sessions' => 'Sitzungen',
        'Reserved_place' => 'Anzahl der Tickets',
        'Checkin_place' => 'Anzahl der gescannten Tickets',
        'Main_entry' => 'Haupteingang',
        'Show_guest_list' => 'Zeigen Sie die Gästeliste an',
    ],
    'decline_page' => [
        'Declined_message' => 'Wir haben Ihre Nichtteilnahme an der Veranstaltung aufgezeichnet für: :event',
    ],
    'ticket_page' => [
        'Download_demo' => 'Vorschauticket herunterladen'
    ],
    'permissions_page' => [
        'Edit_permissions_for' => 'Änderungsberechtigungen für :email',
        'Edit_permissions_explain' => 'Die Bearbeitung von Berechtigungen ermöglicht es diesem Benutzer, auf einige Funktionen der Eventwise Plattform zuzugreifen oder nicht. Der Zugriff auf die Seiten wird entsprechend eingeschränkt und einige Elemente der Oberfläche (Buttons, Links, etc.) werden ausgeblendet.',
        'Users_permissions' => 'Benutzerberechtigungen',
        'Events_permissions' => 'Berechtigungen für Events',
        'Guests_permissions' => 'Berechtigungen für Gäste',
        'Emails_permissions' => 'Berechtigungen für E-Mails',
        'Minisites_permissions' => 'Berechtigungen auf Mini-Seiten',
        'Sessions_permissions' => 'Sitzungsberechtigungen',
        'Others_permissions' => 'Andere Berechtigungen',
        'Attributed_permissions' => 'Erteilte Berechtigungen',
        'Attributed_permissions_association' =>  'Folgende Berechtigungen sind dem User mit der E-Mail-Adresse :email erteilt worden',
        'No_permissions_associated' => 'Für diesen User ist keine Berechtigung definiert',
        'Edit_permissions' => 'Berechtigungen bearbeiten'
    ],
    'guests_import' => [
        'column' => [
            'maximum_reached' => 'Sie können eine Datei mit mehr als :max Spalten nicht importieren. Bitte begrenzen Sie die Anzahl der Spalten vor dem Import.'
        ]
    ],
    'guests_export' => [
        'export_complete_list' => 'Exportieren der Gästeliste',
        'export_companions_list' => 'Exportieren der Begleitliste',
    ],
    'solve_duplicates_conflicts_page' => [
        'Solve_conflict' => 'Konfliktlösung|Konfliktlösung',
        'confirm_modal' => [
            'Please_confirm' => 'Bitte bestätigen Sie den Vorgang',
            'Warning_message' => 'Achtung! Die Bestätigung der Aktion löscht automatisch die anderen an diesem doppelten Konflikt beteiligten Gäste!'
        ],
        'Duplicates_detected_message' => 'Die folgenden doppelten Fälle wurden in Ihrer Gästeliste gefunden. Es wird empfohlen, sie zu behandeln.',
        'Change_at' => 'Modifikation',
        'Choose' => 'Wählen Sie',
        'Compare_in_details' => 'Im Detail vergleichen',
        'Conflict' => 'Konflikt'
    ],
    'companies_page' => [
        'confirm_lock' => 'Das Unternehmen :name wird gesperrt, bestätigen Sie das?',
        'confirm_unlock' => 'Die Firma :name wird freigeschaltet, bestätigen Sie das?'
    ],
    'not_yet_created_cat' => 'Du hast noch keine Kategorie in deiner Liste!',
    'not_yet_created_info_cat' => 'Sie können eigene benutzerdefinierte Kategorien erstellen, um Ihre Events zu filtern und leichter zu finden.',
    'not_yet_created_event' => 'Du hast noch kein Event in deiner Liste!',
    'not_yet_created_info_event' => 'Erstelle dein erstes Event auf der Plattform, damit es in dieser Liste erscheint.',
    'config_updated' => 'Aktualisierte Konfiguration',
    'config_smtp_validation_error' => 'Ungültige SMTP-Konfiguration',
    'config_smtp_validation_same_as_default' => 'Die Adresse :email ist das Standardkonto für den Versand von E-Mails. Es ist nicht erforderlich, den Hostnamen, den Port, den Benutzernamen und das Passwort zu definieren. Diese Felder müssen leer bleiben. Wenn Sie Ihr Passwort ändern, ist Ihre Konfiguration nicht mehr gültig.',
    'confirm_session_checkin' => 'Bestätigen Sie den Eintrag in die Sitzung.',
    'menu' => [
        'Checkin' => 'Einchecken',
    ],
    'minisite' => [
        'languages' => 'Sprache',
    ],
    'maintenance' => [
        'title' => 'Wartung von Eventwise',
        'message' => 'Aufgrund von Aktualisierungen ist diese Website vorübergehend nicht zugänglich.'
    ]
];
