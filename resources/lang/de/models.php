<?php

return [
    'company' => [
        'company' => 'Unternehmen | Unternehmen',
        'create_new' => 'Erstelle ein neues Geschäft',
        'configuration' => 'Konfiguration',
        'configuration_smtp' => 'SMTP-Konfiguration',
    ],
    'user' => [
        'user' => 'user | benutzer',
        'create_new' => 'Erstelle einen neuen Benutzer',
    ],
    'companion' => [
        'edit' => 'Ändern einer Begleitperson',
        'ticketFileName' => 'begleitkarte-:n',
    ],
    'guest' => [
        'guest' => 'eingeladen Gäste ',
        'guest_comp' => 'Gast und Begleitperson|Gäste und Begleitpersonen',
        'companion' => 'Begleiter|Begleiter',
        'create_new' => 'Erstelle einen neuen Gast',
        'delete_all' => 'Alles löschen',
        'upload' => 'Importiere eine XLS-Datei',
        'upload_change' => 'Wählen Sie eine andere Datei',
        'import' => 'Gäste importieren',
        'download' => 'In XLS-Format exportieren',
        'anonymise' => 'anonymisieren',
        'SeeList' => 'Siehe Gästeliste',
        'Edit' => 'Gast bearbeiten',
        'checkin' => [
            'CheckinDone' => 'Checkin erfolgreich abgeschlossen!',
            'InvitValid' => 'Einladung gültig',
            'InvitInvalidUnknownGuest' => 'Diese Einladung ist ungültig. Es wurden keine passenden Benutzer gefunden. ',
            'WarningNotSubscribed' => 'Der Benutzer hat seine Teilnahme nicht bestätigt.',
            'AllreadyDone' => 'Das Einchecken der vom Gast reservierten Plätze ist bereits erfolgt, Sie können noch weitere Plätze registrieren.',
            'WarningNotInvited' => 'Warnung, dass der Benutzer zu dieser Veranstaltung nicht eingeladen wurde',
            'NotRegisterdToSession' => 'Der Gast ist nicht für die Sitzung angemeldet.',
            'TicketAllreadyScanned' => 'Check-in unmöglich, mit diesem Ticket ist an diesem Eingang kein Platz mehr frei oder es wurde bereits gescannt.'
        ],
        'ticketFileName' => 'eintrittskarte',
    ],
    'admin' => [
        'admin' => 'admin | Administratoren',
        'create_new' => 'Neuen Administrator erstellen',
    ],
    'email' => [
        'email' => 'E-Mail|E-Mails',
        'invitEmail' => 'Einladung | Einladungen',
        'confirmEmail' => 'Bestätigung der Registrierung | Bestätigung der Registrierung',
        'confirmEmailCompanions' => 'Bestätigung der Registrierung (Begleiter)|Bestätigung der Registrierung (Begleiter)',
        'otherEmail' => 'Andere | Andere',
        'noEMails' => 'Keine E-Mail zur Anzeige',
        'systemEmail' => 'System-E-Mail|System-E-Mails',
        'fullEvent' => 'Vollständiger Event (Event komplett)'
    ],
    'category' => [
        'category' => 'Kategorie | Kategorien',
        'Categories' => 'Kategorien',
        'create_new' => 'Eine neue Kategorie erstellen',
        'category_name' => 'Name',
        'category_description' => 'Beschreibung',
        'category_edit_following' => 'Kategorie bearbeiten',
    ],
    'event' => [
        'event' => 'Event | Events',
        'dashboard' => 'Dashboard | Dashboards',
        'dates' => 'Daten',
        'create_new' => 'Neues Ereignis erstellen',
        'event_edit' => 'Ereignis bearbeiten',
        'event_edit_following' => 'Ereignis bearbeiten',
        'event_name' => 'Name',
        'event_dates' => 'Termine des Events',
        'event_category' => 'Kategorie | Kategorien',
        'event_type' => 'Typ',
        'event_places' => 'Anzahl Plätze',
        'event_start_date' => 'Startdatum',
        'event_end_date' => 'Enddatum',
        'event_description' => 'Beschreibung',
        'event_address' => 'Ort',
        'event_from' => 'von',
        'event_to' => 'bei',
        'allow_subscription_update' => 'den Gästen die Möglichkeit geben, ihre Registrierung zu ändern',
        'subscription_update_not_allowed' => 'Eine Änderung Ihrer Anmeldung ist nicht möglich, bitte wenden Sie sich bei Bedarf an den Veranstalter.',
        'organisator_firstname' => 'Vorname des Veranstalters',
        'organisator_lastname' => 'Name des Veranstalters',
        'organisator_email' => 'E-Mail des Veranstalters',
        'advanced_minisite_mode' => 'Art der Mini-Site',
        'mini_site_simple_mode' => 'einfacher Modus (nur eine Seite)',
        'mini_site_advanced_mode' => 'Erweiterter Modus (mehrere Seiten)',
        'ctrl_account_password' => 'Kontroller Konto Kennwort',
        'ctrl_account_password_repeat' => 'Kennwort Bestätigung',
        'guests_limit_reached' => 'Die Teilnehmerzahl ist leider schon für diese Veranstaltung erreicht, bitte kontaktieren Sie direkt den Veranstalter der Veranstaltung, um sich anzumelden',
        'guest_registration_updated' => 'Ihre Teilnahme wurde aktualisiert',
        'guest_registration_saved' => 'Vielen Dank, Ihre Teilnahme wurde registriert. Sie erhalten eine Bestätigungs-E-Mail.',
        'guest_registration_created' => 'Vielen Dank, Ihre Teilnahme wurde registriert. Sie erhalten eine Bestätigungs-E-Mail.',
        'guest_registration_updated_partial' => 'Eine Teilnahme ist bereits mit dieser E-Mail registriert, bitte besuchen Sie diese Seite über Ihren persönlichen Zugangslink oder kontaktieren Sie den Veranstalter Ihrer Veranstaltung.',
        'guest_registration_allready_done' => 'Ihre Teilnahme wurde bereits registriert, um sie zu ändern, verwenden Sie bitte Ihren persönlichen Zugangslink.',
        'public_minisite' => 'Öffentliche Veranstaltung',
        'companions_limit' => 'Max Begleiter pro Gast',
        'companion_field' => 'Schreiben Sie 0, um die Unterstützung von Geleitern für dieses Ereignis zu deaktivieren',
        'advanced_subscription_form_mode' => 'Anmeldeformular Modus',
        'form_advanced_subscription_form_mode' => 'Erweiterter Modus (mehrsprachige und bedingte Fragen)',
        'form_simple_subscription_form_mode' => 'Einfacher Modus (einfaches Formular)',
        'nominative_companions' => 'Unterstützung für registrierte Begleiter aktivieren',
        'visible_captcha' => 'Recaptcha',
        'statistic' => [
            'statistic' => 'Statistiken | Statistiken',
            'statistics_guests' => 'Gaststatistiken',
            'statistic_full' => 'Statistik des Event (immer) | Statistiken der Events (immer)',
            'events_past_count' => 'Anzahl vergangener Events (immer)',
            'events_actual_count' => 'Anzahl der laufenden Events (immer)',
            'events_futur_count' => 'Anzahl der zukünftigen Events (immer)',
            'events_count' => 'Anzahl der Events (immer)',
            'download' => 'Exportieren in XLS',
            'guests_invited_count' => 'Anzahl der gesendeten Einladungen',
            'guests_subscribed_count' => 'Anzahl der Registrierungen',
            'guests_checkin_count' => 'Anzahl der Check-in',
            'guests_count' => 'Anzahl der Gäste',
            'guests_declined_count' => 'Anzahl der abgelehnten Gäste',
        ],
    ],
    'subscribeForm' => [
        'subscribeForm' => 'Kleine Website | Kleine Websites',
        'subscribeForm_advanced' => 'Anmeldeformular',
        'badLink' => 'Es scheint, dass dieser Link nicht gültig ist.',
        'contactOrg' => 'Bitte kontaktieren Sie Ihren Veranstalter für einen neuen Link.',
    ],
    'surveyForm' => [
        'surveyForm' => 'Umfrageformular | Umfrageformulare',
        'survey' => 'Umfrage | Umfragen',
        'result' => 'Umfrageergebnis | Umfrageergebnisse',
        'edit' => 'die Umfrage bearbeiten',
    ],
    'minisite' => [
        'minisite_infos' => 'Auskünfte',
        'access' => 'Zugriffsart',
        'slug' => 'Zugriffs-URL',
        'slug_validation_rules' => 'Nur Kleinbuchstaben, Zahlen und Bindestriche (-).',
        'minisite_title' => 'Minisite-Titel',
        'advanced_minisite' => 'erweiterte Mini-Site',
        'manage_minisite' => 'Mini-Site-Management',
        'minisite_address' => 'Minisite-Adresse',
        'minisite_last_modif' => 'letzte Änderung',
        'main_properties' => 'Haupteigenschaften',
        'title' => 'Titel',
        'link' => 'Minisite-Link',
        'site_title_font_color' => 'Schriftfarbe des Seitentitels',
        'site_background_color' => 'Seitenhintergrundfarbe',
        'site_background_image' => 'Seitenhintergrundbild',
        'menu_properties' => 'Menü-Eigenschaften',
        'menu_font_size' => 'Menü-Schriftgröße',
        'menu_font' => 'Menüschrift',
        'menu_font_color' => 'Menü-Schriftfarbe',
        'menu_background_color' => 'Menü-Hintergrundfarbe',
        'menu_hover_properties' => 'Menü Hover-Eigenschaften',
        'menu_hover_font_color' => 'Menüpunkt Hover Schriftfarbe',
        'menu_hover_bg_color' => 'Menüpunkt Hover-Hintergrundfarbe',
        'menu_active_properties' => 'Menüpunkt Aktive Eigenschaften',
        'menu_active_font_color' => 'Menüpunkt aktive Schriftfarbe',
        'menu_active_bg_color' => 'Menüpunkt aktive Hintergrundfarbe',
        'content_properties' => 'Inhaltseigenschaften',
        'content_bg_color' => 'Inhaltshintergrundfarbe',
        'bottom_image' => 'Bild unten',
        'top_image' => 'Bild oben auf der Seite',
        'title_font_size' => 'Schriftgröße des Titels',
        'title_font' => 'Titelschrift',
        'title_alignment' => 'Titelausrichtung',
        'title_menu_spacing' => 'space between title and menu',
        'property_top_image' => 'Abstand zwischen Titel und Menü',
        'property_bottom_image' => 'Bildeigenschaften',
        'top_image_alignment' => 'Ausrichtung des oberen Bildes',
        'bottom_image_alignment' => 'Ausrichtung des unteren Bildes',
        'bottom_image_size' => 'untere Bildgröße',
        'top_image_size' => 'obere Bildgröße',
        'title_properties' => 'Titel-Eigenschaften',
        'size_percentage' => 'In % der Originalgröße des Bildes',
    ],
    'pages' => [
        'page_number' => 'Seitenzahl',
        'under_list' => 'Die folgende Liste enthält die Seiten Ihrer Mini-Site.',
        'site_pages' => 'Webseiten-Seiten',
        'name' => 'Bezeichnung',
        'page_url' => 'Adressenseite',
        'content_type' => 'Inhaltstyp',
        'content' => 'Inhalt',
        'create_new' => 'Eine Seite zur Minisite hinzufügen',
        'edit_lang' => 'Seite wechseln, Sprache: :lang',
        'question_transfer_content_lang' => 'Der Inhalt dieser Seite existiert in einer anderen Sprache, möchten Sie den Inhalt wiederherstellen?',
        'transfer_content_lang' => 'Inhalt wiederherstellen :lang',
    ],
    'permissions' => [
        'edit_permissions' => 'Berechtigungen bearbeiten',
        'descriptions' => [
            'survey-update' => 'Änderung der Umfrage',
            'users-view' => 'Ermöglicht die Anzeige der Benutzerlistenseite eines Unternehmens.',
            'users-create' => 'Ermöglicht die Erstellung neuer Benutzer zur Verwaltung des Unternehmens.',
            'users-update' => 'Ermöglicht die Änderung bestehender Benutzer',
            'users-delete' => 'Ermöglicht das Löschen bestehender Benutzer',
            'configurations-update' => 'Ermöglicht die Änderung der E-Mail-Konfiguration (SMTP) für das Unternehmen.',
            'events-overview' => 'Ermöglicht die Anzeige der Übersichtsseite für jede Veranstaltung.',
            'events-view' => 'Ermöglicht es Ihnen, die vollständige Liste der Elemente anzuzeigen.',
            'events-create' => 'Ermöglicht die Erstellung neuer Ereignisse',
            'events-update' => 'Ermöglicht die Änderung von Ereignissen',
            'events-delete' => 'Ermöglicht das Löschen von Ereignissen',
            'statistics-view' => 'Ermöglicht die Anzeige des Statistikblocks über die Übersicht der Ereignisse sowie der Umfrageergebnisse.',
            'guests-view' => 'Ermöglicht es Ihnen, die Gästeliste in Veranstaltungen anzuzeigen.',
            'guests-update' => 'Ermöglicht es Ihnen, Gäste in Veranstaltungen zu bearbeiten.',
            'guests-delete' => 'Ermöglicht es Ihnen, Gäste in Veranstaltungen zu löschen.',
            'emails-view' => 'Ermöglicht die Anzeige der Liste der E-Mails in Ereignissen.',
            'emails-create' => 'Ermöglicht die Erstellung neuer E-Mails in Ereignissen.',
            'emails-update' => 'Ermöglicht die Änderung von E-Mails in Ereignissen.',
            'emails-delete' => 'Ermöglicht das Löschen von E-Mails in Ereignissen.',
            'minisite-view' => 'Ermöglicht die Anzeige der Übersichtsseite der Miniseiten von Veranstaltungen.',
            'minisite-create' => 'Ermöglicht die Erstellung der Minisite für Veranstaltungen.',
            'minisite-update' => 'Ermöglicht die Änderung der Miniseite und der Seiten für Veranstaltungen sowie des Anmeldeformulars.',
            'sessions-view' => 'Ermöglicht es Ihnen, die Sitzungen einer Veranstaltung anzuzeigen.',
            'sessions-create' => 'Ermöglicht es Ihnen, eine Sitzung für eine Veranstaltung zu erstellen.',
            'sessions-update' => 'Ermöglicht es Ihnen, eine Sitzung zu aktualisieren.',
            'sessions-delete' => 'Ermöglicht es Ihnen, eine Sitzung zu löschen.'
        ],
        'users-view' => 'Benutzerliste anzeigen',
        'users-create' => 'Neue Benutzer anlegen',
        'users-update' => 'Bestehende Benutzer aktualisieren',
        'users-delete' => 'Bestehende Benutzer löschen',
        'configurations-update' => 'SMTP-Konfiguration aktualisieren',
        'events-overview' => 'Ereignisübersicht',
        'events-view' => 'Events anzeigen',
        'events-create' => 'Veranstaltung erstellen',
        'events-update' => 'Ereignis aktualisieren',
        'events-delete' => 'Ereignis löschen',
        'statistics-view' => 'Statistik anzeigen',
        'guests-view' => 'E-Mail-Liste anzeigen',
        'guests-create' => 'Neue E-Mails erstellen',
        'guests-update' => 'Bestehende E-Mails aktualisieren',
        'guests-delete' => 'Bestehende E-Mails löschen',
        'emails-view' => 'E-Mail-Liste anzeigen',
        'emails-create' => 'Neue E-Mails erstellen',
        'emails-update' => 'Bestehende E-Mails aktualisieren',
        'emails-delete' => 'Bestehende E-Mails löschen',
        'minisite-view' => 'Minisite-Übersicht anzeigen',
        'minisite-create' => 'Minisite erstellen',
        'minisite-update' => 'Minisite aktualisieren und/oder Formular abonnieren',
        'survey-update' => 'Umfrageformular aktualisieren',
        'sessions-view' => 'Sitzungen anzeigen',
        'sessions-create' => 'Erstellen von Sitzungen',
        'sessions-update' => 'Aktualisieren von Sitzungen',
        'sessions-delete' => 'Sitzungen löschen'
    ],
    'session' => [
        'name' => 'Name',
        'places' => 'Anzahl der Plätze',
        'create_new' => 'Erstellen einer neuen Sitzung',
        'session' => 'Sitzung|Sitzungen',
        'no_item' => 'Keine Sitzung',
        'guest_session_registration_allready_set' => 'Der Gast ist bereits in der Sitzung registriert.',
        'guest_no_more_place_in_sessions' => 'Es ist nicht genügend Platz für die Sitzungen vorhanden: :sessions',
    ],
    'sessions' => [
        'create_new' => 'Hinzufügen einer Sitzung',
    ],
    'session_groups' => [
        'session_group' => 'Sessions Kategorien',
        'name' => 'Name',
        'create_new' => 'Eine neue Sessions Kategorie hinzufügen',
        'edit' => 'Modifier une catégorie de session',
        'mandatory_category' => 'Obligatorische Kategorie',
        'max_categories_allowed' => 'Maximale Kategorien-Abonnements',
        'mandatory_category_setup' => 'Kategorie-Einrichtung',
        'mandatory_icon' => 'O'
    ],
    'ticket' => [
      'please_present' => 'Bitte geben Sie diesen Code am Eingang der Veranstaltung an.',
      'ticket' => 'Eintrittskarte',
      'ticket_to' => 'Eintrittskarte für ',
      'ticket_preview' => 'Vorschau der Eintrittskarte',
      'image_1' => 'Bild 1 (oben links)',
      'image_2' => 'Bild 2 (Boden)',
      'text' => 'Eintrittskarte Text',
      'empty_field' => 'Wenn Sie dieses Feld leer lassen, verwendet das Textticket automatisch die Veranstaltungsbeschreibung.',
      'update' => 'Update Eintrittskarte',
      'no_sup_info_field' => 'Kein Firmenname',
      'ticket_type' => 'Tickettyp',
      'ticket_type_render' => 'Simpel',
      'ticket_type_four_quarters' => 'Ausweis',
      'ticket_badge_title' => 'Persönliche Zugangskarte',
      'ticket_badge_print' => 'Dieses Blatt drucken',
      'ticket_badge_fold' => 'Falten Sie dieses Blatt zu 4, um ein Abzeichen zu erstellen.',
      'ticket_badge_pocket' => 'Stecken Sie den Badge in die dafür vorgesehene Tasche an der Rezeption.',
      'ticket_badge_fold_here' => 'Hier zangen'
    ],
];
