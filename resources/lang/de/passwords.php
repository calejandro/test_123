<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens sechs Zeichen lang und identisch sein.',
    'reset'    => 'Ihr Passwort wurde zurückgesetzt!',
    'sent'     => 'Wir haben Ihnen den Link zum Zurücksetzen des Passworts per E-Mail geschickt!',
    'token'    => "Dieses Passwort Reset Token ist nicht gültig.",
    'user'     => "Es wurden keine Benutzer mit dieser E-Mail-Adresse gefunden.",

];
