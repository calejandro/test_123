<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Diese Bezeichner stimmen nicht mit unseren Datensätzen überein',
    'throttle' => 'Verbindungsversuche zu viele. Bitte versuchen Sie es noch einmal in: Sekunden Sekunden ',


];
