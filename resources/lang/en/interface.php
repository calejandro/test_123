<?php

return [
    'attributes' => [
        'valid_email' => [
            'valid_email' => 'Validity of the email',
            'valid_email_' => 'Unaudited',
            'valid_email_0' => 'Invalid',
            'valid_email_1' => 'Valid',
            'valid_email_2' => 'Unverifiable',
            'valid_email_3' => 'To check',
        ],
    ],
    'to_define' => 'to define',
    'public' => 'public',
    'private' => 'private',
    'confirm_checkin' => 'Confirm the check-in',
    'save' => 'save',
    'save_template' => 'save as new template',
    'update_existing_template' => 'update an existing template',
    'load_template' => 'load template',
    'delete_template' => 'delete template',
    'edit_template' => 'modifier un template',
    'templates' => 'templates',
    'cancel' => 'cancel',
    'send' => 'send',
    'add' => 'add',
    'edit' => 'edit',
    'edit_properties' => 'edit properties',
    'last_modification' => 'last modification',
    'show' => 'show',
    'open' => 'open',
    'search' => 'search',
    'delete' => 'delete',
    'back' => 'back',
    'update' => 'update',
    'create' => 'create',
    'event' => 'event',
    'configuration' => 'configuration',
    'confirm' => 'confirm',
    'confirm_delete' => 'Please confirm the supression',
    'confirm_delete_object' => 'Please confirm the suppression of ":name" ',
    'logout' => 'Log out',
    'login' => 'Log-in',
    'lock' => 'Lock',
    'unlock' => 'Unlock',
    'yes' => 'yes',
    'no' => 'no',
    'limit' => 'limit',
    'duplicate' => 'Duplicate',
    'filter' => 'Filter',
    'current_future' => 'Current / Future',
    'finished' => 'finished',
    'warn_admin_company_on' => 'You work on the account of: ',
    'quit' => 'quit',
    'apply' => 'apply',
    'valid_import' => 'confirm the importation',
    'import_nominative_companions_warning' => 'Warning: you will no longer be able to change the support setting for name tags once your guests have been imported.',
    'controller_account' => 'controller account',
    'choose_template' => 'Choose a template',
    'anonymise_modal' => [
        'title' => 'Confirm the anonymisation of the guests list',
        'headline' => 'You are about to anonymise the guest list of the event',
        'warning' => 'Warning ! This action is irreversible and will make useless the guests list. The emails could not be send anymore to theses users because the adresses will be changed randomly.',
        'info' => 'The information and statistics regarding the event will remain usable.',
    ],
    'template' => [
        'headline' => 'Please select the template you want to update.',
        'info' => 'Advise: Do not forget to save your last changes !',
        'update' => 'Update',
        'cancel' => 'Cancel',
    ],
    'login_page' => [
        'remember_me' => 'Remember me',
        'forgot_your_password' => 'Forgot your password ?',
        'uncompatible_browser_text' => 'Be careful your browser ({browser}) is not fully compatible with Eventwise, some features may not work as well as they should.',
        'uncompatible_browser_min_version_compatible' =>  'Please update it to version {minVersion} or higher.',
    ],
    'emails_page' => [
        'add_invit' => 'add an invitation',
        'add_other_email' => 'add an other email',
        'sent' => 'sent'
    ],
    'guests_page' => [
        'warning_invalid_emails_title' => 'Invalid email addresses',
        'warning_invalid_emails' => 'This event contains guests with invalid email addresses. Please check the guest list.',
        'warning_duplicates_emails_title' => 'Duplicates detected',
        'warning_duplicates_emails' => 'The guest list for this event seems to contain duplicates (email address). Please process them to make this message disappear.',
        'fix_duplicates' => 'Process duplicates',
        'warning_validation_failed' => '<strong>Error when validating emails. </strong> Emails have not been validated, please restart the validation process.',
        'fix_validation' => 'Restart validation'
    ],
    'messages' => [
        'job_already_running' => [
            'headline' => 'A sending job is already running for this email.',
            'subline' => 'The send functionality of this email will be reactivated when the current job will be finished.',
        ],
        'verif_job_already_running' => [
            'headline' => 'An address verification job is in progress for this event',
            'subline' => 'The mail sending function will be reactivated once the verification is complete',
        ],
        'no_minisite_created' => [
            'headline' => 'Minisite is not created yet',
            'subline' => 'This event is in advanced minisite mode but it is not yet created.',
            'action' => 'Create minisite',
        ],
    ],
    'events_form' => [
        'general_informations' => 'General informations',
        'coords_organisator' => 'Organizer contact information',
        'coords_organisator_infos' => 'Notification mails will be sent to the email address specified below. These fields are not required. If no host email address is specified, notifications will automatically be sent to the company related address.',
        'mini_site_title' => 'Mini-site',
        'mini_site_infos' => 'If you change the type of mini-site, don\'t forget to update the links to it in your emails! However, all forms and pages previously created will remain accessible.',
        'event_languages_title' => 'Event languages',
        'event_languages_infos' => 'Choose in which languages the content of this event will be available.',
        'event_languages_warning_title_change' => 'When changing the available languages, don\'t forget to add the minisite title in the relevant language (minisite property page) and add translations for your existing pages!',
        'event_available_languages' => 'Available languages',
        'ctrl_account_title' => 'Controller',
        'public_minisite_infos' => 'If public, access to the site and registration will be open',
        'advanced_forms_mode' => 'Subscription forms',
        'advanced_forms_infos' => 'The advanced mode allows you to create the registration form in several languages and to integrate conditional questions into it.',
        'places_and_companions' => 'Places and companions',
        'places_and_companions_infos' => 'Manage the total number of seats for your event as well as the number of accompanying persons per guest. You can also choose to enable support for registered companions.',
        'nominative_companions_infos' => 'Guests will have to fill in the accompanying people\'s informations when registering.',
        'nominative_companions_warning' => 'This setting can only be changed if the guest list is empty.',
        'default_controler_will_be_created' => 'A default controller will be created with the event, you will be able to modify it in the event settings',
        'visible_captcha_infos' => 'If checked, the Google recaptcha will be used in your subscribe form',
    ],
    'minisites_create_page' => [
        'create_title' => 'Create the minisite',
        'success_alert' => 'Page successfully created.',
    ],
    'minisites_update_page' => [
        'update_title' => 'Update the minisite',
        'maximum_size' => 'Max :size Mb',
        'success_alert' => 'Page successfully edited.',
    ],
    'minisites_page' => [
        'No_minisite_create_one_message' => 'No minisite exists, please create one.',
        'No_minisite_page_create_one_message' => 'No page exists, please create one.',
        'create_minisite' => 'create a minisite',
        'no_language' => 'no language',
        'no_configurated_language' => 'no language configured',
        'browser_not_compatible' => 'Please use Chrome, Firefox, Safari or Edge to display the content properly. The Internet browser you are using right now is no longer supported. Thank you.',
    ],
    'checkin_page' => [
        'Subscribed_sessions' => 'Sessions',
        'Reserved_place' => 'Number of tickets',
        'Checkin_place' => 'Number of scanned tickets',
        'Main_entry' => 'Main entrance',
        'Show_guest_list' => 'View the guest list',
    ],
    'decline_page' => [
        'Declined_message' => 'We correctly recorded your non-participation to the event: :event',
    ],
    'ticket_page' => [
        'Download_demo' => 'Download preview ticket'
    ],
    'permissions_page' => [
        'Edit_permissions_for' => 'Change permissions for :email',
        'Edit_permissions_explain' => 'The editing of permissions will allow this user to access or not some features of the Eventwise platform. Access to the pages will be restricted accordingly and some elements of the interface (buttons, links, etc.) will be hidden.',
        'Users_permissions' => 'User permissions',
        'Events_permissions' => 'Permissions on events',
        'Guests_permissions' => 'Permissions on guests',
        'Emails_permissions' => 'Permissions on emails',
        'Minisites_permissions' => 'Permissions on mini-sites',
        'Sessions_permissions' => 'Session permissions',
        'Others_permissions' => 'Other permissions',
        'Attributed_permissions' => 'Permissions assigned',
        'Attributed_permissions_association' =>  'The following permissions are associated with the user with email address :email',
        'No_permissions_associated' => 'No permissions set for this user!',
        'Edit_permissions' => 'Edit permissions'
    ],
    'guests_import' => [
        'column' => [
            'maximum_reached' => 'You cannot import a file with more than :max columns. Please limit the number of columns before import.'
        ]
    ],
    'guests_export' => [
        'export_complete_list' => 'Export guest list',
        'export_companions_list' => 'Export companions list',
    ],
    'solve_duplicates_conflicts_page' => [
        'Solve_conflict' => 'Conflict resolution|Conflict resolution',
        'confirm_modal' => [
            'Please_confirm' => 'Please confirm the operation',
            'Warning_message' => 'Attention! Confirming the action will automatically delete the other guests involved in this duplicate conflict!'
        ],
        'Duplicates_detected_message' => 'The following duplicate cases have been detected in your guest list. It is recommended to treat them.',
        'Change_at' => 'Updated at',
        'Choose' => 'Retain',
        'Compare_in_details' => 'Compare in detail',
        'Conflict' => 'Conflict'
    ],
    'companies_page' => [
        'confirm_lock' => 'The company :name will be locked, do you confirm?',
        'confirm_unlock' => 'The company :name will be unlocked, do you confirm?'
    ],
    'not_yet_created_cat' => 'You have no category in your list yet !',
    'not_yet_created_info_cat' => 'You can create your own personnalised categories to filtrer your events and find them easily.',
    'not_yet_created_event' => 'You have no events in your list yet !',
    'not_yet_created_info_event' => 'Create your first event on the platform to see it appear in this list.',
    'config_updated' => 'Configuration updated',
    'config_smtp_validation_error' => 'Invalid SMTP configuration',
    'config_smtp_validation_same_as_default' => 'The address :email is the default account for sending emails. No need to define the host name, port, user name and password. You must leave these fields blank. If you change your password, your configuration will no longer be valid.',
    'confirm_session_checkin' => 'Confirm the entry to the session',
    'menu' => [
        'Checkin' => 'Checkin',
    ],
    'minisite' => [
        'languages' => 'Language',
    ],
    'maintenance' => [
        'title' => 'Eventwise is under maintenance',
        'message' => 'Due to updates, this site is temporarily inaccessible.'
    ]
];
