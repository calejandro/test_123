#!/bin/sh
ls -la
id -u

su $USER -s /bin/sh -c "composer install"
php -v
# composer install
# composer clearcache
php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan clear-compiled
php artisan key:generate
php artisan migrate:refresh --env=testing
php artisan db:seed --env=testing

# Serve for browser tests
php artisan serve --env=testing --quiet --host=0.0.0.0 --port=8000 &

# Compile front-end
# apk add nodejs=8.14.0-r0
# npm config set registry http://registry.npmjs.org/
# npm set strict-ssl false = false
# npm config set user 0
# npm config set unsafe-perm true

# mkdir node_modules
# chmod 777 -R node_modules
# npm install --no-bin-links
# npm install cross-env@5.0.1
# npm rebuild node-sass --no-bin-links
# npm run prod

# PHP Unit & HTTP
./vendor/bin/phpunit

# PHP Laravel Dusk
php artisan dusk --env=testing --debug