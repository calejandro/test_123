.PHONY: build

include .env

infomaniak-build:
	$(info ###############  PLEASE CHECK THAT CONFIG  ################)
	$(info The actual QUEUE_DRIVER is ${QUEUE_DRIVER})
	$(info The actual APP_URL is: ${APP_URL})
	$(info The actual APP_HTTPS is: ${APP_HTTPS})
	$(info Please ensure the configuration in server.js is right)
	$(info ###########################################################)
	php -d allow_url_fopen=on ../../composer.phar install --optimize-autoloader
	php artisan migrate
	php artisan config:cache
	/opt/php7.1/bin/php artisan queue:restart
	bash -l -c 'nvm use 8.12.0 && npm run prod'
