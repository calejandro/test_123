import json
import csv

class CsvToJson:
    """ Convert i18n datas from CSV file (flat format) to JSON format readable by the front-end """

    def __init__(self, datas):
        self.datas = datas
        self.languages = []
        self.languagesJsons = {} # {fr: {}, en: {}, de: {}}

        self._buildTranslations(self.datas)
       
    def _buildTranslations(self, datas):
        self.languages = []
        self.languagesJsons = {}

        for index, row in enumerate(datas, start=0):
            if index == 0:
                self.languages = row[1:]
                for l in self.languages:
                    self.languagesJsons[l] = {}
            else:
                key = row.pop(0)

                for lang in self.languages:
                    value = row.pop(0)
                    self._addLangTranslation(lang, key, value)

    def _addLangTranslation(self, lang, key, value):
        keyPath = key.split('.')
        self.languagesJsons[lang] = self._addKeyAndValueRecusive(self.languagesJsons[lang], keyPath, value)
        
    def _addKeyAndValueRecusive(self, translations, keyPath, value):
        key = keyPath.pop(0)

        if (len(keyPath) == 0):
            translations[key] = value
            return translations
        else:
            if key not in translations:
                translations[key] = {}
            translations[key] = self._addKeyAndValueRecusive(translations[key], keyPath, value)
            return translations

    def export(self, lang):
        return json.dumps(self.languagesJsons[lang], sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)

if __name__ == "__main__":

    with open('./front_translations.csv', mode='r', encoding="utf-8") as i18nFile:
        datas = csv.reader(i18nFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        converter = CsvToJson(datas)
    
    for lang in converter.languages:
        with open(lang + '.json', mode='w', newline='', encoding="utf-8") as exportFile:
            exportFile.write(converter.export(lang))
        
    
  
