import demjson
import csv

def flattenJson(json, level=''):

    flatJson = {}
    for (k, v) in json.items():

        lvlKey = k if level == '' else level + '.' + k

        if isinstance(v, dict):
            flatNested = flattenJson(v, lvlKey)
            flatJson = {**flatJson, **flatNested}
        else:
            flatJson[lvlKey] = v
    
    return flatJson

if __name__ == "__main__":

    flatSrcJson = {}
    flatEnJson = {}
    flatDeJson = {}

    with open('../../resources/assets/js/tools/lang/fr.js', 'r') as frJsonFile:
        rawDatas = frJsonFile.read()
        srcJson = demjson.decode(rawDatas.replace("export const french = {", "{" ,1))
        flatSrcJson = flattenJson(srcJson)
    
    with open('../../resources/assets/js/tools/lang/en.js', 'r') as enJsonFile:
        rawDatas = enJsonFile.read()
        srcJson = demjson.decode(rawDatas.replace("export const english = {", "{" ,1))
        flatEnJson = flattenJson(srcJson)
    
    with open('../../resources/assets/js/tools/lang/de.js', 'r') as deJsonFile:
        rawDatas = deJsonFile.read()
        srcJson = demjson.decode(rawDatas.replace("export const deutsch = {", "{" ,1))
        flatDeJson = flattenJson(srcJson)

    with open('export.csv', mode='w', newline='') as exportCsv:
        writer = csv.writer(exportCsv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        writer.writerow(['key', 'fr', 'en', 'de'])

        for (k, v) in flatSrcJson.items():
            de = ''
            if k in flatDeJson:
                de = flatDeJson[k]
            en = ''
            if k in flatEnJson:
                en = flatEnJson[k]

            writer.writerow([k, v, de, en])
