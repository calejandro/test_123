# Changelog Generator

Generate the changelog for a specific version.

All issues must have the same label.

## How to use

move `forge.exemple.cnf` to `forge.cnf` and edit infos in it.

```
python generate-changelog.py >> template.html
```

Your HTML is now in `template.html` !