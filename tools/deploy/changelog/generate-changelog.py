import os
import requests
from jinja2 import Template

import configparser

config = configparser.ConfigParser()
config.read("./forge.cnf")

config = dict(
    GITLAB_API_URL = config.get('forge', 'url'),
    PRIVATE_TOKEN = config.get('forge', 'private_token'),
    PROJECT_ID = config.get('forge', 'project_id'),
    LABEL = config.get('forge', 'label')
)

r = requests.get(config['GITLAB_API_URL'] + 'projects/' + config['PROJECT_ID'] + '/issues?labels=' + config['LABEL'] + '&private_token=' + config['PRIVATE_TOKEN'])

template = Template("""\
<ul>
    {% for issue in issues %}
        {% if not issue.confidential %}
            <li><a href="{{issue.web_url}}">#{{issue.iid}} {{issue.title}}</a></li>
        {% endif %}
    {%- endfor -%}
    
</ul>
""")

if __name__ == "__main__":
    print(template.render(issues=r.json()))