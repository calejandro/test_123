import csv
from faker import Faker

fake = Faker()

with open('./guests_test.csv', 'w') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(['prenom', 'nom', 'tel', 'email', 'addresse'])

    for i in range(1, 2000):
        name = fake.name().split(' ')[0]
        firstname = fake.name().split(' ')[1]
        wr.writerow([firstname, name, fake.phone_number(), fake.company_email(), fake.street_address()])